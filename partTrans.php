<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	die();
}


$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

if(isset($_GET["part"])){
	$_POST["partNum"] = $_GET["part"];
}

//Step one submitted
if(isset($_POST["partNum"]) && isset($_POST["plant"])){
	$partTransResult = $api->getPartTransactionHistory2($_POST["partNum"],$_POST["plant"]);
	if(hasError($partTransResult)){
		$error = true;
		$errorText = "Unable to get part history";
	} else {
		$_SESSION["partTrans"]["part"] = $partTransResult;
		$_SESSION["partTrans"]["partNum"] = $_POST["partNum"];
		$_SESSION["partTrans"]["plant"] = $_POST["plant"];
		$step = 2;
	}
}

$plantsResult = $api->getAllPlants();
?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Part Transaction History</b>
					</div>
					<div class="card-body">
						<center>
						<form action="partTrans.php" method="POST">
							<br>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="Part Number" name="partNum"

							<?php 
								if(isset($_GET["part"])){
									echo ' value="'.$_GET["part"].'"';
								}
							?>
							>
						  </div>

						  <select class="form-select" aria-label="Plant" name="plant">
							<?php foreach($plantsResult["result"]->returnObj->PlantList as $plant){ 
								$selected = "";
						  		if($plant->Plant == 200){
						  			$selected = "selected";
						  		}
							?>
							  <option value="<?php echo $plant->Plant; ?>" <?php echo $selected;?> ><?php echo $plant->Name; ?></option>
							 <?php } ?>
							</select>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Find</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
			<!-- Step Two -->
			<?php if($step == 2) { ?>
				<center>

				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">Part Transaction History</h5><br>
						<h5 class="card-title"><?php echo $_SESSION["partTrans"]["partNum"]; ?></h5>
					</div>
				</div>
				<br>

				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($_SESSION["partTrans"]["part"]["result"]->returnObj->PartTranHist as $partTrans) { 
					  		if($partTrans->PackNum == 0){
					  			$partTrans->PackNum = "";
					  		}

					  		if($partTrans->PONum == 0){
					  			$partTrans->PONum = "";
					  		}

					  		if($partTrans->OrderNum == 0){
					  			$partTrans->OrderNum = "";
					  		}

					  		?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col">Plant</th>
						      <th scope="col">Warehouse</th>
						      <th scope="col">BinNum</th>
						      <th scope="col">Qty</th>
						      <th scope="col">Type</th>
						    </tr>
					  		<tr>
					  			<td><?php echo $partTrans->Plant; ?></td>
					  			<td><?php echo $partTrans->WareHouseCode; ?></td>
					  			<td><?php echo $partTrans->BinNum; ?></td>
					  			<td><?php echo $partTrans->TranQty; ?></td>
					  			<td><?php echo $partTrans->TranType; ?></td>
					  		</tr>
					  		<tr>
						      <th scope="col">JobNum</th>
						      <th scope="col">PackNum</th>
						      <th scope="col">PackSlip</th>
						      <th scope="col">PONum</th>
						      <th scope="col">OrderNum</th>
						    </tr>
					  		<tr>
					  			<td><?php echo $partTrans->JobNum; ?></td>
					  			<td><?php echo $partTrans->PackNum; ?></td>
					  			<td><?php echo $partTrans->PackSlip; ?></td>
					  			<td><?php echo $partTrans->PONum; ?></td>
					  			<td><?php echo $partTrans->OrderNum; ?></td>
					  		</tr>
					  		<tr>
					  		  <th scope="col" colspan="2">Date</th>
						      <th scope="col" colspan="2">EntryPerson</th>
						      <th scope="col" colspan="1">Total</th>
						    </tr>
						    <tr>
					  			<td colspan="2"><?php echo explode("T", $partTrans->TranDate)[0]; ?></td>
					  			<td colspan="2"><?php echo $partTrans->EntryPerson; ?></td>
					  			<td colspan="1"><?php echo $partTrans->RunningTotal; ?></td>
					  		</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>
			</center>

			<?php } ?>
				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>