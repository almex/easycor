<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	die();
}


$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function getPartHeaderHTML($part, $subpart = false){
	global $api;
	$html = '';
    
    if($subpart){
    	$html .= '<div class="alert alert-warning" role="alert" style="max-width: 500px;"><div class="card-body">';
    	$html .= '<h5 class="card-title"> Sub-part: '.$part["result"]->returnObj->Part[0]->PartNum.' </h5>';
    	$html .= '<p class="card-text"> '.$part["QtyPer"].' </p>';
    } else {
    	$html .= '<div class="card text-white bg-dark mb-3" style="max-width: 500px;"><div class="card-body">';

    	if(isset($_POST["rev"])){
			$html .= '<h5 class="card-title"> '.$part["result"]->returnObj->Part[0]->PartNum.'<br>
			Rev '.$_POST["rev"].' </h5>';
    	} else {
    		$html .= '<h5 class="card-title"> '.$part["result"]->returnObj->Part[0]->PartNum.' </h5>';
    	}

    	$html .= '<center><br><table><tr>';
    	$html .= '<td style="padding-right:20px;"><a target="_blank" href="partPurHist.php?part='.urlencode($part["result"]->returnObj->Part[0]->PartNum).'"><img src="img/cart.png" style="max-width:50px;max-height:70px;"></a></td>';
    	$html .= '<td style="padding-left:20px;padding-right:20px;"><a target="_blank" href="partImage.php?part='.urlencode($part["result"]->returnObj->Part[0]->PartNum).'"><img src="img/photo.png" style="max-width:50px;max-height:70px;"></a></td>';
    	$html .= '<td style="padding-left:20px;"><a target="_blank" href="partTrans.php?part='.urlencode($part["result"]->returnObj->Part[0]->PartNum).'"><img src="img/trans.png" style="max-width:50px;max-height:70px;"></a></td>';
    	$html .= '<td style="padding-left:20px;"><a target="_blank" href="partWhereUsed.php?part='.urlencode($part["result"]->returnObj->Part[0]->PartNum).'"><img src="img/whereUsed.png" style="max-width:50px;max-height:70px;"></a></td>';
    	$html .= '</tr></table><br></center>';
    }


    if($subpart == false && isset($_SESSION["partFind"]["image"]["result"]->returnObj) && trim($_SESSION["partFind"]["image"]["result"]->returnObj) != ""){
    	$html .= '<br><center><img style="max-width: 95%;" 
							src="data:image/png;base64,'.$_SESSION["partFind"]["image"]["result"]->returnObj.'">
				  </center><br>';
    }


    $html .= '<p class="card-text"> '.$part["result"]->returnObj->Part[0]->PartDescription.' </p>';

    if($subpart == false){
    	$partCostResult = $api->getPartCost($part["result"]->returnObj->Part[0]->PartNum);
    	if($partCostResult["http"] == 200){
    		$partCost = $partCostResult["result"]->returnObj->PartCost[0];
    		if($partCost->FIFOAvgTotalCost > $partCost->LastTotalCost){
    			$html .= '<p class="card-text"><b>Cost:</b>  '.number_format((float)$partCost->FIFOAvgTotalCost, 2, '.', '').' </p>';
    		} else {
    			$html .= '<p class="card-text"><b>Cost:</b>  '.number_format((float)$partCost->LastTotalCost, 2, '.', '').' </p>';
    		}
    	}
 	} 

	if($part["result"]->returnObj->Part[0]->InternalUnitPrice != 0){ 
    	$html .= '<p class="card-text"><b>Internal Price:</b>  '.$part["result"]->returnObj->Part[0]->InternalUnitPrice.' </p>';
 	} 

    if(isset($part["result"]->returnObj->PartCOO) && !empty($part["result"]->returnObj->PartCOO)) { 
    	$html .= '<p class="card-text"><b>Country of Origin:</b>  '.$part["result"]->returnObj->PartCOO[0]->CountryDescription.' </p>';
    } 

  	if(strlen($part["result"]->returnObj->Part[0]->CreatedBy) != 0){ 
    	$html .= '<p class="card-text"><b>Created By:</b>  '.$_SESSION["partFind"]["part"]["result"]->returnObj->Part[0]->CreatedBy.' </p>';
    }

	$html .= '</div></div>';
	return $html;
}

function getPartSuppliersHTML($suppliers){
	global $api;
	$html = '';

	foreach ($suppliers["result"]->returnObj->SupplierPart as $supplier) {
		$vendorResult = $api->getVendorInfo($supplier->VendorNum);
		if(hasError($vendorResult)){
			continue;
		}

		$html .= '<div class="alert alert-info" role="alert" style="max-width: 500px;"><div class="card-body">';
		$html .= '<h5 class="card-title">'.$vendorResult["result"]->returnObj->Vendor[0]->Name.' </h5><br>';

		if(strlen($supplier->VendPartNum) > 0){
			$html .= '<p class="card-text"><b>Vendor Part:</b>  <a target="_blank" href="https://www.google.ca/search?q='.urlencode($supplier->VendPartNum).'">'.$supplier->VendPartNum.'</a> </p>';
		}

		if(strlen($supplier->MfgPartNum) > 0){
			$html .= '<p class="card-text"><b>Mfg Part:</b>  <a target="_blank" href="https://www.google.ca/search?q='.urlencode($supplier->MfgPartNum).'">'.$supplier->MfgPartNum.'</a> </p>';
		}
		$html .= '</div></div>';
	}

	return $html;
}

function getPartRevisionsHTML($part){
	$html = "";
	foreach ($part["result"]->returnObj->PartRev as $partRev) {
		$html .= '
		<div class="alert alert-success" role="alert" style="max-width: 500px;">
			<div class="card-title" style="font-size: 1.4rem;">
				<b>Rev '.$partRev->RevisionNum.'</b>
			</div>
			<p class="card-text">'.$partRev->RevShortDesc.'</p>
			<form action="partFind.php" method="POST">
				<input class="form-control" type="hidden" name="partNum" value="'.$part["result"]->returnObj->Part[0]->PartNum.'">
				<input class="form-control" type="hidden" name="rev" value="'.$partRev->RevisionNum.'">
			  	<button type="submit" class="btn btn-primary"><b>View Sub Parts</b></button>
			</form>
		</div>';
	}
	return $html;
}

function getPartLocationsHTML($part){
	global $api;
	$partOnHandFound = false;
	$html = "";

	//Bad Warehouses
	$ignoreWarehouses = array();
	foreach ($part["result"]->returnObj->PartWhse as $partWhse) {
		if($partWhse->PlantOwner != $partWhse->Plant){
			array_push($ignoreWarehouses, array(
				"plant" => $partWhse->Plant,
				"warehouse" => $partWhse->WarehouseCode
			));
		}
	}

	foreach ($_SESSION["partFind"]["plants"] as $key => $val) {
		$partOnHandResult = $api->getPartOnHand($part["result"]->returnObj->Part[0]->PartNum,$key);
		if(hasError($partOnHandResult)){
			continue;
		}

		foreach ($partOnHandResult["result"]->returnObj->PartOnHandBin as $onHand) {

			//Ignore bad warehouses
			$skipThisBin = false;
			foreach ($ignoreWarehouses as $ignoreWarehouse) {
				if($ignoreWarehouse["plant"] == $onHand->Plant && $ignoreWarehouse["warehouse"] == $onHand->WarehouseCode){
					$skipThisBin = true;
					break;
				}
			}

			if($skipThisBin){
				continue;
			}

			$partOnHandFound = true;
			$html .= '
			<div class="card" style="margin:20px;max-width:500px;">
				<div class="card-header" style="font-size: 1.4rem;">
					<b>'.$_SESSION["partFind"]["plants"][$onHand->Plant].'</b>
				</div>
				<ul class="list-group list-group-flush">';
				foreach ($partOnHandResult["result"]->returnObj->PartOnHandWhse as $Whse) {
					if($Whse->WarehouseCode == $onHand->WarehouseCode){
		  				$html .= '<li class="list-group-item"><b>Warehouse:</b> '.$Whse->WarehouseDesc.' </li>';
		  			}
				}

			if(strlen(trim($onHand->LotNum)) > 0){
				$html .= '<li class="list-group-item"><b>Lot:</b> '.$onHand->LotNum.' </li>';
			}

		  $html .= '<li class="list-group-item"><b>Bin:</b> '.$onHand->BinNum.' </li>
					<li class="list-group-item"><b>On Hand:</b> '.$onHand->QuantityOnHand.' '.$onHand->UnitOfMeasure.'</li>
				</ul>
			</div>';
		}

		if(count($partOnHandResult["result"]->returnObj->PartOnHandBin) == 0){
			foreach ($part["result"]->returnObj->PartWhse as $partWhse) {
				if($partWhse->Plant != $key){
					continue;
				}

				$html .= '
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.4rem;">
						<b>'.$_SESSION["partFind"]["plants"][$partWhse->Plant].'</b>
					</div>
					<ul class="list-group list-group-flush">
						<li class="list-group-item"><b>Warehouse:</b> '.$partWhse->WarehouseDescription.' </li>
						<li class="list-group-item"><b>Primary Bin:</b> '.$partWhse->PrimBinNum.' </li>
						<li class="list-group-item"><b>On Hand:</b> '.$partWhse->OnHandQty.' '.$partWhse->PartNumIUM.'</li>
					</ul>
				</div>';
			}
		}
	}

	return $html;
}

if(isset($_GET["part"])){
	$_POST["partNum"] = $_GET["part"];
}


//Step one submitted
if(isset($_POST["partNum"]) && strlen(trim($_POST["partNum"])) > 0){
	$_POST["partNum"] = trim($_POST["partNum"]);
	$partInfoResult = $api->getPartInfo($_POST["partNum"]);
	if(hasError($partInfoResult)){
		$error = true;
		$errorText = "Unable to get part information";
	} else {
		$_SESSION["partFind"]["part"] = $partInfoResult;
		$_SESSION["partFind"]["image"] = $api->getImage($partInfoResult["result"]->returnObj->Part[0]->ImageID);

		$partSupplierResult = $api->getPartSuppliers($_POST["partNum"]);
		if(hasError($partSupplierResult)){
			$error = true;
			$errorText = "Unable to get part suppliers";
		} else {
			$_SESSION["partFind"]["suppliers"] = $partSupplierResult;
			$step = 2;
		}
	}
} else if(isset($_POST["partDesc"]) && strlen(trim($_POST["partDesc"])) > 0){
	$_POST["partDesc"] = trim($_POST["partDesc"]);

	$partSearchResult = $api->searchForPart($_POST["partDesc"]);
	if(hasError($partSearchResult)){
		$error = true;
		$errorText = "Unable to search for part information";
	} else {
		$_SESSION["partFind"]["partSearch"] = $partSearchResult["result"];
	}

	$partMfgSearchResult = $api->mfgPartSearch($_POST["partDesc"]);
	if(hasError($partMfgSearchResult)){
		$error = true;
		$errorText = "Unable to search for part information";
	} else {
		$_SESSION["partFind"]["partSearchMfg"] = $partMfgSearchResult["result"];
	}

	if(!$error){
		$step = 1.5;
	}

	$partSearch = array();

	foreach ($partSearchResult["result"]->returnObj->PartList as $part) {
		if(!isset($partSearch[$part->PartNum])){
			$partSearch[$part->PartNum] = array(
				"PartNum" => $part->PartNum,
				"PartDescription" => $part->PartDescription,
				"SearchWord" => $part->SearchWord,
				"VendPartNum" => "",
				"MfgPartNum" => ""
			);
		}
	}

	foreach ($partMfgSearchResult["result"]->returnObj->SupplierPart as $part) {
		if(!isset($partSearch[$part->PartNum])){
			$partSearch[$part->PartNum] = array(
				"PartNum" => $part->PartNum,
				"PartDescription" => "",
				"SearchWord" => "",
				"VendPartNum" => $part->VendPartNum,
				"MfgPartNum" => $part->MfgPartNum
			);
		} else {
			$partSearch[$part->PartNum]["VendPartNum"] = $part->VendPartNum;
			$partSearch[$part->PartNum]["MfgPartNum"] = $part->MfgPartNum;
		}
	}

	$_SESSION["partFind"]["partSearch"] = $partSearch;
}


//Get sub parts
$hasRevision = false; //Part is made up of other parts
if($step == 2){
	unset($_SESSION["partFind"]["partMtl"]);
	$_SESSION["partFind"]["partMtl"] = array();
	$hasRevision = !empty($_SESSION["partFind"]["part"]["result"]->returnObj->PartRev);

	if(isset($_POST["rev"]) && !empty($_SESSION["partFind"]["part"]["result"]->returnObj->PartRev)){
		$methodParts = $api->getMethodParts($_POST["partNum"],$_POST["rev"]);
		if(hasError($methodParts)){
			$error = true;
			$errorText = "Unable to get part material information";
		} else {
			$hasRevision = true;
			foreach ($methodParts["result"]->returnObj->PartMtl as $partMtl) {
				$partMtlInfo = $api->getPartInfo($partMtl->MtlPartNum);
				if(hasError($partMtlInfo)){
					$error = true;
					$errorText = "Unable to get part material information";
				} else {
					$partMtlInfo["QtyPer"] = $partMtl->QtyPer." ".$partMtl->UOMCode;
					array_push($_SESSION["partFind"]["partMtl"], $partMtlInfo);
				}
			}
		}
		
	}
	
	$step = 3;
}

if(isset($_POST["partNum"]) && isset($_POST["rev"])){

}

//Get plants
if($step == 3){
	$plantsResult = $api->getAllPlants();
	if(hasError($plantsResult)){
		$error = true;
		$errorText = "Unable to get plant information";
	} else {
		foreach ($plantsResult["result"]->returnObj->PlantList as $plant) {
			$_SESSION["partFind"]["plants"][$plant->Plant] = $plant->Name;
		}
	}
}




?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Part Finder</b>
					</div>
					<div class="card-body">
						<center>
						<form action="partFind.php" method="POST">
							<br>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="Part Number" name="partNum">
						  </div>
						  <div style="margin-bottom: 10px;"><b>OR</b></div>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="Search" name="partDesc">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Find</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>


			<?php if($step == 1.5) { ?>
				<center>
					<a href="partFind.php" class="btn btn-primary" style="min-width:150px;"><b>Back</b></a>
					<br>
					<br>
					<?php foreach ($_SESSION["partFind"]["partSearch"] as $part) { ?>
						<div class="card" style="margin:20px;max-width:500px;">
							<div class="card-body">
								<center>
								<h5 class="card-title"><?php echo $part["PartNum"]; ?></h5><br>

								<?php if(strlen(trim($part["PartDescription"])) > 0){ ?>
									<p class="card-text"><?php echo $part["PartDescription"]; ?></p><br>
								<?php } ?>

								<?php if(strlen(trim($part["SearchWord"])) > 0){ ?>
									<p class="card-text"><?php echo $part["SearchWord"]; ?></p><br>
								<?php } ?>

								<?php if(strlen(trim($part["VendPartNum"])) > 0){ ?>
									<p class="card-text"><b>Vendor Part Num:</b><br><?php echo $part["VendPartNum"]; ?></p><br>
								<?php } ?>

								<?php if(strlen(trim($part["MfgPartNum"])) > 0){ ?>
									<p class="card-text"><b>Manufacturer Part Num:</b><br><?php echo $part["MfgPartNum"]; ?></p><br>
								<?php } ?>
								<img 
		                            src="img/photo.png" 
		                            data-bs-toggle="modal" 
		                            data-bs-target="#imageModal"
		                            partNum="<?php echo $part["PartNum"]; ?>"
		                            onclick="prepImageModal(this)" 
		                            style="width: 50px;height:50px;">
								
								<a href="partFind.php?part=<?php echo urlencode($part["PartNum"]); ?>" target="_blank" class="btn btn-primary"><b>View</b></a>
								</center>
							</div>
						</div>
					<?php } ?>
				</center>

			<?php } ?>
				
			<!-- Step Two -->
			<!-- Step Three -->
				<?php if($step == 3) { ?>

					<center>
							<!-- header -->
							<?php echo getPartHeaderHTML($_SESSION["partFind"]["part"]); ?>

							<!-- suppliers -->
							<?php echo getPartSuppliersHTML($_SESSION["partFind"]["suppliers"]); ?>
							
							<!--Has Revision / Sub parts warning -->
							<?php if($hasRevision) { ?>
								<div class="alert alert-warning" role="alert" style="max-width: 500px;">
								  <b>This part is made up of other parts</b>
								</div>
							<?php } ?>

							<!-- revisions -->
							<?php echo getPartRevisionsHTML($_SESSION["partFind"]["part"]); ?>

							<!--Main part -->
							<?php echo getPartLocationsHTML($_SESSION["partFind"]["part"]); ?>

							<!--Sub parts -->
							<?php 
								foreach ($_SESSION["partFind"]["partMtl"] as $subPart) {
									echo getPartHeaderHTML($subPart,true);
									echo getPartLocationsHTML($subPart);
								}
							?>

					</center>

					<center>
						<a href="index.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Home</center></b></a>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="partFind.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Find another</center></b></a>
					</center>
					<br>
				<?php } ?>
				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	
	<!-- Image Modal -->
    <div class="modal" id="imageModal" tabindex="-1" aria-labelledby="imageModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="margin-top: 50px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="imageModalLabel">Image</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              
            <center>
              <img id="partImage" style="width:90%;" src="">
            </center>

          </div>
          <div class="modal-footer">
            <button data-bs-dismiss="modal" class="btn btn-primary" ><b>Close</b></button>
          </div>
        </div>
      </div>
    </div>



    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script language="JavaScript" type="text/javascript">
    	function closeImageModal(){
        	$("#imageModal").modal("toggle");
    	}

    	function prepImageModal(button){
	        $("#imageModalLabel").text($(button).attr("partNum"));
	        $("#partImage").attr("src","");
	        $("#partImage").attr("src","img/load.gif");
	        $.post(
	          "inc/partImg.php",
	          {partNum: $(button).attr("partNum")},
	            function(data) {
	              var result = $.parseJSON(data);
	              if(result.result.returnObj.trim() == ""){
	                $("#partImage").attr("src","img/notfound.png");
	              } else {
	                $("#partImage").attr("src","data:image/png;base64,"+result.result.returnObj);
	              }
	            }
	        );
      }

    </script>
  </body>
</html>