<!doctype html>

<?php 
  
  $db = new PDO('sqlite:db/users.db');
  $error = false;
  $errorText = "";

  function validateForm(){
    global $error;
    global $errorText;

    if(strlen($_POST["name"]) < 3){
      $error = true;
      $errorText = "Your name must be more than 3 characters";
      return;
    }

    if(strlen($_POST["email"]) < 3){
      $error = true;
      $errorText = "Your inputEmail must be more than 3 characters";
      return;
    }

    if(strlen($_POST["company"]) < 3){
      $error = true;
      $errorText = "Your company must be more than 3 characters";
      return;
    }

    if(strlen($_POST["phone"]) < 3){
      $error = true;
      $errorText = "Your phone number must be more than 3 characters";
      return;
    }
  }

  function insertUserIntoDatabase($db,$name,$email,$comp,$phone,$tshirt){
    $qry = $db->prepare(
    'INSERT INTO users (name, email, company, phone, time, ip, tshirt) VALUES (?, ?, ?, ?, ?, ?, ?)');
    $ret = $qry->execute(array($name,$email,$comp,$phone,time(),getUserIP(),$tshirt));
    if(!$ret) {
      $error = true;
      $errorText = "Unable to insert into database";
      return false;
    }

    return true;
  }

  function getUserIP(){
      // Get real visitor IP behind CloudFlare network
      if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
      }

      $client  = @$_SERVER['HTTP_CLIENT_IP'];
      $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote  = $_SERVER['REMOTE_ADDR'];

      if(filter_var($client, FILTER_VALIDATE_IP))
      {
          $ip = $client;
      }
      elseif(filter_var($forward, FILTER_VALIDATE_IP))
      {
          $ip = $forward;
      }
      else
      {
          $ip = $remote;
      }

      return $ip;
  }

  if(isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["company"]) && isset($_POST["phone"])){
    validateForm();

    if(!isset($_POST["tshirt"])){
      $_POST["tshirt"] = "0";
    }

    if(!$error){
      $result = insertUserIntoDatabase($db,$_POST["name"],$_POST["email"],$_POST["company"],$_POST["phone"],$_POST["tshirt"]);
      if($result){
        header("Location: done.php");
        die();
      }
    }
  }

?>

<html lang="en">
  <head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W8ZRBCX');
    </script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-P5HGVGTSR9"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-P5HGVGTSR9');
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Conveyor World</title>
    <link href="css/bootstrap.min.css?v=<?php echo hash_file('md5', 'css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="css/form.css?v=<?php echo hash_file('md5', 'css/form.css'); ?>" rel="stylesheet">
  </head>
  <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W8ZRBCX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
    <center>

      <?php if($error) { ?>
        <br><center>
        <div class="alert alert-danger" role="alert" style="max-width:400px;">
          <b><?php echo $errorText; ?></b>
        </div></center><br>
      <?php } ?>

      <!-- Large and up -->
      <form id="form-container" action="form.php" method="post">
        <div id="first-form-label"  class="mb-3 form-element">
          <input type="text" class="form-control" id="inputName" minlength="3" name="name" required>
          <label for="inputName" class="form-label">Name</label>
        </div>
        <div class="mb-3 form-element">
          <input type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" minlength="3"  name="email" required>
          <label for="inputEmail" class="form-label">Business Email address</label>
        </div>
        <div class="mb-3 form-element">
          <input type="text" class="form-control" id="inputCompany" minlength="3" name="company" required>
          <label for="inputCompany" class="form-label">Company</label>
        </div>
        <div class="mb-3 form-element">
          <input type="text" class="form-control" id="inputPhone" minlength="3" name="phone" required>
          <label for="inputPhone" class="form-label">Phone Number</label>
        </div>
        <br>
        <div class="mb-3 form-element">
          <br>
          <table>
            <tr>
              <td style="min-width: 40px;"><input class="form-check-input" type="checkbox" value="1" name="tshirt" id="flexCheckDefault"></td>
              <td>
              <label class="form-check-label cb-label" for="flexCheckDefault">
                Yes, please send me a ConveyorWorld.net tshirt
              </label>
              </td>
            </tr>
          </table>
        </div>

        <div id="register-btn" onclick="submitFrom()"></div>
      </form>

    </center>

    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
      function submitFrom(){
        document.getElementById("form-container").submit();
      }
    </script>
  </body>
</html>