<?php
  if(!isset($_GET["pass"])){
    die("missing password");
  }

  if($_GET["pass"] != "D73KAD9FM2"){
    die("wrong password");
  }

  $db = new PDO('sqlite:db/users.db');

  function getData($db){
    $qry = $db->prepare(
      'SELECT * FROM users');

    $ret = $qry->execute(array());
    return $qry->fetchAll();
  }

  $data = getData($db);

?>

<!doctype html>
<html lang="en">
  <head>
  	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W8ZRBCX');
    </script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-P5HGVGTSR9"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-P5HGVGTSR9');
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Conveyor World</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
  	<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W8ZRBCX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <center>

      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Company</th>
            <th scope="col">Phone</th>
            <th scope="col">Time</th>
            <th scope="col">TShirt</th>
          </tr>
        </thead>
        <?php for($i = 0; $i < sizeof($data); $i++){
          if($data[$i]["tshirt"] == 1){
            $data[$i]["tshirt"] = "Yes";
          } else {
            $data[$i]["tshirt"] = "No";
          }

          $secondsAgo = time()-$data[$i]["time"];
          $data[$i]["time"] = intval($secondsAgo/60);
        ?>
          <tr>
            <td>
              <?php echo htmlspecialchars($data[$i]["id"]);?>
            </td>
            <td>
              <?php echo htmlspecialchars($data[$i]["name"]);?>
            </td>
            <td>
              <?php echo htmlspecialchars($data[$i]["email"]);?>
            </td>
            <td>
              <?php echo htmlspecialchars($data[$i]["company"]);?>
            </td>
            <td>
              <?php echo htmlspecialchars($data[$i]["phone"]);?>
            </td>
            <td>
              <?php echo htmlspecialchars($data[$i]["time"]);?> Mins ago
            </td>
            <td>
              <?php echo htmlspecialchars($data[$i]["tshirt"]);?>
            </td>
          </tr>
        <?php } ?>

      </table>
      
    </center>

    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>