<!doctype html>

<?php 
  
?>

<html lang="en">
  <head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W8ZRBCX');
    </script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-P5HGVGTSR9"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-P5HGVGTSR9');
    </script>

    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Conveyor World</title>
    <link href="css/bootstrap.min.css?v=<?php echo hash_file('md5', 'css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="css/index.css?v=<?php echo hash_file('md5', 'css/index.css'); ?>" rel="stylesheet">
  </head>
  <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W8ZRBCX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
    <center>

      <div id="bg-div">
        <a href="form.php"><img id="top-btn" class="register-btn" src="img/clicktoregister_button.png"></a>
        <br>
        <a href="form.php"><img id="bottom-btn" class="register-btn" src="img/clicktoregister_button.png"></a>
      </div>

    </center>

    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
      function submitFrom(){
        document.getElementById("form-container").submit();
      }
    </script>
  </body>
</html>