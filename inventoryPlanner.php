<?php
ini_set("memory_limit","256M");
session_start();

include_once 'inc/inventoryPlannerFunctions.php';
include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}

$db = new PDO('sqlite:db/inventoryPlanner.db');
$error = false;
$errorText = "";
$step = 0;
$completeReports = getCompleteReports($db);

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function addBasePart($partNum,$rev,$qty){
	if(!isset($_SESSION["partInv"]["baseParts"])){
		$_SESSION["partInv"]["baseParts"] = array();
	}

	//Update
	foreach ($_SESSION["partInv"]["baseParts"] as $key => $basePart) {
		if($basePart["partNum"] == $partNum && $basePart["rev"] == $rev){
			$_SESSION["partInv"]["baseParts"][$key]["rev"] = $rev;
			$_SESSION["partInv"]["baseParts"][$key]["qty"] = $qty;
			return;
		}
	}

	$part = array(
		"partNum" => $partNum,
		"rev" => $rev,
		"qty" => $qty
	);

	array_push($_SESSION["partInv"]["baseParts"], $part);
}

if(isset($_POST["deleteReport"])){
	deleteReport($db,$_POST["deleteReport"]);
	$completeReports = getCompleteReports($db);
}

if(isset($_POST["clearParts"])){
	unset($_SESSION["partInv"]["baseParts"]);
}

//Step 0
if(isset($_POST["reportName"])){
	$_SESSION["partInv"]["reportName"] = $_POST["reportName"];
	if(isset($_SESSION["partInv"]["baseParts"])){
		unset($_SESSION["partInv"]["baseParts"]);
	}
	$step = 1;
}

if(isset($_POST["addBasePart"]) && isset($_SESSION["partInv"]["reportName"])){
	$step = 1;
}

//Step one submitted
if(isset($_POST["partNum"]) && isset($_POST["qty"])){
	if(is_numeric($_POST["qty"])){
		$_SESSION["partInv"]["qty"] = $_POST["qty"];
	} else {
		$error = true;
		$errorText = "Invalid part qty";
		$_SESSION["partInv"]["qty"] = 1;
	}

	$partInfoResult = $api->getPartInfo($_POST["partNum"]);
	if(hasError($partInfoResult)){
		$error = true;
		$errorText = "Unable to get part information";
	} else {
		$_SESSION["partInv"]["partNum"] = $_POST["partNum"];
		$_SESSION["partInv"]["part"] = $partInfoResult;
		$step = 2;
	}
}

//Get revisions
if(isset($_POST["revNum"])){
	addBasePart(
		$_SESSION["partInv"]["partNum"],
		$_POST["revNum"],
		$_SESSION["partInv"]["qty"]);
	$step = 3;
}

if(isset($_POST["continue"])){
	$step = 4;
	$db = new PDO('sqlite:db/inventoryPlanner.db');

	//Insert report
	$reportID = insertReport($db,$_SESSION["partInv"]["reportName"],$_SESSION["empNum"]);
	if($reportID == false){
		$error = true;
		$errorText = "Unable to insert report";
	} else {
		$_SESSION["partInv"]["reportID"] = $reportID;
		foreach ($_SESSION["partInv"]["baseParts"] as $basePart) {
	  		$partInfoResult = $api->getPartInfo($basePart["partNum"]);
	  		if(hasError($partInfoResult)){
	  			$error = true;
				$errorText = "Unable to get part info ".$basePart["partNum"];
				break;
	  		} else {
	  			//Insert base parts
	  			$result = insertPart(
	  				$db,
	  				$_SESSION["partInv"]["reportID"],
	  				$basePart["partNum"],
	  				$partInfoResult["result"]->returnObj->Part[0]->PartDescription,
	  				$partInfoResult["result"]->returnObj->Part[0]->IUM,
	  				0,
	  				"",
	  				$basePart["rev"],
	  				$basePart["qty"]);

	  			if($result == false){
					$error = true;
					$errorText = "Unable to insert part";
					break;
				}
	  		}
  		}
	}
}


?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">

			<!-- New Step 0 -->
			<?php if($step == 0) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-body">
						<center>
						<h5 class="card-title">Create a report</h5><br>
						
						<form style="max-width:350px;" action="inventoryPlanner.php" method="POST">
						  <input class="form-control" type="text" autocomplete="off" name="reportName" value="" placeholder="Report name">
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>

				<?php foreach ($completeReports as $report) { 
					if($report["id"] == 262){
						continue;
					}
				?>
					<div class="card" style="margin:20px;max-width:500px;">
						<div class="card-body">
							<center>
							<h5 class="card-title"><?php echo $report["name"]; ?></h5><br>
							<p class="card-text">ID: <?php echo $report["id"]; ?></p>
							<p class="card-text"><?php echo date("Y-m-d h:i", $report["time"]); ?></p>
							
							<form style="max-width:350px;" action="inventoryPlannerView.php" method="POST">
							  <input class="form-control" type="hidden" name="reportID" value="<?php echo $report["id"]; ?>" >
							  <br>
							  <button type="submit" class="btn btn-primary"><b>View</b></button>
							</form>

							<form style="max-width:350px;" action="inventoryPlanner.php" method="POST">
							  <input class="form-control" type="hidden" name="deleteReport" value="<?php echo $report["id"]; ?>" >
							  <br>
							  <button type="submit" class="btn btn-danger"><b>Delete</b></button>
							</form>
							</center>
						</div>
					</div>
				<?php } ?>

				</center>
			<?php } ?>

		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Part Inventory</b>
					</div>
					<div class="card-body">
						<center>
						<form action="inventoryPlanner.php" method="POST">
							<br>
						  <div class="mb-3">
							<input id="partNumInput" class="form-control" placeholder="Part Number" name="partNum" style="width: 75%;">
						  </div>
						  <div class="mb-3">
							<input type="number" class="form-control" placeholder="Qty" name="qty" style="width: 20%;">
						  </div>
						  <button type="submit" class="btn btn-primary"><b>Find</b></button>
						</form>
						</center>
					</div>
				</div>

				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Common parts</b>
					</div>
					<div class="card-body">
						<center>
							<ul class="list-group">
								<li class="list-group-item">SG1 60 Amps 220-250 Volts 
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-061"><b>Select</b></button></li>
								<li class="list-group-item">SG1 60 Amps 360-480 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-062"><b>Select</b></button></li>
								<li class="list-group-item">SG1 60 Amps 525-600 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-063"><b>Select</b></button></li>
								<li class="list-group-item">SG1 100 Amps 220-250 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-101"><b>Select</b></button></li>
								<li class="list-group-item">SG1 100 Amps 360-480 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-102"><b>Select</b></button></li>
								<li class="list-group-item">SG1 100 Amps 525-600 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-103"><b>Select</b></button></li>
								<li class="list-group-item">T6GFXP 
									<br><br><button class="part-num-btn btn btn-primary" partnum="T6GFXP"><b>Select</b></button></li>
								<li class="list-group-item">T6
									<br><br><button class="part-num-btn btn btn-primary" partnum="T6"><b>Select</b></button></li>
								<li class="list-group-item">BeltGard 3.0
									<br><br><button class="part-num-btn btn btn-primary" partnum="BG3"><b>Select</b></button></li>
							</ul>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
			<!-- Step Two -->
			<?php if($step == 2) { 
				foreach ($_SESSION["partInv"]["part"]["result"]->returnObj->PartRev as $rev) {
			?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-body">
						<center>
						<h5 class="card-title">Rev <?php echo $rev->RevisionNum; ?></h5>
						<p class="card-text"><?php echo $rev->RevShortDesc; ?></p>
						
						<form style="max-width:350px;" action="inventoryPlanner.php" method="POST">
						  <input type="hidden" name="revNum" value="<?php echo $rev->RevisionNum; ?>">
						  <button type="submit" class="btn btn-primary"><b>Select</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } } ?>

			<!-- Step Three -->
			<?php if($step == 3) { ?>
				<center>
					<div class="card" style="margin:20px;max-width:500px;">
						<div class="card-body">
							<center>
							<h5 class="card-title">Base Parts</h5><br>

							<br>
							<?php foreach ($_SESSION["partInv"]["baseParts"] as $basePart) { ?>
								<p class="card-text"><b><?php echo $basePart["partNum"]; ?></b>  Rev:<?php echo $basePart["rev"]; ?> x<?php echo $basePart["qty"]; ?></p>
							<?php } ?>
							<br>
							
							<form style="max-width:350px;" action="inventoryPlanner.php" method="POST">
							  <input type="hidden" name="addBasePart" value="1">
							  <button type="submit" class="btn btn-primary" style="min-width: 165px;"><b>Add Another Part</b></button>
							</form>
							<br>
							<form style="max-width:350px;" action="inventoryPlanner.php" method="POST">
							  <input type="hidden" name="clearParts" value="1">
							  <button type="submit" class="btn btn-primary" style="min-width: 165px;"><b>Clear parts</b></button>
							</form>
							<br>
							<form style="max-width:350px;" action="inventoryPlanner.php" method="POST">
							  <input type="hidden" name="continue" value="1">
							  <button type="submit" class="btn btn-primary" style="min-width: 165px;"><b>Continue</b></button>
							</form>
							<br>

							</center>
						</div>
					</div>
				</center>
			<?php } ?>

			<!-- Step Three -->
			<?php if($step == 4) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-body">
						<center>
						<h5 class="card-title">Creating report...</h5>
						<br>
						<center>
						<img id="progressImg" src="img/loading.gif" style="margin-top: 20px;margin-bottom: 20px;max-width:200px;max-height:400px;">
						</center>
						<br>
						<p id="progress" class="card-text">Loading...</p>
						<p id="locationProgress" class="card-text"></p>
						 <input id="createReport" class="form-control" type="hidden" value="<?php echo $_SESSION["partInv"]["reportID"];?>" placeholder="Report name">
						 <a id="viewBtn" href="inventoryPlannerView.php?reportID=<?php echo $_SESSION["partInv"]["reportID"];?>" class="btn btn-primary" style="display:none;margin:40px;"><b>View Report</b></a>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>

				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
    	var reportID = "";
    	var type = "grab";
    	var partCount = 0;
    	var locaPartCount = 0;
    	$( document ).ready(function() {
    		if(document.getElementById("createReport")) {
    			reportID = $("#createReport").val();
		    	getSubparts();
			}
		});

		function isJson(str) {
          try {
              JSON.parse(str);
          } catch (e) {
              return false;
          }
          return true;
      	}

      	$('.part-num-btn').click(function(){
		    $('#partNumInput').val($(this).attr('partnum'));
		});

		function getSubparts(){
			console.log("getSubparts "+type);
            $.post(
            "inc/inventoryPlannerPartGrab.php",
            {reportID: reportID, type: type},
              function(data) {
              	console.log(data);
                if(isJson(data)){
                  var result = $.parseJSON(data);

                  if(result.hasNext){
                  	getSubparts();
                  } 

                  if(!result.hasNext & type == "grab"){
                  	console.log("Grabbing parts complete");
                  	type = "location";
                  	getSubparts();
                  	return;
                  } 

                  if(!result.hasNext & type == "location"){
                  	console.log("Grabbing locations complete");
                  	$("#progressImg").attr("src","img/check.png");
                  	$("#viewBtn").show();
                  }

                  if(type == "grab"){
                  	partCount = partCount+result.count;
                  	$("#progress").html("Parts grabbed "+partCount);
              	  }

              	  if(type == "location"){
              	  	locaPartCount = locaPartCount+result.count;
                  	$("#locationProgress").html("Locations grabbed "+locaPartCount);
              	  }
              	}
              }
          );
		}

    </script>
  </body>
</html>