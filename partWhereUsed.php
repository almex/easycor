<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	die();
}


$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

if(isset($_GET["part"])){
	$_POST["partNum"] = $_GET["part"];
}

//Step one submitted
if(isset($_POST["partNum"])){
	$partResult = $api->getPartWhereUsed($_POST["partNum"]);
	if(hasError($partResult)){
		$error = true;
		$errorText = "Unable to get part where used";
	} else {
		$step = 2;
	}
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Part Where Used</b>
					</div>
					<div class="card-body">
						<center>
						<form action="partWhereUsed.php" method="POST">
							<br>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="Part Number" name="partNum">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Find</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
			<!-- Step Two -->
			<?php if($step == 2) { ?>
				<center>

				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">
							<?php echo htmlspecialchars($_POST["partNum"]); ?>
							<br><br>is used in the below parts
						</h5>
					</div>
				</div>
				<br>

				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($partResult["result"]->returnObj->PartWhereUsed as $whereUsed) { ?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col" colspan="4">
						      <center>
							      <?php echo htmlspecialchars($whereUsed->PartNumPartDescription); ?>
							      <br>
							      <a target="_blank" href="partFind.php?part=<?php echo urlencode($whereUsed->PartNum); ?>"><?php echo htmlspecialchars($whereUsed->PartNum); ?></a>
						  	  </center>
						      </th>
						    </tr>
					  		<tr>
						      <th scope="col" colspan="2" >Operation</th>
						      <th scope="col" colspan="2" >Operation Code</th>
						    </tr>
					  		<tr>
					  		   <td colspan="2"><?php echo htmlspecialchars($whereUsed->OpDesc); ?></td>
					  		   <td colspan="2"><?php echo htmlspecialchars($whereUsed->OpCode); ?></td>
					  		</tr>
					  		<tr>
						      <th scope="col" colspan="2" >Qty Per</th>
						      <th scope="col" colspan="2" >Revision</th>
						    </tr>
					  		<tr>
					  		   <td colspan="2"><?php echo htmlspecialchars($whereUsed->QtyPer); ?></td>
					  		   <td colspan="2"><?php echo htmlspecialchars($whereUsed->RevisionNum); ?></td>
					  		</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>
			</center>

			<?php } ?>
				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>