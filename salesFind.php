<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function removeDuplicateLines($salesOrders){
	$newResults = array();
	foreach ($salesOrders["result"]->returnObj->Results as $key => $salesOrder) {
		$duplicate = false;
		foreach ($newResults as $compareKey => $compareSalesOrder) {
			if($salesOrder->OrderHed_OrderNum == $compareSalesOrder->OrderHed_OrderNum &&
		       $salesOrder->OrderDtl_PartNum == $compareSalesOrder->OrderDtl_PartNum &&
		       $salesOrder->OrderRel_OrderLine == $compareSalesOrder->OrderRel_OrderLine &&
		       $salesOrder->OrderDtl_SellingQuantity == $compareSalesOrder->OrderDtl_SellingQuantity &&
		       $salesOrder->OrderRel_OrderRelNum == $compareSalesOrder->OrderRel_OrderRelNum){
				$duplicate = true;
			}
		}

		if(!$duplicate){
			array_push($newResults, $salesOrder);
		}
	}

	$salesOrders["result"]->returnObj->Results = $newResults;
	return $salesOrders;
}

$allPlants = $api->getAllPlants();
hasError($allPlants);

if(isset($_GET["soNum"])){
	$_POST["soNum"] = $_GET["soNum"];
}

//Step one submitted
if(isset($_POST["plant"]) && isset($_POST["search"])){
	$salesOrders = $api->getSalesOrders($_POST["plant"],trim($_POST["search"]));
	if(!hasError($salesOrders)){
		$salesOrders = removeDuplicateLines($salesOrders);
		usort($salesOrders["result"]->returnObj->Results, function($a, $b) {
		    return $b->OrderHed_OrderNum <=> $a->OrderHed_OrderNum;
		});
		$step = 2;
	}
}

if(isset($_POST["soNum"])){
	$salesOrders = $api->getSalesOrder(trim($_POST["soNum"]));
	if(!hasError($salesOrders)){
		$salesOrders = removeDuplicateLines($salesOrders);
		usort($salesOrders["result"]->returnObj->Results, function($a, $b) {
		    return $a->OrderRel_OrderLine <=> $b->OrderRel_OrderLine;
		});
		$step = 2;
	}
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<br>
				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">Only shows <b>OPEN</b> sales orders</h5>
					</div>
				</div>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Sales Order Find</b>
					</div>
					<div class="card-body">
						<center>
						<form action="salesFind.php" method="POST">
							<br>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="SO Num" name="soNum">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Find</b></button>
						</form>
						</center>
					</div>
				</div>
				<br>
				<b> OR </b>
				<br>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Sales Order Search</b>
					</div>
					<div class="card-body">
						<center>
						<form action="salesFind.php" method="POST">
							<br>
						  <div class="mb-3">
						  	<label class="form-label"><b>Plant:</b></label>
						  	<select class="form-select" aria-label="select" name="plant">
						  		<?php 
						  			foreach ($allPlants["result"]->returnObj->PlantList as $plant) {
						  				echo '<option value="'.$plant->Plant.'" selected>'.$plant->Name.'</option>';
						  			}
						  		?>
							</select>
							<br>
							<input class="form-control" autocomplete="off" placeholder="Search" name="search">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Search</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
			<!-- Step Two -->
			<?php if($step == 2) { ?>
				<center>
				<?php if(isset($_POST["soNum"])){ ?>
					<br>
					<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
						<div class="card-body">
							<h5 class="card-title">Sales Order #<?php echo $_POST["soNum"]; ?></h5>
							<?php if(isset($salesOrders["result"]->returnObj->Results[0]->OrderHed_EntryPerson))?>
								<br><h5 class="card-title">Entry Person: <?php echo $salesOrders["result"]->returnObj->Results[0]->OrderHed_EntryPerson; ?></h5>
							<?php ?>
						</div>
					</div>
				<?php } ?>
				<br>
				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($salesOrders["result"]->returnObj->Results as $soDetail) { 

					  			if(strlen(trim($soDetail->OrderHed_OrderDate)) > 0){
					  				$soDetail->OrderHed_OrderDate = explode("T", $soDetail->OrderHed_OrderDate)[0];
					  			}

					  		?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col">SO</th>
						      <th scope="col">Line</th>
						      <th scope="col">Part</th>
						      <th scope="col">Selling Qty</th>
						    </tr>
					  		<tr>
					  			<td><a target="_blank" href="salesFind.php?soNum=<?php echo $soDetail->OrderHed_OrderNum; ?>"><?php echo $soDetail->OrderHed_OrderNum; ?></a></td>
					  			<td><?php echo $soDetail->OrderRel_OrderLine; ?></td>
					  			<td><a target="_blank" href="partFind.php?part=<?php echo urlencode($soDetail->OrderDtl_PartNum); ?>"><?php echo $soDetail->OrderDtl_PartNum; ?></a></td>
					  			<td><?php echo $soDetail->OrderDtl_SellingQuantity; ?></td>
					  		</tr>
					  		<tr>
					  		  <th scope="col" >From Plant</th>
					  		  <th scope="col" >Cust.</th>
					  		  <th scope="col" >Cust. ID</th>
						      <th scope="col" >Prod. Code</th>
						    </tr>
						    <tr>
						    	<td><?php echo $soDetail->OrderRel_Plant; ?></td>
					  			<td><?php echo $soDetail->Customer_Name; ?></td>
					  			<td><?php echo $soDetail->Customer_CustID; ?></td>
					  			<td><?php echo $soDetail->OrderDtl_ProdCode; ?></td>
					  		</tr>
					  		<tr>
					  		  <th scope="col" colspan="2">Order Date</th>
					  		  <th scope="col" colspan="1">PO Num</th>
					  		  <th scope="col" colspan="1">Rel Num</th>
						    </tr>
						    <tr>
						    	<td colspan="2"><?php echo $soDetail->OrderHed_OrderDate; ?></td>
					  			<td colspan="1"><?php echo $soDetail->OrderHed_PONum; ?></td>
					  			<td colspan="1"><?php echo $soDetail->OrderRel_OrderRelNum; ?></td>
					  		</tr>
					  		<tr>
						      <th scope="col" colspan="4">Description</th>
						    </tr>
					  		<tr>
					  			<td colspan="4"><?php echo $soDetail->OrderDtl_LineDesc; ?></td>
					  		</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>
			</center>
			<br>
			<center>
				<a href="index.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Home</center></b></a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="salesFind.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Find another</center></b></a>
			</center>
			<br>

			<?php } ?>

				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>