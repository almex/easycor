<?php
session_start();

include_once 'inc/partPlannerFunctions.php';
include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	die();
}

$db = new PDO('sqlite:db/partPlanner.db');
$error = false;
$errorText = "";
$step = 1;
$plantsResult = $api->getAllPlants();
$count = 0;
$ignoredParts = array();

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

if(isset($_POST["ignorePart"]) && isset($_GET["partNum"]) && isset($_GET["rev"])){
	if(!ignorePartExists($db,$_GET["partNum"],$_GET["rev"],trim($_POST["ignorePart"]))){
		insertIgnorePart($db,$_GET["partNum"],$_GET["rev"],trim($_POST["ignorePart"]));
	}
}

if(isset($_GET["deleteIgnore"])){
	deleteIgnorePart($db,$_GET["deleteIgnore"]);
}

if(isset($_GET["plant"])){
	$plantWarehouses = $api->getWarehouses($_GET["plant"]);
	$parsedWarehouses = array();
	foreach ($plantWarehouses["result"]->returnObj->Warehse as $warehse) {
		array_push($parsedWarehouses, $warehse->WarehouseCode);
	}
	$plantWarehouses = $parsedWarehouses;
}

//Step one submitted
if(isset($_GET["plant"]) && isset($_GET["partNum"]) && !isset($_GET["rev"])){
	$partInfoResult = $api->getPartInfo($_GET["partNum"]);
	if(hasError($partInfoResult)){
		$error = true;
		$errorText = "Unable to get part information";
	} else {
		$step = 2;
	}
}

//Step two submitted
if(isset($_GET["plant"]) && isset($_GET["partNum"]) && isset($_GET["rev"])){
	$ignoredParts = getAllPartsToIgnore($db,$_GET["partNum"],$_GET["rev"]);
	$parsedIgnoredParts = array();
	foreach ($ignoredParts as $ignorePart) {
		array_push($parsedIgnoredParts, strtoupper(trim($ignorePart["ignore"])));
	}

	$step = 3;
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
    	.table{
    		margin-bottom: 40px;
    		max-width: 600px;
    	}

    	.table-header{
    		text-align: center;
    		border-bottom-width: 0px!important;
    	}

    	.table-row-header{
    		border-bottom-width: 0px!important;
    	}

    	.dark-grey-bg{
    		background: #c3c3c3;
    	}

    	.table-td{

    	}

    	.bottom-border{
    		border-bottom: 2px solid #848484;
    	}


    	.thick-bottom-border{
    		border-bottom: 3px solid #000;
    	}

    	.bold-text{
    		font-weight: bold;
    	}

    	.red-text{
    		font-weight: bold;
    		color: #dc0000;
    	}

    	.light-grey-text{
    		color: #969696;
    	}

    	.light-grey-bg{
    		background: #e0e0e0;
    	}

    	a{
    		color: #3c3c3c;
    		text-decoration: none;
    	}
    </style>

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Part Planner</b>
					</div>
					<div class="card-body">
						<center>
						<br>
						<form action="partPlanner.php" method="GET">
						  <select class="form-select" aria-label="Plant" name="plant">
							<?php foreach($plantsResult["result"]->returnObj->PlantList as $plant){ 
								$selected = "";
						  		if($plant->Plant == 200){
						  			$selected = "selected";
						  		}
							?>
							  <option value="<?php echo $plant->Plant; ?>" <?php echo $selected;?> ><?php echo $plant->Name; ?></option>
							 <?php } ?>
							</select>
						  <br>
						  <div class="mb-3">
							<input id="partNumInput" class="form-control" autocomplete="off" placeholder="Part Number" name="partNum">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>

				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Common parts</b>
					</div>
					<div class="card-body">
						<center>
							<ul class="list-group">
								<li class="list-group-item">SG1 60 Amps 220-250 Volts 
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-061"><b>Select</b></button></li>
								<li class="list-group-item">SG1 60 Amps 360-480 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-062"><b>Select</b></button></li>
								<li class="list-group-item">SG1 60 Amps 525-600 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-063"><b>Select</b></button></li>
								<li class="list-group-item">SG1 100 Amps 220-250 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-101"><b>Select</b></button></li>
								<li class="list-group-item">SG1 100 Amps 360-480 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-102"><b>Select</b></button></li>
								<li class="list-group-item">SG1 100 Amps 525-600 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-103"><b>Select</b></button></li>
								<li class="list-group-item">T6GFXP 
									<br><br><button class="part-num-btn btn btn-primary" partnum="T6GFXP"><b>Select</b></button></li>
								<li class="list-group-item">T6
									<br><br><button class="part-num-btn btn btn-primary" partnum="T6"><b>Select</b></button></li>
								<li class="list-group-item">BeltGard 3.0
									<br><br><button class="part-num-btn btn btn-primary" partnum="BG3"><b>Select</b></button></li>
							</ul>
						</center>
					</div>
				</div>

				</center>
			<?php } ?>

			<!-- Step Two -->
			<?php if($step == 2) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Part Planner</b>
					</div>
					<div class="card-body">
						<center>
						<br>
						<form action="partPlanner.php" method="GET">
						  <b>Select Revision:</b>
						  <select class="form-select" aria-label="revision" name="rev" style="margin-top:10px;">
							<?php foreach ($partInfoResult["result"]->returnObj->PartRev as $partRev) { ?>
							  <option value="<?php echo $partRev->RevisionNum; ?>"><?php echo $partRev->RevisionNum; ?></option>
							<?php } ?>
						  </select>
						  <br>
						  <input class="form-control" autocomplete="off" type="hidden" name="plant" value="<?php echo $_GET["plant"]?>">
						  <input class="form-control" autocomplete="off" type="hidden" name="partNum" value="<?php echo $_GET["partNum"]?>">
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>

			<!-- Step Three -->
			<?php if($step == 3) { ?>
				<center>
					<div class="card text-white bg-dark" style="max-width: 600px;margin-top:20px;margin-bottom: 40px;">
						<div class="card-body">
							<h5 class="card-title"><?php echo $_GET["partNum"];?></h5>
							<p>Rev <?php echo $_GET["rev"];?></p>
							<p>Plant <?php echo $_GET["plant"];?></p>
						</div>
					</div>

					<div class="card text-white bg-dark" style="max-width: 600px;margin-top:20px;margin-bottom: 40px;">
						<div class="card-body">
							<h5 class="card-title">Ignored Material</h5>
							<form 
								action="partPlanner.php?rev=<?php echo urlencode($_GET['rev']);?>&plant=<?php echo urlencode($_GET['plant']);?>&partNum=<?php echo urlencode($_GET['partNum']);?>" 
								method="POST" 
								style="margin:30px;max-width:300px;">

								<input type="hidden" name="basePart" value="<?php echo $_GET["partNum"]?>">
								<input type="hidden" name="rev" value="<?php echo $_GET["rev"]?>">
								<div class="input-group mb-3">
									<input type="text" name="ignorePart" placeholder="Part Number" class="form-control">
									<button type="submit" class="btn btn-primary"><b>Add</b></button>
								</div>
							</form>

							<table style="margin:10px;">
							<?php foreach ($ignoredParts as $ignoredPart) { ?>
								<tr>
									<td><?php echo $ignoredPart["ignore"]; ?></td>
									<td>
										<a href="partPlanner.php?rev=<?php echo urlencode($_GET['rev']);?>&plant=<?php echo urlencode($_GET['plant']);?>&partNum=<?php echo urlencode($_GET['partNum']);?>&deleteIgnore=<?php echo $ignoredPart["id"]; ?>" >
										<img src="img/trash.png" style="max-width:25px; margin-left: 15px;">
										</a>
									</td>
								</tr>	
							<?php } ?>
							</table>
						</div>
					</div>

					<button id="start-btn" class="btn btn-primary" onclick="start()" style="min-width:150px;"><b>Start</b></button>

					<div id="loading-alert" class="card text-white bg-dark mb-3" style="max-width: 600px;display: none;">
						<div class="card-body">
							<h5 id="loading-status" class="card-title">Loading...</h5>
							<br>
							<img id="issue-all-loading" src="img/loading3.gif" style="max-width: 125px;">
						</div>
					</div>

					<div id="table-container" style="margin-top: 40px;">
					</div>
			<?php } ?>
				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	
	<!-- Image Modal -->
    <div class="modal" id="imageModal" tabindex="-1" aria-labelledby="imageModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="margin-top: 50px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="imageModalLabel">Image</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              
            <center>
              <img id="partImage" style="width:90%;" src="">
            </center>

          </div>
          <div class="modal-footer">
            <button data-bs-dismiss="modal" class="btn btn-primary" ><b>Close</b></button>
          </div>
        </div>
      </div>
    </div>



    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script language="JavaScript" type="text/javascript">
    	var basePartNum = <?php if(isset($_GET["partNum"])){ echo json_encode($_GET["partNum"]); } else { echo '""'; }; ?>;
    	var baseRev = <?php if(isset($_GET["rev"])){ echo json_encode($_GET["rev"]); } else { echo '""';}; ?>;
    	var allowedWarehouses = <?php if(isset($plantWarehouses)){ echo json_encode($plantWarehouses); } else { echo json_encode(array()); }; ?>;
    	var ignoredParts = <?php if(isset($parsedIgnoredParts)){ echo json_encode($parsedIgnoredParts); } else { echo json_encode(array()); }; ?>;
    	var id = 0;
    	var partList = [];
    	var toGrab = [];
    	var allPartNums = [];
    	var makeableQty = [];
    	var materialParents = [];
    	var materialQtyReq = [];
    	var entityMap = {
	        '&': '&amp;',
	        '<': '&lt;',
	        '>': '&gt;',
	        '"': '&quot;',
	        "'": '&#39;',
	        '/': '&#x2F;',
	        '`': '&#x60;',
	        '=': '&#x3D;'
	    }

    	function start(){
    		$("#start-btn").hide();
    		$("#loading-alert").show();
    		$("#loading-status").html("Getting children of "+basePartNum+"...");
    		addToGrabList(id,basePartNum,baseRev)
    		grabNextPart();
    	}

    	$('.part-num-btn').click(function(){
		    $('#partNumInput').val($(this).attr('partnum'));
		});

    	function grabNextPart(){
    		var nextID;
    		var nextPartNum;
    		var nextRev;

    		for (let i = 0; i < toGrab.length; i++) {
    			var nextPartFound = false;
    			for (let j = 0; j < toGrab[i].length; j++) {
    				if(toGrab[i][j][3] == false){
						nextID = toGrab[i][j][0];
						nextPartNum = toGrab[i][j][1];
						nextRev = toGrab[i][j][2];

						toGrab[i][j][3] = true;
						nextPartFound = true;
						break;
					}
    			}

    			if(nextPartFound){
    				break;
    			}
    		}

    		if(!nextPartFound){
    			getPartsOnHand();
    			return;
    		}

    		$("#loading-status").html("Getting children of "+nextPartNum+"...");

    		$.ajax({
	          url: "inc/methodParts.php",
	          type: 'POST',
	          async: true,
	          data: {
	          	id: nextID,
	            partNum: nextPartNum,
	        	rev: nextRev},
	           success: function(data) {
	              if(!isJson(data)){
	                return;
	              }

	              var result = $.parseJSON(data);

	              var parsedParts = [];
	              result["parts"].forEach(function(item, index, arr){
	              	if(!ignoredParts.includes(item["MtlPartNum"].toUpperCase().trim())){
	              		parsedParts.push(item);
	              	}
	              });
	              result["parts"] = parsedParts;

	              if(partList[result["id"]] === undefined){
		    	  	partList[result["id"]] = [];	
		    	  }
	              partList[result["id"]].push(result);

	              result["parts"].forEach(function(item, index, arr){
	              	//console.log("ID: "+result["id"]+" Part: "+item["MtlPartNum"]+" Parent: "+item["PartNum"]);

	              	if(!allPartNums.includes(item["MtlPartNum"])){
	              		allPartNums.push(item["MtlPartNum"]);
	              	}

	              	if(item["MtlRevisionNum"].length > 0){
	              		var newID = parseInt(result["id"])+1;
	              		addToGrabList(newID,item["MtlPartNum"],item["MtlRevisionNum"]);
	              	}
	              });

	              grabNextPart();
	            }
	        });
    	}

    	function addToGrabList(id,partNum,rev){
    		if(toGrab[id] === undefined){
    			toGrab[id] = [];
    		}

    		toGrab[id].push([id,partNum,rev,false]);
    	}

    	function isJson(str) {
          try {
              JSON.parse(str);
          } catch (e) {
              return false;
          }
          return true;
      	}

      	function getPartsOnHand(){
      		$("#loading-status").html("Getting parts on hand...");

      		//Populate OnHandQty with 0 for every part in part list
      		partList.forEach(function(item, index, arr){
      			var id = index;
      			item.forEach(function(item, index, arr){
      				var parentGroup = index;
      				partList[id][parentGroup]["parts"].forEach(function(item, index, arr){
      					partList[id][parentGroup]["parts"][index]["OnHandQty"] = 0;
      					partList[id][parentGroup]["parts"][index]["OnHandQtyUom"] = partList[id][parentGroup]["parts"][index]["MtlPartNumIUM"];
      				});
      			});
      		});
      		
      		//Add real OnHandQty to the parts that have it
      		var onHandAjaxCount = 0;
      		var onHandAjaxCountCompleted = 0;
      		partList.forEach(function(item, index, arr){
      			var id = index;

				item.forEach(function(item, index, arr){
					var parentGroup = index;
					var partNumbers = [];
					item["parts"].forEach(function(item, index, arr){
						partNumbers.push(item["MtlPartNum"]);
					});

					if(partNumbers.length > 0){
						onHandAjaxCount++;
						$.ajax({
				          url: "inc/onHandQty.php",
				          type: 'POST',
				          async: true,
				          data: {
				          	partNums: partNumbers},
				           success: function(data) {
				           	  onHandAjaxCountCompleted++;
				           	  $("#loading-status").html("Getting parts on hand "+onHandAjaxCountCompleted+"...");

				              if(!isJson(data)){
				                return;
				              }

				              var result = $.parseJSON(data);
				              result["result"]["returnObj"]["Results"].forEach(function(item, index, arr){
				              	if(allowedWarehouses.includes(item["PartBin_WarehouseCode"])){
				              		addOnHandQtyToPartList(id,parentGroup,item["PartBin_PartNum"],item["PartBin_OnhandQty"],item["Part_IUM"]);
				              	}
				              });

				              if(onHandAjaxCount == onHandAjaxCountCompleted){
				              	convertUOMs();
				              }
				            }
				        });
					}

				});
			});
      	}

      	function addOnHandQtyToPartList(id,parentGroup,partNum,qty,qty_uom){
      		partList[id][parentGroup]["parts"].forEach(function(item, index, arr){
      			if(item["MtlPartNum"].toUpperCase().trim() == partNum.toUpperCase().trim()){
      				partList[id][parentGroup]["parts"][index]["OnHandQty"] += qty;
      				partList[id][parentGroup]["parts"][index]["OnHandQtyUom"] = qty_uom;
      			}
      		});
      	}

      	function convertUOMs(){
      		$("#loading-status").html("Converting UOMS...");
      		var uomAjaxCount = 0;
      		var uomAjaxCountCompleted = 0;

      		partList.forEach(function(item, index, arr){
      			var id = index;
      			item.forEach(function(item, index, arr){
      				var parentGroup = index;
      				partList[id][parentGroup]["parts"].forEach(function(item, index, arr){
      					if(item["OnHandQtyUom"] != item["UOMCode"]){
      						uomAjaxCount++;
	      					$.ajax({
					          url: "inc/convertUOM.php",
					          type: 'POST',
					          async: true,
					          data: {
					          	startUOM: item["OnHandQtyUom"],
					            startQTY: item["OnHandQty"],
					        	toUOM: item["UOMCode"],
					        	id: id,
					        	parentGroup: parentGroup},
					           success: function(data) {
					           	  uomAjaxCountCompleted++;
					           	  $("#loading-status").html("Converting UOMS "+uomAjaxCountCompleted+"...");

					              if(!isJson(data)){
					                return;
					              }

					              var result = $.parseJSON(data);
					              partList[id][parentGroup]["parts"][index]["OnHandQty"] = parseFloat(result["qty"]);
					              partList[id][parentGroup]["parts"][index]["OnHandQtyUom"] = result["uom"];

					              if(uomAjaxCount == uomAjaxCountCompleted){
					              	calculateMakeableQty();
					              }
					            }
					        });

      					}
      				});
      			});
      		});

      		if(uomAjaxCount == 0){
      			calculateMakeableQty();
      		}
      	}

      	function calculateMakeableQty(){
      		$("#loading-status").html("Calculating...");
      		partList.forEach(function(item, index, arr){
      			var id = index;
      			item.forEach(function(item, index, arr){
      				var parentGroup = index;

      				if(partList[id][parentGroup]["parts"].length < 1){
      					partList[id][parentGroup]["MakeableQty"] = 0;
      				} else {
      					partList[id][parentGroup]["MakeableQty"] = partList[id][parentGroup]["parts"][0]["OnHandQty"]/partList[id][parentGroup]["parts"][0]["QtyPer"];
      					if(partList[id][parentGroup]["MakeableQty"] < 0){
      						partList[id][parentGroup]["MakeableQty"] = 0;
      					}

      					partList[id][parentGroup]["Parent"] = partList[id][parentGroup]["parts"][0]["PartNum"];
      					partList[id][parentGroup]["ParentDesc"] = partList[id][parentGroup]["parts"][0]["PartNumPartDescription"];
      				}

      				partList[id][parentGroup]["parts"].forEach(function(item, index, arr){

      					var makeableQty = item["OnHandQty"]/item["QtyPer"];
      					if(makeableQty < 0){
      						makeableQty = 0;
      					}

      					partList[id][parentGroup]["parts"][index]["MakeableQty"] = makeableQty;
      					partList[id][parentGroup]["parts"][index]["SubMakeableQty"] = 0;

      					if(makeableQty < partList[id][parentGroup]["MakeableQty"]){
      						partList[id][parentGroup]["MakeableQty"] = makeableQty;
      					}
      				});
      			});
      		});

      		//Update parents backwards
      		for (var i = partList.length - 1; i >= 0; i--) {
      			var id = i;
      			partList[i].forEach(function(item, index, arr){
      				var parentGroup = index;
      				if(id > 0){
      					setMakeableQty(id-1,partList[id][parentGroup]["Parent"],partList[id][parentGroup]["MakeableQty"]);
      				}
      			});
      		}

      		partList.forEach(function(item, index, arr){
      			var id = index;
      			item.forEach(function(item, index, arr){
      				var parentGroup = index;
      				
      				if(partList[id][parentGroup]["parts"].length == 0){
      					partList[id][parentGroup]["MakeableQty"] = 0;
      				} else {
      					partList[id][parentGroup]["MakeableQty"] = (partList[id][parentGroup]["parts"][0]["MakeableQty"]+partList[id][parentGroup]["parts"][0]["SubMakeableQty"]);
      				}

      				partList[id][parentGroup]["parts"].forEach(function(item, index, arr){
      					if((item["MakeableQty"]+item["SubMakeableQty"]) < partList[id][parentGroup]["MakeableQty"]){
      						partList[id][parentGroup]["MakeableQty"] = (item["MakeableQty"]+item["SubMakeableQty"]);

      						if(id-1 >= 0){
      							setMakeableQty(id-1,partList[id][parentGroup]["Parent"],partList[id][parentGroup]["MakeableQty"]);
      						}
      					}
      				});
      			});
      		}); 

      		populateMaterialParents();
      	}

      	function setMakeableQty(id,partNum,qty){
  			partList[id].forEach(function(item, index, arr){
  				var parentGroup = index;
  				partList[id][parentGroup]["parts"].forEach(function(item, index, arr){
  					if(item["MtlPartNum"] == partNum){
  						partList[id][parentGroup]["parts"][index]["SubMakeableQty"] = qty;
  					}
  				});
  			});
      	}

      	function populateMaterialParents(){
      		partList.forEach(function(item, index, arr){
      			var id = index;
      			item.forEach(function(item, index, arr){
      				var parentGroup = index;
      				partList[id][parentGroup]["parts"].forEach(function(item, index, arr){
      					if(materialParents[item["MtlPartNum"]] === undefined){
      						materialParents[item["MtlPartNum"]] = [];
      					}

      					if(!materialParents[item["MtlPartNum"]].includes(item["PartNum"])){
      						materialParents[item["MtlPartNum"]].push(item["PartNum"]);
      					}

      				});
      			});
      		});

      		partList.forEach(function(item, index, arr){
      			var id = index;
      			item.forEach(function(item, index, arr){
      				var parentGroup = index;
      				partList[id][parentGroup]["parts"].forEach(function(item, index, arr){
      					if(materialParents[item["MtlPartNum"]] === undefined){
      						partList[id][parentGroup]["parts"][index]["AllParents"] = [];
      					} else {
      						partList[id][parentGroup]["parts"][index]["AllParents"] = materialParents[item["MtlPartNum"]];
      					}
      				});
      			});
      		});

      		populateMaterialQtyReq();
      	}

      	function populateMaterialQtyReq(){
      		partList.forEach(function(item, index, arr){
      			var id = index;
      			item.forEach(function(item, index, arr){
      				var parentGroup = index;
      				partList[id][parentGroup]["parts"].forEach(function(item, index, arr){
      					if(materialQtyReq[item["MtlPartNum"]] === undefined){
      						materialQtyReq[item["MtlPartNum"]] = 0;
      					}

      					materialQtyReq[item["MtlPartNum"]] += parseFloat(item["QtyPer"]);
      				});
      			});
      		});

      		partList.forEach(function(item, index, arr){
      			var id = index;
      			item.forEach(function(item, index, arr){
      				var parentGroup = index;
      				partList[id][parentGroup]["parts"].forEach(function(item, index, arr){
      					if(materialQtyReq[item["MtlPartNum"]] === undefined){
      						partList[id][parentGroup]["parts"][index]["QtyPerTotal"] = 0;
      					} else {
      						partList[id][parentGroup]["parts"][index]["QtyPerTotal"] = materialQtyReq[item["MtlPartNum"]];
      					}
      				});
      			});
      		});

      		makeTable();
      	}

      	function makeTable(){
      		$("#loading-status").html("Making Tables...");
      		partList.forEach(function(item, index, arr){
      			var id = index;
      			item.forEach(function(item, index, arr){
      				var parentGroup = index;
      				makeTableSection(partList[id][parentGroup]);
      			});
      		});

      		$("#loading-alert").hide();
      		console.log(partList);
      	}

      	function makeTableSection(partlistSection){

      		if(partlistSection["parts"] === 'undefined' || partlistSection["parts"].length < 1){
      			return;
      		}

      		var html = "<table class='table'>";

      		html += "<thead>";
      		html += "<tr class='dark-grey-bg'><th colspan='4' class='table-header'>"+partlistSection["Parent"]+"</th></tr>";
      		html += "<tr class='dark-grey-bg'><th colspan='4' class='table-header'>"+escapeHtml(partlistSection["ParentDesc"])+"</th></tr>";

      		if(Math.floor(partlistSection["MakeableQty"]) <= 0){
      			html += "<tr class='dark-grey-bg thick-bottom-border'><th colspan='4' class='table-header red-text'>Can Make "+Math.floor(partlistSection["MakeableQty"])+"</th></tr>";
      		} else {
      			html += "<tr class='dark-grey-bg thick-bottom-border'><th colspan='4' class='table-header'>Can Make "+Math.floor(partlistSection["MakeableQty"])+"</th></tr>";
      		}
      		
      		html += "<tr class='dark-grey-bg thick-bottom-border'>";
      		html += "<th class='table-row-header'>Part</th>";
      		html += "<th class='table-row-header'>Qty Needed</th>";
      		html += "<th class='table-row-header'>Qty On Hand</th>";
      		html += "<th class='table-row-header'>Can Make</th>";
      		html += "</tr";
      		html += "</thead>";

      		html += "<tbody>";
      		partlistSection["parts"].forEach(function(item, index, arr){
      			html += "<tr class='light-grey-bg'>";

      			html += "<td class='table-td bold-text'><a href='partFind.php?part="+encodeURIComponent(item["MtlPartNum"])+"' target='_blank'>"+escapeHtml(item["MtlPartNum"])+"</a></td>";

	      		html += "<td class='table-td'>"+item["QtyPer"]+" "+item["UOMCode"]+"</td>";

	      		if(item["OnHandQty"] < 0){
	      			html += "<td class='table-td red-text'>"+item["OnHandQty"].toFixed(2)+" "+item["OnHandQtyUom"]+"</td>";
	      		} else {
	      			html += "<td class='table-td'>"+item["OnHandQty"].toFixed(2)+" "+item["OnHandQtyUom"]+"</td>";
	      		}

	      		if(Math.floor(item["MakeableQty"]+item["SubMakeableQty"]) <= 0){
	      			html += "<td class='table-td red-text'>"+Math.floor(item["MakeableQty"]+item["SubMakeableQty"])+"</td>";
	      		} else {
	      			html += "<td class='table-td'>"+Math.floor(item["MakeableQty"]+item["SubMakeableQty"])+"</td>";
	      		}
	      		
	      		html += "</tr>";
	      		html += "<tr>";
	      		html += "<td colspan='4' class='table-td'>"+escapeHtml(item["MtlPartNumPartDescription"])+"</td>";
	      		html += "</tr>";

	      		if(item["AllParents"].length > 1){
	      			html += "</tr>";
		      		html += "<tr>";
		      		if(item["OnHandQty"] < item["QtyPerTotal"]){
		      			html += "<td colspan='4' class='table-td red-text'>Material is shared across multiple assemblies, you DO NOT have enough on hand quantity to make all assemblies.</td>";
		      		} else {
		      			html += "<td colspan='4' class='table-td'>Material is shared across multiple assemblies.</td>";
		      		}
		      		html += "</tr>";

		      		html += "<tr>";
	      			html += "<td colspan='4' class='bottom-border'>";
	      			item["AllParents"].forEach(function(item, index, arr){
	      				html += escapeHtml(item)+" - ";
	      			});
	      			html = html.substring(0, html.length - 3);
	      			html += "</td>";
	      			html += "</tr>";
	      		} else {
	      			html += "</tr>";
		      		html += "<tr>";
		      		html += "<td colspan='4' class='table-td bottom-border'>&nbsp;</td>";
		      		html += "</tr>";
	      		}
      		});
      		html += "</tbody>";

      		html += "</table>";
      		$("#table-container").append(html);
      	}

      	function escapeHtml(string) {
			return String(string).replace(/[&<>"'`=\/]/g, function (s) {
				return entityMap[s];
			});
		}

    </script>
  </body>
</html>