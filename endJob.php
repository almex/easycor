<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}

//In Office
if($_SERVER['REMOTE_ADDR'] != "72.38.180.229" && $_SERVER['REMOTE_ADDR'] != "72.38.180.226" && $_SERVER['REMOTE_ADDR'] != "72.38.196.34" && $_SERVER['REMOTE_ADDR'] != "::1"){
	header("Location: index.php?endJobWarning=1");
	die();
}

//Clocked in check
$checkClockInStatus = $api->checkClockInStatus($_SESSION["empNum"]);
$clockedIn = $checkClockInStatus["result"]->parameters->clockIn == 1;
if(!$clockedIn){
	header("Location: index.php?endJobWarning=2");
	die();
}

//User is clocked into job check
$activeJobResult = $api->getActiveProductionActivityDS($_SESSION["empNum"]);
if(empty($activeJobResult["result"]->returnObj->LaborDtl)){
	header("Location: index.php?endJobWarning=3");
	die();
}

if(!isset($_SESSION["endJob"])){
	$_SESSION["endJob"] = array();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function updateDS($result){
	$_SESSION["endJob"]["ds"] = $result["result"]->parameters->ds;
}

function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function stopActivity($laborQty,$complete,$laborDtlSeq,$api){

	$result = $api->endProductionActivity($laborDtlSeq,$_SESSION["endJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);

	$result = $api->setProductionQty($laborQty,$_SESSION["endJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);

	if(!$complete){
		$result = $api->endProductionActivityNotComplete($_SESSION["endJob"]["ds"]);
		if(hasError($result)){
			return false;
		}
		updateDS($result);
	}

	$result = $api->checkForProductionActivityWarnings($_SESSION["endJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);

	$dtlCount = sizeof($_SESSION["endJob"]["ds"]->LaborDtl);
	if($dtlCount > 0){
		$_SESSION["endJob"]["ds"]->LaborDtl[$dtlCount-1]->RowMod = "U";
	}

	$result = $api->updateProductionActivity($_SESSION["endJob"]["ds"]);

	//Check if user is still on job
	$activeJobResult = $api->getActiveProductionActivityDS($_SESSION["empNum"]);
	if(hasError($activeJobResult)){
		return false;
	}

	$jobStillExists = false;
	foreach ($activeJobResult["result"]->returnObj->LaborDtl as $dtl) {
		if($dtl->LaborDtlSeq == $laborDtlSeq){
			$jobStillExists = true;
		}
	}

	if($jobStillExists){
		return false;
	} else {
		return true;
	}
}


//Step one submitted
$activityStopped = false;
if(isset($_POST["laborDtlSeq"]) && isset($_POST["qty"])){
	$prodActiResult = $api->getProductionDataset(
		$activeJobResult["result"]->returnObj->LaborHed[0]->LaborHedSeq,
		$_POST["laborDtlSeq"]);

	$_SESSION["endJob"]["ds"] = $prodActiResult["result"]->returnObj;
	
	if(stopActivity($_POST["qty"],isset($_POST["completed"]),$_POST["laborDtlSeq"],$api)){
		$step = 2;
		$activityStopped = true;
	} else {
		$step = 2;
		$activityStopped = false;
	}
}


?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { 
				foreach ($activeJobResult["result"]->returnObj->LaborDtl as $dt1) {
			?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Job #<?php echo $dt1->JobNum; ?></b>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<li class="list-group-item"><b>Clocked in:</b> <?php echo explode("T", $dt1->ClockInDate, 2)[0]." ".$dt1->DspClockInTime; ?></li>
							<li class="list-group-item"><b>Operation:</b> <?php echo $dt1->OpDescOpDesc; ?></li>
							<li class="list-group-item"><b>Resource ID:</b> <?php echo $dt1->ResourceID; ?></li>
							<li class="list-group-item"><b>Resource Group:</b> <?php echo $dt1->ResourceGrpID; ?></li>
							<li class="list-group-item"><b>Part:</b> <?php echo $dt1->JobAsmblPartNum; ?><br><br><?php echo $dt1->JobAsmblDescription; ?></li>
						</ul>
						<center>
						<br><br>
						<form action="endJob.php" method="POST">
						  <div class="mb-3" style="width:75%">
						    <label for="qty" class="form-label">Qty:</label>
						    <input id="qty" type="number" class="form-control" autocomplete="off" name="qty" value="0">
						  </div>
						  <div class="form-check" style="width:110px;">
							  <input class="form-check-input" type="checkbox" value="1" name="completed">
							  <label class="form-check-label" for="flexCheckDefault">
							    Completed
							  </label>
							</div>
						  <input type="hidden" name="laborDtlSeq" value="<?php echo $dt1->LaborDtlSeq; ?>">
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Clock out</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } }  ?>
				
				
			<!-- Step Two -->
				<?php if($step == 2) {  ?>
					<center>
					<div class="card" style="margin:20px;max-width:500px;">
						
						<center>
						<?php if($activityStopped) { ?>
							<img src="img/check.png" class="card-img-top" style="margin:10px;max-width:100px;max-height:100px;">
						<?php } else { ?>
							<img src="img/cross.png" class="card-img-top" style="margin:10px;max-width:100px;max-height:100px;">
						<?php } ?>
						</center>
						
						<div class="card-body">
							
							<?php if($activityStopped) { ?>
								<p class="card-text"><b><center>You clocked out of the job</center></b></p>
							<?php } else { ?>
								<p class="card-text"><b><center>An error occurred and you were not clocked out of the job</center></b></p>
							<?php } ?>
							<br>
							
							<center><a href="index.php" class="btn btn-primary"><b><center>Home</center></b></a></center>
						</div>
					</div>
					</center>
				<?php } ?>
				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>