<?php
ini_set('session.gc_maxlifetime', 14400);
session_set_cookie_params(14400);
session_start();
include_once 'inc/api.php';

$loginFailed = false;
$isThisYou = false;

//Attempting to log out
if(isset($_POST["logout"])){
	if(isset($_SESSION["key"])){
		unset($_SESSION["key"]);
	}
	
	if(isset($_SESSION["empNum"])){
		unset($_SESSION["empNum"]);
	}

	if(isset($_SESSION["pilot"])){
		unset($_SESSION["pilot"]);
	}
	
	if(isset($_SESSION["clockInStatusResult"])){
		unset($_SESSION["clockInStatusResult"]);
	}
	
	session_unset();
	session_destroy();
}

//Attempting to log in normally
if(isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["enum"]) && isset($_POST["pilot"])){
	$_SESSION["key"] = base64_encode($_POST["username"].":".$_POST["password"]);
	$_SESSION["empNum"] = $_POST["enum"];
	$_SESSION["pilot"] = $_POST["pilot"];
	
	$api = new API();
	$result = $api->curl("Erp.BO.EmpBasicSvc/CheckClockInStatus",array( "empID" => strval($_SESSION["empNum"]) ));
	
	if($result["http"] == 200){
		$_SESSION["clockInStatusResult"] = $result["result"]->returnObj->EmpBasic[0];
		$isThisYou = true;
	} else {
		//Login failed
		unset($_SESSION["key"]);
		unset($_SESSION["empNum"]);
		unset($_SESSION["pilot"]);
		$loginFailed = true;
	}
}

//Attempting to log in with local storage
if(isset($_POST["localKey"]) && isset($_POST["enum"]) && isset($_POST["pilot"])){
	$_SESSION["key"] = $_POST["localKey"];
	$_SESSION["empNum"] = $_POST["enum"];
	$_SESSION["pilot"] = $_POST["pilot"];
	
	$api = new API();
	$result = $api->curl("Erp.BO.EmpBasicSvc/CheckClockInStatus",array( "empID" => strval($_SESSION["empNum"]) ));
	
	if($result["http"] == 200){
		$_SESSION["clockInStatusResult"] = $result["result"]->returnObj->EmpBasic[0];
		header("Location: index.php");
		die();
	} else {
		//Login failed
		unset($_SESSION["key"]);
		unset($_SESSION["empNum"]);
		unset($_SESSION["pilot"]);
		$loginFailed = true;
	}
}

?>
<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
  
  <!-- LOGIN FORM -->
  <center>
	<img src="img/epicor_logo.png" style="max-width:350px;margin-top:30px;margin-bottom:30px;">
	
	<?php if($loginFailed) { ?>
	<br>
	<div class="alert alert-danger" role="alert" style="max-width:350px;">
	  <b> Login failed! </b>
	</div><br>
	<?php } ?>
	
	
	<?php if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) { ?>
		<div id="loadingWarn"><center><span> <b> Loading... </b> </span></center></div>
		<div id="localStorageLogInWarn" style="display:none;"><center><span> <b> Logging In... </b> </span></center></div>
		<form id="loginForm" style="max-width:350px;display:none;" action="login.php" method="POST">
		  <div class="mb-3">
			<label for="username" class="form-label"><b>Username:</b></label>
			<input class="form-control" autocomplete="off" name="username"
			<?php 
				if(isset($_POST["username"])){
					echo ' value="'.$_POST["username"].'"';
				}
			?>
			>
		  </div>
		  <div class="mb-3">
			<label for="pass" class="form-label"><b>Password:</b></label>
			<input type="password" autocomplete="off" class="form-control" name="password">
		  </div>
		  <div class="mb-3">
			<label for="enum" class="form-label"><b>Employee Number:</b></label>
			<input autocomplete="off" class="form-control" name="enum"
			<?php 
				if(isset($_POST["enum"])){
					echo ' value="'.$_POST["enum"].'"';
				}
			?>
			>
		  </div>
		  <div class="mb-3">
		  	<label for="pilot" class="form-label"><b>Type:</b></label>
		  	<select class="form-select" aria-label="Default select example" name="pilot">
			  <option value="0" selected>Production</option>
			  <option value="1">Pilot</option>
			</select>
		  </div>
		  <button type="submit" class="btn btn-primary"><b>Login</b></button>
		</form>
	<?php } else if($isThisYou) { ?>
		<form style="max-width:350px;" action="login.php" method="POST">
		  <span> <b> Is this you? </b> </span><br><br>
		  <input class="form-control" style="background-color:#d2d2d2;font-weight: bold;" type="text" placeholder="<?php echo $_SESSION["clockInStatusResult"]->Name; ?>" readonly>
		  <input type="hidden" name="logout" value="1">
		  <br><br>
		  <a href="login.php?save=1" class="btn btn-primary"><b>Yes</b></a>
		  <button type="submit" class="btn btn-primary"><b>No</b></button>
		</form>
	<?php } else if(isset($_GET["save"])) { ?>
		<center><span> <b> Loading... </b> </span></center>
	<?php } else { ?>
		<form style="max-width:350px;" action="login.php" method="POST">
		   <span> <b> Logged in as </b> </span><br><br>
		  <input class="form-control" type="text" style="background-color:#d2d2d2;font-weight: bold;" placeholder="<?php echo $_SESSION["clockInStatusResult"]->Name; ?>" readonly>
		  <input type="hidden" name="logout" value="1">
		  <br><br>
		  <button type="submit" class="btn btn-primary"><b>Logout</b></button>
		</form>
		<br>
		<a href="index.php" class="btn btn-primary"><b>Home</b></a>
	<?php } ?>
	
	
	</center>

	<!-- local storage -->
	<?php if(isset($_SESSION["key"]) && isset($_SESSION["empNum"]) && isset($_SESSION["empNum"])){ ?>
		<input id="loggedIn" type="hidden" value="1">
	<?php } else { ?>
		<input id="loggedIn" type="hidden" value="0">
	<?php } ?>

	<?php if($loginFailed || isset($_POST["logout"])){ ?>
		<input id="clearLocalStorage" type="hidden" value="1">
	<?php } else { ?>
		<input id="clearLocalStorage" type="hidden" value="0">
	<?php } ?>

	<?php if(isset($_SESSION["key"]) && isset($_GET["save"])){ ?>
		<input id="localStorageKey" type="hidden" value="<?php echo $_SESSION["key"]; ?>">
	<?php } else { ?>
		<input id="localStorageKey" type="hidden" value="">
	<?php } ?>

	<?php if(isset($_SESSION["empNum"]) && isset($_GET["save"])){ ?>
		<input id="localStorageEmpNum" type="hidden" value="<?php echo $_SESSION["empNum"]; ?>">
	<?php } else { ?>
		<input id="localStorageKey" type="hidden" value="">
	<?php } ?>

	<?php if(isset($_SESSION["pilot"]) && isset($_GET["save"])){ ?>
		<input id="localStoragePilot" type="hidden" value="<?php echo $_SESSION["pilot"]; ?>">
	<?php } else { ?>
		<input id="localStoragePilot" type="hidden" value="0">
	<?php } ?>

	<form id="localStorageLoginForm" style="max-width:350px;" action="login.php" method="POST" style="display: none;">
		<input id="localKeyForm" name="localKey" type="hidden" value="">
		<input id="empNumForm" name="enum" type="hidden" value="">
		<input id="pilotForm" name="pilot" type="hidden" value="">
	</form>

    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script language="JavaScript" type="text/javascript">
		$(document).ready(function() {

			//Clear local storage
			if($("#clearLocalStorage").val() == 1){
				localStorage.removeItem('localKey');
				localStorage.removeItem('empNum');
				localStorage.removeItem('pilot');
			}

			//Saving login
			if($("#localStorageKey").val() != "" && $("#localStorageEmpNum").val() != ""){
		    	localStorage.setItem('localKey', $("#localStorageKey").val());
		    	localStorage.setItem('empNum', $("#localStorageEmpNum").val());
		    	localStorage.setItem('pilot', $("#localStoragePilot").val());
		    	window.location.href = "index.php";
			}

			//Local storage login
			if($("#loggedIn").val() == 0 && localStorage.getItem('localKey') !== null && localStorage.getItem('empNum') !== null){
				$("#loginForm").hide();
				$("#loadingWarn").hide();
				$("#localStorageLogInWarn").show();

				$("#localKeyForm").val(localStorage.getItem('localKey'));
				$("#empNumForm").val(localStorage.getItem('empNum'));

				if(localStorage.getItem('pilot') !== null){
					$("#pilotForm").val(localStorage.getItem('pilot'));
				} else {
					$("#pilotForm").val("0")
				}

				$('#localStorageLoginForm').submit();
			} else {
				$("#loginForm").show();
				$("#loadingWarn").hide();
				$("#localStorageLogInWarn").hide();
			}
		});
    </script>

  </body>
</html>