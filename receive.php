<?php
session_start();

include_once 'inc/api.php';
include_once 'pdf/receivePDF.php';
include_once 'inc/email.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

//Step one submitted
if(isset($_POST["poNum"]) && isset($_POST["base64PS"])){
	
	$poResult = $api->getPOInfo($_POST["poNum"]);
	$_SESSION["receive"]["result"] = $poResult["result"];
	$_SESSION["receive"]["base64PS"] = $_POST["base64PS"];

	if(hasError($poResult)){
		$error = true;
		$errorText = "Unable to get PO information";
	} else {
		$step = 2;
	}
}

//Step two submitted
if(isset($_POST["step2form"])){
	$step = 3;
	$_SESSION["receive"]["fileName"] = $_SESSION["empNum"]."-".time();
	$_SESSION["receive"]["name"] = $_POST["name"];
	$_SESSION["receive"]["poNum"] = $_POST["poNum2"];
	makePDF($_SESSION["receive"]["fileName"]);
}

//Send Emails
$emailsSent = false;
if(isset($_POST["email1"]) && isset($_POST["email2"]) && isset($_POST["link"])){
	$message = $_SESSION["receive"]["name"]." has sent you a PDF. <br><br> <a href='".$_POST["link"]."'>VIEW PDF</a>";
	$email1Result = sendEmail($_POST["email1"],"PO#".$_SESSION["receive"]["poNum"],$message);
	$email2Result = sendEmail($_POST["email2"],"PO#".$_SESSION["receive"]["poNum"],$message);

	if($email1Result && $email2Result){
		$emailsSent = true;
	}
	$step = 4;
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
  	<canvas style="display:none;" id="canvas" width=64 height=64></canvas>
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Receive PO</b>
					</div>
					<div class="card-body">
						<center>
						<form action="receive.php" method="POST">
							<br>
						  <div class="mb-3">
							<input class="form-control" placeholder="PO Number" autocomplete="off" name="poNum">
						  </div>
						  <br>
						  <input type="button" id="loadPS" class="btn btn-primary" style="font-weight: bold;" value="Add packing slip" onclick="document.getElementById('file').click();" />
						  <input type="file" style="display:none;" accept="image/*" id="file" name="packingSlip"/>
						  <input type="hidden" id="base64PS" name="base64PS" value="">
						  <br><br>
						  <button id="continueBTN" type="submit" class="btn btn-primary" disabled><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
				
			<!-- Step Two -->
				<?php if($step == 2) { ?>


					<center>

							<!-- header -->
							<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
							  <div class="card-body">
							    <h5 class="card-title"><?php echo $_SESSION["receive"]["result"]->returnObj->POHeader[0]->PONum; ?></h5>
							    <p class="card-text"><?php echo $_SESSION["receive"]["result"]->returnObj->POHeader[0]->VendorName; ?></p>
							    <p class="card-text">Ordered on <?php echo explode("T", $_SESSION["receive"]["result"]->returnObj->POHeader[0]->OrderDate, 2)[0]; ?></p>
							  </div>
							</div>


							<!-- item list -->
							<?php foreach ($_SESSION["receive"]["result"]->returnObj->PODetail as $poDetail) {  
									$partInfoResult = $api->getPartInfo($poDetail->PartNum); 

									//Get plants
									$plants = array();
									if($partInfoResult["http"] == 200){
										foreach ($partInfoResult["result"]->returnObj->PartPlant as $partPlant) {
											$plants[$partPlant->Plant] = $partPlant->PlantName;
										}
									}
									 ?>

									<!-- PO Details -->
									<div class="card" style="margin:20px;max-width:500px;">
										<div class="card-header" style="font-size: 1.4rem;">
											<b><?php echo $poDetail->PartNum; ?></b>
										</div>
										<ul class="list-group list-group-flush">
											<li class="list-group-item"><b>Vendor Part: </b> <?php echo $poDetail->VenPartNum; ?></li>
											<li class="list-group-item"><b>Mfg Part: </b> <?php echo $poDetail->MfgPartNum; ?></li>
											<li class="list-group-item"><b>Part Desc: </b> <?php echo $poDetail->LineDesc; ?></li>
										</ul>

										<!-- Primary bins -->
										<br>
										<center>
										<select set="<?php echo $poDetail->POLine; ?>bin2" class="bin-select form-select form-select-lg mb-3" aria-label=".form-select-lg example" style="width:90%;">
											<option selected>Select bin</option>
										<?php foreach ($partInfoResult["result"]->returnObj->PartWhse as $partWhse) { 
											if(!empty($partWhse->PrimBinNum)){ ?>

												<option value="<?php echo $partWhse->WarehouseDescription." Bin ".$partWhse->PrimBinNum; ?>"><?php echo $partWhse->WarehouseDescription." in Bin ".$partWhse->PrimBinNum; ?></option>

										<?php } } ?>
										</select>
										</center>

										<!-- bin -->
										<center>
										<div class="input-group mb-3" style="width:90%;">
										  <input type="text" id="<?php echo $poDetail->POLine; ?>bin2" set="<?php echo $poDetail->POLine; ?>bin" class="bin-set form-control" placeholder="Bin" aria-label="Bin" aria-describedby="basic-addon2">
										</div>
										</center>

										<!-- qty -->
										<center>
										<div class="input-group mb-3" style="width:90%;">
										  <input type="number" set="<?php echo $poDetail->POLine; ?>qty" class="qty-set form-control" placeholder="Qty received" aria-label="Qty received" aria-describedby="basic-addon2">
										  <span class="input-group-text" id="basic-addon2"> of <?php echo $poDetail->OrderQty; ?></span>
										</div>
										</center>

									</div>

							<?php } ?>

							<!-- form -->
							<center>
							<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
								<form action="receive.php" method="POST">
									<input type="hidden" id="step2form" name="step2form" value="1">
									<input type="hidden" id="poNum2" name="poNum2" value="<?php echo $_SESSION["receive"]["result"]->returnObj->POHeader[0]->PONum; ?>">

									<?php foreach ($_SESSION["receive"]["result"]->returnObj->PODetail as $poDetail) {  ?>
										<input type="hidden" id="<?php echo $poDetail->POLine; ?>num" name="<?php echo $poDetail->POLine; ?>num" value="<?php echo $poDetail->PartNum; ?>">
										<input type="hidden" id="<?php echo $poDetail->POLine; ?>bin" name="<?php echo $poDetail->POLine; ?>bin" value="">
										<input type="hidden" id="<?php echo $poDetail->POLine; ?>qty" name="<?php echo $poDetail->POLine; ?>qty" value="">
									<?php } ?>

									<div class="mb-3" style="width: 90%; margin: 10px;">
									    <label for="name" class="form-label"><b>Your Name</b></label>
									    <input type="text" class="form-control" id="name" name="name" onkeypress="allSetCheck()">
									 </div>

									 <div class="mb-3" style="width: 90%; margin: 10px;">
									    <label for="name" class="form-label"><b>Comments</b></label>
									    <textarea type="text" class="form-control" id="comments" name="comments" rows="3"></textarea>
									 </div>

									<div class="mb-3" style="width:160px;margin:10px;">
									  <input class="form-check-input" type="checkbox" value="1" name="partial">
									  <label class="form-check-label" for="flexCheckDefault">
									    <b>is partial receipt</b>
									  </label>
									</div>


									<button id="continueBTN2" type="submit" class="btn btn-primary" style="margin:10px;"><b>Continue</b></button>

								</form>
							</div>
						</center>

					</center>

				<?php } ?>
				
			
				<!-- Step Three -->
				<?php if($step == 3) { ?>
						<center>
							<div class="card" style="margin:20px;max-width:500px;">
								<div class="card-body">
									<center>
										<img src="img/check.png" class="card-img-top" style="margin:10px;max-width:100px;max-height:100px;">
									</center>
									<br>
									<br>
									<a href="pdf/viewPDF.php?pdf=<?php echo $_SESSION["receive"]["fileName"]; ?>" target="_blank" class="btn btn-primary" style="min-width: 125px;"><b><center>PDF</center></b></a> &nbsp; &nbsp;
									<a href="index.php" class="btn btn-primary" style="min-width: 125px;"><b><center>HOME</center></b></a>
									<br>
								</div>

								<br>
								<form action="receive.php" method="POST">
									<div class="mb-3" style="width: 90%; margin: 10px;">
									    <label for="name" class="form-label"><b>Your Email:</b></label>
									    <input class="form-control" id="email1" name="email1" type="email">
									 </div>
									 <div class="mb-3" style="width: 90%; margin: 10px;">
									    <label for="name" class="form-label"><b>Their Email:</b></label>
									    <input class="form-control" id="email2" name="email2" type="email">
									 </div>
									 <input  class="form-control" name="link" type="hidden" value="https://easycor.dreamhosters.com/pdf/viewPDF.php?pdf=<?php echo $_SESSION["receive"]["fileName"];?>">

									<button type="submit" class="btn btn-primary" style="margin:10px;"><b>Send Emails</b></button><br>
								</form>
							</div>
						</center>
				<?php } ?>

				<!-- Step Four -->
				<?php if($step == 4) {  ?>
					<center>
					<div class="card" style="margin:20px;max-width:500px;">
						
						<center>
						<?php if($emailsSent) { ?>
							<img src="img/check.png" class="card-img-top" style="margin:10px;max-width:100px;max-height:100px;">
						<?php } else { ?>
							<img src="img/cross.png" class="card-img-top" style="margin:10px;max-width:100px;max-height:100px;">
						<?php } ?>
						</center>
						
						<div class="card-body">
							
							<?php if($emailsSent) { ?>
								<p class="card-text"><b><center>The emails are sent</center></b></p>
							<?php } else { ?>
								<p class="card-text"><b><center>Unable to send the emails</center></b></p>
							<?php } ?>
							<br>
							
							<center><a href="index.php" class="btn btn-primary"><b><center>Home</center></b></a></center>
						</div>
					</div>
					</center>
				<?php } ?>
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
    	var canvas=document.getElementById("canvas");
		var ctx=canvas.getContext("2d");
		var cw=canvas.width;
		var ch=canvas.height;
		var maxW=1500;
		var maxH=1500;
		var input = document.getElementById('file');

		if(input != null){
			input.addEventListener('change', handleFiles);
		}

    	$(".qty-set").on('change keydown paste input', function(){
    		setPartQty()
		});

		$(".bin-select").on('change keydown paste input', function(){
    		var x = $(this);
			var i;
			for (i = 0; i < x.length; i++) {
				if(x[i].selectedIndex != 0){
					document.getElementById(x[i].getAttribute("set")).value = x[i].value;
				} else {
					document.getElementById(x[i].getAttribute("set")).value = "";
				}
			}

			setPartBinFinal()
		});

		$(".bin-set").on('change keydown paste input', function(){
    		setPartBinFinal()
		});

		$("#email").on('change keydown paste input', function(){
		    allSetCheck()
		});

    	function setPartBin(){
			var x = document.getElementsByClassName("bin-select");
			var i;
			for (i = 0; i < x.length; i++) {
				if(x[i].selectedIndex != 0){
					document.getElementById(x[i].getAttribute("set")).value = x[i].value;
				} else {
					document.getElementById(x[i].getAttribute("set")).value = "";
				}
			}

			setPartBinFinal()
    	}

    	function setPartBinFinal(){
			var x = document.getElementsByClassName("bin-set");
			var i;
			for (i = 0; i < x.length; i++) {
				if(x[i].selectedIndex != 0){
					document.getElementById(x[i].getAttribute("set")).value = x[i].value;
				} else {
					document.getElementById(x[i].getAttribute("set")).value = "";
				}
			}

			allSetCheck();
    	}

    	function setPartQty(){
			var x = document.getElementsByClassName("qty-set");
			var i;
			for (i = 0; i < x.length; i++) {
				document.getElementById(x[i].getAttribute("set")).value = x[i].value;
			}

			allSetCheck();
    	}

    	function allSetCheck(){
    		var x = document.getElementsByClassName("bin-select");
			var i;
			for (i = 0; i < x.length; i++) {
				if(document.getElementById(x[i].getAttribute("set")).value.length == 0){
					//document.getElementById("continueBTN2").disabled = true;
					return;
				}
			}

			x = document.getElementsByClassName("qty-set");
			i;
			for (i = 0; i < x.length; i++) {
				if(document.getElementById(x[i].getAttribute("set")).value.length == 0){
					//document.getElementById("continueBTN2").disabled = true;
					return;
				}
			}

			if(document.getElementById("name").value.length == 0){
				//document.getElementById("continueBTN2").disabled = true;
					return;
			}

			document.getElementById("continueBTN2").disabled = false;
    	}

    	function handleFiles(e) {
		  var img = new Image;
		  img.onload = function() {
		    var iw=img.width;
		    var ih=img.height;
		    var scale=Math.min((maxW/iw),(maxH/ih));
		    var iwScaled=iw*scale;
		    var ihScaled=ih*scale;
		    canvas.width=iwScaled;
		    canvas.height=ihScaled;
		    ctx.drawImage(img,0,0,iwScaled,ihScaled);
			document.getElementById("base64PS").value = canvas.toDataURL("image/jpeg",0.5);
		    document.getElementById("loadPS").value = "Packing slip added";
		    document.getElementById("continueBTN").disabled = false;
		  }
		  img.src = URL.createObjectURL(e.target.files[0]);
		}
    </script>

  </body>
</html>