<?php
	session_start();
	unset($_SESSION["key"]);
	unset($_SESSION["empNum"]);
	unset($_SESSION["clockInStatusResult"]);
	session_unset();
	session_destroy();
?>