<?php
ini_set('session.gc_maxlifetime', 14400);
session_set_cookie_params(14400);
session_start();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
  $_SESSION["continue"] = $_SERVER['REQUEST_URI'];
  header("Location: login.php");
  die();
}

include_once 'inc/global.php';
include_once 'inc/api.php';
include_once 'inc/uomConv.php';
$api = new API();
$uomConv = new UOM();

$error = false;
$errorText = "";
$step = 1;

function makeCycleString($plant,$year,$month,$seq){
  return $plant."-".$year."-".$month."-".$seq;
}

function getComments($tagNum,$cycle){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare('SELECT * FROM comment WHERE TagNum=? AND Cycle =?');
  $ret = $qry->execute(array($tagNum,$cycle));
  return $qry->fetchAll();
}

if(isset($_GET["Cycle"])){
  $_GET["Plant"] = explode("-", $_GET["Cycle"])[0];
  $_GET["CCYear"] = explode("-", $_GET["Cycle"])[1];
  $_GET["CCMonth"] = explode("-", $_GET["Cycle"])[2];
  $_GET["CycleSeq"] = explode("-", $_GET["Cycle"])[3];
}

$tags = array();
if(isset($_GET["CCYear"]) && isset($_GET["CCMonth"]) && isset($_GET["CycleSeq"]) && isset($_GET["WarehouseCode"])){
  $tags = $api->getCountCycleTags($_GET["WarehouseCode"],$_GET["CCYear"],$_GET["CCMonth"],$_GET["CycleSeq"]);
  if($tags["http"] == 200){
    $step = 2;
  } else {
    $error = true;
    $errorText = "Unable to get tags";
  }
}

if($step == 2){
  $partNumbers = array();
  foreach ($tags["result"]->returnObj->CCTag as $tag) {
    if(strlen($tag->PartNum) > 0){
      array_push($partNumbers, $tag->PartNum);
    }
  }

  $step = 4;
}


if($step == 4){
  $totalVarianceCost = 0;

  foreach ($tags["result"]->returnObj->CCTag as $key => $tag) {
    $tags["result"]->returnObj->CCTag[$key]->dif = abs($tag->CountedQty - $tag->FrozenQOH);

    if($tag->UOM != $tag->PartNumIUM && $tags["result"]->returnObj->CCTag[$key]->dif > 0){
    	$convertedDif = $uomConv->convert($tag->UOM,$tags["result"]->returnObj->CCTag[$key]->dif,$tag->PartNumIUM);
    	$tags["result"]->returnObj->CCTag[$key]->difCost = $convertedDif*$tag->FrozenCost;
    } else {
    	$tags["result"]->returnObj->CCTag[$key]->difCost = $tags["result"]->returnObj->CCTag[$key]->dif*$tag->FrozenCost;
    }

    if($tag->CountedQty > $tag->FrozenQOH){
		$totalVarianceCost += $tags["result"]->returnObj->CCTag[$key]->difCost;
    } else {
    	$totalVarianceCost -= $tags["result"]->returnObj->CCTag[$key]->difCost;
    }

    $tags["result"]->returnObj->CCTag[$key]->difCost = number_format((float)$tags["result"]->returnObj->CCTag[$key]->difCost, 2, '.', '');
  }


  if(isset($_GET["SortByQty"])){
	usort($tags["result"]->returnObj->CCTag, function($a, $b) {
	      return $b->dif <=> $a->dif;
	 });
  } else {
  	usort($tags["result"]->returnObj->CCTag, function($a, $b) {
      return $b->difCost <=> $a->difCost;
  	});
  }

}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  

  <!-- Step Three -->
    <?php if($step == 4) { ?>
        <center>
        <br>
        <div class="card text-white bg-dark mb-3" style="max-width: 500px;">
          <div class="card-body">
            <h5 class="card-title">Variance Report 
              <br><br><?php echo "Count: ".makeCycleString($_GET["Plant"],$_GET["CCYear"],$_GET["CCMonth"],$_GET["CycleSeq"]);?>
              <br><br><?php echo "Warehouse: ".$_GET["WarehouseCode"]; ?>
              <br><br><br>This is NOT a replacement for the variance report from Epicor
            </h5>
          </div>
        </div>
        <br>
        <div class="alert alert-secondary" style="max-width: 500px;">
            <table>
            	<tr>
            		<td>
            			<a class="btn btn-primary" href="countCycleVariance.php?Cycle=<?php echo urlencode($_GET["Cycle"]);?>&WarehouseCode=<?php echo urlencode($_GET["WarehouseCode"]);?>" style="margin-right:5px;"><b>Sort by cost</b></a>
            		</td>
            		<td>
            			<a class="btn btn-primary" href="countCycleVariance.php?Cycle=<?php echo urlencode($_GET["Cycle"]);?>&WarehouseCode=<?php echo urlencode($_GET["WarehouseCode"]);?>&SortByQty" style="margin-left:5px;"><b>Sort by qty</b></a>
            		</td>
            	</tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" style="max-width: 600px;">
          <table class="table" style="font-size: 0.8rem;">
            <tbody>
              <?php foreach ($tags["result"]->returnObj->CCTag as $tag) {
              	  $comments = getComments($tag->TagNum,makeCycleString($_GET["Plant"],$_GET["CCYear"],$_GET["CCMonth"],$_GET["CycleSeq"]));

                  if(strlen(trim($tag->PartNum)) == 0){
                    continue;
                  }
                ?>
                <tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
                  <th scope="col">Part</th>
                  <th scope="col">Tag</th>
                  <th scope="col">Bin</th>
                  <th scope="col">Count</th>
                  <th scope="col">Counted By</th>
                </tr>
                <tr>
                  <td><a target="_blank" href="partFind.php?part=<?php echo urlencode($tag->PartNum); ?>"><?php echo $tag->PartNum; ?></a></td>
                  <td><?php echo $tag->TagNum; ?></td>
                  <td><?php echo $tag->BinNum; ?></td>
                  <td><?php echo $tag->CountedQty; ?></td>
                  <td><?php echo $tag->CountedBy; ?></td>
                </tr>
                <tr>
                  <th scope="col" colspan="2">Qty Variance</th>
                  <th scope="col" colspan="2">Cost Variance</th>
                  <th scope="col" >UOM</th>
                </tr>
                <tr>
                  <td colspan="2">
                    <?php  
                      if($tag->CountedQty < $tag->FrozenQOH){
                        echo "-";
                      } else if($tag->CountedQty > $tag->FrozenQOH){
                        echo "+";
                      }
                      echo $tag->dif; 
                    ?>
                  </td>
                  <td colspan="2">
                    <?php  
                      if($tag->CountedQty < $tag->FrozenQOH){
                        echo "-";
                      } else if($tag->CountedQty > $tag->FrozenQOH){
                        echo "+";
                      }
                      echo "$".$tag->difCost; 
                    ?>
                    </td>
                    <td> <?php echo $tag->UOM; ?> </td>
                </tr>
                <tr>
                  <th scope="col" colspan="5">Part Description</th>
                </tr>
                <tr>
                  <td colspan="5"><?php echo $tag->PartNumPartDescription; ?></td>
                </tr>
                <?php if(!empty($comments)) { ?> 
                  <tr >
                    <th scope="col" colspan="5"><center>Comments</center></th>
                  </tr>
                  <?php foreach ($comments as $comment) { ?>
                    <tr>
                      <td colspan="5"><b><?php echo $comment["Name"]; ?>:</b></td>
                    </tr>
                    <tr>
                       <td colspan="5" style="word-break:break-word;"><?php echo htmlspecialchars($comment["Comment"]); ?></td>
                    </tr>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </center>
      <br>
      <center>
        <a href="index.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Home</center></b></a>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <a href="jobFind.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Find another</center></b></a>
      </center>
      <br>

    <?php } ?>


    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
  </body>
</html>