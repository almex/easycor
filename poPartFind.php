<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		return true;
	}
	
	return false;
}

if(isset($_GET["poNum"])){
	$_POST["poNum"] = $_GET["poNum"];
}

//Step one submitted
if(isset($_POST["poNum"])){
	
	$poResult = $api->getPOInfo($_POST["poNum"]);
	$_SESSION["poPartFind"] = $poResult["result"];

	if(hasError($poResult)){
		$error = true;
		$errorText = "Unable to get PO information";
	} else {
		$step = 2;
	}
}

if($step == 2){
	
}



?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>PO Part Finder</b>
					</div>
					<div class="card-body">
						<center>
						<form action="poPartFind.php" method="POST">
							<br>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="PO Number" name="poNum">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Find</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
				
			<!-- Step Two -->
				<?php if($step == 2) { ?>

					<center>

							<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
							  <div class="card-body">
							    <h5 class="card-title"><?php echo $_SESSION["poPartFind"]->returnObj->POHeader[0]->PONum; ?></h5>
							    <p class="card-text"><?php echo $_SESSION["poPartFind"]->returnObj->POHeader[0]->VendorName; ?></p>
							    <p class="card-text">Ordered on <?php echo explode("T", $_SESSION["poPartFind"]->returnObj->POHeader[0]->OrderDate, 2)[0]; ?></p>
							  </div>
							</div>

							<?php foreach ( $_SESSION["poPartFind"]->returnObj->PODetail as $poDetail) { 
								$partInfoResult = $api->getPartInfo($poDetail->PartNum);
								$_SESSION["partFind"]["part"] = $partInfoResult;
								$_SESSION["partFind"]["plants"] = array();

								if($_SESSION["partFind"]["part"]["http"] == 200){
									foreach ($_SESSION["partFind"]["part"]["result"]->returnObj->PartPlant as $partPlant) {
										$_SESSION["partFind"]["plants"][$partPlant->Plant] = $partPlant->PlantName;
									}
								}
							?>


									<div class="card" style="margin:20px;max-width:500px;">
										<div class="card-header" style="font-size: 1.4rem;">
											<b><?php echo $poDetail->PartNum; ?></b>
										</div>
										<ul class="list-group list-group-flush">
											<li class="list-group-item"><b>Vendor Part: </b> <?php echo $poDetail->VenPartNum; ?></li>
											<li class="list-group-item"><b>Mfg Part: </b> <?php echo $poDetail->MfgPartNum; ?></li>
											<li class="list-group-item"><b>Part Desc: </b> <?php echo $poDetail->LineDesc; ?></li>
											<li class="list-group-item"><b>Order QTY: </b> <?php echo $poDetail->OrderQty; ?></li>
											<li class="list-group-item"><b>Line: </b> <?php echo $poDetail->POLine; ?></li>
										</ul>

											<?php 
											if($_SESSION["partFind"]["part"]["http"] == 200){
												foreach ($_SESSION["partFind"]["part"]["result"]->returnObj->PartWhse as $partWhse) { 
														if(!empty($partWhse->PrimBinNum)){ ?>
															<ul class="list-group list-group-flush">
																<li class="list-group-item active"><b><?php echo $_SESSION["partFind"]["plants"][$partWhse->Plant]; ?></b></li>
																<li class="list-group-item"><b>Warehouse:</b> <?php echo $partWhse->WarehouseDescription; ?></li>
																<li class="list-group-item"><b>Primary Bin:</b> <?php echo $partWhse->PrimBinNum; ?></li>
																<li class="list-group-item"><b>On Hand:</b> <?php echo $partWhse->OnHandQty; ?></li>
															</ul>
											<?php } } } ?>

										
									</div>

							<?php } ?>
					</center>

					<center>
						<a href="index.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Home</center></b></a>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="poPartFind.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Find another</center></b></a>
					</center>
					<br>
				<?php } ?>
				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	


    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>

  </body>
</html>