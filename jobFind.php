<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}


$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}


//Step one submitted
if(isset($_POST["search"]) && isset($_POST["status"]) && isset($_POST["plant"])){
	if($_POST["status"] == 0){
		$_POST["status"] = false;
	} else {
		$_POST["status"] = true;
	}

	$jobSearchResult = $api->searchForJob($_POST["plant"],$_POST["status"],$_POST["search"]);
	if(hasError($jobSearchResult)){
		$error = true;
		$errorText = "Unable to find jobs";
	} else {
		$_SESSION["jobFind"]["search"] = $jobSearchResult;
		$step = 2;
	}
}

if(isset($_POST["labor"])){
	$jobLaborResult = $api->getJobLabor($_POST["labor"]);
	if(hasError($jobLaborResult)){
		$error = true;
		$errorText = "Unable to find labor for this job";
	} else {
		$_SESSION["jobFind"]["labor"] = $jobLaborResult;
		$step = 3;
	}
}

$plantsResult = $api->getAllPlants();
?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Job Find</b>
					</div>
					<div class="card-body">
						<center>
						<form action="jobFind.php" method="POST">
							<br>
							<select class="form-select" aria-label="Plant" name="plant">
							<?php foreach($plantsResult["result"]->returnObj->PlantList as $plant){ 
								$selected = "";
						  		if($plant->Plant == 200){
						  			$selected = "selected";
						  		}
							?>
							  <option value="<?php echo $plant->Plant; ?>" <?php echo $selected;?> ><?php echo $plant->Name; ?></option>
							 <?php } ?>
							</select>
							<br>
						  <div class="mb-3">
						  	<select class="form-select" aria-label="select" name="status">
							  <option value="0" selected>Open Jobs</option>
							  <option value="1">Closed Jobs</option>
							</select>
							<br>
							<input class="form-control" autocomplete="off" placeholder="Search" name="search">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Find Jobs</b></button>
						</form>
						</center>
					</div>
				</div>

				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Job Labor</b>
					</div>
					<div class="card-body">
						<center>
						<form action="jobFind.php" method="POST">
							<br>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="Job Number" name="labor">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Find Labor</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
			<!-- Step Two -->
			<?php if($step == 2) { ?>
				<center>
				<br>
				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">Shows a maximum of 50 jobs</h5>
					</div>
				</div>
				<br>
				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($_SESSION["jobFind"]["search"]["result"]->returnObj->JobHead as $job) { 
					  		if($job->JobClosed == 0){
					  			$job->JobClosed = "No";
					  		} else {
					  			$job->JobClosed = "Yes";
					  		}

					  		$job->CreateDate = explode("T", $job->CreateDate)[0];

					  		?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col">Job</th>
						      <th scope="col">Part</th>
						      <th scope="col">User</th>
						      <th scope="col">Created</th>
						      <th scope="col">Closed</th>
						    </tr>
					  		<tr>
					  			<td><?php echo $job->JobNum; ?></td>
					  			<td><?php echo $job->PartNum; ?></td>
					  			<td><?php echo $job->CreatedBy; ?></td>
					  			<td><?php echo $job->CreateDate; ?></td>
					  			<td><?php echo $job->JobClosed; ?></td>
					  		</tr>
					  		<tr>
						      <th scope="col" colspan="5">Description</th>
						    </tr>
					  		<tr>
					  			<td colspan="5"><?php echo $job->PartDescription; ?></td>
					  		</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>
			</center>
			<br>
			<center>
				<a href="index.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Home</center></b></a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="jobFind.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Find another</center></b></a>
			</center>
			<br>

			<?php } ?>

			<!-- Step Two -->
			<?php if($step == 3) { ?>
				<center>
				<br>
				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">Job <?php echo $_POST["labor"];?> Labor</h5>
					</div>
				</div>
				<br>
				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($_SESSION["jobFind"]["labor"]["result"]->returnObj->LaborDtl as $labor) { 

					  		$labor->ClockInDate = explode("T", $labor->ClockInDate)[0];
					  		$timeClockedIn = ($labor->ClockOutMinute - $labor->ClockInMInute)/60;
					  		$timeClockedIn = number_format((float)$timeClockedIn, 2, '.', '');

					  		?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col">Employee</th>
						      <th scope="col">Emp. Num</th>
						      <th scope="col">Date</th>
						      <th scope="col">Clock In</th>
						      <th scope="col">Clock Out</th>
						      <th scope="col">Time</th>
						    </tr>
					  		<tr>
					  			<td><?php echo $labor->EmpBasicName; ?></td>
					  			<td><?php echo $labor->EmployeeNum; ?></td>
					  			<td><?php echo $labor->ClockInDate; ?></td>
					  			<td><?php echo $labor->DspClockInTime; ?></td>
					  			<td><?php echo $labor->DspClockOutTime; ?></td>
					  			<td><?php echo $timeClockedIn." Hrs"; ?></td>
					  		</tr>
					  		<tr>
						      <th scope="col" colspan="5">Description</th>
						    </tr>
					  		<tr>
					  			<td colspan="5"><?php echo $labor->OpDescOpDesc; ?></td>
					  		</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>
			</center>
			<br>
			<center>
				<a href="index.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Home</center></b></a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="jobFind.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Find another</center></b></a>
			</center>
			<br>

			<?php } ?>
				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>