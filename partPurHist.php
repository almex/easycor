<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	die();
}


$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function number_of_working_days($from, $to) {
    $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
    $holidayDays = ['*-12-25', '*-01-01', '2013-12-23']; # variable and fixed holidays

    $from = new DateTime($from);
    $to = new DateTime($to);

    $interval = new DateInterval('P1D');
    $periods = new DatePeriod($from, $interval, $to);

    $days = 0;
    foreach ($periods as $period) {
        if (!in_array($period->format('N'), $workingDays)) continue;
        if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
        if (in_array($period->format('*-m-d'), $holidayDays)) continue;
        $days++;
    }
    return $days;
}

if(isset($_GET["part"])){
	$_POST["partNum"] = $_GET["part"];
}

//Step one submitted
if(isset($_POST["partNum"])){
	$partPurResult = $api->getPartPurchaseAdvisor($_POST["partNum"]);
	if(hasError($partPurResult)){
		$error = true;
		$errorText = "Unable to get part purchase history";
	} else {
		$_SESSION["partPur"]["part"] = $partPurResult;
		$_SESSION["partPur"]["partNum"] = $_POST["partNum"];
		$_SESSION["partPur"]["part"]["result"]->returnObj->PAPurchasedBefore = array_reverse($_SESSION["partPur"]["part"]["result"]->returnObj->PAPurchasedBefore);
		$step = 2;
	}
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Part Purchase History</b>
					</div>
					<div class="card-body">
						<center>
						<form action="partPurHist.php" method="POST">
							<br>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="Part Number" name="partNum">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Find</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
			<!-- Step Two -->
			<?php if($step == 2) { ?>
				<center>

				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">Purchase History</h5><br>
						<h5 class="card-title"><?php echo $_SESSION["partPur"]["partNum"]; ?></h5>
					</div>
				</div>
				<br>

				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($_SESSION["partPur"]["part"]["result"]->returnObj->PAOnOrder as $onOrder) { 
					  			$dueDate = $onOrder->DueDate;

					  			if(trim($dueDate) != ""){
					  				$dueDate = explode("T", $onOrder->DueDate)[0];
					  			} else {
					  				$dueDate = "N/A";
					  			}

					  		?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col" colspan="2">Vendor</th>
						      <th scope="col">Qty</th>
						      <th scope="col">Type</th>
						    </tr>
					  		<tr>
					  			<td colspan="2"><?php echo $onOrder->VendorName; ?></td>
					  			<td><?php echo $onOrder->RelQty; ?> <?php echo $onOrder->PUM; ?></td>
					  			<td>On Order</td>
					  		</tr>
					  		<tr>
						      <th scope="col" colspan="2" >Due Date</th>
						      <th scope="col" colspan="2" >Purchase Order</th>
						    </tr>
					  		<tr>
					  			<td colspan="2"><?php echo $dueDate; ?></td>
					  			<td colspan="2"><?php echo $onOrder->PONum; ?></td>
					  		</tr>
					  	<?php } ?>
					  	<?php foreach ($_SESSION["partPur"]["part"]["result"]->returnObj->PAPurchasedBefore as $purBefore) { 
					  			$dueDate = $purBefore->DueDate;
					  			$receiptDate = $purBefore->ReceiptDate;
					  			$orderDate = $purBefore->OrderDate;
					  			$leadB = "N/A";
					  			$lead = "N/A";

					  			if(trim($dueDate) != ""){
					  				$dueDate = explode("T", $purBefore->DueDate)[0];
					  			}

					  			if(trim($receiptDate) != ""){
					  				$receiptDate = explode("T", $purBefore->ReceiptDate)[0];
					  			}

					  			if(trim($orderDate) != ""){
					  				$orderDate = explode("T", $purBefore->OrderDate)[0];
					  			}

					  			if(trim($orderDate) != "" && trim($receiptDate) != ""){
					  				$earlier = new DateTime($orderDate);
									$later = new DateTime($receiptDate);

									$lead = $later->diff($earlier)->format("%a")." Days";
					  				$leadB = number_of_working_days($orderDate, $receiptDate)." Days";
					  			}

					  		?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col" colspan="2">Vendor</th>
						      <th scope="col">Qty</th>
						      <th scope="col">Type</th>
						    </tr>
					  		<tr>
					  			<td colspan="2"><?php echo $purBefore->VendorName; ?></td>
					  			<td><?php echo $purBefore->VendorQty; ?> <?php echo $purBefore->PUM; ?></td>
					  			<td>Previous Purchase</td>
					  		</tr>
					  		<tr>
						      <th scope="col" colspan="2" >Due Date</th>
						      <th scope="col" colspan="2" >Purchase Order</th>
						    </tr>
					  		<tr>
					  			<td colspan="2"><?php echo $dueDate; ?></td>
					  			<td colspan="2"><?php echo $purBefore->PONum; ?></td>
					  		</tr>
					  		<tr>
					  		  <th scope="col" colspan="2" >Order Date</th>
						      <th scope="col" colspan="2" >Receipt Date</th>
						    </tr>
					  		<tr>
					  			<td colspan="2"><?php echo $orderDate; ?></td>
					  			<td colspan="2"><?php echo $receiptDate; ?></td>
					  		</tr>
					  		<tr>
					  		  <th scope="col" colspan="2" >Lead</th>
						      <th scope="col" colspan="2" >Lead (Business Days)</th>
						    </tr>
					  		<tr>
					  			<td colspan="2"><?php echo $lead; ?></td>
					  			<td colspan="2"><?php echo $leadB; ?></td>
					  		</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>
			</center>

			<?php } ?>
				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>