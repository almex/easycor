<?php 
session_start();

$error = false;
$errorText = "";
$step = 1;

function prepend($string, $orig_filename) {
  $context = stream_context_create();
  $orig_file = fopen($orig_filename, 'r', 1, $context);

  $temp_filename = tempnam(sys_get_temp_dir(), 'php_prepend_');
  file_put_contents($temp_filename, $string);
  file_put_contents($temp_filename, $orig_file, FILE_APPEND);

  fclose($orig_file);
  unlink($orig_filename);
  rename($temp_filename, $orig_filename);
}

function postpend($string, $orig_filename) {
  $context = stream_context_create();
  $orig_file = fopen($orig_filename, 'r', 1, $context);

  $temp_filename = tempnam(sys_get_temp_dir(), 'php_postpend_');
  file_put_contents($temp_filename, $orig_file);
  file_put_contents($temp_filename, $string, FILE_APPEND);

  fclose($orig_file);
  unlink($orig_filename);
  rename($temp_filename, $orig_filename);
}

if(isset($_FILES["txtFile"])){
	$target_dir = "uploads/";
	$target_file = $target_dir . basename($_FILES["txtFile"]["name"]);
	$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

	if ($fileType == "txt" && move_uploaded_file($_FILES["txtFile"]["tmp_name"], $target_file)) {
		$_SESSION["xml"]["file"] = $target_file;
    	$step = 2;
	} else {
		$error = true;
		$errorText = "unable to upload file";
	}
}

if($step == 2){
	$xml=@simplexml_load_file($_SESSION["xml"]["file"]);
	if(!$xml){
		prepend("<?xml version='1.0' encoding='UTF-8'?><traces>",$_SESSION["xml"]["file"]);
		postpend("</traces>",$_SESSION["xml"]["file"]);
		$xml=simplexml_load_file($_SESSION["xml"]["file"], null, LIBXML_NOCDATA);
	}

	if(!$xml){
		$error = true;
		$errorText = "Failed to load xml";
	} else {
		$tracePackets = $xml->tracePacket;
	}
}

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Epicor Trace</title>
  </head>
  <body style="background: #eaeaea;">
  	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>


  	<div class="row">
			<div class="col-sm">


			<!-- Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Upload trace file</b>
					</div>
					<div class="card-body">
						<center>
						<form action="xml.php" method="POST" enctype="multipart/form-data">
						  <br>
						  <input type="file" accept="text/plain" id="txtFile" name="txtFile"/>
						  <br><br>
						  <button id="continueBTN" type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>

			<!-- Step Two -->
			<?php if($step == 2) { ?>
			    <?php
			    $count = 0; 
			    foreach ($tracePackets as $tracePacket) { 
			    	$count++;
			    	?>
			    	<div class="card" style="margin: 20px;">
					  <div class="card-body">
					    <h5 class="card-title"><?php echo $count.". ".$tracePacket->businessObject; ?> - <?php echo $tracePacket->methodName; ?></h5>

			    		<div class="accordion" id="accordionExample">
			    			<div class="accordion-item">
						    <h2 class="accordion-header" id="headingOne">
						      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $count;?>" aria-expanded="true" aria-controls="collapse<?php echo $count;?>">
						        Parameters
						      </button>
						    </h2>
						    <div id="collapse<?php echo $count;?>" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
						      <div class="accordion-body">
						        <pre class="prettyprint"><?php echo htmlentities((string)$tracePacket->parameters->asXML()); ?></pre>
						      </div>
						    </div>
						  </div>
						  <div class="accordion-item">
						    <h2 class="accordion-header" id="headingOne">
						      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapserv<?php echo $count;?>" aria-expanded="true" aria-controls="collapserv<?php echo $count;?>">
						        Return Values
						      </button>
						    </h2>
						    <div id="collapserv<?php echo $count;?>" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
						      <div class="accordion-body">
						        <pre class="prettyprint"><?php echo htmlentities((string)$tracePacket->returnValues->asXML()); ?></pre>
						      </div>
						    </div>
						  </div>
						  <div class="accordion-item">
						    <h2 class="accordion-header" id="headingOne">
						      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseapi<?php echo $count;?>" aria-expanded="true" aria-controls="collapseapi<?php echo $count;?>">
						        API
						      </button>
						    </h2>
						    <div id="collapseapi<?php echo $count;?>" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
						      <div class="accordion-body">
						        <pre class="prettyprint">
<?php 
$functionString = "";
$params = array();


	$functionString .= htmlentities('	$data = array(');
		foreach ($tracePacket->parameters->children() as $parameter) {
			if($parameter->attributes()[0] == "CallContext"){
				continue;
			}

			if(strlen(trim($parameter) ) > 0 | $parameter->attributes()[0] == "ds"){
				array_push($params, $parameter->attributes()[0]);
				$functionString .= '
		"'.$parameter->attributes()[0].'" => $'.$parameter->attributes()[0].',';
			} else {
				$functionString .= '
		"'.$parameter->attributes()[0].'" => "",';
			}

			if(strlen(trim($parameter) ) > 0){
				$functionString .= ' //'.trim($parameter).'';
			}
		}
	$functionString .= htmlentities('
	);');

	$methodString = str_replace("Impl","Svc",$tracePacket->businessObject);
	$methodString = str_replace("Proxy.","",$methodString);

	$functionString .= '

	return $this->curl("'.$methodString.'/'.$tracePacket->methodName.'",$data);';

	$functionString .= htmlentities('
}');

$headerString = "function ".lcfirst($tracePacket->methodName)."(";
foreach ($params as $param) {
	$headerString .= '$'.$param.',';
}
$headerString = rtrim($headerString, ',');
$headerString .= "){
";

echo $headerString.$functionString;
?>
	
</pre>
						      </div>
						    </div>
						  </div>
			    		</div>

					  </div>
					</div>
			    <?php } ?>
			<?php } ?>


			</div>
	</div>


    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/run_prettify.js"></script>

  </body>
</html>