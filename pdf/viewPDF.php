<?php


if(!isset($_GET["pdf"])){
	die();
}

if(!file_exists("gen/".$_GET["pdf"].".pdf")){
	die();
}

header('Content-Type: application/pdf');
header('Content-Length: '.filesize("gen/".$_GET["pdf"].".pdf"));
header('Content-disposition: inline; filename="' . $_GET["pdf"].".pdf" . '"');
header('Cache-Control: public, must-revalidate, max-age=0');
header('Pragma: public');
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');

ob_clean(); 
flush(); 
readfile("gen/".$_GET["pdf"].".pdf");      
exit();

?>