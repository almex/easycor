<?php
//============================================================+
// File name   : example_011.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 011 for TCPDF class
//               Colored Table (very simple table)
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Colored Table
 * @author Nicola Asuni
 * @since 2008-03-04
 */
error_reporting(0);

 //Run ID Check
if (isset($_GET['RunID'])) {
		//Place ID in variable
		$id = $_GET['RunID'];
	} else {
		die("Missing Run ID");
	}
	
//Grab Data
	include ("include/PullData.php");
	$Run = PullTestrailData ("run","$id");
	$Tests = PullTestrailData ("tests","$id");

//Was the data grab successful?
	if ($Tests["result"] != "1" OR $Run["result"] != "1")
	{
		//Grabbing data failed
		die("We could NOT grab the data for this Run!");
	}

 
// Include the main TCPDF library (search for installation path).
require_once('PDF/config/tcpdf_config.php');
require_once('PDF/tcpdf.php');


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 011');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.''.$Run["run"]["id"].'', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// set font
$pdf->SetFont('dejavusans', '', 12);

//Run Variables
$NumOfTests = $Run["run"]["passed_count"] + $Run["run"]["blocked_count"] + $Run["run"]["untested_count"] + $Run["run"]["retest_count"] + $Run["run"]["failed_count"];
$PassedPer = ($Run["run"]["passed_count"] / $NumOfTests) * 100;
$BlockedPer = ($Run["run"]["blocked_count"] / $NumOfTests) * 100;
$UntestedPer = ($Run["run"]["untested_count"] / $NumOfTests) * 100;
$RetestPer = ($Run["run"]["retest_count"] / $NumOfTests) * 100;
$FailedPer = ($Run["run"]["failed_count"] / $NumOfTests) * 100;


//Run Details Page ---------------------------------------------------------
$pdf->AddPage();
$pdf->Bookmark('Test Run Details', 0, 0, '', 'B', array(0,64,128));

$TD = ' bgcolor="#E6E6E6" ';
$CSS = '
<style>
td {
font-family: \'Carrois Gothic SC\', sans-serif;
	font-size: 14px;
	color: #000000;
	border: 1px solid #000000;
}
.hide {
display:none;
}
</style>
';
$html = $CSS.'<table cellpadding="4" cellspacing="3" ><tr><td colspan="2" align="center" >Test Run Details</td></tr>
<tr><td width="150" align="Left" '.$TD.' >ID:</td> <td width="478" align="Center" '.$TD.' > '.$Run["run"]["id"].' </td></tr>
<tr><td align="Left" '.$TD.' >Name:</td> <td align="Center" '.$TD.' > '.$Run["run"]["name"].' </td></tr>
<tr><td align="Left" '.$TD.' >Description:</td> <td align="Center" '.$TD.' > '.$Run["run"]["description"].' </td></tr>
<tr><td align="Left" '.$TD.' >Suite ID:</td> <td align="Center" '.$TD.' > '.$Run["run"]["suite_id"].' </td></tr>
<tr><td align="Left" '.$TD.' >Number of Passed Tests:</td> <td align="Center" '.$TD.' > '.$Run["run"]["passed_count"].' </td></tr>
<tr><td align="Left" '.$TD.' >Number of Blocked Tests:</td> <td align="Center" '.$TD.' > '.$Run["run"]["blocked_count"].' </td></tr>
<tr><td align="Left" '.$TD.' >Number of Untested Tests:</td> <td align="Center" '.$TD.' > '.$Run["run"]["untested_count"].' </td></tr>
<tr><td align="Left" '.$TD.' >Number of Retests:</td> <td align="Center" '.$TD.' > '.$Run["run"]["retest_count"].' </td></tr>
<tr><td align="Left" '.$TD.' >Number of Failed Tests:</td> <td align="Center" '.$TD.' > '.$Run["run"]["failed_count"].' </td></tr>
<tr><td align="Left" '.$TD.' >Project ID:</td> <td align="Center" '.$TD.' > '.$Run["run"]["project_id"].' </td></tr>
<tr><td align="Left" '.$TD.' >Plan ID:</td> <td align="Center" '.$TD.' > '.$Run["run"]["plan_id"].' </td></tr>
</table>
';
$pdf->writeHTML($html, true, false, true, false, '');

//Pie Chart
$xc = 105;
$yc = 190;
$r = 30;

//Degree Calculator
	$PassedD = array();
	$PassedD[0] = 0;
	$PassedD[1] = 360 * ($Run["run"]["passed_count"] / $NumOfTests);

	$FailedD = array();
	$FailedD[0] = $PassedD[1];
	$FailedD[1] = (360 * ($Run["run"]["failed_count"] / $NumOfTests)) + $PassedD[1];
	
	$UntestedD = array();
	$UntestedD[0] = $FailedD[1];
	$UntestedD[1] = (360 * ($Run["run"]["untested_count"] / $NumOfTests)) + $FailedD[1];
	
	$RetestD = array();
	$RetestD[0] = $UntestedD[1];
	$RetestD[1] = (360 * ($Run["run"]["retest_count"] / $NumOfTests)) + $UntestedD[1];
	
	$BlockedD = array();
	$BlockedD[0] = $RetestD[1];
	$BlockedD[1] = 0;

	//Passed
	$pdf->SetFillColor(153, 255, 102);
	$pdf->PieSector($xc, $yc, $r, $PassedD[0], $PassedD[1], 'FD', false, 0, 2);

	//Failed
	$pdf->SetFillColor(255, 113, 113);
	$pdf->PieSector($xc, $yc, $r, $FailedD[0], $FailedD[1], 'FD', false, 0, 2);

	//Untested
	$pdf->SetFillColor(153, 153, 102);
	$pdf->PieSector($xc, $yc, $r, $UntestedD[0], $UntestedD[1], 'FD', false, 0, 2);

	//Retest
	$pdf->SetFillColor(255, 255, 102);
	$pdf->PieSector($xc, $yc, $r, $RetestD[0], $RetestD[1] , 'FD', false, 0, 2);

	//Blocked
	$pdf->SetFillColor(51, 204, 204);
	$pdf->PieSector($xc, $yc, $r, $BlockedD[0], $BlockedD[1], 'FD', false, 0, 2); 

//Percentage Table
$html = $CSS.'<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<table cellpadding="4" cellspacing="3" >
<tr>
<td align="Center" bgcolor="#99FF66" >Passed</td>
<td align="Center" bgcolor="#FF7171" >Failed</td>
<td align="Center" bgcolor="#999966" >Untested</td>
<td align="Center" bgcolor="#FFFF66" >Retest</td>
<td align="Center" bgcolor="#33CCCC" >Blocked</td>
</tr><tr>
<td align="Center" bgcolor="#99FF66" >'.$Run["run"]["passed_count"].'/'.$NumOfTests.'</td>
<td align="Center" bgcolor="#FF7171" >'.$Run["run"]["failed_count"].'/'.$NumOfTests.'</td>
<td align="Center" bgcolor="#999966" >'.$Run["run"]["untested_count"].'/'.$NumOfTests.'</td>
<td align="Center" bgcolor="#FFFF66" >'.$Run["run"]["retest_count"].'/'.$NumOfTests.'</td>
<td align="Center" bgcolor="#33CCCC" >'.$Run["run"]["blocked_count"].'/'.$NumOfTests.'</td>
</tr><tr>
<td align="Center" bgcolor="#99FF66" >'.$PassedPer.'%</td>
<td align="Center" bgcolor="#FF7171" >'.$FailedPer.'%</td>
<td align="Center" bgcolor="#999966" >'.$UntestedPer.'%</td>
<td align="Center" bgcolor="#FFFF66" >'.$RetestPer.'%</td>
<td align="Center" bgcolor="#33CCCC" >'.$BlockedPer.'%</td>
</tr></table>';
$pdf->writeHTML($html, true, false, true, false, '');

//---------------------------------------------------------


//Test Results ---------------------------------------------------------

// add a page
$pdf->AddPage();
	
//Create $Table array, When printed out creates the HTML table
	$Table = "";
	
//Parse $Run data for the HTML table
	$RunString = "<tr><td ".$TD." colspan=\"5\" align=\"center\">".$Run["run"]["name"]."</td></tr><tr><td  ".$TD." align=\"center\" colspan=\"5\" >".$Run["run"]["description"]."<a href=\"#YourAnchor\">blabla</a></td></tr>";
	$Table = $Table."$RunString";

//Fund Number Of Tests
	$NumOfTests = sizeof($Tests["tests"]);
	
//Parse $Tests for HTML tabel
	$TestsHeader = "<tr><th align=\"center\" width=\"250\">Name</th><th align=\"center\" >Case ID</th><th align=\"center\" >Test ID</th><th align=\"center\" >Status</th></tr>";
	$Table = $Table."$TestsHeader";
	$OrigTests = $Tests;
	for ($i=0; $i<=($NumOfTests - 1); $i++)
		{ 
			//Replace [status_id] with proper name, EX 1 = PASS
			switch ($Tests["tests"][$i]["status_id"]) {
			case 1:
				$Tests["tests"][$i]["status_id"] = "<td  bgcolor=\"#99FF66\" align=\"center\" class='Passed'>Passed";
				break;
			case 2:
				$Tests["tests"][$i]["status_id"] = "<td  bgcolor=\"#33CCCC\"  align=\"center\"  class='Blocked'>Blocked";
				break;
			case 3:
				$Tests["tests"][$i]["status_id"] = "<td  bgcolor=\"#999966\" align=\"center\" class='Untested'>UnTested";
				break;
			case 4:
				$Tests["tests"][$i]["status_id"] = "<td  bgcolor=\"#FFFF66\" align=\"center\" class='Retest'>Retest";
				break;
			case 5:
				$Tests["tests"][$i]["status_id"] = "<td  bgcolor=\"#FF7171\" align=\"center\"  class='Failed'>Fail";
				break;
			}

			//Push Data in $Table array
			$TestsString = "<tr><td style=\"padding-left:10px;\" ".$TD." >".$Tests["tests"][$i]["title"]."</td><td ".$TD."  align=\"center\" >".$Tests["tests"][$i]["case_id"]."</td><td  ".$TD." align=\"center\">".$Tests["tests"][$i]["id"]."</td>".$Tests["tests"][$i]["status_id"]."</td></tr>";
			$Table = $Table."$TestsString";
		}
// create some HTML content

$html = $CSS."<table cellpadding=\"4\" cellspacing=\"3\"><tr><th colspan=\"5\" class='hide' ></th></tr>".$Table."</table>";

// output the HTML content
$pdf->Bookmark('Results', 0, 0, '', 'B', array(0,64,128));
$pdf->writeHTML($html, true, false, true, false, '');

//Failures/Blocked Page ---------------------------------------------------------
$pdf->AddPage();
$pdf->Bookmark('Failures', 0, 0, '', 'B', array(0,64,128));
$pdf->setDestination("YourAnchor");

//Look for comments in XML + Comment search Function
	$Comments = comments("$id"); 
	function CommentSearch($TestID,$Comments) {
		$Matched = "";
		//Search for matching ID
		for ($i=0; $i<=(sizeof($Comments) - 1); $i++)
		{ 
			if ($i != 0 && $Comments[$i][0] == $TestID) { $Matched = $Matched."  ".$Comments[$i][1];}
		}
		return $Matched;
	}
//Get status dates
	$StatDates = status_dates("$id");
	function StatDate ($CaseID,$StatDates) {
		for ($i=0; $i<=(sizeof($StatDates) - 1); $i++)
			{
				if ($StatDates[$i][0] == $CaseID) { return substr($StatDates[$i][1],0,10); }
			}
	}
	
//Find all failed tests	
	$FailBlock = array();
	for ($i=0; $i<=($NumOfTests - 1); $i++)
		{ 
			if ($OrigTests["tests"][$i]["status_id"] == 2 or $OrigTests["tests"][$i]["status_id"] == 5) {
				array_push($FailBlock,$Tests["tests"][$i]);
			}
		}
		
//Print out table
	$html = $CSS.'<table cellpadding="4" cellspacing="3" ><tr> <th align="center" >Title</th> <th align="center" >ID</th> <th align="center" >Run ID</th> <th align="center" >Case ID</th> <th align="center" >Status</th> <th align="center" >Comments</th></tr>';
		for ($i=0; $i<=(sizeof($FailBlock) - 1); $i++)
		{ 
			$Comment = CommentSearch($FailBlock[$i]["id"],$Comments);
			$html = $html.'
			<tr>
			<td '.$TD.'>'.$FailBlock[$i]["title"].'</td> <td align="center" '.$TD.' >'.$FailBlock[$i]["id"].'</td> <td align="center" '.$TD.' >'.$FailBlock[$i]["run_id"].'</td> <td align="center" '.$TD.' >'.$FailBlock[$i]["case_id"].'</td> '.$FailBlock[$i]["status_id"].'</td></tr><tr><td colspan="4" '.$TD.' >'.$Comment.'</td>
			</tr>
			';
		}
	$html = $html.'</table>';
	$pdf->writeHTML($html, true, false, true, false, '');


//---------------------------------------------------------

//Index Page ---------------------------------------------------------
	// add a new page for TOC
	$pdf->addTOCPage();

	// write the TOC title
	$pdf->MultiCell(0, 0, 'Index', 0, 'C', 0, 1, '', '', true, 0);
	$pdf->Ln();


	// add a simple Table Of Content at first page
	// (check the example n. 59 for the HTML version)
	$pdf->addTOC(1, 'courier', '.', 'INDEX', 'B', array(128,0,0));
	
	// end of TOC page
	$pdf->endTOCPage();
//---------------------------------------------------------


// close and output PDF document
$pdf->Output('example_011.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
