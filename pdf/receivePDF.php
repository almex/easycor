<?php
// Include the main TCPDF library (search for installation path).
require_once('config/tcpdf_config.php');
require_once('tcpdf.php');

function makePDF($fileName){
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetTitle('PO');


	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(0);
	$pdf->SetFooterMargin(0);

	// set font
	$pdf->SetFont('dejavusans', '', 12);

	$imgdata = base64_decode(substr(strstr($_SESSION["receive"]["base64PS"], ','),1));
	$size = getimagesize($_SESSION["receive"]["base64PS"]);
	$width = $size[0];
	$height = $size[1];


	$pdf->setJPEGQuality(100);
	$pdf->setPrintHeader(false);

	$pdf->AddPage();
	$bMargin = $pdf->getBreakMargin();
	$auto_page_break = $pdf->getAutoPageBreak();
	$pdf->SetAutoPageBreak(false, 0);

	if($width > $height){
		$pdf->StartTransform();
		$pdf->Rotate(90, 210/2, 297/2);
		$pdf->Image('@'.$imgdata, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0, "CM", false, false);
		$pdf->StopTransform();
	} else {
		$pdf->Image('@'.$imgdata, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0, "CM", false, false);
	}

	$pdf->AddPage();


	if(isset($_POST["partial"]) && $_POST["partial"] == 1){
		$_POST["partial"] = "Yes";
	} else {
		$_POST["partial"] = "No";
	}

	$html = '<table>
		<tr>
			<td><b>Name:</b> '.$_POST["name"].'</td>
		</tr>

		<tr>
			<td><b>Date:</b> '.date("l jS \of F Y h:i:s A").'</td>
		</tr>

		<tr>
			<td><b>PO:</b> '.$_POST["poNum2"].'</td>
		</tr>

		<tr>
			<td><b>Partial Receipt:</b> '.$_POST["partial"].'</td>
		</tr>

		<tr>
			<td><b>Comments:</b> '.$_POST["comments"].'</td>
		</tr>

		</table><br><br>';

	//Items
	$html .= '<table>';
	$count = 0;
	while(true){
		$count++;

		if(isset($_POST[$count."num"]) && isset($_POST[$count."qty"]) && ($_POST[$count."qty"] != 0 && strlen($_POST[$count."qty"]) != 0)){
			$html .= '
			<tr>
				<td><b>Part:</b>
				'.$_POST[$count."num"].'
				<b>Received Qty:</b>
				'.$_POST[$count."qty"].'
				<b>Bin:</b>
				'.$_POST[$count."bin"].'</td>
			</tr>';
		} else {
			break;
		}

	}
	$html .= '</table>';

	$pdf->writeHTML($html, true, false, true, false, '');

	//$pdf->Output('PO'.$_POST["poNum2"].'.pdf', 'I');

	return $pdf->Output(__DIR__ . '/gen/'.$fileName.'.pdf', 'F');
}
?>