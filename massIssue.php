<?php
session_start();

include_once 'inc/api.php';
include_once 'inc/uomConv.php';

$api = new API();
$uomConv = new UOM();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;
$numOfMtlCompleted = 0;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		return true;
	}
	
	return false;
}

function substrwords($text, $maxchar, $end='...') {
    if (strlen($text) > $maxchar || $text == '') {
        $words = preg_split('/\s/', $text);      
        $output = '';
        $i      = 0;
        while (1) {
            $length = strlen($output)+strlen($words[$i]);
            if ($length > $maxchar) {
                break;
            } 
            else {
                $output .= " " . $words[$i];
                ++$i;
            }
        }
        $output .= $end;
    } 
    else {
        $output = $text;
    }
    return $output;
}

if(isset($_GET["jobNum"])){
	$jobMtl = $api->getJobMtl($_GET["jobNum"]);
	$jobMtl = $jobMtl["result"]->returnObj->JobMtlList;

	$plantWarehouses = $api->getWarehouses($jobMtl[0]->Plant);
	$parsedWarehouses = array();
	foreach ($plantWarehouses["result"]->returnObj->Warehse as $warehse) {
		array_push($parsedWarehouses, $warehse->WarehouseCode);
	}

	if(isset($_GET["onlyNonIssued"]) && $_GET["onlyNonIssued"] == 1){
		$nonIssued = array();
		foreach ($jobMtl as $mtl) {
			if(!$mtl->IssuedComplete){
				array_push($nonIssued, $mtl);
			}
		}
		$jobMtl = $nonIssued;
	}

	$jobAssems = $api->getJobAssemblySeqs($_GET["jobNum"]);
	$jobAssems = $jobAssems["result"]->returnObj->JobAsmblList;

	$step = 2;

	$partNums = array();
	foreach ($jobMtl as $mtl) {

		if($mtl->IssuedComplete){
			$numOfMtlCompleted++;
		}

		if(!in_array($mtl->PartNum, $partNums)){
			array_push($partNums, $mtl->PartNum);
		}
	}

	$partsOnHand = $api->getPartsOnHand($partNums);
	$partsOnHand = $partsOnHand["result"]->returnObj->Results;

	$partsFilterdByPlant = array();
	foreach ($partsOnHand as $part) {
		if(in_array($part->PartBin_WarehouseCode, $parsedWarehouses)){
			array_push($partsFilterdByPlant, $part);
		}
	}

	$partsOnHand = $partsFilterdByPlant;
}

/*Mass Issue Test

$massIssueDS = $api->getNewMassIssueInput();
$massIssueDS = $massIssueDS["result"]->parameters->ds;

$massIssueDS = $api->onMassIssueChangeJobNum("021530",$massIssueDS);
$massIssueDS = $massIssueDS["result"]->parameters->ds;

$massIssueDS = $api->buildMassIssueBrowse($massIssueDS);
$massIssueDS = $massIssueDS["result"]->returnObj;

$changePartBin = $api->onChangeBinNum("200-500",$massIssueDS->MassIssue[2]->PartNum,"100",$massIssueDS->MassIssue[2]);
$massIssueDS->MassIssue[2] = $changePartBin["result"]->parameters->ds->MassIssue[0];

$massIssueDS = $api->massIssueAll($massIssueDS);
$massIssueDS = $massIssueDS["result"]->parameters->ds;

$massIssueDS = $api->massIssueNegativeStockCheck("021530",$massIssueDS);
$negativeStockWarning = $massIssueDS["result"]->parameters->pcError;
$massIssueDS = $massIssueDS["result"]->parameters->ds;

//Not Working
$massIssueDS = $api->prePerformMassIssueHT($massIssueDS);
$massIssueDS = $massIssueDS["result"]->parameters->ds;

$massIssueDS = $api->performMassIssue("021530",$massIssueDS);
$massIssueDS = $massIssueDS["result"]->parameters->ds;

print_r($massIssueDS); */

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Issue Job Material</b>
					</div>
					<div class="card-body">
						<center>
						<form action="massIssue.php" method="GET">
							<br>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="Job Number" name="jobNum">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
				
			<!-- Step Two -->
				<?php if($step == 2) { ?>
					<center>
						<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
							<div class="card-body">
								<h5 class="card-title">Job #<?php echo $_GET["jobNum"]; ?></h5>
							</div>
						</div>
						<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
							<div class="card-body">
								<table>
									<tr>
										<h5 class="card-title">Materials</h5>
										<td id="completed-progress"><?php echo $numOfMtlCompleted; ?></td>
										<td>/ <?php echo sizeof($jobMtl); ?></td>
									</tr>
								</table>
								<br>
								<button id="issue-all-btn" onclick="issueAll()" class="btn btn-primary"><b>Issue All</b></button>
								<img id="issue-all-loading" src="img/loading3.gif" style="max-width: 125px;display: none;">
							</div>
						</div>
						<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
							<div class="card-body">
								<?php if(isset($_GET["onlyNonIssued"]) && $_GET["onlyNonIssued"] == 1){ ?>
									<a href="massIssue.php?jobNum=<?php echo $_GET["jobNum"]; ?>" class="btn btn-primary"><b>Show All Material</b></a>
								<?php } else { ?>
									<a href="massIssue.php?jobNum=<?php echo $_GET["jobNum"]; ?>&onlyNonIssued=1" class="btn btn-primary"><b>Show Only Needed Material</b></a>
								<?php } ?>
							</div>
						</div>
						<?php if(isset($_GET["issueAll"]) && ($numOfMtlCompleted < sizeof($jobMtl))) { ?>
							<br>
							<div class="alert alert-danger" role="alert" style="max-width:500px;">
							  <b>Some matieral couldn't be issued</b>
							</div>
						<?php } ?>
						<br><br>
						<table class="table table-striped" style="max-width: 600px;font-size: 0.9rem;">
							<thead>
						    <tr style="border-bottom: solid #000 3px;">
						      <th scope="col">#</th>
						      <th scope="col" style="min-width: 150px;">Part</th>
						      <th scope="col">Assembly</th>
						      <th scope="col">Qty Issued</th>
						    </tr>
						  </thead>
							<?php
								$count = 0; 
								foreach ($jobMtl as $mtl) { 
									$count++;
									$mtl->PartNum = strtoupper(trim($mtl->PartNum));

									$completeSyle = "";
									if(!$mtl->IssuedComplete){
										$completeSyle = "color:#ce0000;";
									}
							?>
								<tr>
									<td><b><?php echo $count; ?></b></td>
									<td><a href="partFind.php?part=<?php echo urlencode($mtl->PartNum); ?>" 
										   target="_blank"
										   style="color: #000;text-decoration: none;">
											<?php echo $mtl->PartNum; ?>
										</a>
									</td>
									<td><?php foreach ($jobAssems as $assembly) {
										if($assembly->AssemblySeq == $mtl->AssemblySeq){
											echo substrwords($assembly->Description,50);
											break;
										}
									} ?></td>
									<td id="qty-<?php echo $count;?>" style="<?php echo $completeSyle; ?>"><?php echo $mtl->IssuedQty; ?>/<?php echo $mtl->RequiredQty; ?> <?php echo $mtl->IUM; ?></td>
								</tr>
								<tr>
									<td colspan="4"><?php echo $mtl->Description; ?></td>
								</tr>
								<tr>
									<td colspan="4">
										<center>
										<?php
											if($mtl->IssuedComplete){
												echo '<button type="button" class="btn btn-success"><b>Material Issued</b></button>';
											} else {

												$partOnHandFound = false;
												foreach ($partsOnHand as $partOnHand) {
													$partOnHand->PartBin_PartNum = strtoupper(trim($partOnHand->PartBin_PartNum));

													if($partOnHand->PartBin_PartNum == $mtl->PartNum && 
														$mtl->WarehouseCode == $partOnHand->PartBin_WarehouseCode &&
														$partOnHand->PartBin_OnhandQty > 0){

															$partOnHandFound = true;

															if($partOnHand->Part_IUM != $mtl->IUM){
																$partOnHand->PartBin_OnhandQty = $uomConv->convert($partOnHand->Part_IUM,$partOnHand->PartBin_OnhandQty,$mtl->IUM);
																$partOnHand->Part_IUM = $mtl->IUM;
															}

															$partOnHand->PartBin_OnhandQty = round($partOnHand->PartBin_OnhandQty,2);

															echo '<button type="button" id="issue-btn-'.$count.'" class="btn btn-dark issue-btn" ';
															echo 'onclick="issueMtl(this)" ';
															echo 'count="'.$count.'" ';
															echo 'jobNum="'.$mtl->JobNum.'" ';
															echo 'partNum="'.$mtl->PartNum.'" ';
															echo 'company="'.$mtl->Company.'" ';
															echo 'assembSeq="'.$mtl->AssemblySeq.'" ';
															echo 'mtlSeq="'.$mtl->MtlSeq.'" ';
															echo 'warehouse="'.$partOnHand->PartBin_WarehouseCode.'" ';
															echo 'bin="'.$partOnHand->PartBin_BinNum.'" ';
															echo 'lot="'.$partOnHand->PartBin_LotNum.'" ';
															echo 'reqQty="'.$mtl->RequiredQty.'" ';
															if($partOnHand->PartBin_OnhandQty < ($mtl->RequiredQty-$mtl->IssuedQty)){
																echo 'fullAmount="0" ';
																echo 'qty="'.$partOnHand->PartBin_OnhandQty.'" ';
															} else {
																echo 'fullAmount="1" ';
																echo 'qty="'.($mtl->RequiredQty-$mtl->IssuedQty).'" ';
															}
															echo '>';

															if($partOnHand->PartBin_OnhandQty < $mtl->RequiredQty){
																echo "<b style='color:#ce0000;'>".$partOnHand->PartBin_OnhandQty." ".$partOnHand->Part_IUM." found in bin #".$partOnHand->PartBin_BinNum."</b>";
															} else {
																echo "<b>".$partOnHand->PartBin_OnhandQty." ".$partOnHand->Part_IUM." found in bin #".$partOnHand->PartBin_BinNum."</b>";
															}

															echo '</button>';

														break;
													}
												}

												if(!$partOnHandFound){
													echo "<b style='color:#ce0000;'>Not found in any bins</b>";
												}
											}
									 	?>
									 	</center>
									 </td>
								</tr>
								<tr style="border-bottom: solid #000 3px;">
									<td colspan="4">&nbsp;</td>
								</tr>
							<?php } ?>
						</table>

					</center>
				<?php } ?>
				
				<!-- Error Modal -->
			    <div class="modal" id="errorModal" tabindex="-1" aria-labelledby="errorModalLabel" aria-hidden="true">
			      <div class="modal-dialog" style="margin-top: 50px;">
			        <div class="modal-content">
			          <div class="modal-header">
			            <h5 class="modal-title" id="errorModalLabel" style="color: #a00000;font-size: 2rem;">Error</h5>
			            <button type="button" class="btn-close" onclick="closeErrorModal()" aria-label="Close"></button>
			          </div>
			          <div class="modal-body">
			              
			            <center>
			              <p id="modalError">Error</p>
			            </center>

			          </div>
			          <div class="modal-footer">
			            <button onclick="closeErrorModal()" class="btn btn-primary" ><b>close</b></button>
			          </div>
			        </div>
			      </div>
			    </div>
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	


    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script language="JavaScript" type="text/javascript">

    	const issueBtnText = [];
    	var issueButtonIDs = [];
    	var progress = <?php echo $numOfMtlCompleted; ?>;

    	var entityMap = {
	        '&': '&amp;',
	        '<': '&lt;',
	        '>': '&gt;',
	        '"': '&quot;',
	        "'": '&#39;',
	        '/': '&#x2F;',
	        '`': '&#x60;',
	        '=': '&#x3D;'
	      }

    	function isJson(str) {
          try {
              JSON.parse(str);
          } catch (e) {
              return false;
          }
          return true;
      	}

		function escapeHtml(string) {
			return String(string).replace(/[&<>"'`=\/]/g, function (s) {
				return entityMap[s];
			});
		}

      	function issueAll(){
      		$("#issue-all-btn").hide();
      		$("#issue-all-loading").show();

      		console.log("issue all");

      		 $.ajax({
	          url: "inc/issueAll.php",
	          type: 'POST',
	          async: true,
	          data: {
	          	jobNum: "<?php echo $_GET["jobNum"]; ?>",
	            amountToIssue: 15,
	        	cache: "<?php echo 'cache_'.time(); ?>"},
	           success: function(data) {
	              if(!isJson(data)){
	                $("#modalError").html(escapeHtml(data));
	                $("#errorModal").modal().show();
	                return;
	              }

	              var result = $.parseJSON(data);

	              if(result.complete == true){
	              	var url = window.location.href;    
					if (url.indexOf('?') > -1){
					   url += '&issueAll=1'
					}else{
					   url += '?issueAll=1'
					}
					window.location.href = url;
	              } else {
	              	issueAll();
	              }

	              if(result.result){
	              	$("#completed-progress").html(result.count+parseInt($("#completed-progress").html()));
	              } else {

	              }

	            }
	        });
      		
      	}

    	function issueMtl(button){
    		var buttonID = "#issue-btn-"+$(button).attr("count");
    		issueBtnText[buttonID] = $(buttonID).html();
    		$(buttonID).html("<b>Issuing...</b>");
    		$(buttonID).removeClass("issue-btn");

    		$.ajax({
	          url: "inc/issueMtl.php",
	          type: 'POST',
	          async: true,
	          data: {btnID: buttonID,
	           qtyID: "#qty-"+$(button).attr("count"),
	           jobNum: $(button).attr("jobNum"),
	           partNum: $(button).attr("partNum"), 
	           company: $(button).attr("company"), 
	           assembSeq: $(button).attr("assembSeq"), 
	           mtlSeq: $(button).attr("mtlSeq"),
	           qty: $(button).attr("qty"), 
	           reqQty: $(button).attr("reqQty"),
	           fullAmount: $(button).attr("fullAmount"), 
	           warehouse: $(button).attr("warehouse"),
	           lot: $(button).attr("lot"),
	           binNum: $(button).attr("bin")},
	           success: function(data) {

	              if(!isJson(data)){
	                $("#modalError").html(escapeHtml(data));
	                $("#errorModal").modal().show();
	                $(result.btnID).removeClass("btn-dark");
	                $(result.btnID).removeClass("btn-success");
	                $(result.btnID).addClass("btn-danger");
	                $(result.btnID).html("<b>Material NOT Issued</b>");
	                $(result.btnID).addClass("issue-btn");
	                return;
	              }

	              var result = $.parseJSON(data);
	              if(result.result){
	                $(result.btnID).removeClass("btn-dark");
	                $(result.btnID).removeClass("btn-danger");
	                $(result.btnID).addClass("btn-success");
	                $(result.btnID).html("<b>Material Issued</b>");
	                $(result.btnID).prop("onclick", null).off("click");
	                $(result.btnID).removeClass("issue-btn");
	                $(result.qtyID).html(result.qty+"/"+$(result.qtyID).html().split("/")[1]);

	                if(result.fullAmount == 1){
	                	$(result.qtyID).html(result.reqQty+"/"+$(result.qtyID).html().split("/")[1]);
	                	$(result.qtyID).css('color', '#212529');

	                	progress++;
	            		$("#completed-progress").html(progress);
	            	}
	              } else {
	                $("#modalError").html(result.error);
	                //$("#errorModal").modal().show();
	                $(result.btnID).removeClass("btn-dark");
	                $(result.btnID).removeClass("btn-success");
	                $(result.btnID).addClass("btn-danger");
	                $(result.btnID).html("<b>Material NOT Issued</b>")
	                $(result.btnID).addClass("issue-btn");
	              }

	              if(result.variance){
	                $("#modalWarning").html(result.error);
	                $("#warningModal").modal().show();
	              }
	            }
	        });
    	}

    	function closeErrorModal(){
        	$("#errorModal").modal().hide();
      	}
    </script>

  </body>
</html>