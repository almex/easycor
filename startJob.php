<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}

//In Office
if($_SERVER['REMOTE_ADDR'] != "72.38.180.229" && $_SERVER['REMOTE_ADDR'] != "72.38.180.226" && $_SERVER['REMOTE_ADDR'] != "72.38.196.34" && $_SERVER['REMOTE_ADDR'] != "::1"){
	header("Location: index.php");
	die();
}

//Clocked in check
$checkClockInStatus = $api->checkClockInStatus($_SESSION["empNum"]);
$clockedIn = $checkClockInStatus["result"]->parameters->clockIn == 1;
if(!$clockedIn){
	header("Location: index.php");
	die();
}

//Get Open Jobs
$employeeLabor = $api->getEmployeeLabor($_SESSION["empNum"],10,0);
hasError($employeeLabor);

if(!isset($_SESSION["startJob"])){
	$_SESSION["startJob"] = array();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function updateDS($result){
	$_SESSION["startJob"]["ds"] = (array) $result["result"]->parameters->ds;
}

function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];

		if(isset($result["result"]->ErrorMessage)){
			$GLOBALS["errorText"] = $result["result"]->ErrorMessage;
		}
		return true;
	}
	
	return false;
}

function stepFour($api){
	$result = $api->startProductionActivity($_SESSION["startJob"]["LaborHedSeq"]);
											
	if(hasError($result)){
		return false;
	}
											
	if(empty($result["result"]->parameters->ds->LaborDtl)){
		$error = true;
		$errorText = "Unable to start production activity";
		return false;
	}
	
	updateDS($result);
	
	$result = $api->validateJobNum($_SESSION["startJob"]["jobNum"]);
	if(hasError($result)){
		return false;
	}
	
	$result = $api->setDefaultJobNum($_SESSION["startJob"]["jobNum"],$_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);
	
	$result = $api->checkResourceGroup("",$_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);
	
	$result = $api->overridesResource("",$_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);
	
	$result = $api->defaultWCCode("",$_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);
	
	$result = $api->laborRateCalc($_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);

	$result = $api->setDefaultAssemblySeq($_SESSION["startJob"]["assemblySeq"],$_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);
	
	$result = $api->setDefaultJobOperSeq($_SESSION["startJob"]["jobOpSeq"],$_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);
	
	$result = $api->laborRateCalc($_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);
	
	$result = $api->checkForProductionActivityWarnings($_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);
		
	$result = $api->checkFirstArticleWarning($_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);
	
	$result = $api->updateProductionActivity($_SESSION["startJob"]["ds"]);
	if(hasError($result)){
		return false;
	}
	updateDS($result);
	if(hasError($result)){
		return false;
	}
	
	return true;
}

//Step one submitted
if(isset($_POST["jobNum"])){
	$_POST["jobNum"] = trim($_POST["jobNum"]);
	$jobAssembsResult = $api->getJobAssemblySeqs($_POST["jobNum"]);
	$stepOneFailed = false;
	
	if($jobAssembsResult["http"] != 200){
		$error = true;
		$errorText = "Invalid job number";
	}else if(empty($jobAssembsResult["result"]->returnObj->JobAsmblList)){
		$error = true;
		$errorText = "Invalid job number";
	} else {
		$step = 2;
		$_SESSION["startJob"]["jobNum"] = $_POST["jobNum"];
		$_SESSION["startJob"]["jobAsmbList"] = $jobAssembsResult["result"]->returnObj->JobAsmblList;
	}
}

//Step two submitted
if(isset($_POST["assemblySeq"])){
	$jobOperResult = $api->getJobOperations($_SESSION["startJob"]["jobNum"],$_POST["assemblySeq"]);
	
	if($jobOperResult["http"] != 200){
		$error = true;
		$errorText = "No job operations found";
	}else if(empty($jobOperResult["result"]->returnObj->JobOperList)){
		$error = true;
		$errorText = "No job operations found";
	} else {
		$step = 3;
		$_SESSION["startJob"]["assemblySeq"] = $_POST["assemblySeq"];
		$_SESSION["startJob"]["jobOpList"] = $jobOperResult["result"]->returnObj->JobOperList;
	}

}

//Step three submitted
if(isset($_POST["jobOpSeq"]) && isset($_POST["jobOpCode"])){
	//Get laborHed
	$laborListResult = $api->getLaborRows($_SESSION["empNum"]);
	$laborListResult = $api->getLaborByID($laborListResult["result"]->returnObj->LaborHed[0]->LaborHedSeq);

	if($laborListResult["http"] != 200){
		$error = true;
		$errorText = "Unable to get labor list";
	} else {
		$step = 4;

		$_SESSION["startJob"]["jobOpSeq"] = $_POST["jobOpSeq"];
		$_SESSION["startJob"]["jobOpCode"] = $_POST["jobOpCode"];
		$_SESSION["startJob"]["ds"] = new stdClass();
		$_SESSION["startJob"]["ds"]->LaborHed = (array) $laborListResult["result"]->returnObj->LaborHed;
		$_SESSION["startJob"]["LaborHedSeq"] = $_SESSION["startJob"]["ds"]->LaborHed[0]->LaborHedSeq;
	}
}

//Step four (Automatic)
$activityStarted = false;
if($step == 4){
	if(stepFour($api)){
		$step = 5;
		$activityStarted = true;
	} else {
		$step = 5;
		$activityStarted = false;
	}
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
			
			<!-- Step One -->
				<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-body">
						<center>
						<form style="max-width:350px;" action="startJob.php" method="POST">
						  <div class="mb-3">
							<label for="enum" class="form-label"><b>Job Number:</b></label><br><br>
							<input type="number" class="form-control" autocomplete="off" name="jobNum">
						  </div><br>
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>

				<?php if($employeeLabor["http"] == 200) { ?>
					<br>
					<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($employeeLabor["result"]->returnObj->LaborDtl as $labor) { 

					  		$labor->ClockInDate = explode("T", $labor->ClockInDate)[0];
					  		$timeClockedIn = ($labor->ClockOutMinute - $labor->ClockInMInute)/60;
					  		$timeClockedIn = number_format((float)$timeClockedIn, 2, '.', '');

					  		?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col">Job</th>
						      <th scope="col">Emp. Num</th>
						      <th scope="col">Date</th>
						      <th scope="col">Clock In</th>
						      <th scope="col">Clock Out</th>
						      <th scope="col">Time</th>
						    </tr>
					  		<tr>
					  			<td><?php echo $labor->JobNum; ?></td>
					  			<td><?php echo $labor->EmployeeNum; ?></td>
					  			<td><?php echo $labor->ClockInDate; ?></td>
					  			<td><?php echo $labor->DspClockInTime; ?></td>
					  			<td><?php echo $labor->DspClockOutTime; ?></td>
					  			<td><?php echo $timeClockedIn." Hrs"; ?></td>
					  		</tr>
					  		<tr>
						      <th scope="col" colspan="5">Description</th>
						    </tr>
					  		<tr>
					  			<td colspan="5"><?php echo $labor->OpDescOpDesc; ?></td>
					  		</tr>
					  	<?php } ?>
					  </tbody>
					</table>
					</div>
				<?php } ?>
				</center>
				<?php } ?>
				
			<!-- Step Two -->
				<?php if($step == 2) { 
					foreach ($_SESSION["startJob"]["jobAsmbList"] as $assembly) {
				?>
					<center>
					<div class="card" style="margin:20px;max-width:500px;">
						<div class="card-body">
							<center>
							<h5 class="card-title"><?php echo $assembly->PartNum; ?></h5><br>
							<p class="card-text"><?php echo $assembly->Description; ?></p><br>
							
							<form style="max-width:350px;" action="startJob.php" method="POST">
							  <input type="hidden" name="assemblySeq" value="<?php echo $assembly->AssemblySeq; ?>">
							  <button type="submit" class="btn btn-primary"><b>Select</b></button>
							</form>
							</center>
						</div>
					</div>
					</center>
				<?php } } ?>
				
			<!-- Step Three -->
				<?php if($step == 3) { 
					foreach ($_SESSION["startJob"]["jobOpList"] as $jobOp) {
				?>
					<center>
					<div class="card" style="margin:20px;max-width:500px;">
						<div class="card-body">
							<br>
							<h5 class="card-title"><?php echo $jobOp->OpCodeOpDesc; ?></h5>
							<br>
							
							<form style="max-width:350px;" action="startJob.php" method="POST">
							  <input type="hidden" name="jobOpSeq" value="<?php echo $jobOp->OprSeq; ?>">
							  <input type="hidden" name="jobOpCode" value="<?php echo $jobOp->OpCode; ?>">
							  <button type="submit" class="btn btn-primary"><b>Select</b></button>
							</form>
							
						</div>
					</div>
					</center>
				<?php } } ?>
				
				
			<!-- Step four -->
			<!-- Step five -->
				<?php if($step == 5) {  ?>
					<center>
					<div class="card" style="margin:20px;max-width:500px;">
						
						<center>
							<br>
						<?php if($activityStarted) { ?>
							<img src="img/check.png" class="card-img-top" style="margin:10px;max-width:100px;max-height:100px;">
						<?php } else { ?>
							<img src="img/cross.png" class="card-img-top" style="margin:10px;max-width:100px;max-height:100px;">
						<?php } ?>
						</center>
						
						<br>
						<div class="card-body">
							
							<?php if($activityStarted) { ?>
								<p class="card-text"><b><center>You clocked into the job</center></b></p>
							<?php } else { ?>
								<p class="card-text"><b><center>An error occurred and you were not clocked into the job</center></b></p>
							<?php } ?>	
							
							<center>
								<br>
								<a href="index.php" class="btn btn-primary"><b><center>Home</center></b></a>
							</center>
						</div>
					</div>
					</center>
				<?php } ?>
				

				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>