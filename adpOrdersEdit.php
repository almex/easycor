<?php
session_start();

include_once 'inc/inventoryFunctions.php';
include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function getKeywords(){
  $keywordDB = new PDO('sqlite:db/adpKeyword.db');
  $qry = $keywordDB->prepare("SELECT * FROM keywords");

  $ret = $qry->execute(array());
  if($ret == false){
    return array();
  }

  $keywords = $qry->fetchAll();
  if($keywords == false){
    return array();
  }

  return $keywords;
}

function addKeyword($keyword,$type){
	$keywordDB = new PDO('sqlite:db/adpKeyword.db');
	$qry = $keywordDB->prepare("INSERT INTO keywords (keyword, type) VALUES (?, ?)");
	$ret = $qry->execute(array($keyword,$type));
	if(!$ret) {
    	$GLOBALS["error"] = true;
    	$GLOBALS["errorText"] = "Unable to insert into database";
    	return false;
  	}

  return true;
}

function deleteKeyword($keywordID){
  $keywordDB = new PDO('sqlite:db/adpKeyword.db');
  $qry = $keywordDB->prepare(
  'DELETE FROM keywords WHERE id=?');
  $ret = $qry->execute(array($keywordID));
  if($ret == false){
  	$GLOBALS["error"] = true;
    $GLOBALS["errorText"] = "Unable to delete keyword";
    return false;
  } else {
    return true;
  }
}

function getEmails(){
  $keywordDB = new PDO('sqlite:db/adpKeyword.db');
  $qry = $keywordDB->prepare("SELECT * FROM emails");

  $ret = $qry->execute(array());
  if($ret == false){
    return array();
  }

  $keywords = $qry->fetchAll();
  if($keywords == false){
    return array();
  }

  return $keywords;
}

function addEmail($email){
	$keywordDB = new PDO('sqlite:db/adpKeyword.db');
	$qry = $keywordDB->prepare("INSERT INTO emails (email) VALUES (?)");
	$ret = $qry->execute(array($email));
	if(!$ret) {
    	$GLOBALS["error"] = true;
    	$GLOBALS["errorText"] = "Unable to insert into database";
    	return false;
  	}

  return true;
}

function deleteEmail($emailID){
  $keywordDB = new PDO('sqlite:db/adpKeyword.db');
  $qry = $keywordDB->prepare(
  'DELETE FROM emails WHERE id=?');
  $ret = $qry->execute(array($emailID));
  if($ret == false){
  	$GLOBALS["error"] = true;
    $GLOBALS["errorText"] = "Unable to delete email";
    return false;
  } else {
    return true;
  }
}

if(isset($_POST["add"]) && isset($_POST["type"])){
	if(strlen(trim($_POST["add"])) > 0){
		addKeyword($_POST["add"],$_POST["type"]);
	}
}

if(isset($_POST["addEmail"])){
	if(strlen(trim($_POST["addEmail"])) > 0){
		addEmail($_POST["addEmail"]);
	}
}

if(isset($_GET["delete"])){
	deleteKeyword($_GET["delete"]);
}

if(isset($_GET["deleteEmail"])){
	deleteEmail($_GET["deleteEmail"]);
}

$DBkeywords = getKeywords();
$DBemails = getEmails();

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;" class="notranslate" translate="no">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="google" content="notranslate" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>

	<br>
	<center>
	<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
		<div class="card-body">
			<h5 class="card-title">Edit ADP Order List</h5>
			<br>
			<a href="adpOrders.php" class="btn btn-primary"><b>Back</b></a>
		</div>
	</div>
	</center>

	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
				
			<!-- Step Two -->
			<?php if($step == 1) { ?>
				<center>
				<br>
				<div class="card mb-3" style="max-width: 500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Emails</b>
					</div>
					<div class="card-body">
						<table>
						<?php foreach ($DBemails as $email) {?>
							<tr>
								<td>
									<?php echo $email["email"]; ?>&nbsp;
								</td>
								<td>
									<a class="btn btn-danger" style="margin-bottom:5px;" href="adpOrdersEdit.php?deleteEmail=<?php echo $email["id"]; ?>"><b>Delete</b></a>
								</td>
							</tr>
						<?php } ?>
						</table>
						<br>
						<form action="adpOrdersEdit.php" method="POST">
							<input class="form-control" autocomplete="off" placeholder="Email" type="email" name="addEmail">
							<br>
							<button type="submit" class="btn btn-primary"><b>Add</b></button>
						</form>
					</div>
				</div>
				<br>

				<br>
				<div class="card mb-3" style="max-width: 500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Keywords</b>
					</div>
					<div class="card-body">
						<table>
						<?php foreach ($DBkeywords as $keyword) {
							if($keyword["type"] != "1"){
								continue;
							} ?>

							<tr>
								<td>
									<?php echo $keyword["keyword"]; ?>&nbsp;
								</td>
								<td>
									<a class="btn btn-danger" style="margin-bottom:5px;" href="adpOrdersEdit.php?delete=<?php echo $keyword["id"]; ?>"><b>Delete</b></a>
								</td>
							</tr>

						<?php } ?>
						</table>
						<br>
						<form action="adpOrdersEdit.php" method="POST">
							<input class="form-control" autocomplete="off" name="type" value="1" type="hidden">
							<input class="form-control" autocomplete="off" placeholder="Keyword" name="add">
							<br>
							<button type="submit" class="btn btn-primary"><b>Add</b></button>
						</form>
					</div>
				</div>
				<br>

				<div class="card mb-3" style="max-width: 500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Inventory Count Warehouse's</b>
					</div>
					<div class="card-body">
						<table>
						<?php foreach ($DBkeywords as $keyword) {
							if($keyword["type"] != "2"){
								continue;
							} ?>

							<tr>
								<td>
									<?php echo $keyword["keyword"]; ?>&nbsp;
								</td>
								<td>
									<a class="btn btn-danger" style="margin-bottom:5px;" href="adpOrdersEdit.php?delete=<?php echo $keyword["id"]; ?>"><b>Delete</b></a>
								</td>
							</tr>

						<?php } ?>
						</table>
						<br>
						<form action="adpOrdersEdit.php" method="POST">
							<input class="form-control" autocomplete="off" name="type" value="2" type="hidden">
							<input class="form-control" autocomplete="off" placeholder="200-500" name="add">
							<br>
							<button type="submit" class="btn btn-primary"><b>Add</b></button>
						</form>
					</div>
				</div>
				<br>

				<div class="card mb-3" style="max-width: 500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Inventory Planner Report ID's</b>
					</div>
					<div class="card-body">
						<table>
						<?php foreach ($DBkeywords as $keyword) {
							if($keyword["type"] != "3"){
								continue;
							} ?>

							<tr>
								<td>
									<?php echo $keyword["keyword"]; ?>&nbsp;
								</td>
								<td>
									<a class="btn btn-danger" style="margin-bottom:5px;" href="adpOrdersEdit.php?delete=<?php echo $keyword["id"]; ?>"><b>Delete</b></a>
								</td>
							</tr>

						<?php } ?>
						</table>
						<br>
						<form action="adpOrdersEdit.php" method="POST">
							<input class="form-control" autocomplete="off" name="type" value="3" type="hidden">
							<input class="form-control" autocomplete="off" placeholder="54" name="add">
							<br>
							<button type="submit" class="btn btn-primary"><b>Add</b></button>
						</form>
					</div>
				</div>
				<br>

			<?php } ?>

				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>