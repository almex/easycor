<?php
ini_set('session.gc_maxlifetime', 14400);
session_set_cookie_params(14400);
session_start();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
  header("Location: login.php");
  die();
}

include_once 'inc/global.php';
include_once 'inc/api.php';
$api = new API();

$error = false;
$errorText = "";
$step = 1;
$prevCycles = getCycles();

//Admin
$isAdmin = false;
if($_SESSION["empNum"] == "564" || $_SESSION["empNum"] == "613" || $_SESSION["empNum"] == "830"){
	$isAdmin = true;
}

if(isset($_GET["unarchive"])){
	unarchiveCycle($_GET["unarchive"]);
}

function makeCycleString($plant,$year,$month,$seq){
  return $plant."-".$year."-".$month."-".$seq;
}

function getCycles(){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare('SELECT DISTINCT Cycle FROM tags');
  $ret = $qry->execute();
  return $qry->fetchAll();
}

function archiveCycle($cycleString){
	$db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
	$qry = $db->prepare(
	'INSERT INTO archived (cycle) VALUES (?)');
	$ret = $qry->execute(array($cycleString));
	if(!$ret) {
		$error = true;
		$errorText = "Unable to archive cycle";
		return false;
	}

	return true;
}

function unarchiveCycle($cycleString){
	$db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
	$qry = $db->prepare(
	'DELETE FROM archived WHERE cycle=?');
	$ret = $qry->execute(array($cycleString));
	if(!$ret) {
		$error = true;
		$errorText = "Unable to unarchive cycle";
		return false;
	}

	return true;
}

function cycleIsArchived($cycleString){
	$db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);

	$qry = $db->prepare('SELECT * FROM archived WHERE cycle=?');
	$ret = $qry->execute(array($cycleString));
	$exists = $qry->fetch();

	if($exists == false){
		return false;
	} else {
		return true;
	}
}

function tagExists($tagNum,$cycleString,$warehouseCode){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);

  $qry = $db->prepare('SELECT * FROM tags WHERE TagNum=? AND Cycle=? AND WarehouseCode=?');
  $ret = $qry->execute(array($tagNum,$cycleString,$warehouseCode));
  $tag = $qry->fetch();

  if($tag == false){
    return false;
  } else {
    return true;
  }
}

function insertTag($tagNum,$partNum,$PartDesc,$BinNum,$IUM,$serial,$warehouse,$blank,$cycleString){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare(
  'INSERT INTO tags (TagNum, PartNum, PartDesc, BinNum, IUM, SerialNumber, WarehouseCode, Blank, Cycle) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
  $ret = $qry->execute(array($tagNum,$partNum,$PartDesc,$BinNum,$IUM,$serial,$warehouse,$blank,$cycleString));
  if(!$ret) {
    $error = true;
    $errorText = "Unable to insert tag into database";
    return false;
  }

  return true;
}

function updateTag($tagNum,$partNum,$PartDesc,$BinNum,$IUM,$serial,$warehouse,$blank,$cycleString){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare(
  'UPDATE tags SET PartNum=?, PartDesc=?, BinNum=?, IUM=?, SerialNumber=?, WarehouseCode=?, Blank=?, Cycle=? WHERE TagNum=? AND Cycle=? AND WarehouseCode=?');
  $ret = $qry->execute(array($partNum,$PartDesc,$BinNum,$IUM,$serial,$warehouse,$blank,$cycleString,$tagNum,$cycleString,$warehouse));
  if(!$ret) {
    $error = true;
    $errorText = "Unable to update tag info";
    return false;
  }

  return true;
}

//Archive cycle
if(isset($_POST["cycle"])){
	archiveCycle($_POST["cycle"]);
}

$countCycles = array();
if(isset($_POST["plant"]) && isset($_POST["year"])){
  $countCycles = $api->getCountCycles($_POST["plant"],$_POST["year"]);
  if($countCycles["http"] == 200){
  	$step = 2;
  } else {
  	$error = true;
    $errorText = "Unable to get count cycles";
  }
}

$tags = array();
if(isset($_POST["CCYear"]) && isset($_POST["CCMonth"]) && isset($_POST["CycleSeq"]) && isset($_POST["WarehouseCode"])){

  //Had weirdness where some tags were not inserted unless I imported multiple times
  for($i=0; $i < 5; $i++){
    $tags = $api->getCountCycleTags($_POST["WarehouseCode"],$_POST["CCYear"],$_POST["CCMonth"],$_POST["CycleSeq"]);
    if($tags["http"] == 200){
        foreach ($tags["result"]->returnObj->CCTag as $tag) {
          $cycleString = makeCycleString($tag->Plant,$tag->CCYear,$tag->CCMonth,$tag->CycleSeq);

          if($tag->BlankTag){
            $tag->BlankTag = 1;
          } else {
            $tag->BlankTag = 0;
          }

          if(tagExists($tag->TagNum,$cycleString,$tag->WarehouseCode)){
            updateTag($tag->TagNum,$tag->PartNum,$tag->PartNumPartDescription,$tag->BinNum,$tag->UOM,$tag->SerialNumber,$tag->WarehouseCode,$tag->BlankTag,$cycleString);
          } else {
            insertTag($tag->TagNum,$tag->PartNum,$tag->PartNumPartDescription,$tag->BinNum,$tag->UOM,$tag->SerialNumber,$tag->WarehouseCode,$tag->BlankTag,$cycleString);
          }
        }
    } else {
    	$error = true;
      $errorText = "Unable to get count cycle tags";
      break;
    }
  }

  if(!$error){
    $step = 3;
  }
}

$plantsResult = $api->getAllPlants();
?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  

  <!-- Step One -->
      <?php if($step == 1) { ?>
        <center>
        <div class="card" style="margin:20px;max-width:500px;">
          <div class="card-header" style="font-size: 1.5rem;">
            <b>Find count cycles</b>
          </div>
          <div class="card-body">
            <center>
              <form action="countCycleImport.php" method="POST">
                <label  class="form-label" style="font-size: 1.3rem;"><b>Plant:</b></label>
                <select class="form-select" aria-label="Plant" name="plant">
                  <?php foreach($plantsResult["result"]->returnObj->PlantList as $plant){ 
                      $selected = "";
                        if($plant->Plant == 200){
                          $selected = "selected";
                        }
                    ?>
                    <option value="<?php echo $plant->Plant; ?>" <?php echo $selected;?> ><?php echo $plant->Name; ?></option>
                  <?php } ?>
                </select>
                <br>
                <div class="mb-3">
                  <label  class="form-label" style="font-size: 1.3rem;"><b>Year:</b></label>
                  <input class="form-control" autocomplete="off" placeholder="Year" name="year" value="2023">
                </div>
                <br>

                <button type="submit" class="btn btn-primary"><b>Next</b></button>
              </form>

            </center>
          </div>
        </div>

        <?php if($isAdmin){ ?>
        	<br>
        <div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b>DO NOT ARCHIVE ANYONE'S COUNT CYCLES BUT YOUR OWN!</b>
		</div>
        <br>
        	<div class="card" style="margin:20px;max-width:500px;">
	          <div class="card-header" style="font-size: 1.5rem;">
	            <b>Previous count cycles</b>
	          </div>
	        </div>

			<?php 
        $hasPrevCycles = false;
        foreach ($prevCycles as $cycle) { 

  				if(cycleIsArchived($cycle[0])){
  					continue;
  				}

          $hasPrevCycles = true;
			?>
            <center>
            <div class="card" style="margin:20px;max-width:500px;">
              <div class="card-body">
                <center>
                <h5 class="card-title"><?php echo $cycle[0]; ?></h5>
                <br>
                
                <form style="max-width:350px;" action="countCycleImport.php" method="POST">
                  <input type="hidden" name="cycle" value="<?php echo $cycle[0]; ?>">
                  <button type="submit" class="btn btn-danger"><b>Archive</b></button>
                </form>
                </center>
              </div>
            </div>
            </center>

      <?php } ?>

      <?php if(!$hasPrevCycles) { ?>
        <div class="card" style="margin:20px;max-width:500px;">
          <div class="card-body">
            <center>
                  <h5 class="card-title">None</h5>
            </center>
          </div>
        </div>
      <?php } ?>

    
      <?php } } ?>
      </center>

      <!-- Step Two -->
      <?php if($step == 2) { ?>

          <center>
            <div class="alert alert-primary" role="alert" style="margin:20px;max-width:500px;">
              <b>Select a warehouse to import</b>
            </div>
          </center>

          <?php 
          	$cycleFound = false;
          	foreach ($countCycles["result"]->returnObj->CCHdrList as $countCycle) {

          		if($countCycle->CycleStatus != 2 && $countCycle->CycleStatus != 3 ){
          			continue;
          		}

          		$cycleFound = true;
          ?>
            <center>
            <div class="card" style="margin:20px;max-width:500px;">
              <div class="card-body">
                <center>
                <h5 class="card-title">Warehouse: <?php echo $countCycle->WarehouseCode; ?></h5>
                <p class="card-text"><?php echo $countCycle->WhseCodeDescription; ?></p>
                <p class="card-text"><b>Cycle Year:</b> <?php echo $countCycle->CCYear; ?></p>
                <p class="card-text"><b>Cycle Per:</b> <?php echo $countCycle->CCMonth; ?></p>
                <p class="card-text"><b>Cycle:</b> <?php echo $countCycle->CycleSeq; ?></p>
                
                <form style="max-width:350px;" action="countCycleImport.php" method="POST">
                  <input type="hidden" name="CCYear" value="<?php echo $countCycle->CCYear; ?>">
                  <input type="hidden" name="CCMonth" value="<?php echo $countCycle->CCMonth; ?>">
                  <input type="hidden" name="CycleSeq" value="<?php echo $countCycle->CycleSeq; ?>">
                  <input type="hidden" name="WarehouseCode" value="<?php echo $countCycle->WarehouseCode; ?>">
                  <button type="submit" class="btn btn-primary"><b>Select</b></button>
                </form>
                </center>
              </div>
            </div>
            </center>
          <?php }  ?>

          <?php if(!$cycleFound){?>
          	<center>
				<br>
				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">There are no warehouses with an active count cycle</h5>
					</div>
				</div>
				<br>
			</center>
          <?php } ?>

        <?php } ?>

        <!-- Step One -->
      <?php if($step == 3) { ?>
        <center>
        <div class="card" style="margin:20px;max-width:500px;">
          <div class="card-header" style="font-size: 1.5rem;">
            <b>Imported</b>
          </div>
          <div class="card-body">
            <center>

              <br>
              <a class="btn btn-primary" style="min-width: 150px;" href="countCycleView.php"><b>Complete</b></a>
              <br>
              <br>

            </center>
          </div>
        </div>
        </center>
      <?php } ?>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
  </body>
</html>