<?php
ini_set("memory_limit","256M");
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

if(isset($_GET["part"])){
	$_POST["partNum"] = $_GET["part"];
}


//Step one submitted
if(isset($_POST["partNum"]) && isset($_POST["qty"])){

	if(is_numeric($_POST["qty"])){
		$_SESSION["partInv"]["qty"] = $_POST["qty"];
	} else {
		$error = true;
		$errorText = "Invalid part qty";
		$_SESSION["partInv"]["qty"] = 1;
	}

	$partInfoResult = $api->getPartInfo($_POST["partNum"]);
	if(hasError($partInfoResult)){
		$error = true;
		$errorText = "Unable to get part information";
	} else {
		$_SESSION["partInv"]["partNum"] = $_POST["partNum"];
		$_SESSION["partInv"]["part"] = $partInfoResult;
		$step = 2;
	}
}

//Get revisions
if(isset($_POST["revNum"])){
	$_SESSION["partInv"]["revNum"] = $_POST["revNum"];
	$methodParts = $api->getMethodParts($_SESSION["partInv"]["partNum"],$_POST["revNum"]);
	if(hasError($methodParts)){
		$error = true;
		$errorText = "Unable to get sub-part information";
	} else {
		$_SESSION["partInv"]["methodParts"] = array();
		array_push($_SESSION["partInv"]["methodParts"], $methodParts);
		$step = 3;
	}
}

//Get plants
if($step == 3){
	$plants = $api->getAllPlants();
	if(hasError($plants)){
		$error = true;
		$errorText = "Unable to get plant information";
	} else {
		$_SESSION["partInv"]["plants"] = $plants;
	}
}


//Prevent refresh bug
if(isset($_POST["plant"])){
	$_SESSION["partInv"]["plant"] = $_POST["plant"];
	$step = 4;
}

if($step == 4){

	//Prevent refresh bug
	$backup = $_SESSION["partInv"]["methodParts"][0];
	unset($_SESSION["partInv"]["methodParts"]);
	$_SESSION["partInv"]["methodParts"] = array();
	array_push($_SESSION["partInv"]["methodParts"], $backup);

	$first = true;
	$_SESSION["partInv"]["ignoreParts"] = array();
	foreach ($_SESSION["partInv"]["methodParts"][0]["result"]->returnObj->PartRev as $subPart) {
		if($first){
			$first = false;
			continue;
		}
	
		$methodParts = $api->getMethodParts($subPart->PartNum,$subPart->RevisionNum);
		if(hasError($methodParts)){
			$error = true;
			$errorText = "Unable to get sub-part information";
		} else {
			array_push($_SESSION["partInv"]["ignoreParts"], $subPart->PartNum);
			array_push($_SESSION["partInv"]["methodParts"], $methodParts);
		}
	}
	$step = 4;
}


?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Part Inventory</b>
					</div>
					<div class="card-body">
						<center>
						<form action="partInventory.php" method="POST">
							<br>
						  <div class="mb-3">
							<input id="partNumInput" class="form-control" placeholder="Part Number" name="partNum" style="width: 75%;">
						  </div>
						  <div class="mb-3">
							<input type="number" class="form-control" placeholder="Qty" name="qty" style="width: 20%;">
						  </div>
						  <button type="submit" class="btn btn-primary"><b>Find</b></button>
						</form>
						</center>
					</div>
				</div>

				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Common parts</b>
					</div>
					<div class="card-body">
						<center>
							<ul class="list-group">
								<li class="list-group-item">SG1 60 Amps 220-250 Volts 
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-061"><b>Select</b></button></li>
								<li class="list-group-item">SG1 60 Amps 360-480 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-062"><b>Select</b></button></li>
								<li class="list-group-item">SG1 60 Amps 525-600 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-063"><b>Select</b></button></li>
								<li class="list-group-item">SG1 100 Amps 220-250 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-101"><b>Select</b></button></li>
								<li class="list-group-item">SG1 100 Amps 360-480 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-102"><b>Select</b></button></li>
								<li class="list-group-item">SG1 100 Amps 525-600 Volts
									<br><br><button class="part-num-btn btn btn-primary" partnum="BAT-SG1-103"><b>Select</b></button></li>
								<li class="list-group-item">T6GFXP 
									<br><br><button class="part-num-btn btn btn-primary" partnum="T6GFXP"><b>Select</b></button></li>
							</ul>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
			<!-- Step Two -->
			<?php if($step == 2) { 
				foreach ($_SESSION["partInv"]["part"]["result"]->returnObj->PartRev as $rev) {
			?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-body">
						<center>
						<h5 class="card-title">Rev <?php echo $rev->RevisionNum; ?></h5>
						<p class="card-text"><?php echo $rev->RevShortDesc; ?></p>
						
						<form style="max-width:350px;" action="partInventory.php" method="POST">
						  <input type="hidden" name="revNum" value="<?php echo $rev->RevisionNum; ?>">
						  <button type="submit" class="btn btn-primary"><b>Select</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } } ?>

			<!-- Step three -->
			<?php if($step == 3) { 
				foreach ($_SESSION["partInv"]["plants"]["result"]->returnObj->PlantList as $plant) {
			?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-body">
						<center>
						<h5 class="card-title"><?php echo $plant->Name; ?></h5>
						<p class="card-text"><?php echo $plant->Plant; ?></p>
						
						<form style="max-width:350px;" action="partInventory.php" method="POST">
						  <input type="hidden" name="plant" value="<?php echo $plant->Plant; ?>">
						  <button type="submit" class="btn btn-primary"><b>Select</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } } ?>

			<!-- Step Four -->
			<?php if($step == 4) { 

				echo '<center><div class="card text-white bg-dark mb-3" style="max-width: 500px;">
						  <div class="card-body">
						    <h5 class="card-title">Plant '.$_SESSION["partInv"]["plant"].'</h5>
						  </div>
						</div></center>';


				$first = true;
				foreach ($_SESSION["partInv"]["methodParts"] as $methodParts) {

					if($first){
						echo '<center><div class="card text-white bg-dark mb-3" style="max-width: 500px;">
						  <div class="card-body">
						    <h5 class="card-title">'.$methodParts["result"]->returnObj->PartRev[0]->PartNum.' x'.$_SESSION["partInv"]["qty"].'</h5>
						    <p class="card-text">'.$methodParts["result"]->returnObj->PartRev[0]->PartDescriptionPartDescription.'</p>
						  </div>
						</div></center>';
					} else {
						echo '<center><div class="alert alert-warning" style="max-width: 500px;">
						  <div class="card-body">
						    <h5 class="card-title"><b>'.$methodParts["result"]->returnObj->PartRev[0]->PartNum.'</b></h5>
						    <p class="card-text">'.$methodParts["result"]->returnObj->PartRev[0]->PartDescriptionPartDescription.'</p>
						  </div>
						</div></center>';
					}

					echo '<center><div class="table-responsive" style="max-width: 600px;">';
					echo '<table class="table table-striped" style="font-size: 0.8rem;">';
					echo '<thead>
						    <tr style="border-bottom: solid #000 3px;">
						      <th scope="col">Part</th>
						      <th scope="col">Qty Needed</th>
						      <th scope="col">Stock</th>
						    </tr>
						  </thead>';
					foreach ($methodParts["result"]->returnObj->PartMtl as $partMtl) {

							//Ignore subassmblies/manufactured parts in first methodparts
							if($first && in_array($partMtl->MtlPartNum, $_SESSION["partInv"]["ignoreParts"])){
								continue;
							}

							echo '<tr>';
								echo '<td><b>'.$partMtl->MtlPartNum.'</b></td>';
								echo '<td>'.$partMtl->QtyPer*$_SESSION["partInv"]["qty"].' '.$partMtl->MtlPartNumIUM.'</td>';
								echo '<td class="stockQty" qtyNeeded="'.$partMtl->QtyPer*$_SESSION["partInv"]["qty"].'" plant="'.$_SESSION["partInv"]["plant"].'" partNum="'.$partMtl->MtlPartNum.'">Loading...</td>';
							echo '</tr>';
							echo '<tr style="border-bottom: solid #000 3px;">';
								echo '<td colspan="3">'.$partMtl->MtlPartNumPartDescription.'</td>';
							echo '</tr>';
					}
					echo '</table></div><?center><br><br>';

					$first = false;
				}
			}?>

				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
    	$( document ).ready(function() {
		    loadStockQty();
		});

		$('.part-num-btn').click(function(){
		    $('#partNumInput').val($(this).attr('partnum'));
		});

    	function loadStockQty(){
    		var x = document.getElementsByClassName("stockQty");
    		var i;
			for (i = 0; i < x.length; i++) {
				$.post(
				    "inc/partInfo.php",
				    { partNum:  x[i].getAttribute("partNum")},
				    function(data) {
				        var response = jQuery.parseJSON(data);
				        var partWhse = response.result.returnObj.PartWhse;
				        var partNum = response.result.returnObj.Part[0].PartNum;
				        var plant;
				        var qtyNeeded = 0;
				        var valueString = "";
				        var j;

				        j = 0;
				        var x = document.getElementsByClassName("stockQty");
				        for (j = 0; j < x.length; j++) {
				        	if(x[j].getAttribute("partNum") == partNum){
				        		plant = x[j].getAttribute("plant");
				        		qtyNeeded = x[j].getAttribute("qtyNeeded");
				        		valueString = "";
				        		k = 0;
						        for (k = 0; k < partWhse.length; k++) {
						        	if(partWhse[k].Plant != plant){
						        		continue;
						        	}

						        	if(partWhse[k].PrimBinNum.trim() == "" && partWhse[k].OnHandQty == 0){
						        		continue;
						        	}

						        	if(partWhse[k].OnHandQty < qtyNeeded){
										valueString += '<b style="color:#ca0000;">'+partWhse[k].OnHandQty+" "+partWhse[k].PartNumIUM+" in "+partWhse[k].WarehouseDescription+" Bin "+partWhse[k].PrimBinNum+'</b><br>';
						        	} else {
						        		valueString += '<b>'+partWhse[k].OnHandQty+" "+partWhse[k].PartNumIUM+" in "+partWhse[k].WarehouseDescription+" Bin "+partWhse[k].PrimBinNum+'</b><br>';
						        	}
						        }

						        if(valueString.trim() == ""){
									valueString += '<b style="color:#ca0000;">Missing Primary Bin</b><br>';
						        }

						        x[j].innerHTML = valueString;
				        	}
				        }

				    }
				);
			}
    	}
    </script>
  </body>
</html>