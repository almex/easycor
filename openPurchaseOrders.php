<?php
session_start();

include_once 'inc/global.php';
include_once 'inc/inventoryFunctions.php';
include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function htmlEcho($string){
	echo htmlspecialchars($string);
}

function getEmails($plant){
  $emailDB = new PDO('sqlite:db/openPOCron.db');
  $qry = $emailDB->prepare("SELECT * FROM emails WHERE plant = ?");

  $ret = $qry->execute(array($plant));
  if($ret == false){
    return array();
  }

  $emails = $qry->fetchAll();
  if($emails == false){
    return array();
  }

  return $emails;
}

function addEmail($email,$plant){
	$emailDB = new PDO('sqlite:db/openPOCron.db');
	$qry = $emailDB->prepare("INSERT INTO emails (email,plant) VALUES (?,?)");
	$ret = $qry->execute(array($email,$plant));
	if(!$ret) {
    	$GLOBALS["error"] = true;
    	$GLOBALS["errorText"] = "Unable to insert into database";
    	return false;
  	}

  return true;
}

function deleteEmail($emailID){
  $emailDB = new PDO('sqlite:db/openPOCron.db');
  $qry = $emailDB->prepare(
  'DELETE FROM emails WHERE id=?');
  $ret = $qry->execute(array($emailID));
  if($ret == false){
  	$GLOBALS["error"] = true;
    $GLOBALS["errorText"] = "Unable to delete email";
    return false;
  } else {
    return true;
  }
}

$plantsResult = $api->getAllPlants();

if(isset($_GET["plant"]) && isset($_POST["email"])){
	$_POST["email"] = strtolower(trim($_POST["email"]));
	$emails = getEmails($_GET["plant"]);

	$alreadyExists = false;
	foreach ($emails as $email) {
		if($email["email"] == $_POST["email"]){
			$alreadyExists = true;
		}
	}

	if(!$alreadyExists){
		addEmail($_POST["email"],$_GET["plant"]);
	}
}

if(isset($_GET["plant"]) && isset($_GET["remove_email"])){
	deleteEmail($_GET["remove_email"]);
}

if(isset($_GET["plant"])){
	$emails = getEmails($_GET["plant"]);
	$purchaseOrders = $api->getOpenPurchaseOrders($_GET["plant"]);
	hasError($purchaseOrders);

	$sortedPurchaseOrders = array();
	foreach ($purchaseOrders["result"]->returnObj->Results as $order) { 
		if(!isset($sortedPurchaseOrders["PO".$order->POHeader_PONum])){
			$sortedPurchaseOrders["PO".$order->POHeader_PONum] = array();
			$sortedPurchaseOrders["PO".$order->POHeader_PONum]["PONum"] = $order->POHeader_PONum;
			$sortedPurchaseOrders["PO".$order->POHeader_PONum]["Lines"] = array();
		}

		array_push($sortedPurchaseOrders["PO".$order->POHeader_PONum]["Lines"], $order);
	}
	$sortedPurchaseOrders = array_reverse($sortedPurchaseOrders);

	$step = 2;
}


?>

<!doctype html>
<html lang="en" style="background: #eaeaea;" class="notranslate" translate="no">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="google" content="notranslate" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
				
			<!-- Step One -->
			<?php if($step == 1) { ?>
				<center>

					<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Open Purchase Orders</b>
					</div>
					<div class="card-body">
						<center>
						<form action="openPurchaseOrders.php" method="GET">
							<br>
							<select class="form-select" aria-label="Plant" name="plant">
							<?php foreach($plantsResult["result"]->returnObj->PlantList as $plant){ 
								$selected = "";
						  		if($plant->Plant == 200){
						  			$selected = "selected";
						  		}
							?>
							  <option value="<?php echo $plant->Plant; ?>" <?php echo $selected;?> ><?php echo $plant->Name; ?></option>
							 <?php } ?>
							</select>
							<br>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
			
				</center>
			<?php } ?>

			<!-- Step Two -->
			<?php if($step == 2) { ?>

				<center>
				<br>
				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">Open purchase orders in plant <?php htmlEcho($_GET["plant"]); ?></h5>
					</div>
				</div>
				<br>

				<div class="card text-white bg-dark" style="max-width: 600px;margin-top:20px;margin-bottom: 40px;">
					<div class="card-body">
						<h5 class="card-title">Email Notifications</h5>
						<form 
							action="openPurchaseOrders.php?plant=<?php htmlEcho($_GET["plant"]); ?>" 
							method="POST" 
							style="margin:30px;max-width:300px;">
							<div class="input-group mb-3">
								<input type="text" name="email" placeholder="Email" class="form-control">
								<button type="submit" class="btn btn-primary"><b>Add</b></button>
							</div>
						</form>

						<table style="margin:10px;">
						<?php foreach ($emails as $email) { ?>
							<tr>
								<td><?php echo $email["email"]; ?></td>
								<td>
									<a href="openPurchaseOrders.php?plant=<?php htmlEcho($_GET["plant"]); ?>&remove_email=<?php echo $email["id"]; ?>" >
									<img src="img/trash.png" style="max-width:25px; margin-left: 15px;">
									</a>
								</td>
							</tr>	
						<?php } ?>
						</table>
					</div>
				</div>
				<br>


				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($sortedPurchaseOrders as $order) { 
					  		
					  		if(isset($order["Lines"][0]) && strlen(trim($order["Lines"][0]->POHeader_OrderDate)) > 0){
					  			$orderDate = explode("T", $order["Lines"][0]->POHeader_OrderDate)[0];
					  			$orderDate = DateTime::createFromFormat('Y-m-d',$orderDate);
					  			$orderDate = date_format($orderDate,"F j Y");
							} else {
								$orderDate = "Unknown";
							}

							$dueDatePassed = false;
					  		if(isset($order["Lines"][0]) && strlen(trim($order["Lines"][0]->PORel_DueDate)) > 0){
					  			$dueDate = explode("T", $order["Lines"][0]->PORel_DueDate)[0];
							} else {
								$dueDatePassed = true;
								$dueDate = "Unknown";
							}

							if(isset($order["Lines"][0]) && strlen(trim($order["Lines"][0]->POHeader_EntryPerson)) > 0){
					  			$person = $order["Lines"][0]->POHeader_EntryPerson;
							} else {
								$person = "Unknown";
							}

							if(!$dueDatePassed){
								$newDueDate = DateTime::createFromFormat('Y-m-d',$dueDate);
								$dueDate = date_format($newDueDate,"F j Y");

								$now = new DateTime();
								if($newDueDate < $now){
									$dueDatePassed = true;
								}
							}


					  	?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col" colspan="4"><center>Purchase Order #<?php htmlEcho($order["PONum"]); ?> from <?php htmlEcho($order["Lines"][0]->Vendor_Name); ?></center></th>
						    </tr>
					  		<tr>
					  		  <th scope="col" >Line</th>
					  		  <th scope="col" colspan="2">Part</th>
					  		  <th scope="col" >Qty</th>
						    </tr>
						    <?php foreach ($order["Lines"] as $line) { ?>
						    	<tr>
						    		<td><b>#<?php htmlEcho($line->PODetail_POLine); ?></b></td>
						    		<td colspan="2"><?php htmlEcho($line->PODetail_PartNum); ?></td>
						    		<td><?php htmlEcho($line->PORel_ReceivedQty); echo "/"; htmlEcho($line->PORel_RelQty); echo " "; htmlEcho($line->PORel_BaseUOM); ?></td>
						    	</tr>
						    	<tr>
						    		<td colspan="4"><?php htmlEcho($line->PODetail_LineDesc); ?></td>
						    	</tr>
						    <?php } ?>
						    <tr>
						    	<td colspan="4"><center>Ordered on <b><?php htmlEcho($orderDate); ?></b> by <?php htmlEcho($person); ?></center></td>
						    </tr><tr>
						    	<?php if($dueDatePassed) {?>
						    		<td colspan="4" style="color:#da0000;"><center>Due on <b><?php htmlEcho($dueDate); ?></b></center></td>
						    	<?php } else { ?>
						    		<td colspan="4"><center>Due on <b><?php htmlEcho($dueDate); ?></b></center></td>
						    	<?php } ?>
						    </tr>
					  		<tr>
			    				<td colspan="4">&nbsp;</td>
			    			</tr>
			    		<?php } ?>
					  </tbody>
					</table>
				</div>
				</center>
			<?php } ?>

				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>