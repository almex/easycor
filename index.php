<?php
session_start();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}

if(isset($_SESSION["continue"])){
	$continue = $_SESSION["continue"];
	unset($_SESSION["continue"]);
	header("Location: ".$continue);
	die();
}

include_once 'inc/api.php';
$api = new API();

$error = false;
$errorText = "";

if(isset($_POST["clockin"])){
	//$clockInResult = $api->clockIn($_SESSION["empNum"],1);
	//if($clockInResult["http"] != 200){
	//	$error = true;
	//	$errorText = "ERR1: Unable to clock in";
	//}

	$error = true;
	$errorText = "This feature has been disabled<br><br>Clock in and out at a local computer";
}

if(isset($_POST["clockout"])){
	//$clockOutResult = $api->clockOut($_SESSION["empNum"]);
	//if($clockOutResult["http"] != 200){
	//	$error = true;
	//	$errorText = "ERR2: Unable to clock out <br><br> If you are clocked into a job clock out and try again";
	//}

	$error = true;
	$errorText = "This feature has been disabled<br><br>Clock in and out at a local computer";
}


//Check clock in status
$checkClockInStatus = $api->checkClockInStatus($_SESSION["empNum"]);
$clockedIn = $checkClockInStatus["result"]->parameters->clockIn == 1;

//In Office
$inOffice = false;
if($_SERVER['REMOTE_ADDR'] == "72.38.180.229" || $_SERVER['REMOTE_ADDR'] == "72.38.180.226" || $_SERVER['REMOTE_ADDR'] == "72.38.196.34" || $_SERVER['REMOTE_ADDR'] == "::1"){
	$inOffice = true;
}

//Admin
$isAdmin = false;
if($_SESSION["empNum"] == "564" || $_SESSION["empNum"] == "613" || $_SESSION["empNum"] == "830"){
	$isAdmin = true;
}

if(isset($_GET["endJobWarning"])){
	$error = true;
	switch ($_GET["endJobWarning"]) {
		case 1:
			$errorText = "You must be in office to clock out of a job";
			break;
		case 2:
			$errorText = "You must be in office to clocked in";
			break;
		case 3:
			$errorText = "You are not clocked into a job";
			break;
		default:
			$errorText = "Cannot end job";
			break;
	}
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">	
	<?php include_once 'inc/header.php'; ?>
  
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;margin-bottom:10px;">
		  <b><?php echo $errorText; ?></b>
		</div></center>
	<?php } ?>
  
    <div class="container" style="margin-top:60px;">
  	<?php if($inOffice) { ?>
		  <div class="row">
			<div class="col-sm">
				
				<!-- clocked in status -->
				<div class="card" style="margin:20px;">
					<center><br>
					<?php if($clockedIn) { ?>
						<img src="img/user_green.png" class="card-img-top" style="max-width:100px;max-height:100px;">
					<?php } else { ?>
						<img src="img/user_red.png" class="card-img-top" style="max-width:100px;max-height:100px;">
					<?php } ?>
					</center>
				  <div class="card-body">
					<p class="card-text">
						<center><b>
						<?php if($clockedIn) { ?>
							You are clocked in
						<?php } else { ?>
							You are clocked out
						<?php } ?>
						</b></center>
					</p>
					<center>
					
					<!-- clock in / clock out button
					<form action="index.php" method="POST">
						<?php if($clockedIn) { ?>
							<input type="hidden" name="clockout" value="1">
							<button type="submit" class="btn btn-primary"><b>Clock Out</b></button>
						<?php } else { ?>
							<input type="hidden" name="clockin" value="1">
							<button type="submit" class="btn btn-primary"><b>Clock In</b></button>
						<?php } ?>
					</form> -->
					
					</center>
				  </div>
				</div>

			</div>
			
			</div>
				<div class="row">

			<!-- Start production activity -->
			<?php if($clockedIn) { ?>
			
				<div class="col-sm">
				  
					<!-- Start production activity -->
					<div class="card" style="margin:20px;">
					<br><center>
					  <img src="img/work.png" class="card-img-top" style="max-width:100px;max-height:100px;">
					  </center>
					  <div class="card-body">
						<p class="card-text"><b><center>Have a job number?</center></b></p>
						<center><a href="startJob.php" class="btn btn-primary"><b><center>Clock into job</center></b></a></center>
					  </div>
					</div>

				</div>
				
				<div class="col-sm">
				  
					<!-- Start production activity -->
					<div class="card" style="margin:20px;">
					<br><center>
					  <img src="img/clockout.png" class="card-img-top" style="max-width:100px;max-height:100px;">
					  </center>
					  <div class="card-body">
						<p class="card-text"><b><center>Already on a job?</center></b></p>
						<center><a href="endJob.php" class="btn btn-primary"><b><center>Clock out of job</center></b></a></center>
					  </div>
					</div>

				</div>
				
			<?php } ?>
		</div>

	<?php } ?>

		<!-- part finders -->
		<div class="row">
		
			<div class="col-sm">
				<!-- Start production activity -->
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/part.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Have a part number?</center></b></p>
					<center><a href="partFind.php" class="btn btn-primary"><b><center>Find Part</center></b></a></center>
				  </div>
				</div>
			</div>

			<div class="col-sm">
				<!-- Start production activity -->
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/trans.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Have a part number?</center></b></p>
					<center><a href="partTrans.php" class="btn btn-primary"><b><center>Transaction History</center></b></a></center>
				  </div>
				</div>
			</div>
			
		  </div>


		<div class="row">

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/jobFind.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Need a Job Number?</center></b></p>
					<center><a href="jobFind.php" class="btn btn-primary"><b><center>Job Find</center></b></a></center>
				  </div>
				</div>
			</div>

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/po.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Have a PO number?</center></b></p>
					<center><a href="poPartFind.php" class="btn btn-primary"><b><center>Find Parts</center></b></a></center>
				  </div>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/pickList.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Have a Job Number?</center></b></p>
					<center><a href="pickList.php" class="btn btn-primary"><b><center>Get Pick List</center></b></a></center>
				  </div>
				</div>
			</div>

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/inv.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Need to order parts?</center></b></p>
					<center><a href="inventoryPlanner.php" class="btn btn-primary"><b><center>Inventory Planner</center></b></a></center>
				  </div>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/cart.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Have a part number?</center></b></p>
					<center><a href="partPurHist.php" class="btn btn-primary"><b><center>Purchase History</center></b></a></center>
				  </div>
				</div>
			</div>

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/tag.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Starting an inventory count?</center></b></p>
					<center><a href="countCycleView.php" class="btn btn-primary"><b><center>Tag Viewer</center></b></a></center>
				  </div>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/10.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Have a packing Slip?</center></b></p>
					<center><a href="receivePO.php" class="btn btn-primary"><b><center>Receive PO</center></b></a></center>
				  </div>
				</div>
			</div>

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/training.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Need training?</center></b></p>
					<center><a href="/training" class="btn btn-primary"><b><center>Training Area</center></b></a></center>
				  </div>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/sales.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Need to Find a Sales Order?</center></b></p>
					<center><a href="salesFind.php" class="btn btn-primary"><b><center>Sales Find</center></b></a></center>
				  </div>
				</div>
			</div>

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/transfer.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Need to Find a Transfer Order?</center></b></p>
					<center><a href="transferFind.php" class="btn btn-primary"><b><center>Transfer Find</center></b></a></center>
				  </div>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/material.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Have a job number?</center></b></p>
					<center><a href="massIssue.php" class="btn btn-primary"><b><center>Issue Material</center></b></a></center>
				  </div>
				</div>
			</div>

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/adp.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>ADP Transfer & Sales Orders</center></b></p>
					<center><a href="adpOrders.php" class="btn btn-primary"><b><center>ADP Orders</center></b></a></center>
				  </div>
				</div>
			</div>
			

		</div>

		<div class="row">

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/partPlanner.jpg" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Have a part number?</center></b></p>
					<center><a href="partPlanner.php" class="btn btn-primary"><b><center>Part Planner</center></b></a></center>
				  </div>
				</div>
			</div>

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/import.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Starting an inventory count?</center></b></p>
					<center><a href="countCycleImport.php" class="btn btn-primary"><b><center>Count Cycle Import</center></b></a></center>
				  </div>
				</div>
			</div>

		</div>

		<?php if($api->isPilot() || (isset($_SESSION["pilot"]) && $_SESSION["pilot"] == 1)) { ?>

			<div class="row">

				<div class="col-sm">
					<div class="card" style="margin:20px;">
					<br><center>
					  <img src="img/epicor_logo.png" class="card-img-top" style="max-width:200px;min-height:100px;">
					  </center>
					  <div class="card-body">
						<p class="card-text"><b><center>Pilot Epicor Web</center></b></p>
						<center><a href="https://ypds1230.ca.almex.com/EpicorERPPilot/Apps/Erp/Home/#/home" target="_blank" class="btn btn-primary"><b><center>View</center></b></a></center>
					  </div>
					</div>
				</div>

				<div class="col-sm">
					<div class="card" style="margin:20px;">
					<br><center>
					  <img src="img/epicor_logo.png" class="card-img-top" style="max-width:200px;min-height:100px;">
					  </center>
					  <div class="card-body">
						<p class="card-text"><b><center>Pilot Epicor MES</center></b></p>
						<center><a href="https://ypds1230.ca.almex.com/EpicorERPPilot/Apps/Erp/Home/#/home?mode=mes" target="_blank" class="btn btn-primary"><b><center>View</center></b></a></center>
					  </div>
					</div>
				</div>

			</div>

		<?php } else { ?>

			<div class="row">

				<div class="col-sm">
					<div class="card" style="margin:20px;">
					<br><center>
					  <img src="img/epicor_logo.png" class="card-img-top" style="max-width:200px;min-height:100px;">
					  </center>
					  <div class="card-body">
						<p class="card-text"><b><center>Epicor Web</center></b></p>
						<center><a href="https://ypds1230.ca.almex.com/EpicorERP/Apps/Erp/Home/#/home" target="_blank" class="btn btn-primary"><b><center>View</center></b></a></center>
					  </div>
					</div>
				</div>

				<div class="col-sm">
					<div class="card" style="margin:20px;">
					<br><center>
					  <img src="img/epicor_logo.png" class="card-img-top" style="max-width:200px;min-height:100px;">
					  </center>
					  <div class="card-body">
						<p class="card-text"><b><center>Epicor MES</center></b></p>
						<center><a href="https://ypds1230.ca.almex.com/EpicorERP/Apps/Erp/Home/#/home?mode=mes" target="_blank" class="btn btn-primary"><b><center>View</center></b></a></center>
					  </div>
					</div>
				</div>

			</div>

		<?php } ?>

		<div class="row">

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/purchaseorder.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Open Purchase Orders</center></b></p>
					<center><a href="openPurchaseOrders.php" class="btn btn-primary"><b><center>Purchase Orders</center></b></a></center>
				  </div>
				</div>
			</div>

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/neg.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Negative Quantity Check</center></b></p>
					<center><a href="negOnHand.php" class="btn btn-primary"><b><center>Check</center></b></a></center>
				  </div>
				</div>
			</div>

		</div>

	<!-- Admin -->
	<?php if($isAdmin){ ?>
		<div class="row">

			<div class="col-sm">
				<div class="card" style="margin:20px;">
				<br><center>
				  <img src="img/trace.png" class="card-img-top" style="max-width:100px;max-height:100px;">
				  </center>
				  <div class="card-body">
					<p class="card-text"><b><center>Have a trace file?</center></b></p>
					<center><a href="xml.php" class="btn btn-primary"><b><center>Parse</center></b></a></center>
				  </div>
				</div>
			</div>

		</div>
	<?php } ?>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>