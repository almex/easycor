<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		return true;
	}
	
	return false;
}

function updateDS($result){
	$_SESSION["pickList"]["ds"] = (array) $result["result"]->parameters->ds;
}

function getAssemblies($api){
	$result = $api->getNewMassIssueInput();
	if(hasError($result)){
		return false;
	} else {
		updateDS($result);
	}

	$result = $api->massIssueChangeJobNum($_SESSION["pickList"]["jobNum"],$_SESSION["pickList"]["ds"]);
	if(hasError($result)){
		return false;
	} else {
		updateDS($result);
		return true;
	}
}

function setSelectedAssembly($api){
	foreach ($_SESSION["pickList"]["ds"]["MassIssueInput"] as $key => $value) {
		if($value->AssemblySeq == $_SESSION["pickList"]["assemblySeq"]){
			$_SESSION["pickList"]["rowID"] = $_SESSION["pickList"]["ds"]["MassIssueInput"][$key]->SysRowID;
			$_SESSION["pickList"]["ds"]["MassIssueInput"][$key]->RowMod = "U";
			$_SESSION["pickList"]["ds"]["MassIssueInput"][$key]->IncludeSubassemblies = true;
			return true;
		}
	}

	return false;
}

function getPickList($api){
	$result = $api->buildMassIssue($_SESSION["pickList"]["rowID"],$_SESSION["pickList"]["ds"]);
	if(hasError($result)){
		return false;
	} else {
		$_SESSION["pickList"]["list"] = $result["result"]->returnObj->MassIssue;
		return true;
	}
}

function getSubAssemblies(){
	$subAssem = array();
	foreach ($_SESSION["pickList"]["list"] as $part) {
		if($part->TranType == "STK-ASM"){
			array_push($subAssem, $part);
		}
	}
	$_SESSION["pickList"]["subAssemblies"] = $subAssem;
}

function compare($a, $b) {

	$aBin = $a->BinNum;
	$bBin = $b->BinNum;

   if(!is_numeric($aBin)){
   	$aBin = 0;
   }

   if(!is_numeric($bBin)){
   	$bBin = 0;
   }

   return $aBin - $bBin;
}

function pickedParts($job,$seq){
  $db = new PDO('sqlite:db/picklist.db');

  $qry = $db->prepare('SELECT part, picked FROM picked WHERE job=? AND seq=?');
  $ret = $qry->execute(array($job,$seq));
  $fetch = $qry->fetchAll();

  $return = array();
  foreach ($fetch as $part) {
  	$return[$part["part"]] = $part["picked"];
  }

  return $return;
}

//Step one submitted
if(isset($_POST["jobNum"])){
	$_SESSION["pickList"]["jobNum"] = $_POST["jobNum"];
	if(getAssemblies($api)){
		$step = 2;
	} else {
		$error = true;
		$errorText = "Unable to get assemblies for job";
	}
}

//Step two submitted
if(isset($_POST["assemblySeq"])){
	$_SESSION["pickList"]["assemblySeq"] = $_POST["assemblySeq"];
	if(setSelectedAssembly($api)){
		$step = 3;
	} else {
		$error = true;
		$errorText = "Unable to find selected assembly";
	}
}

if($step == 3){
	if(getPickList($api)){
		$_SESSION["pickList"]["assembly"] = $_SESSION["pickList"]["list"][0];
		//array_shift($_SESSION["pickList"]["list"]);
		getSubAssemblies();
		usort($_SESSION["pickList"]["list"],"compare");
		$step = 4;
	} else {
		$error = true;
		$errorText = "Unable to get pick list";
	}
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- Step One - Enter Job Number -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Pick List</b>
					</div>
					<div class="card-body">
						<center>
						<form action="pickList.php" method="POST">
							<br>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="Job Number" name="jobNum">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
				
			<!-- Step Two -->
			<?php if($step == 2) { 
				foreach ($_SESSION["pickList"]["ds"]["MassIssueInput"] as $assembly) {
			?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-body">
						<center>
						<h5 class="card-title"><?php echo $assembly->PartNumAsm; ?></h5>
						<p class="card-text"><?php echo $assembly->PartDescAsm; ?></p>
						
						<form style="max-width:350px;" action="pickList.php" method="POST">
						  <input type="hidden" name="assemblySeq" value="<?php echo $assembly->AssemblySeq; ?>">
						  <button type="submit" class="btn btn-primary"><b>Select</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } } ?>

			<!-- Step Three -->

			<!-- Step Four -->
			<?php if($step == 4) { 
				$count = 0;
			?>
			<center>

				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
				  <div class="card-body">
				    <h5 class="card-title"><b>Job #<?php echo $_SESSION["pickList"]["jobNum"]; ?></b></h5>
				  </div>
				</div>

			<!-- Sub assmblies -->
			<?php 
				foreach ($_SESSION["pickList"]["subAssemblies"] as $subAssem) { 
					$count = 0;
			?>

				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
				  <div class="card-body">
				    <h5 class="card-title"><?php echo $subAssem->PartNum; ?></h5>
				    <p class="card-text">Assembly #<?php echo $subAssem->AssemblySeq; ?></p>
				    <p class="card-text"><?php echo $subAssem->Description; ?></p>
				  </div>
				</div>

				<center>
				<a class="btn btn-primary" 
				   target="_blank"
				   style="max-width: 200px;font-weight:bold;" 
				   href="massIssue.php?jobNum=<?php echo urlencode($_SESSION["pickList"]["jobNum"]); ?>">Issue Materials</a>
				</center>
				<br><br>

				<!-- sub assem tables -->
				<div class="table-responsive" style="max-width: 600px;">
				<table class="table table-striped" style="font-size: 0.8rem;">
					<thead>
					    <tr style="border-bottom: solid #000 3px;">
					      <th scope="col">#</th>
					      <th scope="col">Mtl</th>
					      <th scope="col">Part</th>
					      <th scope="col">Qty</th>
					      <th scope="col"><center>Stock</center></th>
					    </tr>
					  </thead>
					  <tbody>
						<?php foreach ($_SESSION["pickList"]["list"] as $part) { 

							if($part->AssemblySeq != $subAssem->AssemblySeq){
								continue;
							}

							if($part->QtyRequired == 0){
								continue;
							}

							$pickedParts = pickedParts($_SESSION["pickList"]["jobNum"],$part->AssemblySeq);
							$count++;
						?>
							<tr>
								<td scope="row"><?php echo $count; ?></td>
								<td scope="row"><?php echo $part->SeqNum; ?></td>
								<td>
									<a href="partFind.php?part=<?php echo urlencode($part->PartNum); ?>" target="_blank" style="color:#000;font-size: 0.9rem;text-decoration: none;"><?php echo $part->PartNum ?>
									<?php if(!empty($part->RevisionNum)){ ?>
										&nbsp;<img src="img/assemb.png" style="width:30px;height:30px;">
									<?php } ?>
									</a>
								</td>
								<!--<td><?php echo $part->Description ?></td>
								<td>
									<button type="button" class="btn btn-primary" part="<?php echo $part->PartNum ?>" desc="<?php echo htmlentities($part->Description); ?>" onclick="showPartModal(this)" style="font-size: 0.8rem;">
									  <b>Desc</b>
									</button>
								</td> -->

								<td><?php echo round($part->QtyRequired,2); ?> <?php echo $part->UnitMeasure ?></td>

								<?php if($part->StockQty < $part->QtyRequired) { ?>
									<td style="color:#920000;"><center><b><?php echo $part->StockQty ?> in <?php echo $part->WarehouseDescription ?> Bin <?php echo $part->BinNum ?></b></center></td>
								<?php } else { ?>
									<td><center><?php echo round($part->StockQty,2); ?> in <?php echo $part->WarehouseDescription ?> <b>&nbsp;Bin <?php echo $part->BinNum ?></b></center></td>
								<?php } ?>
							</tr>
							<tr>
								<td colspan="5"><?php echo $part->Description ?></td>
							</tr>
							<tr>
								<td colspan="5" style="border-bottom: solid #000 3px;font-size: 1.5rem;" >
									<center>
									<div class="form-check form-switch" style="width:10%">
									  <input class="picked-checkbox form-check-input" type="checkbox" jobnum="<?php echo $_SESSION["pickList"]["jobNum"]; ?>" partnum="<?php echo $part->PartNum; ?>" seq="<?php echo $part->AssemblySeq; ?>" 
									  	  <?php if(isset($pickedParts[$part->PartNum]) && $pickedParts[$part->PartNum] == 1){ echo "checked"; }; ?> >
									</div>
									</center>
								</td>
							</tr>

						<?php } ?>
				  </tbody>
				</table>
			</div>

			<?php } ?>


			</center>
			<?php } ?>
				
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	

	<!-- modal -->
	<div class="modal" tabindex="-1" id="partModal" style="margin-top: 80px;">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="partModalTitle"></h5>
	        <button type="button" class="btn-close" onclick="closePartModal()" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	        <p id="partModalText"></p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" onclick="closePartModal()">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script language="JavaScript" type="text/javascript">
    	$('.picked-checkbox').change(function() {
		    var jobNum =  $(this).attr("jobnum");
		    var partnum =  $(this).attr("partnum");
		    var seq =  $(this).attr("seq");

		    if (this.checked) {
		        var picked = 1;
		    } else {
		        var picked = 0;
		    }

		    $.post(
			    "inc/setPicked.php",
			    { partnum:  partnum, job: jobNum, seq: seq, picked: picked},
			    function(data) {
			    	console.log(data);
				}
			);
		});

		function showPartModal(button) {
		  document.getElementById("partModalTitle").innerHTML = button.getAttribute("part");
		  document.getElementById("partModalText").innerHTML = button.getAttribute("desc");
		  $("#partModal").modal().show();
		}

		function closePartModal() {
		  $("#partModal").modal().hide();
		}
	</script>
  </body>
</html>