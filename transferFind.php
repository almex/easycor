<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

$allPlants = $api->getAllPlants();
hasError($allPlants);

if(isset($_GET["toNum"])){
	$_POST["toNum"] = $_GET["toNum"];
}

//Step one submitted
if(isset($_POST["fromPlant"]) && isset($_POST["toPlant"]) && isset($_POST["search"])){
	$transferOrders = $api->getTransferOrders($_POST["fromPlant"],$_POST["toPlant"],trim($_POST["search"]));
	if(!hasError($transferOrders)){
		usort($transferOrders["result"]->returnObj->Results, function($a, $b) {
		    return $b->TFOrdHed_TFOrdNum <=> $a->TFOrdHed_TFOrdNum;
		});
		$step = 2;
	}
}

if(isset($_POST["toNum"])){
	$transferOrders = $api->getTransferOrder(trim($_POST["toNum"]));
	if(!hasError($transferOrders)){
		usort($transferOrders["result"]->returnObj->Results, function($a, $b) {
		    return $a->TFOrdDtl_TFOrdLine <=> $b->TFOrdDtl_TFOrdLine;
		});
		$step = 2;
	}
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<br>
				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">Only shows <b>OPEN</b> transfer orders</h5>
					</div>
				</div>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Transfer Order Find</b>
					</div>
					<div class="card-body">
						<center>
						<form action="transferFind.php" method="POST">
							<br>
						  <div class="mb-3">
							<input class="form-control" autocomplete="off" placeholder="TO Num" name="toNum">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Find</b></button>
						</form>
						</center>
					</div>
				</div>
				<br>
				<b> OR </b>
				<br>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Transfer Order Search</b>
					</div>
					<div class="card-body">
						<center>
						<form action="transferFind.php" method="POST">
							<br>
						  <div class="mb-3">
						  	<label class="form-label"><b>From:</b></label>
						  	<select class="form-select" aria-label="select" name="fromPlant">
						  		<?php 
						  			foreach ($allPlants["result"]->returnObj->PlantList as $plant) {
						  				echo '<option value="'.$plant->Plant.'" selected>'.$plant->Name.'</option>';
						  			}
						  		?>
							</select>
							<br>
							<label class="form-label"><b>To:</b></label>
							<select class="form-select" aria-label="select" name="toPlant">
						  		<?php 
						  			foreach ($allPlants["result"]->returnObj->PlantList as $plant) {
						  				echo '<option value="'.$plant->Plant.'" selected>'.$plant->Name.'</option>';
						  			}
						  		?>
							</select>

							<br>
							<br>
							<input class="form-control" autocomplete="off" placeholder="Search" name="search">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Search</b></button>
						</form>
						</center>
					</div>
				</div>
				</center>
			<?php } ?>
				
			<!-- Step Two -->
			<?php if($step == 2) { ?>
				<center>
				<?php if(isset($_POST["toNum"])){ ?>
					<br>
					<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
						<div class="card-body">
							<h5 class="card-title">Transfer Order #<?php echo $_POST["toNum"]; ?></h5>
							<?php if(isset($transferOrders["result"]->returnObj->Results[0]->TFOrdHed_EntryPerson))?>
								<br><h5 class="card-title">Entry Person: <?php echo $transferOrders["result"]->returnObj->Results[0]->TFOrdHed_EntryPerson; ?></h5>
							<?php ?>
						</div>
					</div>
				<?php } ?>
				<br>
				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($transferOrders["result"]->returnObj->Results as $toDetail) { 

					  			if(strlen(trim($toDetail->TFOrdDtl_NeedByDate)) > 0){
					  				$toDetail->TFOrdDtl_NeedByDate = explode("T", $toDetail->TFOrdDtl_NeedByDate)[0];
					  			}

					  			if(strlen(trim($toDetail->TFOrdDtl_RequestDate)) > 0){
					  				$toDetail->TFOrdDtl_RequestDate = explode("T", $toDetail->TFOrdDtl_RequestDate)[0];
					  			}
					  		?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col">TO Num</th>
						      <th scope="col">TO Line</th>
						      <th scope="col">Part</th>
						      <th scope="col">Sent Qty</th>
						    </tr>
					  		<tr>
					  			<td><a target="_blank" href="transferFind.php?toNum=<?php echo $toDetail->TFOrdHed_TFOrdNum; ?>"><?php echo $toDetail->TFOrdHed_TFOrdNum; ?></a></td>
					  			<td><?php echo $toDetail->TFOrdDtl_TFOrdLine; ?></td>
					  			<td><a target="_blank" href="partFind.php?part=<?php echo urlencode($toDetail->TFOrdDtl_PartNum); ?>"><?php echo $toDetail->TFOrdDtl_PartNum; ?></a></td>
					  			<td><?php echo $toDetail->TFOrdDtl_OurStockShippedQty."/".$toDetail->TFOrdDtl_SellingQty; ?></td>
					  		</tr>
					  		<tr>
						      <th scope="col">From</th>
						      <th scope="col">To</th>
						      <th scope="col">NeedBy Date</th>
						      <th scope="col">Request Date</th>
						    </tr>
						    <tr>
					  			<td><?php echo $toDetail->TFOrdHed_Plant; ?></td>
					  			<td><?php echo $toDetail->TFOrdHed_ToPlant; ?></td>
					  			<td><?php echo $toDetail->TFOrdDtl_NeedByDate; ?></td>
					  			<td><?php echo $toDetail->TFOrdDtl_RequestDate; ?></td>
					  		</tr>
					  		<tr>
						      <th scope="col" colspan="4">Entry Person</th>
						    </tr>
					  		<tr>
					  			<td colspan="4"><?php echo $toDetail->TFOrdHed_EntryPerson; ?></td>
					  		</tr>
					  		<tr>
						      <th scope="col" colspan="4">Description</th>
						    </tr>
					  		<tr>
					  			<td colspan="4"><?php echo $toDetail->Part_PartDescription; ?></td>
					  		</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>
			</center>
			<br>
			<center>
				<a href="index.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Home</center></b></a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="transferFind.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Find another</center></b></a>
			</center>
			<br>

			<?php } ?>

				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>