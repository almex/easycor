<?php
ini_set('session.gc_maxlifetime', 28800);
session_set_cookie_params(28800);
session_start();
include_once 'inc/global.php';

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
  header("Location: login.php");
  die();
}

$tags = getTags($_GET["part"],$_GET["bin"],$_GET["warehouse"]);

function getTags($part,$bin,$warehouse){
	$db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
	$qry = $db->prepare('SELECT * FROM tags WHERE PartNum=? AND BinNum =? AND WarehouseCode =?');
	$ret = $qry->execute(array($part,$bin,$warehouse));
	return $qry->fetchAll();
}

function getCounts($tagNum,$cycle){
	$db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
	$qry = $db->prepare('SELECT * FROM count WHERE TagNum=? AND Cycle =?');
	$ret = $qry->execute(array($tagNum,$cycle));
	return $qry->fetchAll();
}

function getComments($tagNum,$cycle){
	$db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
	$qry = $db->prepare('SELECT * FROM comment WHERE TagNum=? AND Cycle =?');
	$ret = $qry->execute(array($tagNum,$cycle));
	return $qry->fetchAll();
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
  <?php include_once 'inc/header.php'; ?>

  <center>

  	<br>
  	<br>
  	<div class="alert alert-dark" role="alert" style="margin:20px;max-width:500px;">
  	  <b style="font-size: 1.4rem;">Inventory history</b><br><br>
      <b><?php echo $_GET["part"];?><br>Bin: <?php echo $_GET["bin"];?></b>
    </div>
    <br>
    <br>

  <div class="table-responsive" style="max-width: 600px;">
	  <table class="table" style="font-size: 0.8rem;">
	    <thead>
	        <tr style="border-bottom: solid #000 3px;">
	          <th scope="col">Part</th>
	          <th scope="col">Tag</th>
	          <th scope="col">Bin</th>
	          <th scope="col">Cycle</th>
	          <th scope="col">UOM</th>
	        </tr>
	      </thead>

		  <?php foreach ($tags as $tag) { 
		  	$counts = getCounts($tag["TagNum"],$tag["Cycle"]);
		  	$comments = getComments($tag["TagNum"],$tag["Cycle"]);

		  	if(empty($counts) && empty($comments)){
		  		continue;
		  	}
		  ?>

		  	<tr style="background: #c5c5c5;border-top: solid 3px black;">
		  		<td><?php echo $tag["PartNum"]; ?></td>
		  		<td><?php echo $tag["TagNum"]; ?></td>
		  		<td><?php echo $tag["BinNum"]; ?></td>
		  		<td><b><?php echo $tag["Cycle"]; ?></b></td>
		  		<td><?php echo $tag["IUM"]; ?></td>
		  	</tr>

		  	<?php if(!empty($counts)) { ?> 
              <tr >
                <th scope="col" colspan="5"><center>Count</center></th>
              </tr>
              <?php foreach ($counts as $count) { ?>
                <tr>
                  <td colspan="2"><?php echo $count["Name"]; ?></td>
                  <td><?php echo $count["CountedQty"]; ?> <?php echo htmlspecialchars($tag["IUM"]); ?></td>
                </tr>
              <?php } ?>
            <?php } ?>

            <?php if(!empty($comments)) { ?> 
              <tr >
                <th scope="col" colspan="5"><center>Comments</center></th>
              </tr>
              <?php foreach ($comments as $comment) { ?>
                <tr>
                  <td colspan="3"><?php echo $comment["Name"]; ?>:</td>
                </tr>
                <tr>
                   <td colspan="3" style="word-break:break-word;"><?php echo htmlspecialchars($comment["Comment"]); ?></td>
                </tr>
              <?php } ?>
            <?php } ?>


		  <?php } ?>
		</table>
	</div>
</center>

  <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
  </body>
</html>
