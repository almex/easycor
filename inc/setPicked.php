<?php

function partExists($partnum,$job,$seq){
  $db = new PDO('sqlite:../db/picklist.db');

  $qry = $db->prepare(
  'SELECT * FROM picked WHERE part=? AND job=? AND seq=?');
  $ret = $qry->execute(array($partnum,$job,$seq));
  $part = $qry->fetch();

  if($part == false){
    return false;
  } else {
    return true;
  }
}

function insertPart($partnum,$job,$seq,$picked){
  $db = new PDO('sqlite:../db/picklist.db');
  $qry = $db->prepare(
  'INSERT INTO picked (part, job, seq, picked) VALUES (?, ?, ?, ?)');
  $ret = $qry->execute(array($partnum, $job, $seq, $picked));
  if(!$ret) {
    $error = true;
    $errorText = "Unable to insert part into database";
    return false;
  }

  return true;
}

function updatePart($partnum,$job,$seq,$picked){
	$db = new PDO('sqlite:../db/picklist.db');
	$qry = $db->prepare(
	'UPDATE picked SET picked=? WHERE part=? AND job=? AND seq=?');
	$ret = $qry->execute(array($picked, $partnum, $job, $seq));
	if(!$ret) {
	$error = true;
	$errorText = "Unable to insert part into database";
	return false;
	}

	return true;
}

if(isset($_POST["partnum"]) && isset($_POST["job"]) && isset($_POST["seq"]) && isset($_POST["picked"])){
	if(partExists($_POST["partnum"],$_POST["job"],$_POST["seq"])){
		updatePart($_POST["partnum"],$_POST["job"],$_POST["seq"],$_POST["picked"]);
	} else {
		insertPart($_POST["partnum"],$_POST["job"],$_POST["seq"],$_POST["picked"]);
	}
}

?>