<?php

include_once 'api.php';
$api = new API();
$result = $api->getPartPurchaseAdvisor($_POST["partNum"]);

if($result["http"] != 200){
	sleep(3);
	$result = $api->getPartPurchaseAdvisor($_POST["partNum"]);
}

echo json_encode($result);

?>