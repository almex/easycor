<?php
ini_set('max_execution_time', 0);
set_time_limit(0);

session_start();

//Epicor Login
$username = "";
$pass = "";

$_SESSION["key"] = base64_encode($username.":".$pass);

include_once 'global.php';
include_once 'inventoryFunctions.php';
include_once 'api.php';
include_once 'email.php';
$api = new API();

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function getReportPartNums($db,$reportID){
  $qry = $db->prepare("SELECT DISTINCT partNum FROM parts WHERE reportID=?");

  $ret = $qry->execute(array($reportID));
  if($ret == false){
    return array();
  }

  $nums = $qry->fetchAll();
  if($nums == false){
    return array();
  }

  return $nums;
}

function getKeywords(){
  $keywordDB = new PDO('sqlite:'.__DIR__.'/../db/adpKeyword.db');
  $qry = $keywordDB->prepare("SELECT * FROM keywords");

  $ret = $qry->execute(array());
  if($ret == false){
    return array();
  }

  $keywords = $qry->fetchAll();
  if($keywords == false){
    return array();
  }

  return $keywords;
}

function getEmails(){
  $keywordDB = new PDO('sqlite:'.__DIR__.'/../db/adpKeyword.db');
  $qry = $keywordDB->prepare("SELECT * FROM emails");

  $ret = $qry->execute(array());
  if($ret == false){
    return array();
  }

  $emails = $qry->fetchAll();
  if($emails == false){
    return array();
  }

  return $emails;
}


//Get keywords
$keywords = array();
$DBkeywords = getKeywords();
$DBEmails = getEmails();
foreach ($DBkeywords as $keyword) {
	switch ($keyword["type"]) {
		case 1:
			array_push($keywords, $keyword["keyword"]);
			break;
		case 2:
			$db = new PDO('sqlite:'.__DIR__.'/../db/inventory.db');

			if($keyword["keyword"] == "200-500"){
				//Exclude bin 100 from warehouse 200-500 (Mike dumped non ADP stuff in it)
				$inventoryPartNums = getUniquePartNums($db,$keyword["keyword"],true);
			} else {
				$inventoryPartNums = getUniquePartNums($db,$keyword["keyword"]);
			}

			foreach ($inventoryPartNums as $inventoryPart) {
				if(strlen(trim($inventoryPart["PartNum"])) > 0){
					if(!in_array($inventoryPart["PartNum"], $keywords)){
						array_push($keywords, $inventoryPart["PartNum"]);
					}
				}
			}
			break;
		case 3:
			$db = new PDO('sqlite:'.__DIR__.'/../db/inventoryPlanner.db');
			$reportPartNums = getReportPartNums($db,$keyword["keyword"]);
			foreach ($reportPartNums as $inventoryPart) {
				if(strlen(trim($inventoryPart["partNum"])) > 0){
					if(!in_array($inventoryPart["partNum"], $keywords)){
						array_push($keywords, $inventoryPart["partNum"]);
					}
				}
			}
			break;
	}
}

//Get Orders
$salesOrders = $api->getPlantSalesOrders("200");
$transferOrders = $api->getPlantTransferOrders("200");
$matchedSalesOrders = array();
$matchedTransferOrders = array();
$matchedSalesOrderNums = array();
$matchedTransferOrderNums = array();
$allPlants = $api->getAllPlants();

if(hasError($allPlants) || hasError($salesOrders) || hasError($transferOrders)){
	return;
}

foreach ($salesOrders["result"]->returnObj->Results as $soDetail){
	$matched = false;

	foreach ($keywords as $keyword) {
		if (strpos($soDetail->OrderDtl_LineDesc, $keyword) !== false) {
			if(!in_array($soDetail->OrderHed_OrderNum, $matchedSalesOrderNums)){
	    		array_push($matchedSalesOrderNums, $soDetail->OrderHed_OrderNum);
	    	}

	    	if($matched == false){
	    		array_push($matchedSalesOrders, $soDetail);
	    		$matched = true;
	    	}
		} else if (strpos($soDetail->OrderDtl_PartNum, $keyword) !== false) {
			if(!in_array($soDetail->OrderHed_OrderNum, $matchedSalesOrderNums)){
	    		array_push($matchedSalesOrderNums, $soDetail->OrderHed_OrderNum);
	    	}

	    	if($matched == false){
	    		array_push($matchedSalesOrders, $soDetail);
	    		$matched = true;
	    	}

		}
	}
}

foreach ($transferOrders["result"]->returnObj->Results as $toDetail){
	$matched = false;
	foreach ($keywords as $keyword) {
		if (strpos($toDetail->Part_PartDescription, $keyword) !== false) {
			if(!in_array($toDetail->TFOrdHed_TFOrdNum, $matchedTransferOrderNums)){
	    		array_push($matchedTransferOrderNums, $toDetail->TFOrdHed_TFOrdNum);
	    	}

	    	if($matched == false){
	    		array_push($matchedTransferOrders, $toDetail);
	    		$matched = true;
	    	}
		} else if (strpos($toDetail->TFOrdDtl_PartNum, $keyword) !== false) {
			if(!in_array($toDetail->TFOrdHed_TFOrdNum, $matchedTransferOrderNums)){
	    		array_push($matchedTransferOrderNums, $toDetail->TFOrdHed_TFOrdNum);
	    	}

	    	if($matched == false){
	    		array_push($matchedTransferOrders, $toDetail);
	    		$matched = true;
	    	}
		}
	}
}

//Match Sales Orders
$allMatchedOrders = array(
	"salesOrders" => array(),
	"transferOrders" => array()
);

foreach ($matchedSalesOrderNums as $soNum) {

	if(!in_array($soNum, $allMatchedOrders["salesOrders"])){
		$allMatchedOrders["salesOrders"][$soNum] = array();
		$allMatchedOrders["salesOrders"][$soNum]["lines"] = array();
	}

	foreach ($salesOrders["result"]->returnObj->Results as $salesOrder) {
		if($salesOrder->OrderHed_OrderNum == $soNum){
			$allMatchedOrders["salesOrders"][$soNum]["cust"] = $salesOrder->Customer_Name;
			$allMatchedOrders["salesOrders"][$soNum]["date"] = explode("T", $salesOrder->OrderHed_OrderDate)[0];
			break;
		}
	}

	foreach ($salesOrders["result"]->returnObj->Results as $salesOrder) {
		if($salesOrder->OrderHed_OrderNum != $soNum){
			continue;
		}

		array_push($allMatchedOrders["salesOrders"][$soNum]["lines"], $salesOrder);
	}
}

//Match Transfer Orders
foreach ($matchedTransferOrderNums as $toNum) {
	if(!in_array($toNum, $allMatchedOrders["transferOrders"])){
		$allMatchedOrders["transferOrders"][$toNum] = array();
		$allMatchedOrders["transferOrders"][$toNum]["lines"] = array();
	}

	foreach ($transferOrders["result"]->returnObj->Results as $transferOrder) {
		if($transferOrder->TFOrdHed_TFOrdNum == $toNum){
			$allMatchedOrders["transferOrders"][$toNum]["cust"] = $transferOrder->TFOrdHed_ToPlant;
			$allMatchedOrders["transferOrders"][$toNum]["date"] = explode("T", $transferOrder->TFOrdDtl_RequestDate)[0];
			break;
		}
	}	

	foreach ($allPlants["result"]->returnObj->PlantList as $plant) {
		if($plant->Plant == $allMatchedOrders["transferOrders"][$toNum]["cust"]){
			$allMatchedOrders["transferOrders"][$toNum]["cust"] = $plant->Name;
			break;
		}
	}

	$transferOrderLines = array();
	foreach ($transferOrders["result"]->returnObj->Results as $transferOrder) {

		if($transferOrder->TFOrdHed_TFOrdNum != $toNum){
			continue;
		} 

		array_push($allMatchedOrders["transferOrders"][$toNum]["lines"], $transferOrder);
	}
}

if(!file_exists("adpOrders.ser")){
	$ser = serialize($allMatchedOrders);
	$file = fopen('adpOrders.ser', 'wb');
	fwrite($file, $ser);
	sendEmail("scott.woodhouse@almex.com","ADP Orders Update",".ser file created");
	return;
}

$previousAllMatchedOrders = unserialize(file_get_contents("adpOrders.ser"));

//Test
//$allMatchedOrders["salesOrders"]["72974"]["lines"][0]->OrderDtl_SellingQuantity = 2;
//$allMatchedOrders["salesOrders"]["72974"]["lines"][0]->OrderDtl_PartNum = "SR-A10";

//$newLine = clone $allMatchedOrders["salesOrders"]["72974"]["lines"][0];
//$newLine->OrderRel_OrderLine = 2;
//array_push($allMatchedOrders["salesOrders"]["72974"]["lines"], $newLine);

//$newOrder = $allMatchedOrders["salesOrders"]["72974"];
//$allMatchedOrders["salesOrders"]["72975"] = $newOrder;

//array_shift($allMatchedOrders["salesOrders"]["72974"]["lines"]);
//unset($allMatchedOrders["salesOrders"]["72974"]);


//$allMatchedOrders["transferOrders"]["ATL000978"]["lines"][0]->TFOrdDtl_SellingQty = 10;
//$allMatchedOrders["transferOrders"]["ATL000978"]["lines"][0]->TFOrdDtl_PartNum = "SR-A10";

//$newLine = clone $allMatchedOrders["transferOrders"]["ATL000978"]["lines"][0];
//$newLine->TFOrdDtl_TFOrdLine = 2;
//array_push($allMatchedOrders["transferOrders"]["ATL000978"]["lines"], $newLine);

//$newOrder = $allMatchedOrders["transferOrders"]["ATL000978"];
//$allMatchedOrders["transferOrders"]["ATL000979"] = $newOrder;

//array_shift($allMatchedOrders["transferOrders"]["ATL000978"]["lines"]);
//unset($allMatchedOrders["transferOrders"]["ATL000978"]);


//Compare
$sendEmail = false;
$emailHTML = "<b>EasyCor ADP Orders Email</b> <br><br>";

//Sales Orders
foreach ($allMatchedOrders["salesOrders"] as $soNum => $soData) {

	//Check for new order
	if(!array_key_exists($soNum, $previousAllMatchedOrders["salesOrders"])){
		$sendEmail = true;
		$emailHTML .= "Sales Order <b>#".$soNum."</b> for customer ".htmlspecialchars($soData["cust"])." was added <br><br>";
		foreach ($soData["lines"] as $line) {
			$emailHTML .= "<b>Line</b> #".$line->OrderRel_OrderLine."<br>";
			$emailHTML .= "<b>Part</b> #".htmlspecialchars($line->OrderDtl_PartNum)."<br>";
			$emailHTML .= "<b>Desc:</b> ".htmlspecialchars($line->OrderDtl_LineDesc)."<br>";
			$emailHTML .= "<b>Qty:</b> ".$line->OrderDtl_SellingQuantity."<br><br>";
		}
		$emailHTML .= "<br><br>";
		continue;
	}

	//Check for new line or modified qty or part num
	foreach ($soData["lines"] as $line) {
		$lineFound = false;
		foreach ($previousAllMatchedOrders["salesOrders"][$soNum]["lines"] as $previousLine) {
			if($line->OrderRel_OrderLine == $previousLine->OrderRel_OrderLine){
				$lineFound = true;
			}
		}

		if(!$lineFound){
			$sendEmail = true;
			$emailHTML .= "Line #".$line->OrderRel_OrderLine." was added to SO #".htmlspecialchars($soNum)." with Part #".htmlspecialchars($line->OrderDtl_PartNum)."<br>";
		}

		foreach ($previousAllMatchedOrders["salesOrders"][$soNum]["lines"] as $previousLine) {
			if($line->OrderRel_OrderLine == $previousLine->OrderRel_OrderLine){
				if($line->OrderDtl_PartNum != $previousLine->OrderDtl_PartNum){
					$sendEmail = true;
					$emailHTML .= "SO #".htmlspecialchars($soNum)." Line #".$line->OrderRel_OrderLine." part number was changed to ".htmlspecialchars($line->OrderDtl_PartNum)." from ".htmlspecialchars($previousLine->OrderDtl_PartNum)."<br>";
				}

				if($line->OrderDtl_SellingQuantity != $previousLine->OrderDtl_SellingQuantity){
					$sendEmail = true;
					$emailHTML .= "SO #".htmlspecialchars($soNum)." Line #".$line->OrderRel_OrderLine." Part #".htmlspecialchars($line->OrderDtl_PartNum)." quantity was changed to ".$line->OrderDtl_SellingQuantity." from ".$previousLine->OrderDtl_SellingQuantity."<br>";
				}
			}
		}
	}
}

//Check for removed lines & SO
foreach ($previousAllMatchedOrders["salesOrders"] as $soNum => $soData) {

	if(!array_key_exists($soNum, $allMatchedOrders["salesOrders"])){
		$sendEmail = true;
		$emailHTML .= "Sales Order #".$soNum." has been removed/closed<br>";
		$emailHTML .= "<br>";
		foreach ($soData["lines"] as $line) {
			$emailHTML .= "<b>Line</b> #".$line->OrderRel_OrderLine."<br>";
			$emailHTML .= "<b>Part</b> #".htmlspecialchars($line->OrderDtl_PartNum)."<br>";
			$emailHTML .= "<b>Desc:</b> ".htmlspecialchars($line->OrderDtl_LineDesc)."<br>";
			$emailHTML .= "<b>Qty:</b> ".$line->OrderDtl_SellingQuantity."<br><br>";
		}
		$emailHTML .= "<br>";
		continue;
	}

	foreach ($soData["lines"] as $line) {
		$lineFound = false;
		foreach ($allMatchedOrders["salesOrders"][$soNum]["lines"] as $newLine) {
			if($line->OrderRel_OrderLine == $newLine->OrderRel_OrderLine){
				$lineFound = true;
			}
		}
	}

	if(!$lineFound){
		$sendEmail = true;
		$emailHTML .= "Line #".$line->OrderRel_OrderLine." was removed from SO #".htmlspecialchars($soNum)." with Part #".htmlspecialchars($line->OrderDtl_PartNum)."<br>";
	}
}

//Transfer Orders
foreach ($allMatchedOrders["transferOrders"] as $toNum => $toData) {
	if(!array_key_exists($toNum, $previousAllMatchedOrders["transferOrders"])){
		$sendEmail = true;
		$emailHTML .= "Transfer Order <b>#".$toNum."</b> from ".htmlspecialchars($toData["cust"])." was added <br><br>";
		foreach ($toData["lines"] as $line) {
			$emailHTML .= "<b>Line</b> #".$line->TFOrdDtl_TFOrdLine."<br>";
			$emailHTML .= "<b>Part</b> #".htmlspecialchars($line->TFOrdDtl_PartNum)."<br>";
			$emailHTML .= "<b>Desc:</b> ".htmlspecialchars($line->Part_PartDescription)."<br>";
			$emailHTML .= "<b>Qty:</b> ".$line->TFOrdDtl_SellingQty."<br><br>";
		}
		$emailHTML .= "<br><br>";
		continue;
	}

	foreach ($toData["lines"] as $line) {
		$lineFound = false;
		foreach ($previousAllMatchedOrders["transferOrders"][$toNum]["lines"] as $previousLine) {
			if($line->TFOrdDtl_TFOrdLine == $previousLine->TFOrdDtl_TFOrdLine){
				$lineFound = true;
			}
		}

		if(!$lineFound){
			$sendEmail = true;
			$emailHTML .= "Line #".$line->TFOrdDtl_TFOrdLine." was added to TO #".htmlspecialchars($toNum)." with Part #".htmlspecialchars($line->TFOrdDtl_PartNum)."<br>";
		}

		foreach ($previousAllMatchedOrders["transferOrders"][$toNum]["lines"] as $previousLine) {
			if($line->TFOrdDtl_TFOrdLine == $previousLine->TFOrdDtl_TFOrdLine){
				if($line->TFOrdDtl_PartNum != $previousLine->TFOrdDtl_PartNum){
					$sendEmail = true;
					$emailHTML .= "TO #".htmlspecialchars($toNum)." Line #".$line->TFOrdDtl_TFOrdLine." part number was changed to ".htmlspecialchars($line->TFOrdDtl_PartNum)." from ".htmlspecialchars($previousLine->TFOrdDtl_PartNum)."<br>";
				}

				if($line->TFOrdDtl_SellingQty != $previousLine->TFOrdDtl_SellingQty){
					$sendEmail = true;
					$emailHTML .= "TO #".htmlspecialchars($toNum)." Line #".$line->TFOrdDtl_TFOrdLine." Part #".htmlspecialchars($line->TFOrdDtl_PartNum)." quantity was changed to ".$line->TFOrdDtl_SellingQty." from ".$previousLine->TFOrdDtl_SellingQty."<br>";
				}
			}
		}
	}
}

//Check for removed lines & TO
foreach ($previousAllMatchedOrders["transferOrders"] as $toNum => $toData) {
	if(!array_key_exists($toNum, $allMatchedOrders["transferOrders"])){
		$sendEmail = true;
		$emailHTML .= "Transfer Order #".$toNum." has been removed/closed<br>";
		$emailHTML .= "<br>";
		foreach ($toData["lines"] as $line) {
			$emailHTML .= "<b>Line</b> #".$line->TFOrdDtl_TFOrdLine."<br>";
			$emailHTML .= "<b>Part</b> #".htmlspecialchars($line->TFOrdDtl_PartNum)."<br>";
			$emailHTML .= "<b>Desc:</b> ".htmlspecialchars($line->Part_PartDescription)."<br>";
			$emailHTML .= "<b>Qty:</b> ".$line->TFOrdDtl_SellingQty."<br><br>";
		}
		$emailHTML .= "<br>";
		continue;
	}

	foreach ($toData["lines"] as $line) {
		$lineFound = false;
		foreach ($allMatchedOrders["transferOrders"][$toNum]["lines"] as $newLine) {
			if($line->TFOrdDtl_TFOrdLine == $newLine->TFOrdDtl_TFOrdLine){
				$lineFound = true;
			}
		}
	}

	if(!$lineFound){
		$sendEmail = true;
		$emailHTML .= "Line #".$line->TFOrdDtl_TFOrdLine." was removed from TO #".htmlspecialchars($toNum)." with Part #".htmlspecialchars($line->TFOrdDtl_PartNum)."<br>";
	}
}

$emailHTML .= "<br><a target='_blank' href='https://easycor.dreamhosters.com/adpOrders.php'><b>View Orders</b></a>";

//Save
$ser = serialize($allMatchedOrders);
$file = fopen('adpOrders.ser', 'wb');
fwrite($file, $ser);

if($sendEmail){
	foreach ($DBEmails as $email) {
		sendEmail($email["email"],"ADP Orders Update",$emailHTML);
	}
}

?>