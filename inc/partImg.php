<?php

include_once 'api.php';
$api = new API();
$result = array();

$partInfoResult = $api->getPartInfo($_POST["partNum"]);
if($partInfoResult["http"] == 200){
	$result = $api->getImage($partInfoResult["result"]->returnObj->Part[0]->ImageID);
}

echo json_encode($result);

?>