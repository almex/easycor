<?php

include_once 'api.php';
$api = new API();

if(isset($_POST["partNum"]) && isset($_POST["rev"])){
	$methodParts = $api->getMethodParts($_POST["partNum"],$_POST["rev"]);
	if($methodParts["http"] == 200){

		$return = array();
		if(isset($_POST["id"])){
			$return["id"] = $_POST["id"];
		}
		$return["parts"] = $methodParts["result"]->returnObj->PartMtl;

		echo json_encode($return);
		die();
	}
}

echo json_encode(array());

?>