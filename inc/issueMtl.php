<?php

include_once 'api.php';
$api = new API();

$return = array(
  "result" => true,
  "partNum" => "",
  "error" => ""
);

function errorCheck($result){
	if($result["http"] != 200){
		$GLOBALS["return"]["result"] = false;
		$GLOBALS["return"]["error"]  = "API ERROR: ".$result["method"];
		echo json_encode($GLOBALS["return"]);
		die();
	}
}

if(!isset($_POST["jobNum"]) || 
	!isset($_POST["partNum"]) || 
	!isset($_POST["company"]) || 
	!isset($_POST["assembSeq"]) || 
	!isset($_POST["mtlSeq"]) || 
	!isset($_POST["qty"])){

	$return["result"] = false;
	$return["error"] = "Missing data";
	echo json_encode($GLOBALS["return"]);
	die();
}

if(isset($_POST["btnID"])){
	$return["btnID"] = $_POST["btnID"];
}

if(isset($_POST["qtyID"])){
	$return["qtyID"] = $_POST["qtyID"];
}

if(isset($_POST["fullAmount"])){
	$return["fullAmount"] = $_POST["fullAmount"];
}

if(isset($_POST["reqQty"])){
	$return["reqQty"] = $_POST["reqQty"];
}

$return["partNum"] = $_POST["partNum"];
$return["qty"] = $_POST["qty"];

$issueReturnDataSet = $api->getNewJobAsmblMultiple(
	$_POST["company"],
	$_POST["jobNum"],
	$_POST["assembSeq"]
);
errorCheck($issueReturnDataSet);

$issueReturnDataSet = $issueReturnDataSet["result"]->returnObj->IssueReturn[0];

$onChangingToJobSeq = $api->onChangingToJobSeq($_POST["mtlSeq"],$issueReturnDataSet);
errorCheck($onChangingToJobSeq);
$issueReturnDataSet = $onChangingToJobSeq["result"]->parameters->ds->IssueReturn[0];

$onChangeToJobSeq = $api->onChangeToJobSeq($_POST["mtlSeq"],$issueReturnDataSet);
errorCheck($onChangeToJobSeq);
$issueReturnDataSet = $onChangeToJobSeq["result"]->parameters->ds->IssueReturn[0];

$defaultFromWarehouse = $issueReturnDataSet->FromWarehouseCode; 
if(isset($_POST["warehouse"])){
	$defaultFromWarehouse = $_POST["warehouse"];
} 

$defaultFromBin = $issueReturnDataSet->FromBinNum;
if(isset($_POST["binNum"])){
	$defaultFromBin	 = $_POST["binNum"];
}

$defaultToWarehouse = $issueReturnDataSet->ToWarehouseCode; 
$defaultToBin = $issueReturnDataSet->ToBinNum;

$onChangeToWarehouse = $api->onChangeToWarehouse($defaultToWarehouse,$issueReturnDataSet);
errorCheck($onChangeToWarehouse);
$issueReturnDataSet = $onChangeToWarehouse["result"]->parameters->ds->IssueReturn[0];

$onChangeFromWarehouse = $api->onChangeFromWarehouse($defaultFromWarehouse,$issueReturnDataSet);
errorCheck($onChangeFromWarehouse);
$issueReturnDataSet = $onChangeFromWarehouse["result"]->parameters->ds->IssueReturn[0]; 

$onChangingFromBinNum = $api->onChangingFromBinNum($defaultFromBin,$issueReturnDataSet);
errorCheck($onChangingFromBinNum);
$issueReturnDataSet = $onChangingFromBinNum["result"]->parameters->ds->IssueReturn[0];

$onChangeFromBinNum = $api->onChangeFromBinNum($issueReturnDataSet);
errorCheck($onChangeFromBinNum);
$issueReturnDataSet = $onChangeFromBinNum["result"]->parameters->ds->IssueReturn[0];

if(isset($_POST["lot"]) && strlen(trim($_POST["lot"])) > 1){
	$onChangeLotNum = $api->onChangeLotNum($_POST["lot"],$issueReturnDataSet);
	errorCheck($onChangeLotNum);
	$issueReturnDataSet = $onChangeLotNum["result"]->parameters->ds->IssueReturn[0];	
}

$onChangeTranQty = $api->onChangeTranQty($_POST["qty"],$issueReturnDataSet);
errorCheck($onChangeTranQty);
$issueReturnDataSet = $onChangeTranQty["result"]->parameters->ds->IssueReturn[0];

//Make sure material is not already issued
if($issueReturnDataSet->QtyPreviouslyIssued+$_POST["qty"] > $issueReturnDataSet->QtyRequired){
	$return["result"] = false;
	$return["error"] = "Quantity would exceed the required quantity";
	echo json_encode($GLOBALS["return"]);
	die();
}

$prePerformMaterialMovement = $api->prePerformMaterialMovement($issueReturnDataSet);
errorCheck($prePerformMaterialMovement);
$issueReturnDataSet = $prePerformMaterialMovement["result"]->parameters->ds->IssueReturn[0];

$masterInventoryBinTests = $api->masterInventoryBinTests($issueReturnDataSet);
errorCheck($masterInventoryBinTests);
$issueReturnDataSet = $masterInventoryBinTests["result"]->parameters->ds->IssueReturn[0];

$performMaterialMovement = $api->performMaterialMovement($issueReturnDataSet);
errorCheck($performMaterialMovement);
$issueReturnDataSet = $performMaterialMovement["result"]->parameters->ds->IssueReturn[0]; 

echo json_encode($return);
?>