<?php 
include_once 'api.php';

class UOM {
	private $api;
	private $uomResponse;
	private $responsePassed;
	private $uomClass;
	private $uomConv;

	function __construct() {
		$this->api = new API();
	    $this->uomResponse = $this->api->getUoms();

	    if($this->uomResponse["http"] == 200){
	    	$this->responsePassed = true;
	    	$this->uomClass = $this->uomResponse["result"]->returnObj->UOMClass;
	    	$this->uomConv = $this->uomResponse["result"]->returnObj->UOMConv;
	    } else {
	    	$this->responsePassed = false;
	    }
	}

	function responsePassed(){
		return $this->responsePassed;
	}

	function convert($startUOM,$startQTY,$toUOM){
		if($startUOM == $toUOM){
			return $startQTY;
		}

		$startUomConv = $this->getUomConv($startUOM);
		if(empty($startUomConv)){
			//Not Found
			return $startQTY;
		}

		$toUomConv = $this->getUomConv($toUOM);
		if(empty($toUomConv)){
			//Not Found
			return $startQTY;
		}

		$sharedConv = array();
		foreach ($startUomConv as $startConv) {
			foreach ($toUomConv as $toConv) {
				if($startConv->UOMClassID == $toConv->UOMClassID){
					array_push($sharedConv, $startConv);
				}
			}
		}

		if(empty($sharedConv)){
			//Not Found
			return $startQTY;
		}

		$sharedConv = $sharedConv[0];

		//startQTY To Base UOM
		$converted = false;
		$baseUOM = $this->getConvBase($sharedConv->UOMClassID);
		foreach ($this->uomConv as $conv) {

			if($converted){
				break;
			}

			if($conv->UOMCode == $startUOM && $conv->UOMClassID == $sharedConv->UOMClassID){
				switch ($conv->ConvOperator) {
					case '*':
						$startQTY = $startQTY * $conv->ConvFactor;
						$converted = true;
						break;
					case '/':
						$startQTY = $startQTY / $conv->ConvFactor;
						$converted = true;
						break;
					default:
						//Not found
						return $startQTY;
				}
			}
		}

		if(!$converted){
			return $startQTY;
		}

		//Base UOM to Desired UOM
		$converted = false;
		foreach ($this->uomConv as $conv) {

			if($converted){
				break;
			}

			if($conv->UOMCode == $toUOM && $conv->UOMClassID == $sharedConv->UOMClassID){
				switch ($conv->ConvOperator) {
					case '*':
						$startQTY = $startQTY / $conv->ConvFactor;
						$converted = true;
						break;
					case '/':
						$startQTY = $startQTY * $conv->ConvFactor;
						$converted = true;
						break;
					default:
						//Not found
						return $startQTY;
				}
			}
		}

		return $startQTY;
	}

	private function getUomConv($startUOM){
		$conv = array();
		foreach ($this->uomConv as $uom) {
			if($uom->UOMSymbol == $startUOM){
				array_push($conv, $uom);
			}
		}

		return $conv;
	}

	private function getConvBase($classID){
		foreach ($this->uomClass as $class) {
			if($class->UOMClassID == $classID){
				return $class->BaseUOMCode;
			}
		}
	}
}
?>