<?php

if (session_status() === PHP_SESSION_NONE) {
	session_start();
}

$invDatabase = "inventory.db";
if((isset($_SESSION["pilot"]) && $_SESSION["pilot"] == 1)) { 
	$invDatabase = "pilotInventory.db";
}

?>