<?php
session_start();

include_once 'api.php';
include_once 'inventoryFunctions.php';
include_once 'global.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}

$partInfo = false;
$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

//Step 1
if(isset($_POST["partNum"])){
	$partInfo = $api->getPartInfo($_POST["partNum"]);
	if($partInfo["http"] == 200){
		$step = 2;
	} else {
		$error = true;
		$errorText = "Unable to get part information";
	}
}

//Step 2
if(isset($_POST["countedQTY"])){
	$epicorUpdated = false;
    $cycle = explode("-",$_POST["cycle"]);
	$db = new PDO('sqlite:../db/'.$invDatabase);

	$nextBlankTag = getNextBlankTag($db,$_POST["cycle"],$_POST["warehouse"]);
	if($nextBlankTag == false){
		$error = true;
	  	$errorText = "Unable to get next blank tag";
	} else {
		$nextBlankTag = $nextBlankTag["TagNum"];
	    $tagData = $api->getTagDataSet($_POST["plant"],$nextBlankTag,$_POST["warehouse"]);
	    if($tagData["http"] == 200){
	    $tagData = $api->setBlankTagPart($_POST["partNum"],$_POST["uom"],$tagData["result"]->returnObj->CCTag);
	      if($tagData["http"] == 200){
	          $tagData = $api->setBlankTagBin($_POST["bin"],$tagData["result"]->parameters->ds->CCTag);
	          if($tagData["http"] == 200){
	            $tagData = $api->setTagCount($_POST["countedQTY"],$tagData["result"]->parameters->ds->CCTag);
	            if($tagData["http"] == 200){

	              if(strlen(trim($_POST["serial"])) > 0){ 
	              	$tagData["result"]->parameters->ds->CCTag[0]->SerialNumber = $_POST["serial"];
	          	  }

	              $tagData = $api->updateTag($_POST["name"],$tagData["result"]->parameters->ds);
	              if($tagData["http"] == 200){
	                $epicorUpdated = true;
		          }
		        }
		      }
		   }
		}

	    if(!$epicorUpdated){
	      $error = true;
		  $errorText = "Unable to update epicor";
	      if(isset($tagData["result"]->ErrorMessage)){
	        $errorText = $tagData["result"]->ErrorMessage;
	      }
	    }

	    if($epicorUpdated){
	      	$db = new PDO('sqlite:../db/'.$invDatabase);
	  		if(countExists($db,$nextBlankTag,$_POST["warehouse"],$_POST["cycle"],$_POST["name"])){
	  			updateCount($db,$nextBlankTag,$_POST["warehouse"],$_POST["partNum"],$_POST["cycle"],$_POST["name"],$_POST["countedQTY"]);
	  		} else {
	  			insertCount($db,$nextBlankTag,$_POST["warehouse"],$_POST["partNum"],$_POST["cycle"],$_POST["name"],$_POST["countedQTY"]);
	  		}
	  		
	  		updateBlankTag($db,$nextBlankTag,$_POST["warehouse"],$_POST["partNum"],$_POST["partDesc"],$_POST["cycle"],$_POST["bin"],$_POST["serial"],$_POST["uom"]);
	  		$step = 3;
	    }
	}
}

?>

<!doctype html>
  
  <html lang="en" style="background: #fff;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  	<body style="background: #fff;">

  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>

	<?php if($step == 1) { ?>
		<center>
			<div class="card" style="max-width:500px;">
				<div class="card-body">
					<center>
					<form action="countCycleBlankTag.php" method="POST">
						<br>
					  <div class="mb-3">
						<input class="form-control" placeholder="Part Number" name="partNum">
						<input type="hidden" name="cycle" placeholder="cycle" value="<?php echo $_GET["cycle"]; ?>">
						<input type="hidden" name="warehouse" placeholder="warehouse" value="<?php echo $_GET["warehouse"]; ?>">
						<input type="hidden" name="plant" placeholder="plant" value="<?php echo $_GET["plant"]; ?>">
					  </div>
					  <br>
					  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
					</form>
					</center>
				</div>
			</div>
		</center>
	<?php } ?>


	<?php if($step == 2) { ?>
		<center>
			<div class="card" style="max-width:500px;">
				<div class="card-body">
					<center>
				    <b><?php echo htmlentities($partInfo["result"]->returnObj->Part[0]->PartNum); ?></b>
				    <br>

					<form action="countCycleBlankTag.php" method="POST">
					   <div class="input-group mb-3">
						  <input type="text" class="form-control" name="countedQTY" placeholder="Counted Qty" aria-label="Counted Qty" aria-describedby="basic-addon2">
						  <span class="input-group-text" id="basic-addon2"><?php echo htmlentities($partInfo["result"]->returnObj->Part[0]->IUM); ?></span>
						</div>
					  <input id="bin" class="form-control" name="bin" placeholder="Bin" value="">
					  <br>
					  <input id="serial" class="form-control" name="serial" placeholder="Serial Number" value="">
					  <input type="hidden" name="cycle" value="<?php echo $_POST["cycle"]; ?>">
					  <input type="hidden" name="warehouse" value="<?php echo $_POST["warehouse"]; ?>">
					  <input type="hidden" name="plant" value="<?php echo $_POST["plant"]; ?>">
					  <input type="hidden" name="partNum" value="<?php echo htmlentities($partInfo["result"]->returnObj->Part[0]->PartNum); ?>">
					  <input type="hidden" name="partDesc" value="<?php echo htmlentities($partInfo["result"]->returnObj->Part[0]->PartDescription); ?>">
					  <input type="hidden" name="name" value="<?php echo $_SESSION["clockInStatusResult"]->Name; ?>">
					  <input type="hidden" name="uom" value="<?php echo htmlentities($partInfo["result"]->returnObj->Part[0]->IUM); ?>">
					  <br>
					  <button id="continueBTN" type="submit" class="btn btn-primary"><b>Continue</b></button>
					</form>
					</center>
				</div>
			</div>
		</center>
	<?php } ?>

	<?php if($step == 3) { ?>
		<center>
				<div class="card" style="max-width:500px;">
					<center>
						<img src="../img/check.png" class="card-img-top" style="margin:10px;max-width:100px;max-height:100px;">
						<div class="card-body">
							<p class="card-text"><b><center>The blank tag was updated</center></b></p>
							<center>
						</center>
						</div>
					</center>
				</div>
		</center>
	<?php } ?>



    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">

    </script>
  </body>
</html>