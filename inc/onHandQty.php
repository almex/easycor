<?php

include_once 'api.php';
$api = new API();

if(isset($_POST["partNums"])){
	$partsOnHand = $api->getPartsOnHand($_POST["partNums"]);
	if($partsOnHand["http"] == 200){
		echo json_encode($partsOnHand);
		die();
	}
}

echo json_encode(array());

?>