<?php 
	include_once 'inc/api.php';
	$api = new API();
?>

<div style="width:100%;background:#014358;color:#fff;">
	<div class="container">
	  <div class="row">
		<div class="col-sm d-none d-sm-block d-md-block" align="center">
		  <a href="index.php" class="btn btn-primary" style="margin:10px;min-width: 80px;">
		  	<img src="../img/home.png" style="max-height: 35px;">
		  </a>
		</div>
		<div class="col-sm" align="center">
			<h2 style="margin:10px;">
				<?php if($api->isPilot() || (isset($_SESSION["pilot"]) && $_SESSION["pilot"] == 1)) { ?>
					PILOT <a href="index.php" style="color:#fff;text-decoration:none;"><?php echo $_SESSION["clockInStatusResult"]->Name; ?></a>
				<?php } else { ?>
					<a href="index.php" style="color:#fff;text-decoration:none;"><?php echo $_SESSION["clockInStatusResult"]->Name; ?></a>
				<?php } ?>
			</h2>
		</div>
		<div class="col-sm" align="center">
		  	<table>
		  		<tr>
		  			<td class="d-block d-sm-none">
		  				<a href="index.php" class="btn btn-primary d-block d-sm-none" style="margin:10px;width: 80px;min-width: 80px;">
		  					<img src="../img/home.png" style="max-height: 35px;">
		  				</a>
		  			</td>
		  			<td>
		  				<a href="login.php" class="btn btn-primary" style="margin:10px;min-width: 80px;">
		  					<img src="../img/user.png" style="max-height: 35px;">
		  				</a>
		  			</td>
		  		</tr>
			</table>
		</div>
	  </div>
	</div>
</div>

</div>
<div style="width:100%;height:5px;background:#347ca7;"></div>