<?php

	function insertIgnorePart($db,$basePart,$rev,$part){
		$qry = $db->prepare('INSERT INTO ignore (part, rev, ignore) VALUES (?, ?, ?)');
		$ret = $qry->execute(array($basePart,$rev,$part));
		if(!$ret) {
		    return false;
	  	}

	  	return $db->lastInsertId();
	}

	function deleteIgnorePart($db,$id){
		$qry = $db->prepare('DELETE FROM ignore WHERE id=?');
		$ret = $qry->execute(array($id));
		if(!$ret) {
		    return false;
	  	}

	  	return true;
	}

	function getAllPartsToIgnore($db,$basePart,$rev){
		$qry = $db->prepare('SELECT * FROM ignore WHERE part=? AND rev=?');
		$ret = $qry->execute(array($basePart,$rev));
		return $qry->fetchAll();
	}

	function ignorePartExists($db,$basePart,$rev,$part){
		$qry = $db->prepare('SELECT * FROM ignore WHERE part=? AND rev=? AND ignore=?');
		$ret = $qry->execute(array($basePart,$rev,$part));
		$rows = $qry->fetchAll();;
		if(sizeof($rows) > 0){
			return true;
		} else {
			return false;
		}
	}
?>