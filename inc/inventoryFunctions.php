<?php 

function makeCycleString($plant,$year,$month,$seq){
  return $plant."-".$year."-".$month."-".$seq;
}

function searchTags($db,$cycle,$search){
  $qry = $db->prepare(
    "SELECT * FROM tags WHERE TagNum LIKE ?
    UNION
    SELECT * FROM tags WHERE PartNum LIKE ?
    UNION
    SELECT * FROM tags WHERE PartDesc LIKE ?");

  $search = "%".$search."%";
  $ret = $qry->execute(array($search,$search,$search));
  if($ret == false){
    return array();
  }

  $tags = $qry->fetchAll();
  if($tags == false){
    return array();
  }

  $return = array();
  foreach ($tags as $tag) {
    if($tag["Cycle"] == $cycle){
      array_push($return, $tag);
    }
  }

  return $return;
}

function getUniquePartNums($db,$warehouseCode,$excludeBin100 = false){
  if($excludeBin100){
    $qry = $db->prepare("SELECT DISTINCT PartNum FROM tags WHERE WarehouseCode=? AND BinNum!=100");
  } else {
    $qry = $db->prepare("SELECT DISTINCT PartNum FROM tags WHERE WarehouseCode=?");
  }

  $ret = $qry->execute(array($warehouseCode));
  if($ret == false){
    return array();
  }

  $nums = $qry->fetchAll();
  if($nums == false){
    return array();
  }

  return $nums;
}

function countExists($db,$tagNum,$warehouse,$cycle,$name){
  $qry = $db->prepare(
  'SELECT * FROM count WHERE TagNum=? AND WarehouseCode=? AND Cycle=? AND Name=?');
  $ret = $qry->execute(array($tagNum,$warehouse,$cycle,$name));
  $part = $qry->fetch();

  if($part == false){
    return false;
  } else {
    return true;
  }
}

function deleteCounts($db,$tagNum,$warehouse,$cycle){
  $qry = $db->prepare(
  'DELETE FROM count WHERE TagNum=? AND WarehouseCode=? AND Cycle=?');
  $ret = $qry->execute(array($tagNum,$warehouse,$cycle));
  if($ret == false){
    return false;
  } else {
    return true;
  }
}

function insertCount($db,$tagNum,$warehouse,$partNum,$cycle,$name,$countedQTY){
  $qry = $db->prepare(
  'INSERT INTO count (TagNum, WarehouseCode, PartNum, Cycle, Name, Time, CountedQty) VALUES (?, ?, ?, ?, ?, ?, ?)');
  $ret = $qry->execute(array($tagNum,$warehouse,$partNum,$cycle,$name,time(),$countedQTY));
  if(!$ret) {
    $error = true;
    $errorText = "Unable to insert into database";
    return false;
  }

  return true;
}

function updateCount($db,$tagNum,$warehouse,$partNum,$cycle,$name,$countedQTY){
	$qry = $db->prepare(
	'UPDATE count SET PartNum=?, CountedQty=?, Time=? WHERE TagNum=? AND WarehouseCode=? AND Cycle=? AND Name=?');
	$ret = $qry->execute(array($partNum, $countedQTY, time(), $tagNum, $warehouse, $cycle, $name));
	if(!$ret) {
  	$error = true;
  	$errorText = "Unable to update database";
  	return false;
	}
	return true;
}

function getNextBlankTag($db,$cycle,$warehouse){
  $qry = $db->prepare(
  'SELECT * FROM tags WHERE Cycle=? AND Blank=? AND WarehouseCode=? AND PartNum="" LIMIT 1');
  $ret = $qry->execute(array($cycle,1,$warehouse));
  $part = $qry->fetch();

  if($part == false){
    return false;
  } else {
    return $part;
  }
}

function updateBlankTag($db,$tagNum,$warehouse,$partNum,$partDesc,$cycle,$bin,$serial,$IUM){
  $qry = $db->prepare(
  'UPDATE tags SET PartNum=?, PartDesc=?, BinNum=?, IUM=?, SerialNumber=? WHERE TagNum=? AND WarehouseCode=? AND Cycle=?');
  $ret = $qry->execute(array($partNum,$partDesc,$bin,$IUM,$serial,$tagNum,$warehouse,$cycle));
  if(!$ret) {
    $error = true;
    $errorText = "Unable to update database";
    return false;
  }
  return true;
}

?>