<?php
include_once 'api.php';
$api = new API();

$return = array(
      "result" => true,
      "message" => "",
      "total" => 0,
      "counted" => 0,
      "txtID" => ""
    );

if(isset($_POST["warehouse"]) && 
	isset($_POST["cycle"]) &&
	isset($_POST["txtID"]) &&
	isset($_POST["bin"])){

	$return["txtID"] = $_POST["txtID"];
	$cycle = explode("-", $_POST["cycle"]);
	$tags = $api->getBinCountCycleTags(
		$_POST["warehouse"],
		$cycle[1],
		$cycle[2],
		$cycle[3],
		$_POST["bin"]);

	if($tags["http"] == 200){
		$complete = 0;
		$total = 0;
		foreach ($tags["result"]->returnObj->CCTag as $tag) {
			if(!empty($tag->PartNum)){
				$total++;
			}

			if(!empty($tag->CountedBy)){
				$complete++;
			}
		}

		$return["total"] = $total;
		$return["counted"] = $complete;
		$return["result"] = true;

		if($complete != count($tags["result"]->returnObj->CCTag)){
			$return["message"] = "<div style='color:#c10000;'>".$complete."/".count($tags["result"]->returnObj->CCTag)."</div>";
		} else {
			$return["message"] = "<div style='color:#000;'>".$complete."/".count($tags["result"]->returnObj->CCTag)."</div>";
		}

	} else {
		$return["result"] = false;
		$return["message"] = "unable to get status";
	}
} else {
	$return["result"] = false;
	$return["message"] = "missing data";
}

echo json_encode($return);
?>