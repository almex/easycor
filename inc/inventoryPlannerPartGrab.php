<?php
session_start();
ini_set('memory_limit','300M');
set_time_limit(900);

include_once 'inventoryPlannerFunctions.php';
include_once 'api.php';
$api = new API();

$return = array(
      "result" => true,
      "msg" => "",
      "count" => 0,
      "hasNext" => true
    );


if(isset($_GET["reportID"])){
	$_POST["reportID"] = $_GET["reportID"];
}

if(isset($_GET["type"])){
	$_POST["type"] = $_GET["type"];
}

if(!isset($_POST["reportID"])){
	$return["msg"] = "missing report id";
	$return["result"] = false;
	echo json_encode($return);
	die();
}

if(!isset($_POST["type"])){
	$return["msg"] = "missing type";
	$return["result"] = false;
	echo json_encode($return);
	die();
}

$db = new PDO('sqlite:../db/inventoryPlanner.db');


if($_POST["type"] == "grab"){
	$nextPart = getNextPart($db,$_POST["reportID"]);
	if($nextPart == false){
		$return["msg"] = "unable to grab next part";
		$return["result"] = false;
		$return["hasNext"] = false;
		echo json_encode($return);
		die();
	}

	$subParts = $api->getMethodParts($nextPart["partNum"],$nextPart["rev"]);
	if($subParts["http"] != 200){
		$return["msg"] = "unable to grab sub parts";
		$return["result"] = false;
		echo json_encode($return);
		die();
	}

	$return["count"] = count($subParts["result"]->returnObj->PartMtl);

	//Insert Part
	foreach ($subParts["result"]->returnObj->PartMtl as $subPart) {

		//Conv UOM if found
		if(uomConvExists($db,strtoupper(trim($subPart->UOMCode)))){

			$subPart->QtyPer = uomConvToDefault(
				$db,
				$subPart->QtyPer,
				strtoupper(trim($subPart->UOMCode)));

			$subPart->UOMCode = getDefaultUOM(
				$db,
				strtoupper(trim($subPart->UOMCode)));
		}

		$result= insertPart(
			$db,
			$_POST["reportID"],
			strtoupper(trim($subPart->MtlPartNum)),
			$subPart->MtlPartNumPartDescription,
			$subPart->UOMCode,
			$nextPart["id"],
			$nextPart["partNum"],
			$subPart->MtlRevisionNum,
			$subPart->QtyPer);

		if(!$result){
			$return["msg"] = "unable insert part into database";
			$return["result"] = false;
			echo json_encode($return);
			die();
		}
	}

	if(setSubPartsGrabbed($db,$nextPart["id"])){
		$return["result"] = true;
	} else {
		$return["msg"] = "unable set sub parts as grabbed";
		$return["result"] = false;
		echo json_encode($return);
		die();
	}
}

if($_POST["type"] == "location"){
	$parts = getPartsWithNoLocation($db,$_POST["reportID"]);
	$partNumbers = array();
	foreach ($parts as $part) {
		if(!in_array($part["partNum"], $partNumbers)){
			array_push($partNumbers, $part["partNum"]);
		}
	}

	$return["count"] = count($partNumbers);
	$partLocations = $api->getMultiPartInfo($partNumbers,1);
	if($partLocations["http"] != 200){
		$return["msg"] = "unable to grab part locations";
		$return["result"] = false;
		$return["hasNext"] = false;
		echo json_encode($return);
		die();
	}

	foreach ($partLocations["result"]->returnObj->PartWhse as $PartWhse) {
		if(!partLocationExists($db,$_POST["reportID"],$PartWhse->PartNum,$PartWhse->WarehouseCode,$PartWhse->PrimBinNum)){

			//Skip bad warehouses
			if($PartWhse->PlantOwner != $PartWhse->Plant){
				continue;
			}

			//Conv UOM if found
			if(uomConvExists($db,strtoupper(trim($PartWhse->PartNumIUM)))){
				$PartWhse->OnHandQty = uomConvToDefault(
					$db,
					$PartWhse->OnHandQty,
					strtoupper(trim($PartWhse->PartNumIUM)));

				$PartWhse->PartNumIUM = getDefaultUOM(
					$db,
					strtoupper(trim($PartWhse->PartNumIUM)));
			}


			insertPartLocation(
				$db,
				$_POST["reportID"],
				strtoupper(trim($PartWhse->PartNum)),
				$PartWhse->Plant,
				$PartWhse->WarehouseCode,
				$PartWhse->WarehouseDescription,
				$PartWhse->PrimBinNum,
				$PartWhse->OnHandQty,
				$PartWhse->PartNumIUM);
			
			setPartLocationGrabbed(
				$db,
				$_POST["reportID"],
				strtoupper(trim($PartWhse->PartNum)));
		}
	}

	$return["hasNext"] = hasMorePartsWithNoLocation($db,$_POST["reportID"]);
	if(!$return["hasNext"] ){
		setReportComplete($db,$_POST["reportID"]);
	}
}

echo json_encode($return);
?>