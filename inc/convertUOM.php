<?php

include_once 'uomConv.php';
$uom = new UOM();

if(isset($_POST["startUOM"]) && isset($_POST["startQTY"]) && isset($_POST["toUOM"])){

	$return = array();
	$return["qty"] = $uom->convert($_POST["startUOM"],$_POST["startQTY"],$_POST["toUOM"]);
	$return["uom"] = $_POST["toUOM"];
	$return["start_uom"] = $_POST["startUOM"];

	if(isset($_POST["id"])){
		$return["id"] = $_POST["id"];
	}

	if(isset($_POST["parentGroup"])){
		$return["parentGroup"] = $_POST["parentGroup"];
	}

	echo json_encode($return);
	die();
}

echo json_encode(array());

?>