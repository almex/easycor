<?php
	function getReport($db,$id){
	  $qry = $db->prepare('SELECT * FROM reports WHERE id=?');
	  $ret = $qry->execute(array($id));
	  return $qry->fetch();
	}

	function deleteReport($db,$reportID){
		$qry = $db->prepare('DELETE FROM reports WHERE id=?');
		$ret = $qry->execute(array($reportID));
		if(!$ret) {
		    return false;
	  	}
	  	
	  	$qry = $db->prepare('DELETE FROM parts WHERE reportID=?');
		$ret = $qry->execute(array($reportID));
		if(!$ret) {
		    return false;
	  	}

	  	$qry = $db->prepare('DELETE FROM parts_location WHERE reportID=?');
		$ret = $qry->execute(array($reportID));
		if(!$ret) {
		    return false;
	  	}

	  	return true;
	}

	function getReportParts($db,$reportID){
	  $qry = $db->prepare('SELECT * FROM parts WHERE reportID=?');
	  $ret = $qry->execute(array($reportID));
	  return $qry->fetchAll();
	}

	function getReportPartLocations($db,$reportID){
	  $qry = $db->prepare('SELECT * FROM parts_location WHERE reportID=?');
	  $ret = $qry->execute(array($reportID));
	  return $qry->fetchAll();
	}

	function getReportBaseParts($db,$reportID){
		$qry = $db->prepare(
		'SELECT * FROM parts WHERE reportID=? AND parentPartNum=""');
		$ret = $qry->execute(array($reportID));
		return $qry->fetchAll();
	}

	function partToBeOrdered($db,$reportID,$plant,$partNum,$qtyNeeded){
		$toBeOrdered = true;
		$locations = getAllPartLocations($db,$reportID,$partNum);
		foreach ($locations as $location) {
			if(strlen(trim($plant)) > 0){
				if($location["Plant"] == $plant && $location["OnHandQty"] >= $qtyNeeded){
					$toBeOrdered = false;
				}
			} else {
				if($location["OnHandQty"] >= $qtyNeeded){
					$toBeOrdered = false;
				}
			}
		}

		return $toBeOrdered;
	}

	function getAllParentPartNumbers($db,$reportID,$partNum){
		$partNums = array();

		$qry = $db->prepare(
		'SELECT parentPartNum FROM parts WHERE reportID=? AND partNum=?');
		$ret = $qry->execute(array($reportID,$partNum));
		$parents = $qry->fetchAll();

		foreach ($parents as $parent) {
			array_push($partNums, $parent["parentPartNum"]);
		}

		return array_unique($partNums);
	}

	function getPartsWithNoLocation($db,$reportID){
		$qry = $db->prepare(
		'SELECT * FROM parts WHERE reportID=? AND locationGrabbed=? LIMIT 25');
		$ret = $qry->execute(array($reportID,0));
		return $qry->fetchAll();
	}

	function setPartLocationGrabbed($db,$reportID,$partNum){
		$qry = $db->prepare(
		'UPDATE parts SET locationGrabbed=? WHERE reportID=? AND partNum=?');
		$ret = $qry->execute(array(1,$reportID,$partNum));
		if(!$ret) {
		  	return false;
		}
		return true;
	}

	function uomConvExists($db,$uom){
		$qry = $db->prepare(
		'SELECT * FROM uom WHERE baseUOM=? LIMIT 1');
		$ret = $qry->execute(array($uom));
		$part = $qry->fetch();

		if($part == false){
			return false;
		} else {
			return true;
		}
	}

	function getDefaultUOM($db,$uom){
		$qry = $db->prepare(
		'SELECT defaultUOM FROM uom WHERE baseUOM=? LIMIT 1');
		$ret = $qry->execute(array($uom));
		$uom = $qry->fetch();

		if($uom == false){
			return false;
		} else {
			return $uom["defaultUOM"];
		}
	}

	function uomConvToDefault($db,$qty,$uom){
		$qry = $db->prepare(
		'SELECT convRate FROM uom WHERE baseUOM=? LIMIT 1');
		$ret = $qry->execute(array($uom));
		$uom = $qry->fetch();

		if($uom == false){
			return false;
		} 

		$conv = $uom["convRate"];
		return $qty*$conv;
	}

	function hasMorePartsWithNoLocation($db,$reportID){
		$qry = $db->prepare(
		'SELECT * FROM parts WHERE reportID=? AND locationGrabbed=? LIMIT 1');
		$ret = $qry->execute(array($reportID,0));
		$part = $qry->fetch();

		if($part == false){
			return false;
		} else {
			return true;
		}
	}

	function getAllPartLocations($db,$reportID,$partNum){
		$qry = $db->prepare(
		'SELECT * FROM parts_location WHERE reportID=? AND partNum=?');
		$ret = $qry->execute(array($reportID,$partNum));
		return $qry->fetchAll();
	}

	function getSubParts($db,$reportID,$parentPartID){
		$qry = $db->prepare(
		'SELECT * FROM parts WHERE reportID=? AND parentPartID=?');
		$ret = $qry->execute(array($reportID,$parentPartID));
		return $qry->fetchAll();
	}

	function insertReport($db,$name,$user){
		$qry = $db->prepare('INSERT INTO reports (name, user, time) VALUES (?, ?, ?)');
		$ret = $qry->execute(array($name,$user,time()));
		if(!$ret) {
		    return false;
	  	}

	  	return $db->lastInsertId();
	}

	function getReportPlants($db,$reportID){
		$qry = $db->prepare(
		'SELECT DISTINCT Plant FROM parts_location WHERE reportID=?');
		$ret = $qry->execute(array($reportID));
		return $qry->fetchAll();
	}

	function setReportComplete($db,$reportID){
		$qry = $db->prepare(
		'UPDATE reports SET complete=? WHERE id=?');
		$ret = $qry->execute(array(1,$reportID));
		if(!$ret) {
		  	return false;
		}
		return true;
	}

	function getCompleteReports($db){
	  $qry = $db->prepare('SELECT * FROM reports WHERE complete=?');
	  $ret = $qry->execute(array(1));
	  return $qry->fetchAll();
	}

	function insertPart($db,$reportID,$partNum,$partDesc,$uom,$parentPartID,$parentPartNum,$rev,$qty){
		$qry = $db->prepare('INSERT INTO parts (reportID, partNum, partDesc, uom, parentPartID, parentPartNum, rev, qty) 
			VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
		$ret = $qry->execute(array($reportID,$partNum,$partDesc,$uom,$parentPartID,$parentPartNum,$rev,$qty));
		if(!$ret) {
		    return false;
	  	}
	  	return true;
	}

	function insertPartLocation($db,$reportID,$partNum,$plant,$warehouseCode,$warehouseDesc,$primBin,$onHand,$uom){
		$qry = $db->prepare('INSERT INTO parts_location (reportID, PartNum, Plant, WarehouseCode, WarehouseDesc, PrimBinNum, OnHandQty, uom) 
			VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
		$ret = $qry->execute(array($reportID,$partNum,$plant,$warehouseCode,$warehouseDesc,$primBin,$onHand,$uom));
		if(!$ret) {
		    return false;
	  	}
	  	return true;
	}

	function partLocationExists($db,$reportID,$partNum,$warehouseCode,$primBin){
	  $qry = $db->prepare('SELECT id FROM parts_location WHERE reportID=? AND PartNum=? AND WarehouseCode=? AND PrimBinNum=?');
	  $ret = $qry->execute(array($reportID,$partNum,$warehouseCode,$primBin));
	  $part = $qry->fetch();

	  if($part == false){
	    return false;
	  } else {
	    return true;
	  }
	}

	function setSubPartsGrabbed($db,$id){
		$qry = $db->prepare(
		'UPDATE parts SET subPartsGrabbed=? WHERE id=?');
		$ret = $qry->execute(array(1,$id));
		if(!$ret) {
		  	return false;
		}
		return true;
	}

	function getNextPart($db,$reportID){
		$qry = $db->prepare(
		'SELECT * FROM parts WHERE reportID=? AND subPartsGrabbed=? AND rev IS NOT NULL AND rev!=""');
		$ret = $qry->execute(array($reportID,0));
		$part = $qry->fetch();

		if($part == false){
			return false;
		} else {
			return $part;
		}
	}
?>