<?php

include_once 'global.php';

function commentExists($tagNum,$warehouse,$cycle,$name){
  $db = new PDO('sqlite:../db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare(
  'SELECT * FROM comment WHERE TagNum=? AND WarehouseCode=? AND Cycle=? AND Name=?');
  $ret = $qry->execute(array($tagNum,$warehouse,$cycle,$name));
  $part = $qry->fetch();

  if($part == false){
    return false;
  } else {
    return true;
  }
}

function insertComment($tagNum,$warehouse,$partNum,$cycle,$name,$comment){
  $db = new PDO('sqlite:../db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare(
  'INSERT INTO comment (TagNum, WarehouseCode, PartNum, Cycle, Name, Time, Comment) VALUES (?, ?, ?, ?, ?, ?, ?)');
  $ret = $qry->execute(array($tagNum,$warehouse,$partNum,$cycle,$name,time(),$comment));
  if(!$ret) {
    $error = true;
    $errorText = "Unable to insert into database";
    return false;
  }

  return true;
}

function updateComment($tagNum,$warehouse,$partNum,$cycle,$name,$comment){
	$db = new PDO('sqlite:../db/'.$GLOBALS["invDatabase"]);
	$qry = $db->prepare(
	'UPDATE comment SET PartNum=?, Comment=?, Time=? WHERE TagNum=? AND WarehouseCode=? AND Cycle=? AND Name=?');
	$ret = $qry->execute(array($partNum, $comment, time(), $tagNum, $warehouse, $cycle, $name));
	if(!$ret) {
  	$error = true;
  	$errorText = "Unable to update database";
  	return false;
	}
	return true;
}

if(isset($_POST["tagNum"]) && 
   isset($_POST["warehouse"]) &&
   isset($_POST["partNum"]) && 
   isset($_POST["cycle"]) && 
   isset($_POST["name"]) &&
   isset($_POST["comment"])){

  	if(commentExists($_POST["tagNum"],$_POST["warehouse"],$_POST["cycle"],$_POST["name"])){
  		updateComment($_POST["tagNum"],$_POST["warehouse"],$_POST["partNum"],$_POST["cycle"],$_POST["name"],$_POST["comment"]);
  	} else {
  		insertComment($_POST["tagNum"],$_POST["warehouse"],$_POST["partNum"],$_POST["cycle"],$_POST["name"],$_POST["comment"]);
  	}
}

?>