<?php
session_start();
include_once 'api.php';
include_once 'uomConv.php';

$api = new API();
$uomConv = new UOM();

$return = array(
  "result" => true,
  "error" => "",
  "count" => 0,
  "complete" => false
);

function errorCheck($result){
	if($result["http"] != 200){
		//$GLOBALS["return"]["result"] = false;
		//$GLOBALS["return"]["error"]  = "API ERROR: ".$result["method"];
		//echo json_encode($GLOBALS["return"]);
		//die();
		return false;
	}

	return true;
}

function fatalErrorCheck($result){
	if($result["http"] != 200){
		$GLOBALS["return"]["result"] = false;
		$GLOBALS["return"]["error"]  = "API ERROR: ".$result["method"];
		echo json_encode($GLOBALS["return"]);
		die();
	}
}

if(!$uomConv->responsePassed()){
	$GLOBALS["return"]["result"] = false;
	$GLOBALS["return"]["error"]  = "API ERROR: UOM CONV";
	echo json_encode($GLOBALS["return"]);
	die();
}

if(isset($_GET["jobNum"])){
	$_POST["jobNum"] = $_GET["jobNum"];
}

if(isset($_GET["amountToIssue"])){
	$_POST["amountToIssue"] = $_GET["amountToIssue"];
}

if(isset($_GET["cache"])){
	$_POST["cache"] = $_GET["cache"];
}

if(!isset($_POST["jobNum"]) ||
	!isset($_POST["amountToIssue"]) ||
	!isset($_POST["cache"])){

	$return["result"] = false;
	$return["error"] = "Missing data";
	echo json_encode($GLOBALS["return"]);
	die();
}

if(!isset($_SESSION[$_POST["cache"]])){
	//Get Job MTL
	$jobMtl = $api->getJobMtl($_POST["jobNum"]);
	fatalErrorCheck($jobMtl);
	$jobMtl = $jobMtl["result"]->returnObj->JobMtlList;

	$plantWarehouses = $api->getWarehouses($jobMtl[0]->Plant);
	fatalErrorCheck($plantWarehouses);
	$parsedWarehouses = array();
	foreach ($plantWarehouses["result"]->returnObj->Warehse as $warehse) {
		array_push($parsedWarehouses, $warehse->WarehouseCode);
	}

	//Get only non issued MTL
	$nonIssued = array();
	foreach ($jobMtl as $mtl) {
		if(!$mtl->IssuedComplete){
			array_push($nonIssued, $mtl);
		}
	}
	$jobMtl = $nonIssued;

	//Get parts on hand locations
	$partNums = array();
	foreach ($jobMtl as $mtl) {
		if(!in_array($mtl->PartNum, $partNums)){
			array_push($partNums, $mtl->PartNum);
		}
	}

	$partsOnHand = $api->getPartsOnHand($partNums);
	fatalErrorCheck($partsOnHand);
	$partsOnHand = $partsOnHand["result"]->returnObj->Results;

	$partsFilterdByPlant = array();
	foreach ($partsOnHand as $part) {
		if(in_array($part->PartBin_WarehouseCode, $parsedWarehouses)){
			array_push($partsFilterdByPlant, $part);
		}
	}

	$partsOnHand = $partsFilterdByPlant;

	//Find MTL with qty on hand
	$mtlToIsse = array();
	foreach ($jobMtl as $mtl) {
		$mtl->PartNum = strtoupper($mtl->PartNum);

		foreach ($partsOnHand as $partOnHand) {
			$partOnHand->PartBin_PartNum = strtoupper($partOnHand->PartBin_PartNum);

			if($partOnHand->PartBin_PartNum == $mtl->PartNum && 
				$mtl->WarehouseCode == $partOnHand->PartBin_WarehouseCode &&
				$partOnHand->PartBin_OnhandQty > 0){

					//Convert UOM if needed
					if($partOnHand->Part_IUM != $mtl->IUM){
						$partOnHand->PartBin_OnhandQty = $uomConv->convert($partOnHand->Part_IUM,$partOnHand->PartBin_OnhandQty,$mtl->IUM);
						$partOnHand->Part_IUM = $mtl->IUM;
					}


					//Found Mtl
					$foundMtl = array(
						"partNum" => $mtl->PartNum,
						"company" => $mtl->Company,
						"assembSeq" => $mtl->AssemblySeq,
						"mtlSeq" => $mtl->MtlSeq,
						"warehouse" => $partOnHand->PartBin_WarehouseCode,
						"bin" => $partOnHand->PartBin_BinNum,
						"lot" => $partOnHand->PartBin_LotNum,
						"completed" => false
					);

					//Set Qty
					if($partOnHand->PartBin_OnhandQty < ($mtl->RequiredQty-$mtl->IssuedQty)){
						$foundMtl["qty"] = $partOnHand->PartBin_OnhandQty;
					} else {
						$foundMtl["qty"] = ($mtl->RequiredQty-$mtl->IssuedQty);
					}

					array_push($mtlToIsse, $foundMtl);

			}
		}
	}

	$_SESSION[$_POST["cache"]] = $mtlToIsse;
}

$mtlToIsse = $_SESSION[$_POST["cache"]];

$hasUncompletedMTL = false;
for ($i=0; $i < sizeof($mtlToIsse); $i++) {
	if(!$mtlToIsse[$i]["completed"]){
		$hasUncompletedMTL = true;
	}
}

if(!$hasUncompletedMTL){
	$return["complete"] = true;
	echo json_encode($GLOBALS["return"]);
	die();
}

$completedCount = 0;
for ($i=0; $i < sizeof($mtlToIsse); $i++) {

	if(!array_key_exists($i, $mtlToIsse)){
		break;
	}

	$mtl = $mtlToIsse[$i];
	if($mtl["completed"]){
		continue;
	}
	
	$issueReturnDataSet = $api->getNewJobAsmblMultiple(
		$mtl["company"],
		$_POST["jobNum"],
		$mtl["assembSeq"]
	);
	if(!errorCheck($issueReturnDataSet)){
		$_SESSION[$_POST["cache"]][$i]["completed"] = true;
		continue;
	}

	$issueReturnDataSet = $issueReturnDataSet["result"]->returnObj->IssueReturn[0];

	$onChangingToJobSeq = $api->onChangingToJobSeq($mtl["mtlSeq"],$issueReturnDataSet);
	if(!errorCheck($onChangingToJobSeq)){
		$_SESSION[$_POST["cache"]][$i]["completed"] = true;
		continue;
	}

	$issueReturnDataSet = $onChangingToJobSeq["result"]->parameters->ds->IssueReturn[0];

	$onChangeToJobSeq = $api->onChangeToJobSeq($mtl["mtlSeq"],$issueReturnDataSet);
	if(!errorCheck($onChangeToJobSeq)){
		$_SESSION[$_POST["cache"]][$i]["completed"] = true;
		continue;
	}

	$issueReturnDataSet = $onChangeToJobSeq["result"]->parameters->ds->IssueReturn[0];

	$fromWarehouseChanged = $issueReturnDataSet->FromWarehouseCode != $mtl["warehouse"];
	$defaultFromWarehouse = $issueReturnDataSet->FromWarehouseCode; 
	if(isset($mtl["warehouse"])){
		$defaultFromWarehouse = $mtl["warehouse"];
	} 

	$fromBinChanged = $issueReturnDataSet->FromBinNum != $mtl["bin"];
	$defaultFromBin = $issueReturnDataSet->FromBinNum;
	if(isset($mtl["bin"])){
		$defaultFromBin	 = $mtl["bin"];
	}

	$defaultToWarehouse = $issueReturnDataSet->ToWarehouseCode; 
	$defaultToBin = $issueReturnDataSet->ToBinNum;

	/*
	$onChangeToWarehouse = $api->onChangeToWarehouse($defaultToWarehouse,$issueReturnDataSet);
	if(!errorCheck($onChangeToWarehouse)){
		$_SESSION[$_POST["cache"]][$i]["completed"] = true;
		continue;
	}

	$issueReturnDataSet = $onChangeToWarehouse["result"]->parameters->ds->IssueReturn[0]; */

	if($fromWarehouseChanged){
		$onChangeFromWarehouse = $api->onChangeFromWarehouse($defaultFromWarehouse,$issueReturnDataSet);
		if(!errorCheck($onChangeFromWarehouse)){
			$_SESSION[$_POST["cache"]][$i]["completed"] = true;
			continue;
		}

		$issueReturnDataSet = $onChangeFromWarehouse["result"]->parameters->ds->IssueReturn[0]; 
	}


	if($fromBinChanged){
		$onChangingFromBinNum = $api->onChangingFromBinNum($defaultFromBin,$issueReturnDataSet);
		if(!errorCheck($onChangingFromBinNum)){
			$_SESSION[$_POST["cache"]][$i]["completed"] = true;
			continue;
		}

		$issueReturnDataSet = $onChangingFromBinNum["result"]->parameters->ds->IssueReturn[0];

		$onChangeFromBinNum = $api->onChangeFromBinNum($issueReturnDataSet);
		if(!errorCheck($onChangeFromBinNum)){
			$_SESSION[$_POST["cache"]][$i]["completed"] = true;
			continue;
		}

		$issueReturnDataSet = $onChangeFromBinNum["result"]->parameters->ds->IssueReturn[0];
	}

	if(isset($mtl["lot"]) && strlen(trim($mtl["lot"])) > 1){
		$onChangeLotNum = $api->onChangeLotNum($mtl["lot"],$issueReturnDataSet);
		if(!errorCheck($onChangeLotNum)){
			$_SESSION[$_POST["cache"]][$i]["completed"] = true;
			continue;
		}
		$issueReturnDataSet = $onChangeLotNum["result"]->parameters->ds->IssueReturn[0];	
	}

	$onChangeTranQty = $api->onChangeTranQty($mtl["qty"],$issueReturnDataSet);
	if(!errorCheck($onChangeTranQty)){
		$_SESSION[$_POST["cache"]][$i]["completed"] = true;
		continue;
	}

	$issueReturnDataSet = $onChangeTranQty["result"]->parameters->ds->IssueReturn[0];

	//Make sure material is not already issued
	if($issueReturnDataSet->QtyPreviouslyIssued+$mtl["qty"] > $issueReturnDataSet->QtyRequired){
		$_SESSION[$_POST["cache"]][$i]["completed"] = true;
		continue;
	}

	$prePerformMaterialMovement = $api->prePerformMaterialMovement($issueReturnDataSet);
	if(!errorCheck($prePerformMaterialMovement)){
		$_SESSION[$_POST["cache"]][$i]["completed"] = true;
		continue;
	}

	$issueReturnDataSet = $prePerformMaterialMovement["result"]->parameters->ds->IssueReturn[0];

	/*
	$masterInventoryBinTests = $api->masterInventoryBinTests($issueReturnDataSet);
	if(!errorCheck($masterInventoryBinTests)){
		$_SESSION[$_POST["cache"]][$i]["completed"] = true;
		continue;
	}

	$issueReturnDataSet = $masterInventoryBinTests["result"]->parameters->ds->IssueReturn[0]; */

	$performMaterialMovement = $api->performMaterialMovement($issueReturnDataSet);
	if(!errorCheck($performMaterialMovement)){
		$_SESSION[$_POST["cache"]][$i]["completed"] = true;
		continue;
	}

	$issueReturnDataSet = $performMaterialMovement["result"]->parameters->ds->IssueReturn[0];

	$_SESSION[$_POST["cache"]][$i]["completed"] = true;
	$completedCount++;

	if($completedCount >= $_POST["amountToIssue"]){
		break;
	}
}

$return["count"] = $completedCount;
echo json_encode($return);
?>