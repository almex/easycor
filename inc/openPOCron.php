<?php
ini_set('max_execution_time', 0);
set_time_limit(0);

session_start();

//Epicor Login
$username = "";
$pass = "";

$_SESSION["key"] = base64_encode($username.":".$pass);

include_once 'global.php';
include_once 'api.php';
include_once 'email.php';
$api = new API();

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function getEmails(){
  $keywordDB = new PDO('sqlite:'.__DIR__.'/../db/openPOCron.db');
  $qry = $keywordDB->prepare("SELECT * FROM emails WHERE plant = ?");

  $ret = $qry->execute(array($_GET["plant"]));
  if($ret == false){
    return array();
  }

  $emails = $qry->fetchAll();
  if($emails == false){
    return array();
  }

  return $emails;
}

function htmlEcho($string){
	return htmlspecialchars($string);
}

if(isset($argv) && isset($argv[1])){
	parse_str($argv[1], $params);
	if(isset($params["plant"])){
		$_GET["plant"] = $params["plant"];
	}
}

if(!isset($_GET["plant"]) || !is_numeric($_GET["plant"])){
	echo "Missing data!";
	die();
}

//Get Orders
$emails = getEmails();
if(empty($emails)){
	die();
}

$purchaseOrders = $api->getOpenPurchaseOrders($_GET["plant"]);
if(hasError($purchaseOrders)){
	echo "API Issue!";
	die();
}

if(!isset($purchaseOrders["result"]->returnObj->Results) || empty($purchaseOrders["result"]->returnObj->Results)){
	echo "No purchase orders!";
	die();
}

$sortedPurchaseOrders = array();
foreach ($purchaseOrders["result"]->returnObj->Results as $order) { 
	if(!isset($sortedPurchaseOrders["PO".$order->POHeader_PONum])){
		$sortedPurchaseOrders["PO".$order->POHeader_PONum] = array();
		$sortedPurchaseOrders["PO".$order->POHeader_PONum]["PONum"] = $order->POHeader_PONum;
		$sortedPurchaseOrders["PO".$order->POHeader_PONum]["Lines"] = array();
	}

	array_push($sortedPurchaseOrders["PO".$order->POHeader_PONum]["Lines"], $order);
}
$sortedPurchaseOrders = array_reverse($sortedPurchaseOrders);


//Email
$emailHTML = "<center><b>Plant ".$_GET["plant"]."</b></center><br><br>";
$emailHTML .= "<table>";
foreach ($sortedPurchaseOrders as $order) { 
	if(isset($order["Lines"][0]) && strlen(trim($order["Lines"][0]->POHeader_OrderDate)) > 0){
		$orderDate = explode("T", $order["Lines"][0]->POHeader_OrderDate)[0];
		$orderDate = DateTime::createFromFormat('Y-m-d',$orderDate);
		$orderDate = date_format($orderDate,"F j Y");
	} else {
		$orderDate = "Unknown";
	}

	$dueDatePassed = false;
		if(isset($order["Lines"][0]) && strlen(trim($order["Lines"][0]->PORel_DueDate)) > 0){
			$dueDate = explode("T", $order["Lines"][0]->PORel_DueDate)[0];
	} else {
		$dueDatePassed = true;
		$dueDate = "Unknown";
	}

	if(isset($order["Lines"][0]) && strlen(trim($order["Lines"][0]->POHeader_EntryPerson)) > 0){
			$person = $order["Lines"][0]->POHeader_EntryPerson;
	} else {
		$person = "Unknown";
	}

	if(!$dueDatePassed){
		$newDueDate = DateTime::createFromFormat('Y-m-d',$dueDate);
		$dueDate = date_format($newDueDate,"F j Y");

		$now = new DateTime();
		if($newDueDate < $now){
			$dueDatePassed = true;
		}
	}

	if(!$dueDatePassed){
		continue;
	}

	$emailHTML .= '<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
      <th scope="col" colspan="4"><center>Purchase Order #'.htmlEcho($order["PONum"]).' from '.htmlEcho($order["Lines"][0]->Vendor_Name).'</center></th>
    </tr>';

    $emailHTML .= '<tr>
		  <th scope="col" >Line</th>
		  <th scope="col" colspan="2">Part</th>
		  <th scope="col" ><center>Qty</center></th>
    </tr>';

    foreach ($order["Lines"] as $line) { 

    	$emailHTML .= '<tr>
    		<td><b>#'.htmlEcho($line->PODetail_POLine).'</b></td>
    		<td colspan="2">'.htmlEcho($line->PODetail_PartNum).'</td>
    		<td><center>'.htmlEcho($line->PORel_ReceivedQty).'/'.htmlEcho($line->PORel_RelQty).' '.htmlEcho($line->PORel_BaseUOM).'</center></td>
    	</tr>';

    	$emailHTML .= '<tr>
    		<td colspan="4">'.htmlEcho($line->PODetail_LineDesc).'</td>
    	</tr>';
    }

    $emailHTML .= '<tr>
    	<td colspan="4"><center>Ordered on <b>'.htmlEcho($orderDate).'</b> by '.htmlEcho($person).'</center></td>
    </tr>';

    $emailHTML .= '<tr>
    	<td colspan="4" style="color:#da0000;"><center>Due on <b>'.htmlEcho($dueDate).'</b></center></td>
    </tr>';

    $emailHTML .= '<tr>
		<td colspan="4">&nbsp;</td>
	</tr>';
}
$emailHTML .= "</table>";

$emailHTML .= "<br><center><a target='_blank' href='https://easycor.dreamhosters.com/openPurchaseOrders.php?plant=".htmlEcho($_GET["plant"])."'><b>View</b></a></center>";

//Email
foreach ($emails as $email) {
	sendEmail($email["email"],"Overdue Purchase Orders",$emailHTML);
}

?>