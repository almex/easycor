<?php
class API {
	const API_URL = 'https://ypds1230.ca.almex.com/EpicorERP/api/v1/';
	const API_PILOT_URL = 'https://ypds1230.ca.almex.com/EpicorERPPilot/api/v1/';
	const DEBUG = false;
	const PILOT = false;

	function isPilot(){
		return self::PILOT;
	}
	
	function clockIn($empNum, $shift){
		$data = array(
			"employeeID" => $empNum,
			"shift" => $shift
		);
		
		return $this->curl("Erp.BO.EmpBasicSvc/ClockIn",$data);
	}
	
	function clockOut($empNum){
		$data = array(
			"employeeID" => $empNum
		);
		
		return $this->curl("Erp.BO.EmpBasicSvc/ClockOut",$data);
	}
	
	function checkClockInStatus($empNum){
		$data = array(
			"empID" => $empNum
		);
		
		return $this->curl("Erp.BO.EmpBasicSvc/CheckClockInStatus",$data);
	}
	
	function getLaborList($empNum){
		$data = array(
			"whereClause" => "EmployeeNum = '".$empNum."' and ActiveTrans = yes",
			"pageSize" => 0,
			"absolutePage" => 0
		);
		
		return $this->curl("Erp.BO.LaborSvc/GetList",$data);
	}

	function getNewLaborDtlWithHdr($laborHedSeq,$ds){
		$data = array(
			"ds" => $ds,
			"ipClockInDate" => $ds->LaborHed[0]->ClockInDate,
			"ipClockInTime" => $ds->LaborHed[0]->ClockInTime,
			"ipClockOutDate" => "",
			"ipClockOutTime" => $ds->LaborHed[0]->ClockOutTime,
			"ipLaborHedSeq" => $laborHedSeq
		);

		return $this->curl("Erp.BO.LaborSvc/GetNewLaborDtlWithHdr",$data);
	}

	function getLaborRows($empNum){
		$data = array(
			"whereClauseLaborHed" => "EmployeeNum = '".$empNum."' and ActiveTrans = yes BY EmployeeNum",
			"whereClauseLaborDtl" => "",
			"whereClauseLaborDtlAttch" => "",
			"whereClauseLaborDtlAction" => "",
			"whereClauseLaborDtlComment" => "",
			"whereClauseLaborEquip" => "",
			"whereClauseLaborPart" => "",
			"whereClauseLbrScrapSerialNumbers" => "",
			"whereClauseLaborDtlGroup" => "",
			"whereClauseSelectedSerialNumbers" => "",
			"whereClauseSNFormat" => "",
			"whereClauseTimeWeeklyView" => "",
			"whereClauseTimeWorkHours" => "",
			"pageSize" => 0,
			"absolutePage" => 0,
		);

		return $this->curl("Erp.BO.LaborSvc/GetRows",$data);
	}

	function getLaborByID($laborHedSeq){
		$data = array(
			"laborHedSeq" => $laborHedSeq,
		);

		return $this->curl("Erp.BO.LaborSvc/GetByID",$data);
	}
	
	function startProductionActivity($laborHedSeq){
		$data = array(
			"LaborHedSeq" => $laborHedSeq,
			"StartType" => "P",
			"ds" => new stdClass()
		);

		$data["ds"]->LaborDtlAction = array();
		$data["ds"]->LaborDtlComment = array();
		$data["ds"]->LaborDtlGroup = array();
		$data["ds"]->LaborEquip = array();
		$data["ds"]->LaborPart = array();
		$data["ds"]->LbrScrapSerialNumbers = array();
		$data["ds"]->SelectedSerialNumbers = array();
		$data["ds"]->SNFormat = array();
		$data["ds"]->TimeWeeklyView = array();
		$data["ds"]->TimeWorkHours = array();
		$data["ds"]->LaborDtl = array();
		$data["ds"]->LaborDtlAttch = array();
		$data["ds"]->LaborHed = array();
		
		return $this->curl("Erp.BO.LaborSvc/StartActivity",$data);
	}
	
	function validateJobNum($jobNum){
		$data = array(
			"ipJobNum" => $jobNum
		);
		
		return $this->curl("Erp.BO.JobEntrySvc/ValidateJobNum",$data);
	}
	
	function setDefaultJobNum($jobNum,$ds){
		$data = array(
			"jobNum" => $jobNum,
			"ds" => $ds
		);
		
		return $this->curl("Erp.BO.LaborSvc/DefaultJobNum",$data);
	}
	
	function checkResourceGroup($resourceID,$ds){
		$data = array(
			"ProposedResourceID" => $resourceID,
			"ds" => $ds
		);
		
		return $this->curl("Erp.BO.LaborSvc/CheckResourceGroup",$data);
	}
	
	function overridesResource($resourceID,$ds){
		$data = array(
			"ProposedResourceID" => $resourceID,
			"ds" => $ds
		);
		
		return $this->curl("Erp.BO.LaborSvc/OverridesResource",$data);
	}
	
	function defaultWCCode($wccode,$ds){
		$data = array(
			"wcCode" => $wccode,
			"ds" => $ds
		);
		
		return $this->curl("Erp.BO.LaborSvc/DefaultWCCode",$data);
	}
	
	function laborRateCalc($ds){
		$data = array(
			"ds" => $ds
		);
		
		return $this->curl("Erp.BO.LaborSvc/LaborRateCalc",$data);
	}
	
	function getJobAssemblySeqs($jobNum){
		$data = array(
			"serviceNamespace" => "Erp:BO:JobAsmSearch",
			"whereClause" => "JobNum = '".$jobNum."'",
			"columnList" => "AssemblySeq,PartNum,Description"
		);
		
		return $this->curl("Ice.LIB.BOReaderSvc/GetList",$data);
	}
	
	function getJobOperations($jobNum,$asmSeq){
		$data = array(
			"serviceNamespace" => "Erp:BO:JobOperSearch",
			"whereClause" => "((JobNum = '".$jobNum."') AND (AssemblySeq = ".$asmSeq."))",
			"columnList" => "SubContract,LaborEntryMethod,OprSeq,OpCode,OpCodeOpDesc"
		);
		
		return $this->curl("Ice.LIB.BOReaderSvc/GetList",$data);
	}
	
	function setDefaultJobOperSeq($oprSeq,$ds){
		$data = array(
			"oprSeq" => $oprSeq,
			"ds" => $ds
		);
		
		return $this->curl("Erp.BO.LaborSvc/DefaultOprSeq",$data);
	}

	function setDefaultAssemblySeq($assemSeq,$ds){
		$data = array(
			"assemblySeq" => $assemSeq,
			"ds" => $ds
		);
		
		return $this->curl("Erp.BO.LaborSvc/DefaultAssemblySeq",$data);
	}
	
	function checkForProductionActivityWarnings($ds){
		$data = array(
			"ds" => $ds
		);
		
		return $this->curl("Erp.BO.LaborSvc/CheckWarnings",$data);
	}
	
	function checkEmployeeActivity($empId,$laborHedSeq,$jobNum,$asmSeq,$OprSeq,$resourceID){
		$data = array(
			"ipEmpID" => $empId,
			"ipLaborHedSeq" => $laborHedSeq,
			"ipJobNum" => $jobNum,
			"ipAsmSeq" => $asmSeq,
			"ipOprSeq" => $OprSeq,
			"ipResourceID" => $resourceID
		);
		
		return $this->curl("Erp.BO.LaborSvc/CheckEmployeeActivity",$data);
	}
	
	function checkFirstArticleWarning($ds){
		$data = array(
			"ds" => $ds
		);
		
		return $this->curl("Erp.BO.LaborSvc/CheckFirstArticleWarning",$data);
	}
	
	function updateProductionActivity($ds){
		$data = array(
			"ds" => $ds
		);

		return $this->curl("Erp.BO.LaborSvc/Update",$data);
	}

	function getProductionDataset($laborHedSeq,$laborDtlSeq){
		$data = array(
			"iLaborHedSeq" => $laborHedSeq,
			"iLaborDtlSeq" => $laborDtlSeq
			
		);
		
		return $this->curl("Erp.BO.LaborSvc/GetDetail",$data);
	}
	
	function getProductionActivityDS($empNum){
		$data = array(
			"whereClauseLaborHed" => "EmployeeNum = '".strval($empNum)."' and ActiveTrans = yes BY PayrollDate",
			"whereClauseLaborDtl" => "",
			"whereClauseLaborDtlAttch" => "",
			"whereClauseLaborDtlComment" => "",
			"whereClauseLaborEquip" => "",
			"whereClauseLaborPart" => "",
			"whereClauseLbrScrapSerialNumbers" => "",
			"whereClauseLaborDtlGroup" => "",
			"whereClauseSelectedSerialNumbers" => "",
			"whereClauseSNFormat" => "",
			"whereClauseTimeWeeklyView" => "",
			"whereClauseTimeWorkHours" => "",
			"whereClauseLaborDtlAction" => "",
			"pageSize" => 0,
			"absolutePage" => 0
			
		);
		
		return $this->curl("Erp.BO.LaborSvc/GetRows",$data);
	}
	
	function getActiveProductionActivityDS($empNum){
		$data = array(
			"whereClauseLaborHed" => "EmployeeNum = '".strval($empNum)."' and ActiveTrans = yes BY PayrollDate",
			"whereClauseLaborDtl" => "EmployeeNum = '".strval($empNum)."' and ActiveTrans = yes",
			"whereClauseLaborDtlAttch" => "",
			"whereClauseLaborDtlComment" => "",
			"whereClauseLaborEquip" => "",
			"whereClauseLaborPart" => "",
			"whereClauseLbrScrapSerialNumbers" => "",
			"whereClauseLaborDtlGroup" => "",
			"whereClauseSelectedSerialNumbers" => "",
			"whereClauseSNFormat" => "",
			"whereClauseTimeWeeklyView" => "",
			"whereClauseTimeWorkHours" => "",
			"whereClauseLaborDtlAction" => "",
			"pageSize" => 0,
			"absolutePage" => 0
			
		);
		
		return $this->curl("Erp.BO.LaborSvc/GetRows",$data);
	}

	function setProductionQty($laborQty,$ds){
		$data = array(
			"laborQty" => $laborQty,
			"vMessage" => "",
			"ds" => $ds
		);

		return $this->curl("Erp.BO.LaborSvc/DefaultLaborQty",$data);
	}
	
	function endProductionActivity($laborDtlSeq,$ds){
		$data = array(
			"ds" => $ds
		);

		foreach($data["ds"]->LaborDtl as $x => $dt1) {
			if($dt1->LaborDtlSeq == $laborDtlSeq){
				$data["ds"]->LaborDtl[$x]->RowMod = "U";
			}
		}

		return $this->curl("Erp.BO.LaborSvc/EndActivity",$data);
	}

	function endProductionActivityNotComplete($ds){
		$data = array(
			"cmplete" => false,
			"ds" => $ds
		);

		return $this->curl("Erp.BO.LaborSvc/EndActivityComplete",$data);
	}


	function getPartInfo($partNum){
		$data = array(
			"partNum" => $partNum
		);
		
		return $this->curl("Erp.BO.PartSvc/GetByID",$data);
	}

	function getPartWhereUsed($partNum){
		$data = array(
			"whereUsedPartNum" => $partNum,
			"pageSize" => 0,
			"absolutePage" => 0
		);
		
		return $this->curl("Erp.BO.PartSvc/GetPartWhereUsed",$data);
	}

	function getPartOnHand($partNum,$plant){
		$data = array(
			"cPartNum" => $partNum,
			"cPlant" => $plant
		);
		
		return $this->curl("Erp.BO.PartOnHandWhseSvc/GetPartOnHandWhse",$data);
	}

	function getPartsWarehouse($partNums){
		$data = array(
			"queryID" => "SW_PARTWHSE",
			"executionParams" => new stdClass(),
		);

		$whereValue = "";
		foreach ($partNums as $partNum) {
			$whereValue .= "PartWhse_PartNum = '".$partNum."' OR ";
		}

		if($this->endsWith($whereValue,"OR ")){
			$whereValue = substr($whereValue, 0, -3);
		}

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => $whereValue
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function getPartsOnHand($partNums){
		$data = array(
			"queryID" => "SW_PARTONHAND",
			"executionParams" => new stdClass(),
		);

		$whereString = "";
		foreach ($partNums as $partNum) {
			$whereString .= "PartBin_PartNum = '".$partNum."' OR ";
		}

		if($this->endsWith($whereString,"OR ")){
			$whereString = substr($whereString, 0, -3);
		}

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => $whereString
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function searchForPart($search){
		$search = trim($search);
		$data = array(
			"whereClause" => "PartDescription LIKE '%".$search."%' OR SearchWord LIKE '%".$search."%' OR PartNum LIKE '%".$search."%'",
			"pageSize" => 0,
			"absolutePage" => 0
		);
		
		return $this->curl("Erp.BO.PartSvc/GetList",$data);
	}

	function updatePart($part){
		$data = array(
			"ds" => new stdClass(),
		);
		$data["ds"]->Part = array();
		$data["ds"]->Part[0] = $part;

		return $this->curl("Erp.BO.PartSvc/Update",$data);
	}

	function updatePartFull($ds){
		$data = array(
			"ds" => $ds,
		);

		return $this->curl("Erp.BO.PartSvc/Update",$data);
	}

	function getMultiPartInfo($partNums,$page){
		$where = "";
		$count = count($partNums);
		for($i = 0; $i < $count; $i++){
			if($i == $count-1){
				$where .= "PartNum = '".$partNums[$i]."' ";
			} else {
				$where .= "PartNum = '".$partNums[$i]."' or ";
			}
		}

		$data = array(
			"whereClausePart" => $where,
			"whereClausePartAttch" => "",
			"whereClausePartCOO" => "",
			"whereClausePartDim" => "",
			"whereClausePartLangDesc" => "",
			"whereClausePartPlant" => "",
			"whereClausePartPlantPlanningAttribute" => "",
			"whereClausePartRestriction" => "",
			"whereClausePartRestrictSubst" => "",
			"whereClausePartRev" => "",
			"whereClausePartRevAttch" => "",
			"whereClausePartAudit" => "",
			"whereClausePartCOPart" => "",
			"whereClausePartRevInspPlan" => "",
			"whereClausePartRevInspPlanAttch" => "",
			"whereClausePartRevInspVend" => "",
			"whereClausePartSubs" => "",
			"whereClausePartPC" => "",
			"whereClausePartWhse" => "",
			"whereClausePartBinInfo" => "",
			"whereClausePartUOM" => "",
			"whereClauseEntityGLC" => "",
			"whereClauseTaxExempt" => "",
			"whereClausePartPlanningPool" => "",
			"pageSize" => 30,
			"absolutePage" => $page
		);
		
		return $this->curl("Erp.BO.PartSvc/GetRows",$data);
	}

	function getPOInfo($poNum){
		$data = array(
			"poNum" => $poNum
		);
		
		return $this->curl("Erp.BO.POSvc/GetByID",$data);
	}

	//Pick List
	function getNewMassIssueInput(){
		$data = array(
			"ds" => new stdClass(),
			"isReturn" => false
		);
		
		return $this->curl("Erp.BO.MassIssueToMfgSvc/GetNewMassIssueInput",$data);
	}

	function massIssueChangeJobNum($jobNum,$ds){
		$data = array(
			"pcJobNum" => $jobNum,
			"ds" => $ds
		);

		return $this->curl("Erp.BO.MassIssueToMfgSvc/OnChangeJobNum",$data);
	}


	function buildMassIssue($sysRowID,$ds){
		$data = array(
			"ds" => $ds,
			"ipSysRowID" => $sysRowID,
			"pcMessage" => ""
		);

		return $this->curl("Erp.BO.MassIssueToMfgSvc/BuildMassIssueBrowse",$data);
	}

	function getMethodParts($partNum,$revision){
		date_default_timezone_set('America/Toronto');
		$data = array(
			"ipPartNum" => $partNum,
			"ipRevisionNum" => $revision,
			"ipAltMethod" => "",
			"ipUseDefaultMethod" => true,
			"ipAsOfDate" => date("Y-m-d 12:00:00 \A\M", time()),
			"ipCompleteTree" => false,
		);

		return $this->curl("Erp.BO.BomSearchSvc/GetDatasetForTree",$data);
	}

	function getAllPlants(){
		return $this->curl("Erp.BO.PlantSvc/GetAllPlants");
	}

	function getImage($partNum){
		$data = array(
			"imageID" => $partNum
		);

		return $this->curl("Erp.BO.ImageSvc/GetOriginalImage",$data);
	}

	function getCountCycles($plant,$year){
		$data = array(
			"whereClause" => "Plant = '".$plant."' and CCYear = '".$year."'",
			"pageSize" => 0,
			"absolutePage" => 0,
		);

		return $this->curl("Erp.BO.CCCountCycleSvc/GetList",$data);
	}

	function getCountCycle($year,$month,$seq){
		$data = array(
			"whereClause" => "CCYear = '".$year."' and CCMonth = '".$month."' and CycleSeq = '".$seq."'",
			"pageSize" => 0,
			"absolutePage" => 0,
		);

		return $this->curl("Erp.BO.CCCountCycleSvc/GetList",$data);
	}

	function getCountCycleTags($warehouse,$year,$month,$cycleSeq){
		$data = array(
			"whereClauseCCTag" => "WarehouseCode = '".$warehouse."' and CCYear = '".$year."' and CCMonth = '".$month."' and CycleSeq = '".$cycleSeq."'",
			"whereClauseCCTagWrkSht" => "",
			"pageSize" => 0,
			"absolutePage" => 0,
		);

		return $this->curl("Erp.BO.CountTagSvc/GetRows",$data);
	}

	function getWarehouse($warehouse){
		$data = array(
			"warehouseCode" => $warehouse,
		);

		return $this->curl("Erp.BO.WarehseSvc/GetByID",$data);
	}

	//Inventory cycle count tag
	function getTagDataSet($plant,$tagNum,$warehouse){
		$data = array(
			"plant" => $plant,
			"warehouseCode" => $warehouse,
			"tagNum" => $tagNum
		);

		return $this->curl("Erp.BO.CountTagSvc/GetByID",$data);
	}

	function setTagCount($qty,$CCTag){
		$data = array(
			"pQty" => $qty,
			"ds" => new stdClass(),
		);
		$CCTag[0]->RowMod = "U";
		$data["ds"]->CCTag = $CCTag;

		return $this->curl("Erp.BO.CountTagSvc/OnChangeCountedQty",$data);
	}

	function setBlankTagPart($partNum,$uomCode,$CCTag){
		$data = array(
			"pcPartNum" => $partNum,
			"uomCode" => $uomCode,
			"ds" => new stdClass(),
		);
		$CCTag[0]->RowMod = "U";
		$data["ds"]->CCTag = $CCTag;

		return $this->curl("Erp.BO.CountTagSvc/OnChangePartNum",$data);
	}

	function setBlankTagBin($binNum,$CCTag){
		$data = array(
			"inBinNum" => $binNum,
			"ds" => new stdClass(),
		);
		$CCTag[0]->RowMod = "U";
		$data["ds"]->CCTag = $CCTag;

		return $this->curl("Erp.BO.CountTagSvc/OnChangeBinNum",$data);
	}

	function setBlankTagUOM($uom,$CCTag){
		$data = array(
			"ipCCUOM" => $binNum,
			"ds" => new stdClass(),
		);
		$CCTag[0]->RowMod = "U";
		$data["ds"]->CCTag = $CCTag;

		return $this->curl("Erp.BO.CountTagSvc/OnChangeUOM",$data);
	}


	function updateTag($name,$ds){
		date_default_timezone_set('America/Toronto');
		$data = array(
			"ds" => $ds,
		);
		$data["ds"]->CCTag[0]->CountedBy = $name;
		$data["ds"]->CCTag[0]->TagReturned = true;
		$data["ds"]->CCTag[0]->CountedDate = date("Y/m/d");
		$data["ds"]->CCTag[0]->CountedTime = date("H:i:s");
		$data["ds"]->CCTag[0]->TagNote = "Entered using EasyCor";

		return $this->curl("Erp.BO.CountTagSvc/Update",$data);
	}

	function getBinCountCycleTags($warehouse,$year,$month,$cycleSeq,$bin){
		$data = array(
			"whereClauseCCTag" => "WarehouseCode = '".$warehouse."' and CCYear = '".$year."' and CCMonth = '".$month."' and CycleSeq = '".$cycleSeq."' and BinNum = '".$bin."'",
			"whereClauseCCTagWrkSht" => "",
			"pageSize" => 0,
			"absolutePage" => 0,
		);

		return $this->curl("Erp.BO.CountTagSvc/GetRows",$data);
	}

	//$tagNums is array
	function getCountCycleTagMulti($tagNums,$year,$month,$cycleSeq){
		$where = "";
		$count = count($tagNums);
		for($i = 0; $i < $count; $i++){
			if($i == $count-1){
				$where .= "TagNum = '".$tagNums[$i]."' ";
			} else {
				$where .= "TagNum = '".$tagNums[$i]."' or ";
			}
		}

		$data = array(
			"whereClauseCCTag" => $where." and CCYear = '".$year."' and CCMonth = '".$month."' and CycleSeq = '".$cycleSeq."'",
			"whereClauseCCTagWrkSht" => "",
			"pageSize" => 0,
			"absolutePage" => 0,
		);

		return $this->curl("Erp.BO.CountTagSvc/GetRows",$data);
	}

	function getPartSuppliers($partNum){
		$data = array(
			"whereClauseSupplierPart" => "PartNum = '".$partNum."'",
			"whereClauseSupplierPartAttch" => "",
			"pageSize" => 0,
			"absolutePage" => 0,
		);

		return $this->curl("Erp.BO.SupplierPartSvc/GetRows",$data);
	}

	function mfgPartSearch($mfgNum){
		$data = array(
			"whereClauseSupplierPart" => "VendPartNum LIKE '%".$mfgNum."%' or MfgPartNum LIKE '%".$mfgNum."%'",
			"whereClauseSupplierPartAttch" => "",
			"pageSize" => 0,
			"absolutePage" => 0,
		);

		return $this->curl("Erp.BO.SupplierPartSvc/GetRows",$data);
	}

	function getVendorInfo($vendorNum){
		$data = array(
			"vendorNum" => $vendorNum,
		);

		return $this->curl("Erp.BO.VendorSvc/GetByID",$data);
	}

	function getPartTransactionHistory($partNum){
		$data = array(
			"whereClausePartTranHist" => "PartNum = '".$partNum."'",
			"whereClausePartTranAttrValueSet" => "",
			"pageSize" => 0,
			"absolutePage" => 0,
		);

		return $this->curl("Erp.BO.PartTranHistSvc/GetRows",$data);
	}

	function getPartTransactionHistory2($partNum,$plantCode){
		date_default_timezone_set('America/Toronto');
		$data = array(
			"ipDimCode" => "",
			"ipLotNum" => "",
			"ipPartNum" => $partNum,
			"ipPlant" => $plantCode,
			"ipSortBy" => "",
			"ipStartingDate" => "",
			"ipTranDate" => date("Y-m-d", time())."T00:00:00",
		);

		return $this->curl("Erp.BO.PartTranHistSvc/RunPartTranHistory",$data);
	}

	function getPartPurchaseAdvisor($partNum){
		$data = array(
			"cPartNum" => $partNum,
			"iPONum" => 0,
			"cAPVType" => "",
			"cClassID" => "",
			"cOpCode" => "",
			"iCustNum" => 0,
		);

		return $this->curl("Erp.BO.PurchaseAdvisorSvc/GetPurchaseAdvisor",$data);
	}

	function imageExists($imageID){
		$data = array(
			"imageID" => $imageID
		);

		return $this->curl("Erp.BO.ImageSvc/ExistsImageID",$data);
	}

	function getImageDS($imageID){
		$data = array(
			"imageID" => $imageID
		);

		return $this->curl("Erp.BO.ImageSvc/GetByID",$data);
	}

	function getNewImageDS(){
		$data = array(
			"ds" => new stdClass()
		);

		return $this->curl("Erp.BO.ImageSvc/GetNewImage",$data);
	}

	function deleteImage($company,$imageID){
		return $this->curl("Erp.BO.ImageSvc/Images(".$company.",".$imageID.")",false,"DELETE");
	}

	function updateImage($image){
		return $this->curl("Erp.BO.ImageSvc/Images(".$image->Company.",".$image->ImageID.")",$image,"PATCH");
	}

	function searchForJob($plant,$closed,$search){
		$search = trim($search);
		$converted_closed = $closed ? '1' : '0';
		$data = array(
			"whereClauseJobHead" => "JobClosed = ".$converted_closed." and Plant = '".$plant."' and (CreatedBy LIKE '%".$search."%' OR PartDescription LIKE '%".$search."%' OR PartNum LIKE '%".$search."%' OR JobNum LIKE '%".$search."%')",
			"whereClauseJobHeadAttch" => "Company = '0'",
			"whereClauseJobAsmbl" => "Company = '0'",
			"whereClauseJobAsmblAttch" => "Company = '0'",
			"whereClauseJobAsmblInsp" => "Company = '0'",
			"whereClauseJobMtl" => "Company = '0'",
			"whereClauseJobMtlAttch" => "Company = '0'",
			"whereClauseJobMtlInsp" => "Company = '0'",
			"whereClauseJobMtlRefDes" => "Company = '0'",
			"whereClauseJobMtlRestriction" => "Company = '0'",
			"whereClauseJobMtlRestrictSubst" => "Company = '0'",
			"whereClauseJobOper" => "Company = '0'",
			"whereClauseJobOperAttch" => "Company = '0'",
			"whereClauseJobOperAction" => "Company = '0'",
			"whereClauseJobOperActionParam" => "Company = '0'",
			"whereClauseJobStage" => "Company = '0'",
			"whereClauseJobOperInsp" => "Company = '0'",
			"whereClauseJobOperMachParam" => "Company = '0'",
			"whereClauseJobOpDtl" => "Company = '0'",
			"whereClauseJobResources" => "Company = '0'",
			"whereClauseJobOperRestriction" => "Company = '0'",
			"whereClauseJobOperRestrictSubst" => "Company = '0'",
			"whereClauseJobAsmblRestriction" => "Company = '0'",
			"whereClauseJobAsmblRestrictSubst" => "Company = '0'",
			"whereClauseJobAsmRefDes" => "Company = '0'",
			"whereClauseJobAudit" => "Company = '0'",
			"whereClauseJobPart" => "Company = '0'",
			"whereClauseJobProd" => "Company = '0'",
			"pageSize" => 50,
			"absolutePage" => 0
		);
		
		return $this->curl("Erp.BO.JobEntrySvc/GetRows",$data);
	}

	function getJobLabor($jobNum){
		$data = array(
			"whereClauseLaborDtl" => "JobNum = '".$jobNum."'",
			"pageSize" => 0,
			"absolutePage" => 0
		);

		return $this->curl("Erp.BO.LaborDtlSearchSvc/GetRows",$data);
	}

	function getEmployeeLabor($EmpNum,$pageSize,$absolutePage){
		$data = array(
			"whereClauseLaborDtl" => "EmployeeNum = '".$EmpNum."' BY ClockInDate DESC",
			"pageSize" => $pageSize,
			"absolutePage" => $absolutePage
		);

		return $this->curl("Erp.BO.LaborDtlSearchSvc/GetRows",$data);
	}

	function getNewRcvHead(){
		$data = array(
			"ds" => new stdClass(),
			"vendorNum" => 0,
			"purPoint" => ""
		);
		$data["ds"]->ReceiptDataSet =  new stdClass();

		return $this->curl("Erp.BO.ReceiptSvc/GetNewRcvHead",$data);
	}

	function getPurchaseOrderInfo($poNum,$ds){
		$data = array(
			"ds" => $ds,
			"poNum" => $poNum,
			"fromReceiptEntryNewRcpt" =>false,
			"vendorNum" => 0,
			"purPoint" => "",
			"warnMsg" => "",
			"poStatusWarnMsg" => ""
		);

		return $this->curl("Erp.BO.ReceiptSvc/GetPOInfo",$data);
	}

	function getVenInfo($vendorNum){
		$data = array(
			"whereClauseVendor" => "VendorNum = ".$vendorNum." BY Name",
			"whereClauseVendorAttch" => "",
			"whereClauseEntityGLC" => "",
			"whereClauseTaxExempt" => "",
			"whereClausePartner" => "",
			"whereClausePEVendWhldHist" => "",
			"whereClauseVendRestriction" => "",
			"whereClauseVendBank" => "",
			"whereClauseVendBankAttch" => "",
			"whereClauseVendCntMain" => "",
			"whereClauseVenMFBill" => "",
			"whereClauseVendorPP" => "",
			"whereClauseVenPPMFBill" => "",
			"whereClauseVenPPUPSEmail" => "",
			"whereClauseVendPPRestriction" => "",
			"whereClauseVendCnt" => "",
			"whereClauseVendCntAttch" => "",
			"whereClauseVenUPSEmail" => "",
			"whereClauseVendRemitTo" => "",
			"pageSize" => 0,
			"absolutePage" => 0,
			"morePages" => false,
		);

		return $this->curl("Erp.BO.VendorSvc/GetRows",$data);
	}

	function getByIdChkContainerID($vendorNum,$packSlipNum,$poNum){
		$data = array(
			"piVendorNum" => $vendorNum,
			"pcPurPoint" => "",
			"pcPackSlip" => $packSlipNum,
			"piPONum" => $poNum
		);

		return $this->curl("Erp.BO.ReceiptSvc/GetByIdChkContainerID",$data);
	}

	function GetNewRcvHeadWithPONum($vendorNum,$poNum){
		$data = array(
			"ds" => new stdClass(),
			"vendorNum" => $vendorNum,
			"purPoint" => "",
			"poNum" => $poNum
		);

		return $this->curl("Erp.BO.ReceiptSvc/GetNewRcvHeadWithPONum",$data);
	}

	function receiptUpdate($vendorNum,$packSlipNum,$packLine,$runChkDtl,$partNum,$ds){
		$data = array(
			"RunChkLCAmtBeforeUpdate" => true,
			"RunChkHdrBeforeUpdate" => true,
			"ipVendorNum" => $vendorNum,
			"ipPurPoint" => "",
			"ipPackSlip" => $packSlipNum,
			"ipPackLine" => $packLine,
			"cLCAmtMessage" => "",
			"opUpliftWarnMsg" => "",
			"opReceiptWarnMsg" => "",
			"opArriveWarnMsg" => "",
			"lRunChkDtl" => $runChkDtl,
			"qMessageStr" => "",
			"sMessageStr" => "",
			"lcMessageStr" => "",
			"pcMessageStr" => "",
			"lRunChkDtlCompliance" => false,
			"qDtlComplianceMsgStr" => "",
			"lRunCheckCompliance" => false,
			"lCompliant" => false,
			"lRunPreUpdate" => true,
			"lRequiresUserInput" => false,
			"lRunCreatePartLot" => false,
			"partNum" => $partNum,
			"lotNum" => "",
			"lOkToUpdate" => true,
			"lUpdateWasRun" => false,
			"wrnLines" => "",
			"ds" => $ds
		);

		return $this->curl("Erp.BO.ReceiptSvc/UpdateMaster",$data);
	}
	
	function getNewRcvDtl($vendorNum,$packSlipNum,$ds){
		$data = array(
			"ds" => $ds,
			"vendorNum" => $vendorNum,
			"purPoint" => $ds->RcvHead[0]->PurPoint,
			"packSlip" => $packSlipNum
		);

		return $this->curl("Erp.BO.ReceiptSvc/GetNewRcvDtl",$data);
	}

	function getDtlPOLineInfo($vendorNum,$packSlipNum,$poLine,$packLine,$ds){
		$data = array(
			"ds" => $ds,
			"vendorNum" => $vendorNum,
			"purPoint" => $ds->RcvHead[0]->PurPoint,
			"packSlip" => $packSlipNum,
			"packLine" => $packLine,
			"poLine" => $poLine,
			"serialWarning" => ""
		);

		return $this->curl("Erp.BO.ReceiptSvc/GetDtlPOLineInfo",$data);
	}

	function getDtlQtyInfo($vendorNum,$packSlipNum,$packLine,$qty,$ds){
		$data = array(
			"ds" => $ds,
			"vendorNum" => $vendorNum,
			"purPoint" => $ds->RcvHead[0]->PurPoint,
			"packSlip" => $packSlipNum,
			"packLine" => $packLine,
			"inputOurQty" => $qty,
			"inputIUM" => "",
			"whichField" => "QTY",
			"warnMsg" => "",
		);

		return $this->curl("Erp.BO.ReceiptSvc/GetDtlQtyInfo",$data);
	}

	function setReceiptToLocation($vendorNum,$packSlipNum,$packLine,$ds){
		$data = array(
			"vendorNum" => $vendorNum,
			"purPoint" => $ds->RcvHead[0]->PurPoint,
			"packSlip" => $packSlipNum,
			"packLine" => $packLine,
			"ds" => $ds
		);

		return $this->curl("Erp.BO.ReceiptSvc/SetToLocation",$data);
	}

	function checkReceiptOnLeaveHead($vendorNum,$packSlipNum,$purPoint){
		$data = array(
			"vendorNum" => $vendorNum,
			"purPoint" => $purPoint,
			"packSlip" => $packSlipNum,
			"warnMsg" => "",
		);

		return $this->curl("Erp.BO.ReceiptSvc/CheckOnLeaveHead",$data);
	}

	function onChangeDtlWareHouseCode($vendorNum,$packSlip,$packLine,$newWareHouseCode,$ds){
		$data = array(
			"vendorNum" => $vendorNum,
			"purPoint" => $ds->RcvHead[0]->PurPoint,
			"packSlip" => $packSlip,
			"packLine" => $packLine, 
			"newWareHouseCode" => $newWareHouseCode,
			"requiresUserInput" => true,
			"questionMsg" => "",
			"warnMsg" => "",
			"ds" => $ds,
		);

		return $this->curl("Erp.BO.ReceiptSvc/OnChangeDtlWareHouseCode",$data);
	}

	function onChangeDtlBinNum($vendorNum,$packSlip,$packLine,$newBinNum,$ds){
		$data = array(
			"vendorNum" => $vendorNum,
			"purPoint" => $ds->RcvHead[0]->PurPoint,
			"packSlip" => $packSlip,
			"packLine" => $packLine,
			"newBinNum" => $newBinNum,
			"requiresUserInput" => true,
			"questionMsg" => "",
			"ds" => $ds,
		);

		return $this->curl("Erp.BO.ReceiptSvc/OnChangeDtlBinNum",$data);
	}

	function onReceiptChangeDtlReceived($vendorNum,$packSlipNum,$packLine,$ds){
		$data = array(
			"vendorNum" => $vendorNum,
			"purPoint" => $ds->RcvHead[0]->PurPoint,
			"packSlip" => $packSlipNum,
			"packLine" => $packLine,
			"ipReceived" => true,
			"ds" => $ds
		);

		return $this->curl("Erp.BO.ReceiptSvc/OnChangeDtlReceived",$data);
	}

	function getWarehouses($plant){
		$data = array(
			"whereClauseWarehse" => "Plant = '".$plant."' BY WarehouseCode", 
			"whereClauseWarehseAttch" => "",
			"whereClauseEntityGLC" => "",
			"whereClauseWhsePrinter" => "",
			"whereClauseWarehseABC" => "",
			"pageSize" => 100,
			"absolutePage" => 0,
			"morePages" => false
		);

		return $this->curl("Erp.BO.WarehseSvc/GetRows",$data);
	}

	function getPartBinSearch($warehouseCode){
		$data = array(
			"pageSize" => 100,
			"absolutePage" => 0,
			"whereClause" => "(WhseCode = '".$warehouseCode."' AND displayAllBins = true AND BinType = 'STD') AND  DisplayAllBins=True",
			"morePages" => false,
		);

		return $this->curl("Erp.BO.PartBinSearchSvc/GetPartBinSearch",$data);
	}

	function fileExists($parentTable,$fileName){
		$data = array(
			"docTypeID" => "",
			"parentTable" => $parentTable, //RcvHead
			"fileName" => $fileName, //Packing Slip Print - Labels_1285234.pdf
		);

		return $this->curl("Ice.BO.AttachmentSvc/FileExists",$data);
	}

	function uploadFile($parentTable,$fileName,$psData){
		$data = array(
			"docTypeID" => "",
			"parentTable" => $parentTable, //RcvHead
			"fileName" => $fileName, //Packing Slip Print - Labels_1285234.pdf
			"data" => $psData
		);

		return $this->curl("Ice.BO.AttachmentSvc/UploadFile",$data);
	}

	function getNewRcvHeadAttch($ds,$vendorNum,$packSlip){
		$data = array(
			"ds" => $ds,
			"vendorNum" => $vendorNum, //2114
			"purPoint" => "",
			"packSlip" => $packSlip, //NEWPACKINGSLIP01
		);

		return $this->curl("Erp.BO.ReceiptSvc/GetNewRcvHeadAttch",$data);
	}

	function getTransferOrdersWithKeywords($plant,$keywords){
		$data = array(
			"queryID" => "JB_TOSHIPPEDNOTCLOSED",
			"executionParams" => new stdClass(),
		);

		$whereValue = "(TFOrdHed_Plant = N'".$plant."') AND (";
		foreach ($keywords as $keyword) {
			$whereValue .= "TFOrdDtl_PartNum LIKE '%".$keyword."%' OR ";
			$whereValue .= "Part_PartDescription LIKE '%".$keyword."%' OR ";
		}

		if($this->endsWith($whereValue,"OR ")){
			$whereValue = substr($whereValue, 0, -3);
		}

		$whereValue .= " )";

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => $whereValue
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function getPlantTransferOrders($plant){
		$data = array(
			"queryID" => "JB_TOSHIPPEDNOTCLOSED",
			"executionParams" => new stdClass(),
		);

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => "TFOrdHed_Plant = N'".$plant."'"
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function getTransferOrders($plant,$ToPlant,$search){
		$data = array(
			"queryID" => "JB_TOSHIPPEDNOTCLOSED",
			"executionParams" => new stdClass(),
		);

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => "((TFOrdHed_ToPlant = N'".$ToPlant."') AND (TFOrdHed_Plant = N'".$plant."')) AND ("
			."TFOrdHed_EntryPerson LIKE '%".$search."%' OR "
			."TFOrdHed_TFOrdNum LIKE '%".$search."%' OR "
			."TFOrdDtl_PartNum LIKE '%".$search."%' OR "
			."Part_PartDescription LIKE '%".$search."%' OR "
			."PartClass_Description LIKE '%".$search."%' OR "
			."Person_Name LIKE '%".$search."%' OR "
			."TFOrdDtl_SupplyJobNum LIKE '%".$search."%' OR "
			."TFOrdDtl_DemandJobNum LIKE '%".$search."%' ) "
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function getTransferOrder($toNum){
		$data = array(
			"queryID" => "JB_TOSHIPPEDNOTCLOSED",
			"executionParams" => new stdClass(),
		);

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => "TFOrdHed_TFOrdNum = '".$toNum."'"
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function getSalesOrdersWithKeywords($plant,$keywords){
		$data = array(
			"queryID" => "JB_OPENORDERSBYRELEASEPLANT",
			"executionParams" => new stdClass(),
		);

		$whereValue = "(OrderRel_Plant = '".$plant."') AND (";
		foreach ($keywords as $keyword) {
			$whereValue .= "OrderDtl_PartNum LIKE '%".$keyword."%' OR ";
			$whereValue .= "OrderDtl_LineDesc LIKE '%".$keyword."%' OR ";
		}

		if($this->endsWith($whereValue,"OR ")){
			$whereValue = substr($whereValue, 0, -3);
		}

		$whereValue .= " )";

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => $whereValue
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function getPlantSalesOrders($plant){
		$data = array(
			"queryID" => "JB_OPENORDERSBYRELEASEPLANT",
			"executionParams" => new stdClass(),
		);

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => "OrderRel_Plant = '".$plant."'"
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function getSalesOrders($plant,$search){
		$data = array(
			"queryID" => "JB_OPENORDERSBYRELEASEPLANT",
			"executionParams" => new stdClass(),
		);

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => "(OrderRel_Plant = '".$plant."') AND ( "
			          ."Customer_CustID LIKE '%".$search."%' OR "
			          ."Customer_Name LIKE '%".$search."%' OR "
			          ."OrderHed_OrderNum LIKE '%".$search."%' OR "
			          ."OrderHed_PONum LIKE '%".$search."%' OR "
			          ."OrderDtl_PartNum LIKE '%".$search."%' OR "
			          ."OrderDtl_LineDesc LIKE '%".$search."%' OR "
			          ."OrderRel_Plant LIKE '%".$search."%' OR "
			          ."OrderDtl_ProdCode LIKE '%".$search."%' ) "
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function getSalesOrder($soNum){
		$data = array(
			"queryID" => "JB_OPENORDERSBYRELEASEPLANT",
			"executionParams" => new stdClass(),
		);

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => "OrderHed_OrderNum = '".$soNum."'"
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function getPartCost($partNum){
		$data = array(
			"whereClausePartCost" => "PartNum='".$partNum."' BY PartNum",
			"pageSize" => 0,
			"absolutePage" => 0,
			"morePages" => False,
		);

		return $this->curl("Erp.BO.PartCostSearchSvc/GetRows",$data);
	}

	function endsWith($haystack,$needle) {
	    $length = strlen( $needle );
	    if( !$length ) {
	        return true;
	    }
	    return substr( $haystack, -$length ) === $needle;
	}

	function getPartsCost($partNums){
		$data = array(
			"whereClausePartCost" => "",
			"pageSize" => 0,
			"absolutePage" => 0,
			"morePages" => False,
		);

		foreach ($partNums as $partNum) {
			$data["whereClausePartCost"] .= "PartNum='".$partNum."' OR ";
		}

		if($this->endsWith($data["whereClausePartCost"],"OR ")){
			$data["whereClausePartCost"] = substr($data["whereClausePartCost"], 0, -3);
		}

		return $this->curl("Erp.BO.PartCostSearchSvc/GetRows",$data);
	}

	function getUoms(){
		$data = array(
			"whereClauseUOMClass" => " BY UOMClassID",
			"whereClauseUOMConv" => "",
			"pageSize" => 100,
			"absolutePage" => 0,
			"morePages" => false,
		);

		return $this->curl("Erp.BO.UOMClassSvc/GetRows",$data);
	}

	function getJobMtl($jobNum){
		$data = array(
			"serviceNamespace" => "Erp:BO:JobMtlSearch",
			"whereClause" => "JobNum = '".$jobNum."'",
			"columnList" => "JobNum,MtlSeq,AssemblySeq,PartNum,Description,IssuedComplete,QtyPer,RequiredQty,IUM,RelatedOperation,IssuedQty,WarehouseCode,Plant,BaseUOM,JobComplete,Company"
		);

		return $this->curl("Ice.Lib.BOReaderSvc/GetList",$data);
	}

	function getNewJobAsmblMultiple($company,$jobNum,$assemSeq){
		$data = array(
			"pcTranType" => "STK-MTL",
			"pcMtlQueueRowID" => "00000000-0000-0000-0000-000000000000",
			"pCallProcess" => "IssueMaterial",
			"ds" => new stdClass(),
			"pcMessage" => ""
		);

		$data["ds"]->SelectedJobAsmbl = array();
		$data["ds"]->SelectedJobAsmbl[0] = new stdClass();
		$data["ds"]->SelectedJobAsmbl[0]->Company = $company;
		$data["ds"]->SelectedJobAsmbl[0]->JobNum = $jobNum;
		$data["ds"]->SelectedJobAsmbl[0]->AssemblySeq = $assemSeq;
		$data["ds"]->SelectedJobAsmbl[0]->SysRowID = "00000000-0000-0000-0000-000000000000";
		$data["ds"]->SelectedJobAsmbl[0]->RowMod = "A";
		$data["ds"]->ExtensionTables = array();

		return $this->curl("Erp.BO.IssueReturnSvc/GetNewJobAsmblMultiple",$data);
	}

	function onChangingToJobSeq($mtlSeq,$issueReturnDS){
		$data = array(
			"piToJobSeq" => $mtlSeq,
			"ds" => new stdClass()
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;

		return $this->curl("Erp.BO.IssueReturnSvc/OnChangingToJobSeq",$data);
	}

	function onChangeToJobSeq($mtlSeq,$issueReturnDS){
		$data = array(
			"ds" => new stdClass(),
			"pCallProcess" => "IssueMaterial",
			"pcMessage" => ""
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;
		$data["ds"]->IssueReturn[0]->ToJobSeq = $mtlSeq;
		$data["ds"]->IssueReturn[0]->RowMod = "U";

		return $this->curl("Erp.BO.IssueReturnSvc/OnChangeToJobSeq",$data);
	}

	function onChangeToWarehouse($warehouseCode,$issueReturnDS){
		$data = array(
			"ds" => new stdClass(),
			"pCallProcess" => "IssueMaterial",
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;
		$data["ds"]->IssueReturn[0]->ToWarehouseCode = $warehouseCode;
		$data["ds"]->IssueReturn[0]->RowMod = "U";

		return $this->curl("Erp.BO.IssueReturnSvc/OnChangeToWarehouse",$data);
	}

	function onChangeFromWarehouse($warehouseCode,$issueReturnDS){
		$data = array(
			"ds" => new stdClass(),
			"pCallProcess" => "IssueMaterial",
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;
		$data["ds"]->IssueReturn[0]->FromWarehouseCode = $warehouseCode;
		$data["ds"]->IssueReturn[0]->RowMod = "U";

		return $this->curl("Erp.BO.IssueReturnSvc/OnChangeFromWarehouse",$data);
	}

	function onChangingFromBinNum($binNum,$issueReturnDS){
		$data = array(
			"ds" => new stdClass(),
			"pcMessage" => "",
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;
		$data["ds"]->IssueReturn[0]->FromBinNum = $binNum;
		$data["ds"]->IssueReturn[0]->RowMod = "U";

		return $this->curl("Erp.BO.IssueReturnSvc/OnChangingFromBinNum",$data);
	}

	function onChangeFromBinNum($issueReturnDS){
		$data = array(
			"ds" => new stdClass(),
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;
		$data["ds"]->IssueReturn[0]->RowMod = "U";

		return $this->curl("Erp.BO.IssueReturnSvc/OnChangeFromBinNum",$data);
	}

	function onChangeTranQty($qty,$issueReturnDS){
		$data = array(
			"pdTranQty" => $qty,
			"ds" => new stdClass()
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;

		return $this->curl("Erp.BO.IssueReturnSvc/OnChangeTranQty",$data);
	}

	function onChangeLotNum($lotNum,$issueReturnDS){
		$data = array(
			"pcLotNum" => $lotNum,
			"ds" => new stdClass()
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;

		return $this->curl("Erp.BO.IssueReturnSvc/OnChangeLotNum",$data);
	}

	function prePerformMaterialMovement($issueReturnDS){
		$data = array(
			"ds" => new stdClass()
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;

		return $this->curl("Erp.BO.IssueReturnSvc/PrePerformMaterialMovement",$data);
	}

	function masterInventoryBinTests($issueReturnDS){
		$data = array(
			"ds" => new stdClass()
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;

		return $this->curl("Erp.BO.IssueReturnSvc/MasterInventoryBinTests",$data);
	}

	function performMaterialMovement($issueReturnDS){
		$data = array(
			"plNegQtyAction" => false,
			"ds" => new stdClass(),
			"legalNumberMessage" => "",
			"partTranPKs" => "",
		);

		$data["ds"]->IssueReturn = array();
		$data["ds"]->IssueReturn[0] = $issueReturnDS;

		return $this->curl("Erp.BO.IssueReturnSvc/PerformMaterialMovement",$data);
	}

	function onMassIssueChangeJobNum($jobNum,$ds){
		$data = array(
			"pcJobNum" => $jobNum,
			"ds" => $ds,
		);

		return $this->curl("Erp.BO.MassIssueToMfgSvc/OnChangeJobNum",$data);
	}

	function buildMassIssueBrowse($ds){
		$data = array(
			"ds" => $ds,
			"ipSysRowID" => $ds->MassIssueInput[0]->SysRowID,
			"pcMessage" => "",
		);

		for ($i=0; $i < sizeof($ds->MassIssueInput); $i++) {
			$ds->MassIssueInput[$i]->RowMod = "U";
			$ds->MassIssueInput[$i]->IncludeSubassemblies = true;
		}

		return $this->curl("Erp.BO.MassIssueToMfgSvc/BuildMassIssueBrowse",$data);
	}

	function onChangeBinNum($warehouse,$partNum,$binNum,$massIssueInputPart){
		$data = array(
			"pcWarehouse" => $warehouse,
			"pcPartNum" => $partNum,
			"pcBinNum" => $binNum,
			"ds" => new stdClass(),
		);

		$data["ds"]->MassIssue = array();
		$data["ds"]->MassIssue[0] = $massIssueInputPart;
		$data["ds"]->MassIssue[0]->RowMod = "U";

		return $this->curl("Erp.BO.MassIssueToMfgSvc/OnChangeBinNum",$data);
	}

	function massIssueAll($ds){
		$data = array(
			"pcView" => "Open",
			"ds" => $ds,
		);

		for ($i=0; $i < sizeof($data["ds"]->MassIssue); $i++) {
			$data["ds"]->MassIssue[$i]->RowMod = "U";
		}

		return $this->curl("Erp.BO.MassIssueToMfgSvc/IssueAll",$data);
	}

	function massIssueNegativeStockCheck($jobNum,$ds){
		$data = array(
			"pcJobNum" => $jobNum,
			"ds" => $ds,
			"pcQuestion" => "",
			"pcError" => "",
		);

		for ($i=0; $i < sizeof($data["ds"]->MassIssue); $i++) {
			$data["ds"]->MassIssue[$i]->RowMod = "U";
		}

		return $this->curl("Erp.BO.MassIssueToMfgSvc/NegativeStockCheck",$data);
	}

	function prePerformMassIssueHT($ds){
		$data = array(
			"ds" => $ds,
			"htPrePerform" => new stdClass()
		);

		for ($i=0; $i < sizeof($data["ds"]->MassIssue); $i++) {
			$data["ds"]->MassIssue[$i]->RowMod = "U";
		}

		return $this->curl("Erp.BO.MassIssueToMfgSvc/PrePerformMassIssueHT",$data);
	}

	function prePerformMassIssue($ds){
		$data = array(
			"pdtTranDate" => "2022-02-23T15:27:22.600Z",
			"ipTranDocType" => "",
			"ipSysTranType" => "STK-MTL",
			"ds" => $ds
		);

		for ($i=0; $i < sizeof($data["ds"]->MassIssue); $i++) {
			$data["ds"]->MassIssue[$i]->RowMod = "U";
		}

		return $this->curl("Erp.BO.MassIssueToMfgSvc/PrePerformMassIssue",$data);
	}

	function performMassIssue($jobNum,$ds){
		date_default_timezone_set('America/Toronto');
		$data = array(
			"pcJobNum" => $jobNum,
			"pdtTranDate" => date("Y-m-d 12:00:00 \A\M", time()),
			"piCallNum" => 0, 
			"plMaterialComplete" => false, 
			"plNegStockCheckContinue" => true, 
			"ds" => $ds,
			"pcMessage" => "",
		);

		for ($i=0; $i < sizeof($data["ds"]->MassIssue); $i++) {
			$data["ds"]->MassIssue[$i]->RowMod = "U";
		}

		return $this->curl("Erp.BO.MassIssueToMfgSvc/PerformMassIssue",$data);
	}

	function getOpenPurchaseOrders($plantNum){
		$data = array(
			"queryID" => "SW_OPEN_PO",
			"executionParams" => new stdClass(),
		);

		$data["executionParams"]->ExecutionSetting = array();
		$data["executionParams"]->ExecutionSetting[0] = new stdClass();
		$data["executionParams"]->ExecutionSetting[0] = array(
			"Name" => "Where",
			"Value" => "PORel_Plant = '".$plantNum."'"
		);

		return $this->curl("Ice.BO.DynamicQuerySvc/ExecuteByID",$data);
	}

	function negOnHandQty($warehouseCode,$page){
		$data = array(
			"whereClausePartWhseSearch" => "WarehouseCode = '".$warehouseCode."' and OnHandQty < 0",
			"pageSize" => 25,
			"absolutePage" => $page
		);

		return $this->curl("Erp.BO.PartWhseSearchSvc/GetRows",$data);
	}

	function curl($method,$data = false,$type = "POST"){
		if (session_status() === PHP_SESSION_NONE) {
		    session_start();
		}
		
		if(!isset($_SESSION["key"])){
			//User not logged in
			return array( "http" => 999, "result" => array());
		}

		if(self::PILOT || (isset($_SESSION["pilot"]) && $_SESSION["pilot"] == 1)){
			$methodURL = self::API_PILOT_URL.$method;
		} else {
			$methodURL = self::API_URL.$method;
		}
		
		$ch = curl_init($methodURL);
		
		if($data != false){
			$payload = json_encode($data,JSON_UNESCAPED_SLASHES);
		}

		if(self::DEBUG){
			echo "
			-----
			".
			$method."

			";
			print_r($data);
			echo "
			-
			";
			print_r($payload);
			echo "
			-----

			";
		}

		if($data == false){
			curl_setopt($ch, CURLOPT_POSTFIELDS, array());
		} else {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $payload );
		}
			
		$header = array();
		$header[] = 'Content-Type: application/json';
		$header[] = 'Accept: application/json';
		$header[] = 'Authorization: Basic '.$_SESSION["key"];

		#Not recommended, but required when the API url's have an invalid cert
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		curl_close($ch);
		return array( "http" => $httpcode, "method" => $method, "result" => json_decode($result));
	}
}

?>