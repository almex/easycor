<?php
include_once 'api.php';
include_once 'inventoryFunctions.php';
include_once 'global.php';
$api = new API();

if(isset($_POST["tagNum"]) && 
   isset($_POST["partNum"]) && 
   isset($_POST["cycle"]) && 
   isset($_POST["name"]) &&
   isset($_POST["warehouse"]) &&
   isset($_POST["btnID"]) &&
   isset($_POST["plant"]) &&
   isset($_POST["countedQTY"])){

    $return = array(
      "result" => true,
      "variance" => false,
      "message" => "",
      "varianceMsg" => "",
      "btnID" => $_POST["btnID"]
    );

    $epicorUpdated = false;
    $cycle = explode("-",$_POST["cycle"]);

    $tagData = $api->getTagDataSet($_POST["plant"],$_POST["tagNum"],$_POST["warehouse"]);
    if($tagData["http"] == 200){

      //Variance check
      $plant = $tagData["result"]->returnObj->CCTag[0]->Plant;
      $binNum = $tagData["result"]->returnObj->CCTag[0]->BinNum;
      $frozenCost = $tagData["result"]->returnObj->CCTag[0]->FrozenCost;
      $frozenQOH = $tagData["result"]->returnObj->CCTag[0]->FrozenQOH;

      $dif = abs($_POST["countedQTY"] - $frozenQOH);
      $cost = $dif*$frozenCost;
      $cost = number_format((float)$cost, 2, '.', '');

      if($dif > 10){
        $return["variance"] = true;
        if($_POST["countedQTY"] < $frozenQOH){
          $return["varianceMsg"] = "There is a quantity variance of -".$dif."<br><br>";
        } else {
          $return["varianceMsg"] = "There is a quantity variance of +".$dif."<br><br>";
        }
      }

      if($cost > 100){
        $return["variance"] = true;
        if($_POST["countedQTY"] < $frozenQOH){
          $return["varianceMsg"] = "There is a quantity variance of -".$dif."<br><br>";
          $return["varianceMsg"] .= "There is a cost variance of -$".$cost."<br><br>";
        } else {
          $return["varianceMsg"] = "There is a quantity variance of +".$dif."<br><br>";
          $return["varianceMsg"] .= "There is a cost variance of +$".$cost."<br><br>";
        }
      }

      if($return["variance"]){
        $return["varianceMsg"] .= "<b>DOUBLE CHECK THE COUNT FOR ".$_POST["partNum"]." AND ADD A COMMENT!</b>";
      }

      //Countinue updating tag
      $tagData = $api->setTagCount($_POST["countedQTY"],$tagData["result"]->returnObj->CCTag);
      if($tagData["http"] == 200){

        if(isset($_POST["lot"]) && strlen(trim($_POST["lot"])) > 0){
          $tagData["result"]->parameters->ds->CCTag[0]->LotNum = $_POST["lot"];
        }

        $tagData = $api->updateTag($_POST["name"],$tagData["result"]->parameters->ds);
        if($tagData["http"] == 200){
          $epicorUpdated = true;
        }
      }
    }

    if(!$epicorUpdated){
      $return["result"] = false;
      $return["message"] = "Unable to update Epicor";

      if(isset($tagData["result"]->ErrorMessage)){
        $return["message"] = $tagData["result"]->ErrorMessage;
      }
    }

    if($epicorUpdated){
      $db = new PDO('sqlite:../db/'.$invDatabase);
      
  		if(countExists($db,$_POST["tagNum"],$_POST["warehouse"],$_POST["cycle"],$_POST["name"])){
  			updateCount($db,$_POST["tagNum"],$_POST["warehouse"],$_POST["partNum"],$_POST["cycle"],$_POST["name"],$_POST["countedQTY"]);
  		} else {
  			insertCount($db,$_POST["tagNum"],$_POST["warehouse"],$_POST["partNum"],$_POST["cycle"],$_POST["name"],$_POST["countedQTY"]);
  		}
    }

    echo json_encode($return);
}

?>