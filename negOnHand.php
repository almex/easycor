<?php
session_start();

include_once 'inc/global.php';
include_once 'inc/inventoryFunctions.php';
include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;

//negOnHandQty($warehouseCode,$page)
//getWarehouses($plant)

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function htmlEcho($string){
	echo htmlspecialchars($string);
}


$plantsResult = $api->getAllPlants();
if(isset($_GET["plant"])){
	$warehouseResult = $api->getWarehouses($_GET["plant"]);
	$step = 2;
}

if(isset($_GET["warehouse"]) && isset($_GET["page"])){
	$qtyResult = $api->negOnHandQty($_GET["warehouse"],$_GET["page"]);
	$step = 3;
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;" class="notranslate" translate="no">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="google" content="notranslate" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
				
			<!-- Step One -->
			<?php if($step == 1) { ?>
				<center>

					<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Select Plant</b>
					</div>
					<div class="card-body">
						<center>
						<form action="negOnHand.php" method="GET">
							<br>
							<select class="form-select" aria-label="Plant" name="plant">
							<?php foreach($plantsResult["result"]->returnObj->PlantList as $plant){ 
								$selected = "";
						  		if($plant->Plant == 200){
						  			$selected = "selected";
						  		}
							?>
							  <option value="<?php echo $plant->Plant; ?>" <?php echo $selected;?> ><?php echo $plant->Name; ?></option>
							 <?php } ?>
							</select>
							<br>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
			
				</center>
			<?php } ?>

			<!-- Step Two -->
			<?php if($step == 2) { ?>
				<center>

					<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Select Warehouse</b>
					</div>
					<div class="card-body">
						<center>
						<form action="negOnHand.php" method="GET">
							<br>
							<select class="form-select" aria-label="Warehouse" name="warehouse">
							<?php 
								foreach($warehouseResult["result"]->returnObj->Warehse as $whse){ 
							?>
							  <option value="<?php echo $whse->WarehouseCode; ?>"><?php echo $whse->Description; ?></option>
							 <?php } ?>
							</select>
							<br>
						  <br>
						  <input type="hidden" name="page" value="0">
						  <input type="hidden" name="plant" value="<?php echo $_GET["plant"]; ?>">
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
			
				</center>
			<?php } ?>

			<!-- Step Three -->
			<?php if($step == 3) { ?>

				<center>
					<br>
					<a href="negOnHand.php?plant=<?php echo $_GET["plant"]; ?>" class="btn btn-primary"><b>Change Warehouse</b></a>
				</center>

				<?php if(count($qtyResult["result"]->returnObj->PartWhseSearch) == 0) { ?>
					<br><center>
					<div class="alert alert-success" role="alert" style="max-width:400px;">
					  <b>No parts with negative on hand quantity found!</b>
					  <br><br>
					  <img src="img/check.png" style="max-width: 200px;">
					</div></center><br>
				<?php } else { ?>

					<br><center>
					<div class="alert alert-danger" role="alert" style="max-width:400px;">
					  <b>Parts with negative on hand quantity found!</b>
					</div></center><br>

					<center>
					<div class="table-responsive" style="max-width: 600px;">
						<table class="table" style="font-size: 0.8rem;">
						  <tbody>
						  	<?php foreach ($qtyResult["result"]->returnObj->PartWhseSearch as $part) { 

						  			if(strlen(trim($part->CountedDate)) > 0){
						  				$part->CountedDate = explode("T", $part->CountedDate)[0];
						  			}

						  		?>
						  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
							      <th scope="col">Part</th>
							      <th scope="col">Plant</th>
							      <th scope="col">Warehouse</th>
							    </tr>
						  		<tr>
						  			<td><a target="_blank" href="partFind.php?part=<?php echo urlencode($part->PartNum); ?>"><?php echo $part->PartNum; ?></a></td>
						  			<td><?php echo $part->Plant; ?></td>
						  			<td><?php echo $part->WarehouseCode; ?></td>
						  		</tr>
						  		<tr>
						  		  <th scope="col" >On Hand Qty</th>
							    </tr>
							    <tr>
							    	<td><?php echo $part->OnHandQty; ?></td>
						  		</tr>
						  	<?php } ?>
						  </tbody>
						</table>
					</div>
					</center>

				<?php } ?>

			<?php } ?>

				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>