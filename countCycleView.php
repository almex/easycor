<?php
ini_set('session.gc_maxlifetime', 28800);
session_set_cookie_params(28800);
session_start();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
  header("Location: login.php");
  die();
}

include_once 'inc/global.php';
include_once 'inc/inventoryFunctions.php';
include_once 'inc/api.php';
$api = new API();

$error = false;
$errorText = "";
$step = 1;
$showAllBins = false;

$cycles = getCycles();
$warehouses = array();
$bins = array();
$tagsThatNeedLots = array();


function logout(){
  if(isset($_SESSION["countCycleView"]["name"])){
  	unset($_SESSION["countCycleView"]["name"]);
  }

  if(isset($_SESSION["countCycleView"]["cycle"])){
  	unset($_SESSION["countCycleView"]["cycle"]);
  }
  if(isset($_SESSION["countCycleView"]["warehouse"])){
  	unset($_SESSION["countCycleView"]["warehouse"]);
  }
  if(isset($_SESSION["countCycleView"]["bin"])){
  	unset($_SESSION["countCycleView"]["bin"]);
  }

  if(isset($_POST)){
  	unset($_POST);
  }
}

function htmlEcho($string){
	echo htmlspecialchars($string);
}

function getCounts($tagNum,$cycle,$warehouse){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare('SELECT * FROM count WHERE TagNum=? AND Cycle=? AND WarehouseCode=?');
  $ret = $qry->execute(array($tagNum,$cycle,$warehouse));
  return $qry->fetchAll();
}

//Fix comments with no warehouse
function commentFix($tags){
  foreach ($tags["result"]->returnObj->CCTag as $tag) {
    $cycleString = makeCycleString($tag->Plant,$tag->CCYear,$tag->CCMonth,$tag->CycleSeq);
    $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
    $qry = $db->prepare('SELECT * FROM comment WHERE TagNum=? AND Cycle=?');
    $ret = $qry->execute(array($tag->TagNum,$cycleString));
    $comments = $qry->fetchAll();
    foreach ($comments as $comment) {
      if(strlen(trim($comment["WarehouseCode"])) == 0){
        $qry2 = $db->prepare('UPDATE comment SET WarehouseCode=? WHERE id=?');
        $ret = $qry2->execute(array($tag->WarehouseCode,$comment["id"]));
      }
    }
  }
}

function getComments($tagNum,$cycle,$warehouse){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare('SELECT * FROM comment WHERE TagNum=? AND Cycle=? AND WarehouseCode=?');
  $ret = $qry->execute(array($tagNum,$cycle,$warehouse));
  return $qry->fetchAll();
}

function getCycles(){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare('SELECT DISTINCT Cycle FROM tags');
  $ret = $qry->execute();
  return $qry->fetchAll();
}

function cycleIsArchived($cycleString){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);

  $qry = $db->prepare('SELECT * FROM archived WHERE cycle=?');
  $ret = $qry->execute(array($cycleString));
  $exists = $qry->fetch();

  if($exists == false){
    return false;
  } else {
    return true;
  }
}

function getWarehouses($cycleString){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare('SELECT DISTINCT WarehouseCode FROM tags WHERE Cycle=?');
  $ret = $qry->execute(array($cycleString));
  return $qry->fetchAll();
}

function getBins($WarehouseCode,$cycleString){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare('SELECT DISTINCT UPPER(BinNum) FROM tags WHERE WarehouseCode=? AND Cycle=?');
  $ret = $qry->execute(array($WarehouseCode,$cycleString));
  return $qry->fetchAll();
}

function getTags($WarehouseCode,$cycleString,$limit,$page){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare('SELECT * FROM tags WHERE WarehouseCode=? AND Cycle=? LIMIT ? OFFSET ? COLLATE NOCASE');
  $offset = $limit * $page;
  $ret = $qry->execute(array($WarehouseCode,$cycleString,$limit,$offset));
  return $qry->fetchAll();
}

function getParts($WarehouseCode,$cycleString,$bin){
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $qry = $db->prepare('SELECT * FROM tags WHERE WarehouseCode=? AND Cycle=? AND BinNum=? COLLATE NOCASE');
  $ret = $qry->execute(array($WarehouseCode,$cycleString,$bin));
  return $qry->fetchAll();
}

function syncTagsFromEpicor($warehouse,$year,$month,$seq,$bin){
  global $api;
  global $tagsThatNeedLots;
  $tags = $api->getBinCountCycleTags($warehouse,$year,$month,$seq,$bin);

  if($tags["http"] != 200){
    return;
  }

  //Find tags the need lot numbers
  foreach ($tags["result"]->returnObj->CCTag as $tag) {
    if($tag->PartNumTrackLots == 1 && strlen(trim($tag->LotNum)) == 0){
      array_push($tagsThatNeedLots, $tag->TagNum);
    }
  }

  commentFix($tags);

  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  foreach ($tags["result"]->returnObj->CCTag as $tag) {
    $cycleString = makeCycleString($tag->Plant,$tag->CCYear,$tag->CCMonth,$tag->CycleSeq);

    if(!empty($tag->CountedBy) && !countExists($db,$tag->TagNum,$tag->WarehouseCode,$cycleString,$tag->CountedBy)){
      insertCount($db,$tag->TagNum,$tag->WarehouseCode,$tag->PartNum,$cycleString,$tag->CountedBy,$tag->CountedQty);
    }

    if(empty($tag->CountedBy)){
      deleteCounts($db,$tag->TagNum,$tag->WarehouseCode,$cycleString);
    }
  }
}

function syncMultiTagFromEpicor($tagNums,$year,$month,$seq){
  global $api;
  global $tagsThatNeedLots;
  $tags = $api->getCountCycleTagMulti($tagNums,$year,$month,$seq);

  if($tags["http"] != 200){
    return;
  }

  //Find tags the need lot numbers
  foreach ($tags["result"]->returnObj->CCTag as $tag) {
    if($tag->PartNumTrackLots == 1 && strlen(trim($tag->LotNum)) == 0){
      array_push($tagsThatNeedLots, $tag->TagNum);
    }
  }

  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  foreach ($tags["result"]->returnObj->CCTag as $tag) {
    $cycleString = makeCycleString($tag->Plant,$tag->CCYear,$tag->CCMonth,$tag->CycleSeq);

    if(!empty($tag->CountedBy) && !countExists($db,$tag->TagNum,$tag->WarehouseCode,$cycleString,$tag->CountedBy)){
      insertCount($db,$tag->TagNum,$tag->WarehouseCode,$tag->PartNum,$cycleString,$tag->CountedBy,$tag->CountedQty);
    }

    if(empty($tag->CountedBy)){
      deleteCounts($db,$tag->TagNum,$tag->WarehouseCode,$cycleString);
    }
  }
}

//Go Back
if(isset($_GET["logout"])){
  logout();
}

//Steps
if($step == 1 && isset($_SESSION["clockInStatusResult"]->Name)){
  $_POST["name"] = $_SESSION["clockInStatusResult"]->Name;
}

if(isset($_POST["name"])){
  $_SESSION["countCycleView"]["name"] = $_POST["name"];
  $step = 2;
}

if(isset($_GET["cycle"]) && !isset($_GET["warehouse"])){
  $cycle = explode("-",$_GET["cycle"]);

  $cycleInfo = $api->getCountCycle($cycle[1],$cycle[2],$cycle[3]);
  if($cycleInfo["http"] != 200){
  	header("Location: countCycleView.php");
	exit();
  }

  $_GET["plant"] = $cycleInfo["result"]->returnObj->CCHdrList[0]->Plant;

  $warehouses = getWarehouses($_GET["cycle"]);
  $step = 3;
}

if(isset($_GET["warehouse"]) && isset($_GET["plant"]) && isset($_GET["cycle"]) && !isset($_GET["bin"])){
  $bins = getBins($_GET["warehouse"],$_GET["cycle"]);
  $step = 4;
}

if(isset($_GET["bin"]) && is_numeric($_GET["bin"]) && $_GET["bin"] == 0 && isset($_GET["page"])){
  //All Bins
  $showAllBins = true;
  $binParts = getTags($_GET["warehouse"],$_GET["cycle"],50,$_GET["page"]);
  $cycle = explode("-",$_GET["cycle"]);
  $tagNumbers = array();

  foreach ($binParts as $tag) {
  	array_push($tagNumbers, $tag["TagNum"]);
  }

  syncMultiTagFromEpicor($tagNumbers,$cycle[1],$cycle[2],$cycle[3]);
  $step = 5;
} else if(isset($_GET["bin"])){
  //Specific Bin
  $cycle = explode("-",$_GET["cycle"]);
  syncTagsFromEpicor($_GET["warehouse"],$cycle[1],$cycle[2],$cycle[3],$_GET["bin"]);

  $binParts = getParts($_GET["warehouse"],$_GET["cycle"],$_GET["bin"]);
  $step = 5;
}

if(isset($_GET["search"]) && isset($_GET["cycle"])){
  $_GET["search"] = trim($_GET["search"]);
  $db = new PDO('sqlite:db/'.$GLOBALS["invDatabase"]);
  $binParts = searchTags($db,$_GET["cycle"],$_GET["search"]);

  $tagNums = array();
  $cycle = explode("-",$_GET["cycle"]);
  foreach ($binParts as $part) {
  	array_push($tagNums, $part["TagNum"]);
  }
  syncMultiTagFromEpicor($tagNums,$cycle[1],$cycle[2],$cycle[3]);

  $binParts = searchTags($db,$_GET["cycle"],$_GET["search"]);

  $step = 5;
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php htmlEcho($errorText); ?></b>
		</div></center><br>
	<?php } ?>

  <!-- Name -->
    <?php if($step > 1) { ?>
      <center>
        <div class="alert alert-primary" role="alert" style="margin:20px;max-width:500px;">
          <img src="img/scale.png" onclick="showScaleModal()" style="width:50px;height:50px;">

          <?php if($step >= 4 && isset($_GET["warehouse"]) && isset($_GET["cycle"]) && isset($_GET["plant"])) { ?>
            <img src="img/btag.png" 
            warehouse="<?php htmlEcho($_GET["warehouse"]); ?>" 
            cycle="<?php htmlEcho($_GET["cycle"]); ?>" 
            plant="<?php htmlEcho($_GET["plant"]); ?>" 
            onclick="showBtagModal(this)" 
            style="width:50px;height:50px;">
          <?php } ?>
        </div>
    </center>
    <?php } ?>

  <!-- Search -->
  <?php if($step >= 3) { ?>
      <center>
        <div class="alert alert-primary" role="alert" style="margin:20px;max-width:500px;">
            <form action="countCycleView.php" method="GET">
                <div class="mb-3">
                  <input type="hidden" name="cycle" value="<?php htmlEcho($_GET['cycle']); ?>">
                  <input type="hidden" name="plant" value="<?php htmlEcho($_GET['plant']); ?>">
                  <input class="form-control" placeholder="Search" id="search" autocomplete="off" name="search" 
                  <?php if(isset($_GET["search"])){ ?>
                  	value="<?php htmlEcho($_GET["search"]); ?>"
              	  <?php } else { ?>
              	  	value=""
              	  <?php }  ?>
                  >
                </div>
                <button type="submit" class="btn btn-primary"><b>Search</b></button>
              </form>

               <?php if(isset($_GET["search"])){ ?>
			   		<center>
			   			<br>
			   			<form action="countCycleView.php" method="GET">
		                	<input type="hidden" name="cycle" value="<?php htmlEcho($_GET['cycle']); ?>">
		                	<button type="submit" class="btn btn-dark"><b>Exit Search</b></button>
		              	</form>
			   		</center>
			   <?php } ?>

        </div>
    </center>
   <?php } ?>
  
  <!-- Step One -->
      <?php if($step == 1) { ?>
        <center>
        <div class="card" style="margin:20px;max-width:500px;">
          <div class="card-header" style="font-size: 1.5rem;">
            <b>Inventory</b>
          </div>
          <div class="card-body">
            <center>

              <br>
              <form action="countCycleView.php" method="POST">
                <div class="mb-3">
                  <?php if(isset($_SESSION["clockInStatusResult"]->Name)){ ?>
                    <input class="form-control" placeholder="Your Name" id="name" name="name" value="<?php htmlEcho($_SESSION["clockInStatusResult"]->Name);?>">
                  <?php } else { ?>
                    <input class="form-control" placeholder="Your Name" id="name" name="name" value="">
                  <?php }?>
                </div>
                <br>

                <button type="submit" class="btn btn-primary"><b>Next</b></button>
              </form>


            </center>
          </div>
        </div>
        </center>
      <?php } ?>

      <!-- Step Two -->
      <?php if($step == 2) { 

        $parsedCycles = [];

        foreach ($cycles as $cycle) { 
            if(cycleIsArchived($cycle[0])){
              continue;
            }

            $explodedCycle = explode("-",$cycle[0]);
            if(!isset($parsedCycles[$explodedCycle[0]])){
              $parsedCycles[$explodedCycle[0]] = [];
            }

            array_push($parsedCycles[$explodedCycle[0]], $cycle[0]);
        }

      ?>
        <center>

        	<!--
          <div class="alert alert-warning" role="alert" style="margin:20px;max-width:500px;">
          <b>Don't see a count cycle? Import a warehouse within that count cycle.</b>
          <br><br>
            <a class="btn btn-primary" href="countCycleImport.php" style="min-width: 150px;"><b>Import warehouse</b></a>
          </div> -->

          <?php foreach ($parsedCycles as $plant => $parsedCycle) { ?>

          <div class="card" style="margin:20px;max-width:500px;">
            <div class="card-header" style="font-size: 1.5rem;">
              <b>Plant <?php echo $plant; ?> Count Cycles</b>
            </div>
            <div class="card-body">
              <center>

                <?php foreach ($parsedCycle as $cycle) { 
                  if(cycleIsArchived($cycle)){
                    continue;
                  }
                ?>
                  <center>
                  <div class="card" style="margin:20px;max-width:500px;">
                    <div class="card-body">
                      <center>
                      <h5 class="card-title"><?php htmlEcho($cycle); ?></h5>
                      <br>
                      
                      <form style="max-width:350px;" action="countCycleView.php" method="GET">
                        <input type="hidden" name="cycle" value="<?php htmlEcho($cycle); ?>">
                        <button type="submit" class="btn btn-primary"><b>Select</b></button>
                      </form>
                      </center>
                    </div>
                  </div>
                  </center>
                <?php } ?>

              </center>
            </div>
          </div>

          <?php } ?>



        </center>
      <?php } ?>

      <!-- Step Three -->
      <?php if($step == 3) { ?>
        <center>

          <div class="alert alert-dark" role="alert" style="margin:20px;max-width:500px;">
            <b>Count Cycle <?php htmlEcho($_GET["cycle"]); ?></b>
            <br><br>
            <a class="btn btn-primary" href="countCycleView.php" style="min-width: 150px;"><b>Change Count Cycle</b></a>
          </div>

          <!--
          <div class="alert alert-warning" role="alert" style="margin:20px;max-width:500px;">
          <b>Don't see a warehouse?</b>
          <br><br>
            <a class="btn btn-primary" href="countCycleImport.php" style="min-width: 150px;"><b>Import warehouse</b></a>
          </div> -->

          <div class="card" style="margin:20px;max-width:500px;">
            <div class="card-header" style="font-size: 1.5rem;">
              <b>Select warehouse</b>
            </div>
            <div class="card-body">
              <center>

                <?php foreach ($warehouses as $warehouse) { 
                  $warehouseDesc = "";
                  $warehouseResult = $api->getWarehouse($warehouse[0]);
                  if($warehouseResult["http"] == 200){
                    $warehouseDesc = $warehouseResult["result"]->returnObj->Warehse[0]->Description;
                    $warehousePlant = $warehouseResult["result"]->returnObj->Warehse[0]->Plant;
                  }
                ?>
                  <center>
                  <div class="card" style="margin:20px;max-width:500px;">
                    <div class="card-body">
                      <center>
                      <h5 class="card-title"><?php htmlEcho($warehouseDesc); ?></h5>
                      <br>
                      <h5 class="card-title">Warehouse ID: <?php htmlEcho($warehouse[0]); ?></h5>
                      <br>
                      <a target="_blank" href="countCycleVariance.php?Cycle=<?php echo urlencode($_GET["cycle"]); ?>&WarehouseCode=<?php echo urlencode($warehouse[0]); ?>" class="btn btn-secondary"><b>Variance</b></a>
                      <br>
                      <br>
                      <form style="max-width:350px;" action="countCycleView.php" method="GET">
                      	<input type="hidden" name="cycle" value="<?php htmlEcho($_GET['cycle']); ?>">
                        <input type="hidden" name="warehouse" value="<?php htmlEcho($warehouse[0]); ?>">
                        <input type="hidden" name="plant" value="<?php htmlEcho($warehousePlant); ?>">
                        <button type="submit" class="btn btn-primary"><b>Select</b></button>
                      </form>
                      <br>
                      </center>
                    </div>
                  </div>
                  </center>
                <?php } ?>

              </center>
            </div>
          </div>
        </center>
      <?php } ?>

      <!-- Step Four -->
      <?php if($step == 4) { ?>
        <center>

          <div class="alert alert-dark" role="alert" style="margin:20px;max-width:500px;">
            <b>Count Cycle <?php htmlEcho($_GET["cycle"]); ?></b>
          </div>

          <?php if(!isset($_GET["search"])){ ?>
	          <div class="alert alert-dark" role="alert" style="margin:20px;max-width:500px;">
	            <b>Warehouse <?php htmlEcho($_GET["warehouse"]); ?></b>
	            <br><br>
	            <form id="next-page-form" style="max-width:350px;" action="countCycleView.php" method="GET">
					<input type="hidden" name="cycle" value="<?php htmlEcho($_GET['cycle']); ?>">
					<button class="btn btn-primary" style="min-width: 150px;" type="submit"><b>Change Warehouse</b></button>
				</form>
	          </div>
      	  <?php } ?>

      	  <div class="alert alert-dark" role="alert" style="margin:20px;max-width:500px;">
      	  	<button id="progress-btn" class="btn btn-primary" style="" onclick="loadBinCompleteStatus()"><b>Show Progress</b></button>
      	  	<div id="progress-container" style="display:none;">
            	<b id="warehouseTotal">Loading...</b> of parts counted<br><br>
            	<b id="warehouseTotalPercent">Loading...</b> complete
        	</div>
          </div>

          <div class="card" style="margin:20px;max-width:500px;">
            <div class="card-header" style="font-size: 1.5rem;">
              <b>Select Bin</b>
            </div>
            <div class="card-body">
              <center>

              	<div class="card" style="margin:20px;max-width:500px;">
                    <div class="card-body">
                      <center>
                        <h5 class="card-title">All Bins</h5><br>
						<form id="all-bins-form" style="max-width:350px;" action="countCycleView.php?page=0" method="GET">
							<input type="hidden" name="cycle" value="<?php htmlEcho($_GET['cycle']); ?>">
	                        <input type="hidden" name="warehouse" value="<?php htmlEcho($_GET['warehouse']); ?>">
	                        <input type="hidden" name="plant" value="<?php htmlEcho($_GET['plant']); ?>">
							<input type="hidden" name="bin" value="0">
							<input type="hidden" name="page" value="0">
							<button id="all-bins-btn" class="btn btn-secondary" onclick="allBinsForm()"><b>Select</b></button>
						</form>
                      </center>
                    </div>
                  </div>

                <?php foreach ($bins as $bin) { 
                  if($bin[0] == ""){
                    //Blank Tags
                    continue;
                  }
                ?>
                  <center>
                  <div class="card" style="margin:20px;max-width:500px;">
                    <div class="card-body">
                      <center>

                      <?php if($bin[0] != "") { ?>
                        <h5 class="card-title">Bin #<?php htmlEcho($bin[0]); ?></h5>
                      <?php } else { ?>
                        <h5 class="card-title" style="color:#ca0000;">BLANK TAGS</h5>
                      <?php } ?>
                      <br>
                      
                      <form style="max-width:350px;" action="countCycleView.php" method="GET">
                      	<input type="hidden" name="cycle" value="<?php htmlEcho($_GET['cycle']); ?>">
                        <input type="hidden" name="warehouse" value="<?php htmlEcho($_GET['warehouse']); ?>">
                        <input type="hidden" name="plant" value="<?php htmlEcho($_GET['plant']); ?>">
                        <input type="hidden" name="bin" value="<?php htmlEcho($bin[0]); ?>">
                        <button type="submit" class="btn btn-primary"><b>Select</b></button>
                      </form>

                      <br>
                      <h5 
                      id="binCompleteStatus<?php htmlEcho($bin[0]); ?>"
                      class="binCompleteStatus"
                      warehouse="<?php htmlEcho($_GET["warehouse"]); ?>"
                      cycle="<?php htmlEcho($_GET["cycle"]); ?>"
                      bin="<?php htmlEcho($bin[0]); ?>"
                      style="display:none;"
                      ><b>Loading...</b></h5>

                      </center>
                    </div>
                  </div>
                  </center>
                <?php } ?>

              </center>
            </div>
          </div>
        </center>
      <?php } ?>


      <!-- Step Five -->
      <?php if($step == 5) { ?>
        <center>

        <div class="alert alert-dark" role="alert" style="margin:20px;max-width:500px;">
            <b>Count Cycle <?php htmlEcho($_GET["cycle"]); ?></b>
          </div>

          <?php if(!isset($_GET["search"])){ ?>
	          <div class="alert alert-dark" role="alert" style="margin:20px;max-width:500px;">
	            <b>Warehouse <?php htmlEcho($_GET["warehouse"]); ?></b>
	          </div>

	          <div class="alert alert-dark" role="alert" style="margin:20px;max-width:500px;">
	            <b>
                <?php 
				  if($showAllBins){
				  	htmlEcho("All Bins");
				  } else if(strlen(trim($_GET["bin"])) > 0){
                    htmlEcho("Bin ");
                    htmlEcho($_GET["bin"]); 
                  } else {
                    htmlEcho("Blank Tags");
                  }
                ?>
              </b>
	            <br><br>
	            <form id="next-page-form" style="max-width:350px;" action="countCycleView.php" method="GET">
					<input type="hidden" name="cycle" value="<?php htmlEcho($_GET['cycle']); ?>">
                    <input type="hidden" name="warehouse" value="<?php htmlEcho($_GET['warehouse']); ?>">
                    <input type="hidden" name="plant" value="<?php htmlEcho($_GET['plant']); ?>">
					<button class="btn btn-primary" style="min-width: 150px;" type="submit"><b>Change Bin</b></button>
				</form>
	          </div>

	          <?php if(!$showAllBins) {?>
	            <div class="alert alert-danger" role="alert" style="margin:20px;max-width:500px;">
	              <b>If there is a part in this bin that is NOT on this list, you must fill out a blank tag!</b>
	            </div>
        	<?php } ?>
      	  <?php } ?>

        <div class="table-responsive" style="max-width: 600px;">
          <table class="table" style="font-size: 0.8rem;">
            <thead>
                <tr style="border-bottom: solid #000 3px;">
                  <th scope="col">Part</th>
                  <th scope="col">Tag</th>
                  <th scope="col">Bin</th>
                  <th scope="col">Blank Tag</th>
                  <th scope="col">UOM</th>
                </tr>
              </thead>

              <?php foreach ($binParts as $part) { 

              	if($part["Blank"] == 1 && strlen(trim($part["PartNum"])) == 0){
              		continue;
              	}

                $counts = getCounts($part["TagNum"],$_GET["cycle"],$part["WarehouseCode"]);
                $comments = getComments($part["TagNum"],$_GET["cycle"],$part["WarehouseCode"]);
              ?>
                <tr style="background: #dadada;">
                  <td><b><a href="partFind.php?part=<?php htmlEcho($part["PartNum"]); ?>" target="_blank"><?php htmlEcho($part["PartNum"]); ?></a></b></td>
                  <td><?php htmlEcho($part["TagNum"]); ?></td>
                  <td><?php htmlEcho($part["BinNum"]); ?></td>
                  <td>
                      <?php if($part["Blank"] == 1){
                        htmlEcho("Yes");
                      } else {
                        htmlEcho("No");
                      } ?>  
                  </td>
                  <td><?php htmlEcho($part["IUM"]); ?></td>
                </tr>

                <tr>
                  <td colspan="5"><?php htmlEcho($part["PartDesc"]); ?></td>
                </tr>

                <?php if(!empty($part["SerialNumber"])) { ?> 
                  <tr>
                    <td colspan="5">Serial <b><?php htmlEcho($part["SerialNumber"]); ?></b></td>
                  </tr>
                <?php } ?>

                <?php if(!empty($counts)) { ?> 
                  <tr >
                    <th scope="col" colspan="5"><center>Count</center></th>
                  </tr>
                  <?php foreach ($counts as $count) { ?>
                    <tr>
                      <td colspan="2"><?php htmlEcho($count["Name"]); ?></td>
                      <td><?php htmlEcho($count["CountedQty"]); ?> <?php htmlEcho($part["IUM"]); ?></td>
                    </tr>
                  <?php } ?>
                <?php } ?>

                <?php if(!empty($comments)) { ?> 
                  <tr >
                    <th scope="col" colspan="5"><center>Comments</center></th>
                  </tr>
                  <?php foreach ($comments as $comment) { ?>
                    <tr>
                      <td colspan="3"><?php htmlEcho($comment["Name"]); ?>:</td>
                    </tr>
                    <tr>
                       <td colspan="3" style="word-break:break-word;"><?php htmlEcho($comment["Comment"]); ?></td>
                    </tr>
                  <?php } ?>
                <?php } ?>

                <tr style="border-bottom: solid #000 3px;">
                  <td colspan="5"><center>

                    <table>
                      <tr>

                        <td>
                          <button
                          id="btn-count-<?php htmlEcho(str_replace(".","",$part["TagNum"])); ?>"
                          type="button"
                          <?php if(empty($counts)) { ?> 
                            class="btn btn-danger" 
                          <?php } else { ?>
                            class="btn btn-success" 
                          <?php } ?>
                          style="min-width: 120px;margin-right: 10px;" 
                          data-bs-toggle="modal" 
                          data-bs-target="#countModal"
                          onclick="prepCountModal(this)"
                          tagNum="<?php htmlEcho($part["TagNum"]); ?>"
                          partNum="<?php htmlEcho($part["PartNum"]); ?>"
                          bin="<?php htmlEcho($part["BinNum"]); ?>"
                          warehouse="<?php htmlEcho($part["WarehouseCode"]); ?>"
                          UOM="<?php htmlEcho($part["IUM"]); ?>"
                          cycle="<?php htmlEcho($part["Cycle"]); ?>"
                          plant="<?php htmlEcho($_GET["plant"]); ?>"
                          name="<?php htmlEcho($_SESSION["countCycleView"]["name"]); ?>"
                          needsLot="<?php 
                            if(in_array($part["TagNum"], $tagsThatNeedLots)){
                              htmlEcho("1");
                            } else {
                              htmlEcho("0");
                            }
                          ?>">
                            <?php if(empty($counts)) { ?> 
                              <b>Add Count</b>
                            <?php } else { ?>
                              <b>Counted</b>
                            <?php } ?>
                          </button>
                        </td>

                        <td>
                          <center>
                            <img 
                            src="img/photo.png" 
                            data-bs-toggle="modal" 
                            data-bs-target="#imageModal"
                            partNum="<?php htmlEcho($part["PartNum"]); ?>"
                            onclick="prepImageModal(this)" 
                            style="width: 50px;height:50px;">
                          </center>
                        </td>

                        <td>
                          <center>
                            <a href="countCycleHist.php?part=<?php echo urlencode($part["PartNum"]); ?>&bin=<?php echo urlencode($part["BinNum"]); ?>&warehouse=<?php echo urlencode($part["WarehouseCode"]); ?>" target="_blank">
                            <img 
                            src="img/history.png" 
                            style="width: 50px;height:50px;">
                            </a>
                          </center>
                        </td>

                        <td>
                          <button
                            id="btn-cmmt-<?php htmlEcho(str_replace(".","",$part["TagNum"])); ?>"
                            type="button" 
                            class="btn btn-primary" 
                            style="min-width: 120px;margin-left: 10px;" 
                            data-bs-toggle="modal" 
                            data-bs-target="#commentModal"
                            onclick="prepCommentModal(this)"
                            tagNum="<?php htmlEcho($part["TagNum"]); ?>"
                            partNum="<?php htmlEcho($part["PartNum"]); ?>"
                            warehouse="<?php htmlEcho($part["WarehouseCode"]); ?>"
                            cycle="<?php htmlEcho($part["Cycle"]); ?>"
                            name="<?php htmlEcho($_SESSION["countCycleView"]["name"]); ?>">
                              <b>Comment</b>
                          </button>
                        </td>
                        
                      </tr>
                    </table>

                    </center>
                  </td>
                </tr>
              <?php } ?>

              </table></div></center>

              <?php if($showAllBins) {?>
              	<center>

              		<table style="margin-bottom: 10px;">
              			<tr>
              				<td>
              					<?php if($_GET['page']-1 >= 0) { ?>
	              					<form id="next-page-form" style="max-width:350px;" action="countCycleView.php" method="GET">
	              						<input type="hidden" name="cycle" value="<?php htmlEcho($_GET['cycle']); ?>">
				                        <input type="hidden" name="warehouse" value="<?php htmlEcho($_GET['warehouse']); ?>">
				                        <input type="hidden" name="plant" value="<?php htmlEcho($_GET['plant']); ?>">
										<input type="hidden" name="bin" value="0">
										<input type="hidden" name="page" value="<?php htmlEcho($_GET['page']-1);?>">
										<button id="all-bins-btn" class="btn btn-secondary" style="min-width: 150px;margin:10px;" type="submit"><b>Previous Page</b></button>
									</form>
								<?php } ?>
              				</td>
              				<td>
              					<b>Page #<?php htmlEcho($_GET['page']+1);?></b>
              				</td>
              				<td>
              					<form id="next-page-form" style="max-width:350px;" action="countCycleView.php" method="GET">
              						<input type="hidden" name="cycle" value="<?php htmlEcho($_GET['cycle']); ?>">
			                        <input type="hidden" name="warehouse" value="<?php htmlEcho($_GET['warehouse']); ?>">
			                        <input type="hidden" name="plant" value="<?php htmlEcho($_GET['plant']); ?>">
									<input type="hidden" name="bin" value="0">
									<input type="hidden" name="page" value="<?php htmlEcho($_GET['page']+1);?>">
									<button id="all-bins-btn" class="btn btn-secondary" style="min-width: 150px;margin:10px;" type="submit"><b>Next Page</b></button>
								</form>
              				</td>
              			</tr>
              		</table>
              	
				</center>

              <?php } ?>
      <?php } ?>

    <!-- Count Modal -->
    <div class="modal" id="countModal" tabindex="-1" aria-labelledby="countModalLabel" aria-hidden="true" >
      <div class="modal-dialog" style="margin-top: 100px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="countModalLabel">Count</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="alert alert-danger" role="alert">
                <center>If you found this part in any other bin then bin <div id="countModalBin" style="display: inline;"></div> you must fill out a blank tag! <br><br>Do <b>NOT</b> add the quantity to this one!</b></center>
              </div><br>
              <div class="mb-3">
                <input class="form-control" placeholder="Lot" id="countLotInput" name="lot" autocomplete="off" value="" style="margin-bottom: 15px;">
                <input class="form-control" placeholder="Qty" type="number" id="countQtyInput" name="qty" autocomplete="off" value="">
              </div>
          </div>
          <div class="modal-footer">
            <button 
              id="countModalButton"
              type="button" 
              class="btn btn-primary" 
              data-bs-dismiss="modal" 
              onclick="insertCount(this)"
              tagNum=""
              partNum=""
              UOM=""
              warehouse=""
              cycle=""
              plant=""
              name=""><b>Save</b></button>
          </div>
        </div>
      </div>
    </div>

    <!-- Comment Modal -->
    <div class="modal" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="margin-top: 50px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="commentModalLabel">Comment</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <div class="mb-3">
                <input class="form-control" placeholder="Comment" id="commentInput" autocomplete="off" name="qty" value="">
              </div>
          </div>
          <div class="modal-footer">
            <button 
              id="commentModalButton"
              type="button" 
              class="btn btn-primary" 
              data-bs-dismiss="modal" 
              onclick="insertComment(this)"
              tagNum=""
              partNum=""
              warehouse=""
              cycle=""
              name=""><b>Save</b></button>
          </div>
        </div>
      </div>
    </div>

    <!-- Image Modal -->
    <div class="modal" id="imageModal" tabindex="-1" aria-labelledby="imageModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="margin-top: 50px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="imageModalLabel">Image</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              
            <center>
              <img id="partImage" style="width:90%;" src="">
              <iframe id="partImgFrame" partNum="" src="" style="width:95%;height:360px;display:none;"></iframe>
            </center>

          </div>
          <div class="modal-footer">
            <a id="addImageLink" onclick="updatePartImage()" class="btn btn-primary"><b>Update Image</b></a>
            <button data-bs-dismiss="modal" class="btn btn-primary" ><b>Close</b></button>
          </div>
        </div>
      </div>
    </div>

    <!-- Blank Tag Modal -->
    <div class="modal" id="btagModal" tabindex="-1" aria-labelledby="btagModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="margin-top: 50px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="btagModalLabel">Blank Tag Entry</h5>
            <button type="button" class="btn-close" onclick="closeBtagModal()" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              
            <center>
              <img id="btagLoadImage" style="width:90%;" src="img/load.gif">
              <iframe id="btagFrame" partNum="" onload="onBtagFrameLoad(this)" src="" style="width:95%;height:285px;display:none;"></iframe>
            </center>

          </div>
          <div class="modal-footer">
            <button data-bs-dismiss="modal" class="btn btn-primary" onclick="closeBtagModal()"><b>Close</b></button>
          </div>
        </div>
      </div>
    </div>

    <!-- Error Modal -->
    <div class="modal" id="errorModal" tabindex="-1" aria-labelledby="errorModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="margin-top: 50px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="errorModalLabel" style="color: #a00000;font-size: 2rem;">Error</h5>
            <button type="button" class="btn-close" onclick="closeErrorModal()" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              
            <center>
              <p id="modalError">Error</p>
            </center>

          </div>
          <div class="modal-footer">
            <button onclick="closeErrorModal()" class="btn btn-primary" ><b>close</b></button>
          </div>
        </div>
      </div>
    </div>

    <!-- Warning Modal -->
    <div class="modal" id="warningModal" tabindex="-1" aria-labelledby="warningModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="margin-top: 50px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="errorModalLabel" style="color: #a00000;font-size: 2rem;">Warning</h5>
            <button type="button" class="btn-close" onclick="closeWarningModal()" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              
            <center>
              <p id="modalWarning">Warning</p>
            </center>

          </div>
          <div class="modal-footer">
            <button onclick="closeWarningModal()" class="btn btn-primary" ><b>close</b></button>
          </div>
        </div>
      </div>
    </div>

    <!-- Scale Modal -->
    <div class="modal" id="scaleModal" tabindex="-1" aria-labelledby="scaleModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="margin-top: 50px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="scaleModalLabel">Scale</h5>
            <button type="button" class="btn-close" onclick="closeScaleModal()" aria-label="Close"></button>
          </div>
          <div class="modal-body">

          <div class="row">
            <div class="col">

              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">Qty</div>
                </div>
                <input type="text" class="form-control" id="scaleQty" autocomplete="off" placeholder="10">
                <div class="input-group-prepend input-group-append">
                  <div class="input-group-text">is</div>
                </div>
                <input id="scaleWeight1" type="text" class="form-control" autocomplete="off" placeholder="100">
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon2">g/kg</span>
                </div>
              </div>

            </div>
          </div>

          <div class="row">

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <div class="input-group-text">Full weight</div>
                </div>
                <input id="scaleWeight2" type="text" class="form-control" autocomplete="off" placeholder="1000">
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon2">g/kg</span>
                </div>
              </div>

          </div>

          <br>
          <center>
            <button onclick="calScale()" class="btn btn-primary" ><b>Calculate</b></button>
          </center>
          <br>

          <input id="scaleResult" type="text" class="form-control" style="text-align:center;font-weight:bold;" placeholder="N/A" disabled>

          </div>
          <div class="modal-footer">
            <button onclick="closeScaleModal()" class="btn btn-primary" ><b>Close</b></button>
          </div>
        </div>
      </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script language="JavaScript" type="text/javascript">
      $( document ).ready(function() {
          //loadBinCompleteStatus();
      });

      var entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;',
        '`': '&#x60;',
        '=': '&#x3D;'
      }

      function escapeHtml(string) {
        return String(string).replace(/[&<>"'`=\/]/g, function (s) {
          return entityMap[s];
        });
      }

      function isJson(str) {
          try {
              JSON.parse(str);
          } catch (e) {
              return false;
          }
          return true;
      }

      function calScale(){
        $('#scaleQty').val()
        $('#scaleWeight1').val()
        $('#scaleWeight2').val()

        var finalWeight2 = 0;
        var weight2 = $('#scaleWeight2').val().split("+");
        var arrayLength = weight2.length;
        for (var i = 0; i < arrayLength; i++) {
            finalWeight2 = parseFloat(finalWeight2)+parseFloat(weight2[i]);
        }
        console.log(finalWeight2);


        var weightPer = $('#scaleWeight1').val()/$('#scaleQty').val();
        var amount = finalWeight2/weightPer;

        $('#scaleResult').val(amount);        
      }

      var totalNumber = 0;
      var totalCounted = 0;
      function setWarehouseCompleteStatus(total,counted){
      	totalNumber = totalNumber+total;
      	totalCounted = totalCounted+counted;
      	$("#warehouseTotal").html(totalCounted+"/"+totalNumber);
      	$("#warehouseTotalPercent").html((totalCounted/totalNumber*100).toFixed(2)+"%");
      }

      function allBinsForm(){
      	$("#all-bins-btn").html("<b>Loading...</b>");
      	$("#all-bins-form").submit();
      }

      function loadBinCompleteStatus(){
      	$('#progress-btn').hide();
      	$('#progress-container').show();
      	$('.binCompleteStatus').show();

        $('.binCompleteStatus').each(function(i, obj) {
            $.post(
            "inc/inventoryBinStatus.php",
            {warehouse: $(this).attr("warehouse"), 
            cycle: $(this).attr("cycle"),
            txtID: $(this).attr("id"),
            bin: $(this).attr("bin")},
              function(data) {
                if(isJson(data)){
                  var result = $.parseJSON(data);
                  setWarehouseCompleteStatus(result.total,result.counted);

                  if(result.result){
                    $("#"+result.txtID).html(result.message);
                  } else {
                    $("#"+result.txtID).html("");
                  }
                } else {
                  $("#"+result.txtID).html("");
                }
              }
          );
        });
      }

      function closeErrorModal(){
        $("#errorModal").modal().hide();
      }

      function closeWarningModal(){
        $("#warningModal").modal().hide();
      }

      function showScaleModal(){
        $('#scaleWeight1').val("")
        $('#scaleWeight2').val("")
        $('#scaleResult').val(""); 
        $("#scaleModal").modal().show();
      }

      function showBtagModal(button){
        $("#btagModal").modal().show();
        $("#btagFrame").attr("src","/inc/countCycleBlankTag.php?cycle="+$(button).attr("cycle")+"&warehouse="+$(button).attr("warehouse")+"&plant="+$(button).attr("plant"));
      }

      function closeBtagModal(){
        $("#btagFrame").attr("src","");
        $("#btagModal").modal().hide();
      }

      function closeScaleModal(){
        $("#scaleModal").modal().hide();
      }

      function closeImageModal(){
        $("#imageModal").modal("toggle");
      }

      function updatePartImage(){
        $("#partImage").hide();
        $("#partImgFrame").attr("src","/partImage.php?part="+encodeURI($("#partImgFrame").attr("partNum"))+"&frame=1");
        $("#partImgFrame").show();
      }

      function prepImageModal(button){
        $("#partImgFrame").attr("src","");
        $("#partImgFrame").hide();
        $("#partImage").show();

        $("#imageModalLabel").text($(button).attr("partNum"));
        $("#partImage").attr("src","");
        $("#partImage").attr("src","img/load.gif");
        $("#partImgFrame").attr("partNum",$(button).attr("partNum"));
        $.post(
          "inc/partImg.php",
          {partNum: $(button).attr("partNum")},
            function(data) {
              var result = $.parseJSON(data);
              if(result.result.returnObj.trim() == ""){
                $("#partImage").attr("src","img/notfound.png");
              } else {
                $("#partImage").attr("src","data:image/png;base64,"+result.result.returnObj);
              }
            }
        );
      }

      function prepCountModal(button){
        $("#countModalButton").attr("tagNum",$(button).attr("tagNum"));
        $("#countModalButton").attr("partNum",$(button).attr("partNum"));
        $("#countModalButton").attr("cycle",$(button).attr("cycle"));
        $("#countModalButton").attr("name",$(button).attr("name"));
        $("#countModalButton").attr("UOM",$(button).attr("UOM"));
        $("#countModalButton").attr("plant",$(button).attr("plant"));
        $("#countModalButton").attr("warehouse",$(button).attr("warehouse"));
        $("#countModalBin").html($(button).attr("bin"));
        $("#countModalLabel").text($(button).attr("partNum"));
        $("#countQtyInput").val("");
        $("#countLotInput").val("");

        if($(button).attr("needsLot") == 1){
          $("#countLotInput").show();
        } else {
          $("#countLotInput").hide();
        }
      }

      function onBtagFrameLoad(iframe){
        if($(iframe).attr("src") == ""){
          $("#btagLoadImage").show();
          $("#btagFrame").hide();
        } else {
          $("#btagLoadImage").hide();
          $("#btagFrame").show();
        }
      }

      function insertCount(button){
        var buttonID = "#btn-count-"+$(button).attr("tagNum").replace(".","");
        $(buttonID).removeClass("btn-danger");
        $(buttonID).removeClass("btn-success");
        $(buttonID).addClass("btn-secondary");
        $(buttonID).html("<b>Updating...</b>");

        if($("#countQtyInput").val().trim() == ""){
          $("#modalError").html("Cannot set Qty to nothing, try 0.");
          $("#errorModal").modal().show();
          $(buttonID).removeClass("btn-secondary");
          $(buttonID).addClass("btn-danger");
          $(buttonID).html("<b>Add Count</b>");
          return;
        }

        $.post(
          "inc/insertCount.php",
          {btnID: buttonID,
           tagNum: $(button).attr("tagNum"),
           partNum: $(button).attr("partNum"), 
           warehouse: $(button).attr("warehouse"), 
           cycle: $(button).attr("cycle"), 
           name: $(button).attr("name"),
           plant: $(button).attr("plant"), 
           countedQTY: $("#countQtyInput").val(),
           lot: $("#countLotInput").val()},
            function(data) {

              console.log(data);
              if(!isJson(data)){
                $("#modalError").html(escapeHtml(data));
                $("#errorModal").modal().show();
                $(result.btnID).removeClass("btn-secondary");
                $(result.btnID).removeClass("btn-success");
                $(result.btnID).addClass("btn-danger");
                $(result.btnID).html("<b>Add Count</b>");
                return;
              }

              var result = $.parseJSON(data);
              if(result.result){
                $(result.btnID).removeClass("btn-secondary");
                $(result.btnID).removeClass("btn-danger");
                $(result.btnID).addClass("btn-success");
                $(result.btnID).html("<b>Counted "+$("#countQtyInput").val()+" "+$(result.btnID).attr("UOM")+"</b>");
              } else {
                $("#modalError").html(result.message);
                $("#errorModal").modal().show();
                $(result.btnID).removeClass("btn-secondary");
                $(result.btnID).removeClass("btn-success");
                $(result.btnID).addClass("btn-danger");
                $(result.btnID).html("<b>Add Count</b>")
              }

              if(result.variance){
                $("#modalWarning").html(result.varianceMsg);
                $("#warningModal").modal().show();
              }
            }
        );
      }

      function prepCommentModal(button){
        $("#commentModalButton").attr("tagNum",$(button).attr("tagNum"));
        $("#commentModalButton").attr("partNum",$(button).attr("partNum"));
        $("#commentModalButton").attr("cycle",$(button).attr("cycle"));
        $("#commentModalButton").attr("name",$(button).attr("name"));
        $("#commentModalButton").attr("warehouse",$(button).attr("warehouse"));
        $("#commentModalLabel").text($(button).attr("partNum"));
      }

      function insertComment(button){
        var buttonID = "#btn-cmmt-"+$(button).attr("tagNum").replace(".","");
        $(buttonID).removeClass("btn-primary");
        $(buttonID).addClass("btn-success");
        $.post(
          "inc/insertComment.php",
          {tagNum: $(button).attr("tagNum"),
           partNum: $(button).attr("partNum"), 
           warehouse: $(button).attr("warehouse"), 
           cycle: $(button).attr("cycle"), 
           name: $(button).attr("name"), 
           comment: $("#commentInput").val()},
            function(data) {
              
            }
        );
      }

    </script>
  </body>
</html>