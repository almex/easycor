<?php
session_start();

include_once 'inc/global.php';
include_once 'inc/inventoryFunctions.php';
include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	die();
}

$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function removeDuplicateLines($salesOrders){
	$newResults = array();
	foreach ($salesOrders["result"]->returnObj->Results as $key => $salesOrder) {
		$duplicate = false;
		foreach ($newResults as $compareKey => $compareSalesOrder) {
			if($salesOrder->OrderHed_OrderNum == $compareSalesOrder->OrderHed_OrderNum &&
		       $salesOrder->OrderDtl_PartNum == $compareSalesOrder->OrderDtl_PartNum &&
		       $salesOrder->OrderRel_OrderLine == $compareSalesOrder->OrderRel_OrderLine &&
		       $salesOrder->OrderDtl_SellingQuantity == $compareSalesOrder->OrderDtl_SellingQuantity &&
		       $salesOrder->OrderRel_OrderRelNum == $compareSalesOrder->OrderRel_OrderRelNum){
				$duplicate = true;
			}
		}

		if(!$duplicate){
			array_push($newResults, $salesOrder);
		}
	}

	$salesOrders["result"]->returnObj->Results = $newResults;
	return $salesOrders;
}

function getReportPartNums($db,$reportID){
  $qry = $db->prepare("SELECT DISTINCT partNum FROM parts WHERE reportID=?");

  $ret = $qry->execute(array($reportID));
  if($ret == false){
    return array();
  }

  $nums = $qry->fetchAll();
  if($nums == false){
    return array();
  }

  return $nums;
}


function getKeywords(){
  $keywordDB = new PDO('sqlite:db/adpKeyword.db');
  $qry = $keywordDB->prepare("SELECT * FROM keywords");

  $ret = $qry->execute(array());
  if($ret == false){
    return array();
  }

  $keywords = $qry->fetchAll();
  if($keywords == false){
    return array();
  }

  return $keywords;
}

//Get keywords
$keywords = array();
$DBkeywords = getKeywords();
foreach ($DBkeywords as $keyword) {
	switch ($keyword["type"]) {
		case 1:
			array_push($keywords, $keyword["keyword"]);
			break;
		case 2:
			$db = new PDO('sqlite:db/'.$invDatabase);

			if($keyword["keyword"] == "200-500"){
				//Exclude bin 100 from warehouse 200-500 (Mike dumped non ADP stuff in it)
				$inventoryPartNums = getUniquePartNums($db,$keyword["keyword"],true);
			} else {
				$inventoryPartNums = getUniquePartNums($db,$keyword["keyword"]);
			}

			foreach ($inventoryPartNums as $inventoryPart) {
				if(strlen(trim($inventoryPart["PartNum"])) > 0){
					if(!in_array($inventoryPart["PartNum"], $keywords)){
						array_push($keywords, $inventoryPart["PartNum"]);
					}
				}
			}
			break;
		case 3:
			$db = new PDO('sqlite:db/inventoryPlanner.db');
			$reportPartNums = getReportPartNums($db,$keyword["keyword"]);
			foreach ($reportPartNums as $inventoryPart) {
				if(strlen(trim($inventoryPart["partNum"])) > 0){
					if(!in_array($inventoryPart["partNum"], $keywords)){
						array_push($keywords, $inventoryPart["partNum"]);
					}
				}
			}
			break;
	}
}

//Get Orders
$salesOrders = $api->getPlantSalesOrders("200");
$salesOrders = removeDuplicateLines($salesOrders);
$transferOrders = $api->getPlantTransferOrders("200");
$matchedSalesOrders = array();
$matchedTransferOrders = array();
$matchedSalesOrderNums = array();
$matchedTransferOrderNums = array();
$allPlants = $api->getAllPlants();
hasError($allPlants);

foreach ($salesOrders["result"]->returnObj->Results as $soDetail){
	$matched = false;

	foreach ($keywords as $keyword) {
		if (strpos($soDetail->OrderDtl_LineDesc, $keyword) !== false) {
			if(!in_array($soDetail->OrderHed_OrderNum, $matchedSalesOrderNums)){
	    		array_push($matchedSalesOrderNums, $soDetail->OrderHed_OrderNum);
	    	}

	    	if($matched == false){
	    		array_push($matchedSalesOrders, $soDetail);
	    		$matched = true;
	    	}
		} else if (strpos($soDetail->OrderDtl_PartNum, $keyword) !== false) {
			if(!in_array($soDetail->OrderHed_OrderNum, $matchedSalesOrderNums)){
	    		array_push($matchedSalesOrderNums, $soDetail->OrderHed_OrderNum);
	    	}

	    	if($matched == false){
	    		array_push($matchedSalesOrders, $soDetail);
	    		$matched = true;
	    	}

		}
	}
}

foreach ($transferOrders["result"]->returnObj->Results as $toDetail){
	$matched = false;
	foreach ($keywords as $keyword) {
		if (strpos($toDetail->Part_PartDescription, $keyword) !== false) {
			if(!in_array($toDetail->TFOrdHed_TFOrdNum, $matchedTransferOrderNums)){
	    		array_push($matchedTransferOrderNums, $toDetail->TFOrdHed_TFOrdNum);
	    	}

	    	if($matched == false){
	    		array_push($matchedTransferOrders, $toDetail);
	    		$matched = true;
	    	}
		} else if (strpos($toDetail->TFOrdDtl_PartNum, $keyword) !== false) {
			if(!in_array($toDetail->TFOrdHed_TFOrdNum, $matchedTransferOrderNums)){
	    		array_push($matchedTransferOrderNums, $toDetail->TFOrdHed_TFOrdNum);
	    	}

	    	if($matched == false){
	    		array_push($matchedTransferOrders, $toDetail);
	    		$matched = true;
	    	}
		}
	}
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;" class="notranslate" translate="no">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="google" content="notranslate" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
				
			<!-- Step Two -->
			<?php if($step == 1) { ?>
				<center>
				<br>
				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">ADP Transfer & Sales orders</h5>
						<br>
						<a href="adpOrdersEdit.php" class="btn btn-primary"><b>Edit</b></a>
					</div>
				</div>
				<br>
				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($matchedSalesOrderNums as $soNum) { 
					  		$cust = "";
					  		$date = "";
					  		$entryPerson = "";
					  		foreach ($salesOrders["result"]->returnObj->Results as $salesOrder) {
					    		if($salesOrder->OrderHed_OrderNum == $soNum){
					    			$cust = $salesOrder->Customer_Name;
					    			$entryPerson = $salesOrder->OrderHed_EntryPerson;
					    			$date = explode("T", $salesOrder->OrderHed_OrderDate)[0];
					    			break;
					    		}
							 }

					  	?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col" colspan="4">
						      	<center>
						      		Sales Order #<a target="_blank" href="salesFind.php?soNum=<?php echo urlencode($soNum); ?>"><?php echo $soNum; ?></a> for <?php echo $cust; ?> - <?php echo $date; ?>
						      		<?php echo "<br>".$entryPerson;?>

						  		</center>
						  	  </th>
						    </tr>
					  		<tr>
					  		  <th scope="col" >Line</th>
					  		  <th scope="col" colspan="2">Part</th>
					  		  <th scope="col" >Qty</th>
						    </tr>
						    <tr>
						    	<?php 
						    		$salesOrderLines = array();
						    		foreach ($salesOrders["result"]->returnObj->Results as $salesOrder) {

							    		if($salesOrder->OrderHed_OrderNum != $soNum){
							    			continue;
							    		}

							    		array_push($salesOrderLines, $salesOrder);
						    		}

						    		usort($salesOrderLines, function($a, $b) {
									    return $a->OrderRel_OrderLine <=> $b->OrderRel_OrderLine;
									});


						    		foreach ($salesOrderLines as $salesOrder) {
						    	?>
						    		<tr>
						    			<td><?php echo $salesOrder->OrderRel_OrderLine; ?></td>
						    			<td colspan="2"><a target="_blank" href="partFind.php?part=<?php echo urlencode($salesOrder->OrderDtl_PartNum); ?>"><?php echo $salesOrder->OrderDtl_PartNum; ?></a></td>
						    			<td><?php echo $salesOrder->OrderDtl_SellingQuantity; ?></td>
						    		</tr>
					    			<tr>
					    				<td></td>
					    				<td colspan="3"><?php echo $salesOrder->OrderDtl_LineDesc; ?></td>
					    			</tr>
						    	<?php } ?>
					  		</tr>
					  		<tr>
			    				<td colspan="4">&nbsp;</td>
			    			</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>
				<br>
				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php 
					  		foreach ($matchedTransferOrderNums as $toNum) { 
					  			$cust = "";
					  			$date = "";
					  			$entryPerson = "";
					  			foreach ($transferOrders["result"]->returnObj->Results as $transferOrder) {
					  				if($transferOrder->TFOrdHed_TFOrdNum == $toNum){
							    		$cust = $transferOrder->TFOrdHed_ToPlant;
							    		$entryPerson = $transferOrder->TFOrdHed_EntryPerson;
							    		$date = explode("T", $transferOrder->TFOrdDtl_RequestDate)[0];
					    				break;
							    	}
							 	}	

							 	foreach ($allPlants["result"]->returnObj->PlantList as $plant) {
							 		if($plant->Plant == $cust){
							 			$cust = $plant->Name;
							 			break;
							 		}
					  			}
					  	?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col" colspan="4"><center>Transfer Order #<a target="_blank" href="transferFind.php?toNum=<?php echo urlencode($toNum); ?>"><?php echo $toNum; ?></a> for <?php echo $cust; ?> - <?php echo $date; ?><?php echo "<br>".$entryPerson;?></center></th>
						    </tr>
					  		<tr>
					  		  <th scope="col" >Line</th>
					  		  <th scope="col" colspan="2">Part</th>
					  		  <th scope="col" >Qty</th>
						    </tr>
						    <tr>
						    	<?php 
						    		$transferOrderLines = array();
						    		foreach ($transferOrders["result"]->returnObj->Results as $transferOrder) {

							    		if($transferOrder->TFOrdHed_TFOrdNum != $toNum){
							    			continue;
							    		} 

							    		array_push($transferOrderLines, $transferOrder);
							    	}

							    	usort($transferOrderLines, function($a, $b) {
									    return $a->TFOrdDtl_TFOrdLine <=> $b->TFOrdDtl_TFOrdLine;
									});

							    	foreach ($transferOrderLines as $transferOrder) {
						    	?>
						    		<tr>
						    			<td><?php echo $transferOrder->TFOrdDtl_TFOrdLine; ?></td>
						    			<td colspan="2"><a target="_blank" href="partFind.php?part=<?php echo urlencode($transferOrder->TFOrdDtl_PartNum); ?>"><?php echo $transferOrder->TFOrdDtl_PartNum; ?></a></td>
						    			<td><?php echo $transferOrder->TFOrdDtl_OurStockShippedQty."/".$transferOrder->TFOrdDtl_SellingQty; ?></td>
						    		<tr>
					    			<tr>
					    				<td></td>
					    				<td colspan="3"><?php echo $transferOrder->Part_PartDescription; ?></td>
					    			</tr>
						    	<?php } ?>
					  		</tr>
					  		<tr>
			    				<td colspan="4">&nbsp;</td>
			    			</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>
			</center>
			<br>
			<center>
				<a href="index.php" class="btn btn-primary" style="min-width: 125px;"><b><center>Home</center></b></a>
			</center>
			<br>

			<?php } ?>

				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>