<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}


$error = false;
$errorText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

if(isset($_GET["part"])){
	$_POST["partNum"] = urldecode($_GET["part"]);
}

//Step one submitted
if(isset($_POST["base64Img"])){
	$gotImageDS = false;
	$partNum = strtoupper(trim($_POST["part"]));
	$partNumNoSpaces = preg_replace('/\s/', '', $partNum);
	$partNumNoSpaces .= "EasyCor";

	$imageExists = $api->imageExists($partNumNoSpaces);
	if($imageExists["http"] == 200 && $imageExists["result"]->returnObj == 1){
		$imageDS = $api->getImageDS($partNumNoSpaces);
		if($imageDS["http"] == 200){
			$imageDS = $imageDS["result"]->returnObj->Image[0];
			$gotImageDS = true;
		} else {
			$error = true;
			$errorText = "unable to update image";
		}
	} else {
		$imageDS = $api->getNewImageDS();
		if($imageDS["http"] == 200){
			$imageDS = $imageDS["result"]->parameters->ds->Image[0];
			$gotImageDS = true;
		} else {
			$error = true;
			$errorText = "unable to update image";
		}
	}

	if($gotImageDS){
	  $imageDS->ImageID = $partNumNoSpaces;
	  $imageDS->FileSize = $_POST["size"]; //kb
	  $imageDS->Width = $_POST["width"];
	  $imageDS->Height = $_POST["height"];
	  $imageDS->FileType = $_POST["fileType"];
	  $imageDS->ImageContent = $_POST["base64Img"];
	  $imageDS->SysRevID = strval($imageDS->SysRevID);

	  $updateImage = $api->updateImage($imageDS);
	  if($updateImage["http"] != 200 && $updateImage["http"] != 204){
	    $error = true;

	    if(isset($updateImage["result"]->ErrorMessage)){
	    	$errorText = $updateImage["result"]->ErrorMessage;
	    } else {
	    	$errorText = "unable to upload image";
	    }
	  } else {
	  	//Update part
	  	$partInfoResult = $api->getPartInfo($partNum);
	  	if($partInfoResult["http"] == 200){
	  		$partInfoResult["result"]->returnObj->Part[0]->ImageID = $partNumNoSpaces;
			$partInfoResult["result"]->returnObj->Part[0]->RowMod = "U";
			$partUpdateResult = $api->updatePart($partInfoResult["result"]->returnObj->Part[0]);
			if($partUpdateResult["http"] == 200){
				$step = 2;
			} else {
				$error = true;
	  			$errorText = "unable to update part with image id";
			}
	  	} else {
	  		$error = true;
	  		$errorText = "unable to get part info";
	  	}
	  }
	}
}

?>

<!doctype html>
  
  <?php if(!isset($_GET["frame"])){ ?>
  	<html lang="en" style="background: #eaeaea;">
  <?php } else { ?>
  	<html lang="en" style="background: #fff;">
  <?php } ?>

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>

  <?php if(!isset($_GET["frame"])){ ?>
  	<body style="background: #eaeaea;">
  <?php } else { ?>
  	<body style="background: #fff;">
  <?php } ?>



	<?php if(!isset($_GET["frame"])){
		include_once 'inc/header.php';
	}?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>

	<?php if($step == 1) { ?>
		<center>
			<canvas style="display:none;" id="canvas" width=64 height=64></canvas>
			<div class="card" style="margin:20px;max-width:500px;">
				<div class="card-header" style="font-size: 1.5rem;">
					<b>Add part image</b>
				</div>
				<div class="card-body">
					<center>

					<?php if(!isset($_GET["frame"])){ ?>
						<form action="partImage.php" method="POST">
					<?php } else { ?>
						<form action="partImage.php?frame=1" method="POST">
					<?php } ?>
						<br>
					  <div class="mb-3">
					  	<?php if(isset($_POST["partNum"])) { ?>
					  		<div class="alert alert-dark" role="alert" style="max-width:400px;">
							  <b><?php echo $_POST["partNum"]; ?></b>
							</div>
					  		<input type="hidden" id="part" name="part" value="<?php echo $_POST["partNum"]; ?>">
					  	<?php } else { ?>
					  		<input class="form-control" autocomplete="off" placeholder="Part Number" name="part" value="">
					  	<?php } ?>
					  
					  </div>
					  <br>
					  <input type="button" id="loadImg" class="btn btn-primary" style="font-weight: bold;" value="Add Image" onclick="document.getElementById('file').click();" />
					  <input type="file" style="display:none;" accept="image/*" id="file" name="img"/>
					  <input type="hidden" id="base64Img" name="base64Img" value="">
					  <input type="hidden" id="width" name="width" value="">
					  <input type="hidden" id="height" name="height" value="">
					  <input type="hidden" id="size" name="size" value="">
					  <input type="hidden" id="fileType" name="fileType" value="">
					  <br><br>
					  <button id="continueBTN" type="submit" class="btn btn-primary" disabled><b>Continue</b></button>
					</form>
					</center>
				</div>
			</div>
		</center>
	<?php } ?>

	<?php if($step == 2) { ?>
		<center>
					<div class="card" style="margin:20px;max-width:500px;">
						<center>
							<img src="img/check.png" class="card-img-top" style="margin:10px;max-width:100px;max-height:100px;">
							<div class="card-body">
								<p class="card-text"><b><center>The image was uploaded</center></b></p>
								<center>
							</center>
							</div>
						</center>
					</div>
		</center>
	<?php } ?>
  


    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
    	var canvas=document.getElementById("canvas");
		var ctx=canvas.getContext("2d");
		var cw=canvas.width;
		var ch=canvas.height;
		var maxW=1500;
		var maxH=1500;
		var input = document.getElementById('file');

		if(input != null){
			input.addEventListener('change', handleFiles);
		}

    	function handleFiles(e) {
		  var img = new Image;
		  img.onload = function() {
		    var iw=img.width;
		    var ih=img.height;
		    var scale=Math.min((maxW/iw),(maxH/ih));
		    var iwScaled=iw*scale;
		    var ihScaled=ih*scale;
		    canvas.width=iwScaled;
		    canvas.height=ihScaled;
		    ctx.drawImage(img,0,0,iwScaled,ihScaled);
			document.getElementById("base64Img").value = canvas.toDataURL("image/jpeg",0.5).slice(23);
			document.getElementById("width").value = iwScaled;
			document.getElementById("height").value = ihScaled;
			document.getElementById("size").value = Math.round(e.target.files[0].size/1000);
			document.getElementById("fileType").value  = ".jpg";
		    document.getElementById("loadImg").value = "Image Selected";
		    document.getElementById("continueBTN").disabled = false;
		  }
		  img.src = URL.createObjectURL(e.target.files[0]);
		}
    </script>
  </body>
</html>