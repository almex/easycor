<?php
session_start();

include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	header("Location: login.php");
	die();
}


$error = false;
$errorText = "";
$warning = false;
$warningText = "";
$step = 1;

//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];

		if(isset($result["result"]->ErrorMessage) && strlen($result["result"]->ErrorMessage) > 0){
			$GLOBALS["errorText"] = $result["result"]->ErrorMessage;
		}

		//print_r($result);
		return true;
	}
	
	return false;
}

function attachPackingSlip($postVarName,$attachNum){
	global $api;

	$fileName = "PO-".$_POST["po"]."-".time()."-".$attachNum.".jpg";

	//One
	$fileExistsResult = $api->fileExists("RcvHead",$fileName);
	if(hasError($fileExistsResult)){
		return false;
	}

	//Two
	$uploadFileResult = $api->uploadFile("RcvHead",$fileName,str_replace("data:image/jpeg;base64,","",$_POST[$postVarName]));
	if(hasError($uploadFileResult)){
		return false;
	}

	//Three
	$newAttchResult = $api->getNewRcvHeadAttch(
		$_SESSION["receivePO"]["ds"],
		$_SESSION["receivePO"]["vendorNum"],
		$_SESSION["receivePO"]["ps"]);
	if(hasError($newAttchResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $newAttchResult["result"]->parameters->ds;
	$_SESSION["receivePO"]["ds"]->RcvHeadAttch[sizeof($_SESSION["receivePO"]["ds"]->RcvHeadAttch)-1]->FileName = $uploadFileResult["result"]->returnObj;
	$_SESSION["receivePO"]["ds"]->RcvHeadAttch[sizeof($_SESSION["receivePO"]["ds"]->RcvHeadAttch)-1]->DrawDesc = $fileName;
	$_SESSION["receivePO"]["ds"]->RcvHeadAttch[sizeof($_SESSION["receivePO"]["ds"]->RcvHeadAttch)-1]->PurPoint = $_SESSION["receivePO"]["ds"]->RcvHead[0]->PurPoint;

	//Four
	$updateResult = $api->receiptUpdate(
		$_SESSION["receivePO"]["vendorNum"],
		$_SESSION["receivePO"]["ps"],
		0,
		true,
		"",
		$_SESSION["receivePO"]["ds"]
	);
	if(hasError($updateResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $updateResult["result"]->parameters->ds;
}

function receive1($po,$ps){
	global $api;
	$_SESSION["receivePO"]["ds"] = new stdClass();

	//One
	$rcvHeadResult = $api->getNewRcvHead();
	if(hasError($rcvHeadResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $rcvHeadResult["result"]->parameters->ds;

	//Two
	$poInfoResult = $api->getPurchaseOrderInfo($po,$_SESSION["receivePO"]["ds"]);
	if(hasError($poInfoResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $poInfoResult["result"]->parameters->ds;
	$_SESSION["receivePO"]["vendorNum"] = $poInfoResult["result"]->parameters->ds->RcvHead[0]->VendorNum;

	//Three
	$vendorInfoResult = $api->getVenInfo($_SESSION["receivePO"]["vendorNum"]);
	if(hasError($vendorInfoResult)){
		return false;
	}
	$_SESSION["receivePO"]["vendorInfo"] = $vendorInfoResult;

	//Four
	$newRcvHeadResult = $api->GetNewRcvHeadWithPONum($_SESSION["receivePO"]["vendorNum"],$po);
	if(hasError($newRcvHeadResult)){
		return false;
	}

	//Five
	$_SESSION["receivePO"]["ds"]->RcvHead[0]->PackSlip = $ps;
	$updateResult = $api->receiptUpdate(
		$_SESSION["receivePO"]["vendorNum"],
		$ps,
		0,
		false,
		"",
		$_SESSION["receivePO"]["ds"]
	);
	if(hasError($updateResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $updateResult["result"]->parameters->ds;

	return true;
}

function receive2($line,$qty){
	global $api;

	if(!is_numeric($line)){
		$error = true;
		$errorText = "Line number is incorrect";
		return false;
	}

	if(!is_numeric($qty)){
		$error = true;
		$errorText = "Qty is incorrect";
		return false;
	}

	$qty = bcdiv($qty,1,2);

	//One
	if($_SESSION["receivePO"]["needsNewRcvDtl"]){
		$newDtlResult = $api->getNewRcvDtl(
			$_SESSION["receivePO"]["vendorNum"],
			$_SESSION["receivePO"]["ps"],
			$_SESSION["receivePO"]["ds"]);

		if(hasError($newDtlResult)){
			return false;
		}

		$_SESSION["receivePO"]["ds"] = $newDtlResult["result"]->parameters->ds;
		$_SESSION["receivePO"]["needsNewRcvDtl"] = false;
	}

	//Two
	$lineInfoResult = $api->getDtlPOLineInfo(
		$_SESSION["receivePO"]["vendorNum"],
		$_SESSION["receivePO"]["ps"],
		$line,
		0,
		$_SESSION["receivePO"]["ds"]
	);
	if(hasError($lineInfoResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $lineInfoResult["result"]->parameters->ds;

	//Three
	$qtyResult = $api->getDtlQtyInfo(
		$_SESSION["receivePO"]["vendorNum"],
		$_SESSION["receivePO"]["ps"],
		0,
		$qty,
		$_SESSION["receivePO"]["ds"]
	);
	if(hasError($qtyResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $qtyResult["result"]->parameters->ds;

	if(isset($qtyResult["result"]->parameters->warnMsg) && strlen($qtyResult["result"]->parameters->warnMsg) > 0){
		$GLOBALS["warning"] = true;
		$GLOBALS["warningText"] = $qtyResult["result"]->parameters->warnMsg;
	}

	//Four
	$toLocationResult = $api->setReceiptToLocation(
		$_SESSION["receivePO"]["vendorNum"],
		$_SESSION["receivePO"]["ps"],
		0,
		$_SESSION["receivePO"]["ds"]
	);
	if(hasError($toLocationResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $toLocationResult["result"]->parameters->ds;

	//Five
	$checkReceiptResult = $api->checkReceiptOnLeaveHead(
		$_SESSION["receivePO"]["vendorNum"],
		$_SESSION["receivePO"]["ps"],
		$_SESSION["receivePO"]["ds"]->RcvHead[0]->PurPoint
	);
	if(hasError($checkReceiptResult)){
		return false;
	}

	return true;

}

function receive3($warehouse,$bin){
	global $api;

	//One
	$warehouseResult = $api->onChangeDtlWareHouseCode(
		$_SESSION["receivePO"]["vendorNum"],
		$_SESSION["receivePO"]["ps"],
		0,
		$warehouse,
		$_SESSION["receivePO"]["ds"]
	);
	if(hasError($warehouseResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $warehouseResult["result"]->parameters->ds;

	//Two
	$binResult = $api->onChangeDtlBinNum(
		$_SESSION["receivePO"]["vendorNum"],
		$_SESSION["receivePO"]["ps"],
		0,
		$bin,
		$_SESSION["receivePO"]["ds"]
	);
	if(hasError($binResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $binResult["result"]->parameters->ds;

	//Three
	$receiveResult = $api->onReceiptChangeDtlReceived(
		$_SESSION["receivePO"]["vendorNum"],
		$_SESSION["receivePO"]["ps"],
		0,
		$_SESSION["receivePO"]["ds"]
	);
	if(hasError($receiveResult)){
		return false;
	}
	$_SESSION["receivePO"]["ds"] = $receiveResult["result"]->parameters->ds;

	//Four
	$updateResult = $api->receiptUpdate(
		$_SESSION["receivePO"]["vendorNum"],
		$_SESSION["receivePO"]["ps"],
		0,
		true,
		$_SESSION["receivePO"]["ds"]->RcvDtl[0]->PartNum,
		$_SESSION["receivePO"]["ds"]
	);
	if(hasError($updateResult)){
		return false;
	}
	//Could be warnings in result
	$_SESSION["receivePO"]["ds"] = $updateResult["result"]->parameters->ds;

	array_push($_SESSION["receivePO"]["receivedLines"], $_SESSION["receivePO"]["line"]);
	$_SESSION["receivePO"]["needsNewRcvDtl"] = true;

	return true;
}

function updatePoInfo(){
	global $api;

	$poInfoResult = $api->getPOInfo($_SESSION["receivePO"]["po"]);
	if(!hasError($poInfoResult)){
		$_SESSION["receivePO"]["poInfo"] = $poInfoResult;
	} else if(isset($_SESSION["receivePO"]["poInfo"])){
		unset($_SESSION["receivePO"]["poInfo"]);
	}
}

if(isset($_POST["po"]) && isset($_POST["ps"])){
	$_POST["po"] = trim($_POST["po"]);
	$_POST["ps"] = trim($_POST["ps"]);

	$_SESSION["receivePO"]["po"] = $_POST["po"];
	$_SESSION["receivePO"]["ps"] = $_POST["ps"];
	$_SESSION["receivePO"]["needsNewRcvDtl"] = true;

	if(receive1($_POST["po"],$_POST["ps"])){
		attachPackingSlip("bytePS","1");

		if(strlen(trim($_POST["bytePS2"])) > 0){
			attachPackingSlip("bytePS2","2");
		}

		if(strlen(trim($_POST["bytePS3"])) > 0){
			attachPackingSlip("bytePS3","3");
		}

		$step = 2;
	}
}

if(isset($_POST["line"]) && isset($_POST["qty"])){
	$_POST["line"] = trim($_POST["line"]);
	$_POST["qty"] = trim($_POST["qty"]);

	$_SESSION["receivePO"]["receivedLines"] = array();
	$_SESSION["receivePO"]["line"] = $_POST["line"];
	$_SESSION["receivePO"]["qty"] = $_POST["qty"];

	if(receive2($_POST["line"],$_POST["qty"])){
		$step = 3;
	} else {
		$step = 2;
	}
}	

if(isset($_POST["warehouse"]) && isset($_POST["bin"])){
	$_POST["warehouse"] = trim($_POST["warehouse"]);
	$_POST["bin"] = trim($_POST["bin"]);

	$_SESSION["receivePO"]["warehouse"] = $_POST["warehouse"];
	$_SESSION["receivePO"]["bin"] = $_POST["bin"];
	if(receive3($_POST["warehouse"],$_POST["bin"])){
		//Back to receive another line
		$step = 2;
	} else {
		$step = 1;
	}
}

if($step == 1){
	unset($_SESSION["receivePO"]);
}	

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;" lang="en" class="notranslate" translate="no">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="google" content="notranslate" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>EasyCor</title>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>

	<?php if($warning) { ?>
		<br><center>
		<div class="alert alert-warning" role="alert" style="max-width:400px;">
		  <b>WARNING: <?php echo $warningText; ?></b>
		</div></center><br>
	<?php } ?>
  
  	<canvas style="display:none;" id="canvas" width=64 height=64></canvas>
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">
		
			<!-- New Step One -->
			<?php if($step == 1) { ?>
				<center>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Receive Purchase Order
						</b>
					</div>
					<div class="card-body">
						<center>
						<form action="receivePO.php" method="POST">
						<br>
						  <div class="mb-3">
						  	<label for="poNum" class="form-label">PO Number</label>
							<input id="poNum" class="form-control" autocomplete="off" placeholder="54221" name="po">
						  </div>
						  <br>
						  <div class="mb-3">
						  	<label for="psNum" class="form-label">Packing Slip</label>
							<input id="psNum" class="form-control" autocomplete="off" placeholder="PS0128" name="ps">
						  </div>
						  <br>
						  <input type="button" id="loadPS" class="btn btn-primary" style="font-weight: bold;" value="Add First packing slip" onclick="document.getElementById('file').click();" />
						  <input type="file" style="display:none;" accept="image/*" id="file" name="packingSlip"/>
						  <input type="hidden" id="bytePS" name="bytePS" value="">
						  <br><br>
						  <input type="button" id="loadPS2" class="btn btn-primary" style="font-weight: bold;" value="Add Second packing slip" onclick="document.getElementById('file2').click();" />
						  <input type="file" style="display:none;" accept="image/*" id="file2" name="packingSlip2"/>
						  <input type="hidden" id="bytePS2" name="bytePS2" value="">
						  <br><br>
						  <input type="button" id="loadPS3" class="btn btn-primary" style="font-weight: bold;" value="Add Third packing slip" onclick="document.getElementById('file3').click();" />
						  <input type="file" style="display:none;" accept="image/*" id="file3" name="packingSlip3"/>
						  <input type="hidden" id="bytePS3" name="bytePS3" value="">
						  <br>
						  <br>
						  <button id="continueBTN" type="submit" class="btn btn-primary" disabled><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
				<br><br>
				</center>
			<?php } ?>
				
			<!-- Step Two -->
			<?php if($step == 2) { 
				updatePoInfo();
			?>
			<center>
				<br>

				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($_SESSION["receivePO"]["poInfo"]["result"]->returnObj->PODetail as $poDetail) { 

					  			//Get line received qty
					  			$receivedQty = 0;
					  			foreach ($_SESSION["receivePO"]["poInfo"]["result"]->returnObj->PORel as $poRel) {
					  				if($poDetail->POLine == $poRel->POLine){
					  					$receivedQty = $poRel->ReceivedQty;
					  					break;
					  				}
					  			}

					  		?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col">Line</th>
						      <th scope="col">Part</th>
						      <th scope="col">Vendor Part</th>
						      <th scope="col">Mfg Part</th>
						    </tr>
					  		<tr>
					  			<td><?php echo $poDetail->POLine; ?></td>
					  			<td><a href="partFind.php?part=<?php echo urlencode($poDetail->PartNum) ?>" target="_blank"><?php echo $poDetail->PartNum; ?></a></td>
					  			<td><?php echo $poDetail->VenPartNum; ?></td>
					  			<td><?php echo $poDetail->MfgPartNum; ?></td>
					  		</tr>
					  		<tr style="background: #cccccc;">
						      <th scope="col">Received</th>
						      <th scope="col">Total</th>
						      <th scope="col">UOM</th>
						      <th scope="col"></th>
						    </tr>
						    <tr>
						    	<td><?php echo $receivedQty."/".$poDetail->OrderQty." <b>".$poDetail->PUM."</b>"; ?></td>
					  			<td><?php echo $poDetail->XOrderQty." <b>".$poDetail->IUM."</b>"; ?></td>
					  			<td class="lineUom" line="<?php echo $poDetail->POLine; ?>" uom="<?php echo $poDetail->IUM; ?>" ><b><?php echo $poDetail->IUM; ?></b></td>
					  			<td></td>
						    </tr>
					  		<tr>
						      <th scope="col" colspan="5">Description</th>
						    </tr>
					  		<tr>
					  			<td colspan="5"><?php echo $poDetail->LineDesc; ?></td>
					  		</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>

				<br>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Receive Purchase Order
						</b>
					</div>
					<div class="card-body">
						<center>
						<form action="receivePO.php" method="POST">
						<br>
						  <div class="mb-3">
						  	<label for="lineNum" class="form-label">Line Number</label>
							<input id="lineNum" class="form-control" autocomplete="off" placeholder="1" name="line">
						  </div>
						  <br>
						  <label for="qty" class="form-label">Counted Quantity</label>
						  <div class="input-group mb-3">
							<input id="qty" type="text" class="form-control" autocomplete="off" placeholder="3" name="qty" aria-describedby="qtyUOM">
							<span class="input-group-text" id="qtyUOM"><b>UOM</b></span>
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
				<br><br>
				</center>
			<?php } ?>

			<!-- Step Three -->
			<?php if($step == 3) { ?>
			<center>
				<?php 
					$warehousesResult = $api->getWarehouses($_SESSION["receivePO"]["ds"]->RcvHead[0]->Plant);
					hasError($warehousesResult);

					$toLocationWarehouse = end($_SESSION["receivePO"]["ds"]->RcvDtl)->WareHouseCode;
					$toLocationBin = end($_SESSION["receivePO"]["ds"]->RcvDtl)->BinNum;
					$toLocationBin = trim($toLocationBin);

					$partOnHandResult = $api->getPartOnHand(
						end($_SESSION["receivePO"]["ds"]->RcvDtl)->PartNum,
						$_SESSION["receivePO"]["ds"]->RcvHead[0]->Plant);
					hasError($partOnHandResult);
				?>
				<br>

				<div class="table-responsive" style="max-width: 600px;">
					<table class="table" style="font-size: 0.8rem;">
					  <tbody>
					  	<?php foreach ($partOnHandResult["result"]->returnObj->PartOnHandBin as $onHand) { ?>
					  		<tr style="border-bottom: solid #000 3px;border-top: solid #000 3px;background: #cccccc;">
						      <th scope="col">Warehouse</th>
						      <th scope="col">Bin</th>
						      <th scope="col">Quantity</th>
						    </tr>
					  		<tr>
					  			<?php 
					  			$warehouseDescFound = false;
					  			foreach ($partOnHandResult["result"]->returnObj->PartOnHandWhse as $Whse) {
									if($Whse->WarehouseCode == $onHand->WarehouseCode){
						  				echo '<td>'.$Whse->WarehouseDesc.'</td>';
						  				$warehouseDescFound = true;
						  				break;
						  			}
								}

								if(!$warehouseDescFound){
									echo '<td></td>';
								}

					  			?>
					  			<td><?php echo $onHand->BinNum; ?></td>
					  			<td><?php echo $onHand->QuantityOnHand." ".$onHand->UnitOfMeasure; ?></td>
					  		</tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>

				<br>
				<div class="card" style="margin:20px;max-width:500px;">
					<div class="card-header" style="font-size: 1.5rem;">
						<b>Receive Purchase Order
						</b>
					</div>
					<div class="card-body">
						<center>
						<form action="receivePO.php" method="POST">
						<br>
						  <div class="mb-3">
						  	<label for="warehouseSelect" class="form-label">Warehouse</label>
							<select id="warehouseSelect" class="form-select" aria-label="Selet Warhouse" name="warehouse">
							  <?php foreach ($warehousesResult["result"]->returnObj->Warehse as $warehouse) { 
							  	$selected = "";
							  	if($toLocationWarehouse == $warehouse->WarehouseCode){
							  		$selected = "selected";
							  	}
							  ?>
							  	<option value="<?php echo $warehouse->WarehouseCode; ?>" <?php echo $selected; ?> ><?php echo $warehouse->Description; ?></option>
							  <?php } ?>
							</select>
						  </div>
						  <br>
						  <div class="mb-3">
						  	<label for="binNum" class="form-label">Bin Number</label>
							<input id="binNum" class="form-control" autocomplete="off" placeholder="100" name="bin" value="<?php echo $toLocationBin; ?>">
						  </div>
						  <br>
						  <button type="submit" class="btn btn-primary"><b>Continue</b></button>
						</form>
						</center>
					</div>
				</div>
				<br><br>
				</center>
			<?php } ?>
				
			
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>
	




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script type="text/javascript">
    	var canvas=document.getElementById("canvas");
		var ctx=canvas.getContext("2d");
		var cw=canvas.width;
		var ch=canvas.height;
		var maxW=1200;
		var maxH=1200;
		var input = document.getElementById('file');
		var input2 = document.getElementById('file2');
		var input3 = document.getElementById('file3');

		if(input != null){
			input.addEventListener('change', handleFiles);
		}

		if(input2 != null){
			input2.addEventListener('change', handleFiles2);
		}

		if(input3 != null){
			input3.addEventListener('change', handleFiles3);
		}


		$("#lineNum").change(function(){
		  $("#qtyUOM").html("<b>UOM</b>");
		  $('.lineUom').each(function(i, obj) {
			    if($("#lineNum").val() == $(obj).attr("line")){
			    	$("#qtyUOM").html("<b>"+$(obj).attr("uom")+"</b>");
			    }
			});
		});

		function base64ToArrayBuffer(base64) {
			base64 = base64.replace('data:image/jpeg;base64,', '');
		    var binary_string = window.atob(base64);
		    var len = binary_string.length;
		    var bytes = new Uint8Array(len);
		    for (var i = 0; i < len; i++) {
		        bytes[i] = binary_string.charCodeAt(i);
		    }
		    return bytes;
		}

    	function handleFiles(e) {
		  var img = new Image;
		  img.onload = function() {
		    var iw=img.width;
		    var ih=img.height;
		    var scale=Math.min((maxW/iw),(maxH/ih));
		    var iwScaled=iw*scale;
		    var ihScaled=ih*scale;
		    canvas.width=iwScaled;
		    canvas.height=ihScaled;
		    ctx.drawImage(img,0,0,iwScaled,ihScaled);
			document.getElementById("bytePS").value = canvas.toDataURL("image/jpeg",0.7);
		    document.getElementById("loadPS").value = "First packing slip added";
		    document.getElementById("continueBTN").disabled = false;
		  }
		  img.src = URL.createObjectURL(e.target.files[0]);
		}

		function handleFiles2(e) {
		  var img = new Image;
		  img.onload = function() {
		    var iw=img.width;
		    var ih=img.height;
		    var scale=Math.min((maxW/iw),(maxH/ih));
		    var iwScaled=iw*scale;
		    var ihScaled=ih*scale;
		    canvas.width=iwScaled;
		    canvas.height=ihScaled;
		    ctx.drawImage(img,0,0,iwScaled,ihScaled);
			document.getElementById("bytePS2").value = canvas.toDataURL("image/jpeg",0.7);
		    document.getElementById("loadPS2").value = "Second packing slip added";
		  }
		  img.src = URL.createObjectURL(e.target.files[0]);
		}

		function handleFiles3(e) {
		  var img = new Image;
		  img.onload = function() {
		    var iw=img.width;
		    var ih=img.height;
		    var scale=Math.min((maxW/iw),(maxH/ih));
		    var iwScaled=iw*scale;
		    var ihScaled=ih*scale;
		    canvas.width=iwScaled;
		    canvas.height=ihScaled;
		    ctx.drawImage(img,0,0,iwScaled,ihScaled);
			document.getElementById("bytePS3").value = canvas.toDataURL("image/jpeg",0.7);
		    document.getElementById("loadPS3").value = "Third packing slip added";
		  }
		  img.src = URL.createObjectURL(e.target.files[0]);
		}
    </script>
  </body>
</html>