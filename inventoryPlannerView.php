<?php
ini_set("memory_limit","256M");
session_start();

include_once 'inc/inventoryPlannerFunctions.php';
include_once 'inc/api.php';
$api = new API();

//Login check
if(!isset($_SESSION["key"]) || !isset($_SESSION["empNum"])) {
	$_SESSION["continue"] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	die();
}

$db = new PDO('sqlite:db/inventoryPlanner.db');
$error = false;
$errorText = "";
$step = 0;
$partList = array();
$partNumList = array();
$test = array();
$allPlants = array();
$csvData = "";
$csvHeaders = "#,Part,Rev,Order Needed,Description,Parents,";
$maxNumOfLocations = 0;

$plantsResult = $api->getAllPlants();
if(!hasError($plantsResult)){
	foreach ($plantsResult["result"]->returnObj->PlantList as $plant) {
		$allPlants[$plant->Plant] = $plant->Name;
	}
}


//Functions
function hasError($result){
	if($result["http"] != 200){
		$GLOBALS["error"] = true;
		$GLOBALS["errorText"] = "API ERROR: ".$result["method"];
		print_r($result);
		return true;
	}
	
	return false;
}

function saveCSVFile(){
	global $csvHeaders;
	global $csvData;
	global $csvFilePath;
	global $maxNumOfLocations;

	for ($i=0; $i < $maxNumOfLocations; $i++) {
		$num = $i+1;
		$csvHeaders .= "Location #".$num." Plant,Location #".$num." Warehouse,Location #".$num." Bin,Location #".$num." Qty On Hand,Location #".$num." Total Needed,Location #".$num." To Order,";
	}

	if(file_exists($csvFilePath)){
	    unlink($csvFilePath);
	}

	$fp = fopen($csvFilePath, 'w+');
	fwrite($fp, $csvHeaders."\n".$csvData);
	fclose($fp);
}

function csvText($text){
	$text = str_replace(',', ' ', $text);
	$text = trim(preg_replace('/\s+/', ' ', $text));
	return $text;
}

function addPartToList($part){
	global $partList;
	global $partNumList;

	foreach ($partList as $key => $partInList) {
		if($partInList["partNum"] == $part["partNum"] && $partInList["rev"] == $part["rev"]){
			$partList[$key]["qty"] += $part["qty"];
			return;
		}
	}

	array_push($partNumList,$part["partNum"]);
	array_push($partList, $part);
}

function partParse($reportID,$part,$baseQty){
	global $db;

	$subParts = getSubParts($db,$reportID,$part["id"]);
	foreach ($subParts as $subPart) {
		$subPart["qty"] = $subPart["qty"]*$baseQty;
		addPartToList($subPart);
		partParse($reportID,$subPart,$subPart["qty"]);
	}
}

if(isset($_GET["reportID"])){
	$_POST["reportID"] = $_GET["reportID"];
}

if(isset($_GET["plant"])){
	$_POST["plant"] = $_GET["plant"];
}

if(isset($_GET["onlyNeeded"])){
	$_POST["onlyNeeded"] = $_GET["onlyNeeded"];
}

if(!isset($_POST["reportID"])){
	echo "Missing report ID";
	die();
}

$csvName = "InventoryPlanner_".$_POST["reportID"].".csv";
$csvFilePath = __DIR__.'/csv/'.$csvName;

$showOnlyNeeded = false;
if(isset($_POST["onlyNeeded"]) && $_POST["onlyNeeded"] == 1){
	$showOnlyNeeded = true;
}

$report = getReport($db,$_POST["reportID"]);
$baseParts = getReportBaseParts($db,$_POST["reportID"]);
$plants = getReportPlants($db,$_POST["reportID"]);

foreach ($baseParts as $basePart) {
	partParse($_POST["reportID"],$basePart,$basePart["qty"]);
}

?>

<!doctype html>
<html lang="en" style="background: #eaeaea;">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <title>EasyCor</title>
    <style>
    	tr td{
		  padding: 0 !important;
		}

		a {
			color:#000;
			text-decoration: none;
		}
    </style>
  </head>
  <body style="background: #eaeaea;">
	<?php include_once 'inc/header.php'; ?>
  
	<?php if($error) { ?>
		<br><center>
		<div class="alert alert-danger" role="alert" style="max-width:400px;">
		  <b><?php echo $errorText; ?></b>
		</div></center><br>
	<?php } ?>
  
	<div class="container" style="margin-top:20px;">
	  <div class="row">
		<div class="col-md">

			<center>
				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title"><?php echo $report["name"];?></h5>
						<p class="card-text"><?php echo date("Y-m-d h:i", $report["time"]); ?></p>
					</div>
				</div>
				<br>
			</center>

			<center>
				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">Purchase Advisor</h5>
						<p id="purAdProgress" class="card-text">Loading...</p>
					</div>
				</div>
				<br>
			</center>

			<center>
				<div class="card text-white bg-dark mb-3" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title">Plant</h5>
						<form action="inventoryPlannerView.php" method="POST">
							<input type="hidden" name="reportID" value="<?php echo $_POST["reportID"]; ?>">
							<select class="form-select" aria-label="Plant" name="plant">
							  <?php if(isset($_POST["plant"]) && strlen(trim($_POST["plant"])) > 0 && $location["Plant"] != $_POST["plant"]){ ?>
							  	<option value="">All</option>
							  <?php } else { ?>
							  	<option selected value="">All</option>
							  <?php } ?>


							  <?php 
							  	foreach($plants as $plant){ 
							  		$selected = "";
							  		if(isset($_POST["plant"]) && strlen(trim($_POST["plant"])) > 0 && $plant["Plant"] == $_POST["plant"]){
							  			$selected = "selected";
							  		}
							  ?>
							  	<option value="<?php echo $plant["Plant"]; ?>" <?php echo $selected;?> ><?php echo $allPlants[$plant["Plant"]]; ?></option>
							  <?php } ?>
							</select>

							<br>
							<h5 class="card-title">Show only parts that need to be ordered</h5>
							<select class="form-select" aria-label="OnlyNeeded" name="onlyNeeded">
								<?php if(isset($_POST["onlyNeeded"]) && $_POST["onlyNeeded"] == 0) {?>
									<option selected value="0">No</option>
								<?php } else { ?>
									<option value="0">No</option>
								<?php } ?>

								<?php if(isset($_POST["onlyNeeded"]) && $_POST["onlyNeeded"] == 1) {?>
									<option selected value="1">Yes</option>
								<?php } else { ?>
									<option value="1">Yes</option>
								<?php } ?>
							</select>
							<br>
							<button type="submit" class="btn btn-primary"><b>Submit</b></button>
						</form>
					</div>
				</div>

				<br>
				<a href="<?php echo '/csv/'.$csvName; ?>" class="btn btn-secondary" style="min-width:250px;" target="_blank"><b>CSV</b></a>
				<br>
				<br>
			</center>

			<?php foreach ($baseParts as $basePart) { ?>
				<center>
				<div class="alert alert-info" style="max-width: 500px;">
					<div class="card-body">
						<h5 class="card-title"><?php echo $basePart["partNum"];?> x<?php echo $basePart["qty"];?></h5>
						<p class="card-text">Rev <?php echo $basePart["rev"];?></p>
						<p class="card-text"><?php echo $basePart["partDesc"];?></p>
					</div>
				</div>
				<br>
				</center>
			<?php } ?>
			
			<center>
			<div class="table-responsive" style="max-width: 600px;">
				<table class="table" style="font-size: 0.8rem;">
						<thead>
							<tr style="border-bottom: solid #000 3px;">
						      <th scope="col">#</th>
						      <th scope="col">Part</th>
						      <th scope="col">Rev</th>
						      <th scope="col" colspan="2" style="min-width:140px;"><center>Order Needed</center></th>
						    </tr>
						</thead>
						<tbody>
						<?php 
							$count = 0;
							foreach ($partList as $part) { 
								$parents = getAllParentPartNumbers($db,$_POST["reportID"],$part["partNum"]);
								$locations = getAllPartLocations($db,$_POST["reportID"],$part["partNum"]);

								if(isset($_POST["plant"]) && strlen(trim($_POST["plant"])) > 0){
                  					$toBeOrdered = partToBeOrdered($db,$_POST["reportID"],$_POST["plant"],$part["partNum"],$part["qty"]);
                  				} else {
                  					$toBeOrdered = partToBeOrdered($db,$_POST["reportID"],"",$part["partNum"],$part["qty"]);
                  				}

                  				//CSV
                  				$csvData .= csvText($count+1).",";
                  				$csvData .= csvText($part["partNum"]).",";
                  				$csvData .= csvText(" ".$part["rev"]).",";

                  				if($toBeOrdered){
                  					$csvData .= csvText("yes").",";
                  				} else {
                  					$csvData .= csvText("no").",";
                  				}
                  				
                  				$csvData .= csvText($part["partDesc"]).",";

                  				$parentString = "";
								foreach ($parents as $parent) {
									$parentString .= $parent." - ";
								}
								$parentString = substr($parentString, 0, -2);
								$csvData .= csvText($parentString).",";

                  				if($showOnlyNeeded && !$toBeOrdered){
                  					continue;
                  				}

                  				$count++;

						?>
							<tr style="background: #dadada;" class="partHeader" part="<?php echo $part["partNum"]; ?>">
								<td><?php echo $count; ?></td>
								<td><a target="_blank" href="partFind.php?part=<?php echo urlencode($part["partNum"]); ?>"><b><?php echo $part["partNum"]; ?></b></a></td>
								<td><?php echo $part["rev"]; ?></td>

								<?php if($toBeOrdered) { ?>
									<td style='color:#dc3535;' colspan="2"><center><b>Yes</b></center></td>
								<?php } else { ?>
									<td colspan="2"><center>No</center></td>
								<?php } ?>

							</tr>
							<tr >
                    			<th scope="col" colspan="5">Description</th>
                  			</tr>
							<tr>
								<td colspan="5"><?php echo $part["partDesc"]; ?></td>
							</tr>

							<tr>
                    			<th scope="col" colspan="2">On Hand</th>
                    			<th scope="col" ><center>Total Needed</center></th>
                    			<th scope="col" colspan="2"><center>Order</center></th>
                  			</tr>

                  			<?php
                  			$visLocations = 0; 
                  			foreach ($locations as $location) {

                  				if($location["OnHandQty"] == 0){
                  					continue;
                  				}

                  				if(isset($_POST["plant"]) && strlen(trim($_POST["plant"])) > 0 && $location["Plant"] != $_POST["plant"]){
                  					continue;
                  				}

                  				//CSV
                  				if($visLocations+1 > $maxNumOfLocations){
                  					$maxNumOfLocations = $visLocations+1;
                  				}

                  				$csvData .= csvText("Plant ".$location["Plant"]).",";
                  				$csvData .= csvText($location["WarehouseDesc"]).",";
                  				$csvData .= csvText($location["PrimBinNum"]).",";
                  				$csvData .= csvText(round($location["OnHandQty"],2))." ".csvText($location["uom"]).",";
                  				$csvData .= csvText(round($part["qty"],2))." ".csvText($location["uom"]).",";
                  				if($location["OnHandQty"] < $part["qty"]){ 
									$csvData .= csvText(round(($part["qty"]-$location["OnHandQty"]),2))." ".csvText($location["uom"]).",";
								} else {
									$csvData .= "0 ".csvText($location["uom"]).","; 
								}

                  				$visLocations++;
                  			?>

                  				<tr>
									<td colspan="2" >
										<?php echo round($location["OnHandQty"],2); ?> <?php echo $location["uom"]; ?> in <?php echo $location["WarehouseDesc"]; ?> Bin <?php echo $location["PrimBinNum"]; ?>
									</td>
									<td>
										<center>
										<?php echo round($part["qty"],2)." ".$location["uom"]; ?>
										</center>
									</td>
									<td colspan="2">
										<center>
										<?php if($location["OnHandQty"] < $part["qty"]){ 
											echo round(($part["qty"]-$location["OnHandQty"]),2)." ".$location["uom"];
										} else {
											echo "0 ".$location["uom"];
										}?>
										</center>
									</td>
								</tr>
							<?php } ?>

							<?php 
                  				if($visLocations == 0) { ?>
	                  				<tr>
	                  					<td colspan="2">0 <?php echo $location["uom"]; ?></td>
	                  					<td><center><?php echo round($part["qty"],2)." ".$location["uom"]; ?></center></td>
	                  					<td colspan="2"><center><?php echo round($part["qty"],2)." ".$location["uom"]; ?></center></td>
	                  				</tr>
                  			<?php 
                  				} 
                  				$csvData .= "\n";
                  			?>

                  			<tr id="parents<?php echo $part["partNum"];?>">
                    			<th scope="col" colspan="5">Parents</th>
                  			</tr>
							<tr style="border-bottom: solid #000 3px;">
								<td colspan="5">
									<?php 
										$parentString = "";
										foreach ($parents as $parent) {
											$parentString .= "<a target='_blank' href='partFind.php?part=".urlencode($parent)."'>".$parent."</a> - ";
										}
										$parentString = substr($parentString, 0, -2);
										echo $parentString;
									?>	
								</td>
							</tr>
					

						<?php } ?>
						</tbody>
				</table>
			</div>
			</center>


		
			
				</div>
			</div>
			
		</div>
	  </div>
	</div>

	<?php 
		//csv
		saveCSVFile();
	?>
	
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
    	var arrayFromPHP = <?php echo json_encode($partNumList); ?>;
    	var splitInto = 3;
    	var chunks = [];
    	var chunkPositions = [];
    	var completePos = 0;

    	$( document ).ready(function() {

    		//Split part array and get purchase history concurrently
    		var i;
			for (i = 0; i < splitInto; i++) {
				var portion = i*arrayFromPHP.length/splitInto;
			  	chunks.push(
			  		arrayFromPHP.slice(
			  			portion,
			  			portion+arrayFromPHP.length/splitInto)
			    );

			    chunkPositions.push(0);
			    getPartPur(i);
			}
		});

    	function isJson(str) {
          try {
              JSON.parse(str);
          } catch (e) {
              return false;
          }
          return true;
      	}

      	function parseDate(date){
      		var parsedDate;
      		var firstSplit = date.split("T");
      		return firstSplit[0].split("-");
      	}

      	function getBusinessDatesCount(startDate, endDate) {
		    let count = 0;
		    const curDate = new Date(startDate.getTime());
		    while (curDate <= endDate) {
		        const dayOfWeek = curDate.getDay();
		        if(!(dayOfWeek in [0, 6])) count++;
		        curDate.setDate(curDate.getDate() + 1);
		    }
		    return count;
		}

      	function addPartPurToTable(result){
      		var partNum = result["result"]["returnObj"]["PurchaseAdvisor"][0]["PartNum"];
      		var onOrder = result["result"]["returnObj"]["PAOnOrder"];
      		var prevPur = result["result"]["returnObj"]["PAPurchasedBefore"].reverse();
      		var select = '#parents'+partNum;
      		var prevCount = 0;

      		$(select).before('<tr><th><b>On Order</b></th> <th><b>PO</b></th> <th><b>Qty</b></th> <th colspan="2"><b>Due</b></th> </tr>');
      		$.each(onOrder, function (i, elem) {
      			var due = elem["DueDate"];
      			if (due !== null && due !== undefined) {
      				due = elem["DueDate"].split("T")[0];
				}


			    $(select).before('<tr><td>'+elem["VendorName"]+'</td><td>'+elem["PONum"]+'</td><td>'+elem["RelQty"]+' '+elem["PUM"]+'</td><td colspan="2">'+due+'</td></tr>');
			});

			if (onOrder.length === 0) {
				$(select).before('<tr><td colspan="5">None</td></tr>');
			}

			$(select).before('<tr><th><b>Previous Order</b></th> <th><b>PO</b></th> <th><b>Qty</b></th><th><b>Received</b></th><th><b>Lead</b></th></tr>');
      		$.each(prevPur, function (i, elem) {
      			prevCount++;
      			if(prevCount <= 6){
      				var due = elem["DueDate"];
      				var orderDate = elem["OrderDate"];
      				var receiptDate = elem["ReceiptDate"];
      				var lead = "N/A";

	      			if (due !== null && due !== undefined) {
	      				due = elem["DueDate"].split("T")[0];
					}

					if (orderDate !== null && orderDate !== undefined && receiptDate !== null && receiptDate !== undefined) {
	      				orderDate = new Date(parseDate(orderDate)[0],parseDate(orderDate)[1],parseDate(orderDate)[2]);
      					receiptDate = new Date(parseDate(receiptDate)[0],parseDate(receiptDate)[1],parseDate(receiptDate)[2]);
      					lead = getBusinessDatesCount(orderDate,receiptDate);
					}

      				$(select).before('<tr><td>'+elem["VendorName"]+'</td><td>'+elem["PONum"]+'</td><td>'+elem["VendorQty"]+' '+elem["PUM"]+'</td><td>'
      					+(parseDate(elem["ReceiptDate"])[0])+"-"
      					+(parseDate(elem["ReceiptDate"])[1])+"-"
      					+(parseDate(elem["ReceiptDate"])[2])+'&nbsp;&nbsp;&nbsp;</td><td>'+lead+' Days</td></tr>');
      			}
			});

			if (prevPur.length === 0) {
				$(select).before('<tr><td colspan="5">None</td></tr>');
			}
      	}

    	function getPartPur(chunkNum){
    		console.log("getPartPur chunk#"+chunkNum);
    		var partArray = chunks[chunkNum];
    		var pos = chunkPositions[chunkNum];

    		if(typeof partArray[pos] === 'undefined') {
			    console.log("getPartPur end chunk#"+chunkNum);
			    return;
			}

            $.post(
            "inc/partPur.php",
            {partNum: partArray[pos]},
              function(data) {
              	//console.log(data);
                if(isJson(data)){
                  $("#purAdProgress").html((completePos+1)+"/"+(arrayFromPHP.length));

                  var result = $.parseJSON(data);
                  if(result["http"] == 200){
                  	addPartPurToTable(result);
                  } else {
                  	console.log("Failed to get part pur for "+partArray[pos]);
                  }


                  chunkPositions[chunkNum] = chunkPositions[chunkNum]+1;
                  completePos++;
                  getPartPur(chunkNum);
              	}
              }
          );
    	}
    </script>
  </body>
</html>